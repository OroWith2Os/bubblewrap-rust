use ::libc;
#[c2rust::header_src = "internal:0"]
pub mod internal {
    #[c2rust::src_loc = "0:0"]
    pub type __builtin_va_list = [__va_list_tag; 1];
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "0:0"]
    pub struct __va_list_tag {
        pub gp_offset: libc::c_uint,
        pub fp_offset: libc::c_uint,
        pub overflow_arg_area: *mut libc::c_void,
        pub reg_save_area: *mut libc::c_void,
    }
}
#[c2rust::header_src = "/usr/include/bits/types.h:21"]
pub mod types_h {
    #[c2rust::src_loc = "145:1"]
    pub type __dev_t = libc::c_ulong;
    #[c2rust::src_loc = "146:1"]
    pub type __uid_t = libc::c_uint;
    #[c2rust::src_loc = "147:1"]
    pub type __gid_t = libc::c_uint;
    #[c2rust::src_loc = "148:1"]
    pub type __ino_t = libc::c_ulong;
    #[c2rust::src_loc = "149:1"]
    pub type __ino64_t = libc::c_ulong;
    #[c2rust::src_loc = "150:1"]
    pub type __mode_t = libc::c_uint;
    #[c2rust::src_loc = "151:1"]
    pub type __nlink_t = libc::c_ulong;
    #[c2rust::src_loc = "152:1"]
    pub type __off_t = libc::c_long;
    #[c2rust::src_loc = "153:1"]
    pub type __off64_t = libc::c_long;
    #[c2rust::src_loc = "154:1"]
    pub type __pid_t = libc::c_int;
    #[c2rust::src_loc = "160:1"]
    pub type __time_t = libc::c_long;
    #[c2rust::src_loc = "175:1"]
    pub type __blksize_t = libc::c_long;
    #[c2rust::src_loc = "180:1"]
    pub type __blkcnt_t = libc::c_long;
    #[c2rust::src_loc = "194:1"]
    pub type __ssize_t = libc::c_long;
    #[c2rust::src_loc = "197:1"]
    pub type __syscall_slong_t = libc::c_long;
    #[c2rust::src_loc = "210:1"]
    pub type __socklen_t = libc::c_uint;
}
#[c2rust::header_src = "/usr/include/bits/dirent.h:21"]
pub mod dirent_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "22:8"]
    pub struct dirent {
        pub d_ino: __ino64_t,
        pub d_off: __off64_t,
        pub d_reclen: libc::c_ushort,
        pub d_type: libc::c_uchar,
        pub d_name: [libc::c_char; 256],
    }
    use super::types_h::{__ino64_t, __off64_t};
}
#[c2rust::header_src = "/usr/include/dirent.h:21"]
pub mod include_dirent_h {
    #[c2rust::src_loc = "127:1"]
    pub type DIR = __dirstream;
    use super::dirent_h::dirent;
    extern "C" {
        #[c2rust::src_loc = "127:16"]
        pub type __dirstream;
        #[c2rust::src_loc = "226:1"]
        pub fn dirfd(__dirp: *mut DIR) -> libc::c_int;
        #[c2rust::src_loc = "167:1"]
        pub fn readdir(__dirp: *mut DIR) -> *mut dirent;
        #[c2rust::src_loc = "149:1"]
        pub fn fdopendir(__fd: libc::c_int) -> *mut DIR;
        #[c2rust::src_loc = "134:1"]
        pub fn closedir(__dirp: *mut DIR) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/lib/clang/15.0.7/include/stddef.h:21"]
pub mod stddef_h {
    #[c2rust::src_loc = "46:1"]
    pub type size_t = libc::c_ulong;
}
#[c2rust::header_src = "/usr/include/bits/types/struct_iovec.h:21"]
pub mod struct_iovec_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "26:8"]
    pub struct iovec {
        pub iov_base: *mut libc::c_void,
        pub iov_len: size_t,
    }
    use super::stddef_h::size_t;
}
#[c2rust::header_src = "/usr/include/fcntl.h:21"]
pub mod fcntl_h {
    #[c2rust::src_loc = "50:1"]
    pub type mode_t = __mode_t;
    #[c2rust::src_loc = "69:1"]
    pub type pid_t = __pid_t;
    use super::types_h::{__mode_t, __pid_t};
    extern "C" {
        #[c2rust::src_loc = "258:1"]
        pub fn creat(__file: *const libc::c_char, __mode: mode_t) -> libc::c_int;
        #[c2rust::src_loc = "237:1"]
        pub fn openat(
            __fd: libc::c_int,
            __file: *const libc::c_char,
            __oflag: libc::c_int,
            _: ...
        ) -> libc::c_int;
        #[c2rust::src_loc = "212:1"]
        pub fn open(__file: *const libc::c_char, __oflag: libc::c_int, _: ...) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/bits/types/struct_timespec.h:21"]
pub mod struct_timespec_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "11:8"]
    pub struct timespec {
        pub tv_sec: __time_t,
        pub tv_nsec: __syscall_slong_t,
    }
    use super::types_h::{__syscall_slong_t, __time_t};
}
#[c2rust::header_src = "/usr/include/bits/struct_stat.h:21"]
pub mod struct_stat_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "26:8"]
    pub struct stat {
        pub st_dev: __dev_t,
        pub st_ino: __ino_t,
        pub st_nlink: __nlink_t,
        pub st_mode: __mode_t,
        pub st_uid: __uid_t,
        pub st_gid: __gid_t,
        pub __pad0: libc::c_int,
        pub st_rdev: __dev_t,
        pub st_size: __off_t,
        pub st_blksize: __blksize_t,
        pub st_blocks: __blkcnt_t,
        pub st_atim: timespec,
        pub st_mtim: timespec,
        pub st_ctim: timespec,
        pub __glibc_reserved: [__syscall_slong_t; 3],
    }
    use super::struct_timespec_h::timespec;
    use super::types_h::{
        __blkcnt_t, __blksize_t, __dev_t, __gid_t, __ino_t, __mode_t, __nlink_t, __off_t,
        __syscall_slong_t, __uid_t,
    };
}
#[c2rust::header_src = "/usr/lib/clang/15.0.7/include/stdarg.h:21"]
pub mod stdarg_h {
    #[c2rust::src_loc = "14:1"]
    pub type va_list = __builtin_va_list;
    use super::internal::__builtin_va_list;
}
#[c2rust::header_src = "/usr/include/bits/types/struct_FILE.h:21"]
pub mod struct_FILE_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "49:8"]
    pub struct _IO_FILE {
        pub _flags: libc::c_int,
        pub _IO_read_ptr: *mut libc::c_char,
        pub _IO_read_end: *mut libc::c_char,
        pub _IO_read_base: *mut libc::c_char,
        pub _IO_write_base: *mut libc::c_char,
        pub _IO_write_ptr: *mut libc::c_char,
        pub _IO_write_end: *mut libc::c_char,
        pub _IO_buf_base: *mut libc::c_char,
        pub _IO_buf_end: *mut libc::c_char,
        pub _IO_save_base: *mut libc::c_char,
        pub _IO_backup_base: *mut libc::c_char,
        pub _IO_save_end: *mut libc::c_char,
        pub _markers: *mut _IO_marker,
        pub _chain: *mut _IO_FILE,
        pub _fileno: libc::c_int,
        pub _flags2: libc::c_int,
        pub _old_offset: __off_t,
        pub _cur_column: libc::c_ushort,
        pub _vtable_offset: libc::c_schar,
        pub _shortbuf: [libc::c_char; 1],
        pub _lock: *mut libc::c_void,
        pub _offset: __off64_t,
        pub _codecvt: *mut _IO_codecvt,
        pub _wide_data: *mut _IO_wide_data,
        pub _freeres_list: *mut _IO_FILE,
        pub _freeres_buf: *mut libc::c_void,
        pub __pad5: size_t,
        pub _mode: libc::c_int,
        pub _unused2: [libc::c_char; 20],
    }
    #[c2rust::src_loc = "43:1"]
    pub type _IO_lock_t = ();
    use super::stddef_h::size_t;
    use super::types_h::{__off64_t, __off_t};
    extern "C" {
        #[c2rust::src_loc = "38:8"]
        pub type _IO_wide_data;
        #[c2rust::src_loc = "37:8"]
        pub type _IO_codecvt;
        #[c2rust::src_loc = "36:8"]
        pub type _IO_marker;
    }
}
#[c2rust::header_src = "/usr/include/bits/types/FILE.h:21"]
pub mod FILE_h {
    #[c2rust::src_loc = "7:1"]
    pub type FILE = _IO_FILE;
    use super::struct_FILE_h::_IO_FILE;
}
#[c2rust::header_src = "/usr/include/stdio.h:21"]
pub mod stdio_h {
    #[c2rust::src_loc = "77:1"]
    pub type ssize_t = __ssize_t;
    use super::internal::__va_list_tag;
    use super::types_h::__ssize_t;
    use super::FILE_h::FILE;
    extern "C" {
        #[c2rust::src_loc = "145:14"]
        pub static mut stderr: *mut FILE;
        #[c2rust::src_loc = "350:12"]
        pub fn fprintf(_: *mut FILE, _: *const libc::c_char, _: ...) -> libc::c_int;
        #[c2rust::src_loc = "365:12"]
        pub fn vfprintf(
            _: *mut FILE,
            _: *const libc::c_char,
            _: ::core::ffi::VaList,
        ) -> libc::c_int;
        #[c2rust::src_loc = "390:1"]
        pub fn vasprintf(
            __ptr: *mut *mut libc::c_char,
            __f: *const libc::c_char,
            __arg: ::core::ffi::VaList,
        ) -> libc::c_int;
        #[c2rust::src_loc = "655:1"]
        pub fn fputs(__s: *const libc::c_char, __stream: *mut FILE) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/sys/types.h:21"]
pub mod sys_types_h {
    #[c2rust::src_loc = "64:1"]
    pub type gid_t = __gid_t;
    #[c2rust::src_loc = "79:1"]
    pub type uid_t = __uid_t;
    use super::types_h::{__gid_t, __uid_t};
}
#[c2rust::header_src = "/usr/include/unistd.h:21"]
pub mod unistd_h {
    #[c2rust::src_loc = "274:1"]
    pub type socklen_t = __socklen_t;
    use super::stddef_h::size_t;
    use super::stdio_h::ssize_t;
    use super::types_h::{__gid_t, __pid_t, __socklen_t, __uid_t};
    extern "C" {
        #[c2rust::src_loc = "358:1"]
        pub fn close(__fd: libc::c_int) -> libc::c_int;
        #[c2rust::src_loc = "371:1"]
        pub fn read(__fd: libc::c_int, __buf: *mut libc::c_void, __nbytes: size_t) -> ssize_t;
        #[c2rust::src_loc = "378:1"]
        pub fn write(__fd: libc::c_int, __buf: *const libc::c_void, __n: size_t) -> ssize_t;
        #[c2rust::src_loc = "640:1"]
        pub fn sysconf(__name: libc::c_int) -> libc::c_long;
        #[c2rust::src_loc = "650:1"]
        pub fn getpid() -> __pid_t;
        #[c2rust::src_loc = "700:1"]
        pub fn geteuid() -> __uid_t;
        #[c2rust::src_loc = "706:1"]
        pub fn getegid() -> __gid_t;
        #[c2rust::src_loc = "778:1"]
        pub fn fork() -> __pid_t;
        #[c2rust::src_loc = "838:1"]
        pub fn readlink(
            __path: *const libc::c_char,
            __buf: *mut libc::c_char,
            __len: size_t,
        ) -> ssize_t;
        #[c2rust::src_loc = "1091:1"]
        pub fn syscall(__sysno: libc::c_long, _: ...) -> libc::c_long;
    }
}
#[c2rust::header_src = "/usr/include/bits/confname.h:21"]
pub mod confname_h {
    #[c2rust::src_loc = "71:1"]
    pub type C2RustUnnamed = libc::c_uint;
    #[c2rust::src_loc = "534:5"]
    pub const _SC_SIGSTKSZ: C2RustUnnamed = 250;
    #[c2rust::src_loc = "531:5"]
    pub const _SC_MINSIGSTKSZ: C2RustUnnamed = 249;
    #[c2rust::src_loc = "528:5"]
    pub const _SC_THREAD_ROBUST_PRIO_PROTECT: C2RustUnnamed = 248;
    #[c2rust::src_loc = "526:5"]
    pub const _SC_THREAD_ROBUST_PRIO_INHERIT: C2RustUnnamed = 247;
    #[c2rust::src_loc = "523:5"]
    pub const _SC_XOPEN_STREAMS: C2RustUnnamed = 246;
    #[c2rust::src_loc = "520:5"]
    pub const _SC_TRACE_USER_EVENT_MAX: C2RustUnnamed = 245;
    #[c2rust::src_loc = "518:5"]
    pub const _SC_TRACE_SYS_MAX: C2RustUnnamed = 244;
    #[c2rust::src_loc = "516:5"]
    pub const _SC_TRACE_NAME_MAX: C2RustUnnamed = 243;
    #[c2rust::src_loc = "514:5"]
    pub const _SC_TRACE_EVENT_NAME_MAX: C2RustUnnamed = 242;
    #[c2rust::src_loc = "511:5"]
    pub const _SC_SS_REPL_MAX: C2RustUnnamed = 241;
    #[c2rust::src_loc = "508:5"]
    pub const _SC_V7_LPBIG_OFFBIG: C2RustUnnamed = 240;
    #[c2rust::src_loc = "506:5"]
    pub const _SC_V7_LP64_OFF64: C2RustUnnamed = 239;
    #[c2rust::src_loc = "504:5"]
    pub const _SC_V7_ILP32_OFFBIG: C2RustUnnamed = 238;
    #[c2rust::src_loc = "502:5"]
    pub const _SC_V7_ILP32_OFF32: C2RustUnnamed = 237;
    #[c2rust::src_loc = "499:5"]
    pub const _SC_RAW_SOCKETS: C2RustUnnamed = 236;
    #[c2rust::src_loc = "497:5"]
    pub const _SC_IPV6: C2RustUnnamed = 235;
    #[c2rust::src_loc = "493:5"]
    pub const _SC_LEVEL4_CACHE_LINESIZE: C2RustUnnamed = 199;
    #[c2rust::src_loc = "491:5"]
    pub const _SC_LEVEL4_CACHE_ASSOC: C2RustUnnamed = 198;
    #[c2rust::src_loc = "489:5"]
    pub const _SC_LEVEL4_CACHE_SIZE: C2RustUnnamed = 197;
    #[c2rust::src_loc = "487:5"]
    pub const _SC_LEVEL3_CACHE_LINESIZE: C2RustUnnamed = 196;
    #[c2rust::src_loc = "485:5"]
    pub const _SC_LEVEL3_CACHE_ASSOC: C2RustUnnamed = 195;
    #[c2rust::src_loc = "483:5"]
    pub const _SC_LEVEL3_CACHE_SIZE: C2RustUnnamed = 194;
    #[c2rust::src_loc = "481:5"]
    pub const _SC_LEVEL2_CACHE_LINESIZE: C2RustUnnamed = 193;
    #[c2rust::src_loc = "479:5"]
    pub const _SC_LEVEL2_CACHE_ASSOC: C2RustUnnamed = 192;
    #[c2rust::src_loc = "477:5"]
    pub const _SC_LEVEL2_CACHE_SIZE: C2RustUnnamed = 191;
    #[c2rust::src_loc = "475:5"]
    pub const _SC_LEVEL1_DCACHE_LINESIZE: C2RustUnnamed = 190;
    #[c2rust::src_loc = "473:5"]
    pub const _SC_LEVEL1_DCACHE_ASSOC: C2RustUnnamed = 189;
    #[c2rust::src_loc = "471:5"]
    pub const _SC_LEVEL1_DCACHE_SIZE: C2RustUnnamed = 188;
    #[c2rust::src_loc = "469:5"]
    pub const _SC_LEVEL1_ICACHE_LINESIZE: C2RustUnnamed = 187;
    #[c2rust::src_loc = "467:5"]
    pub const _SC_LEVEL1_ICACHE_ASSOC: C2RustUnnamed = 186;
    #[c2rust::src_loc = "465:5"]
    pub const _SC_LEVEL1_ICACHE_SIZE: C2RustUnnamed = 185;
    #[c2rust::src_loc = "462:5"]
    pub const _SC_TRACE_LOG: C2RustUnnamed = 184;
    #[c2rust::src_loc = "460:5"]
    pub const _SC_TRACE_INHERIT: C2RustUnnamed = 183;
    #[c2rust::src_loc = "458:5"]
    pub const _SC_TRACE_EVENT_FILTER: C2RustUnnamed = 182;
    #[c2rust::src_loc = "456:5"]
    pub const _SC_TRACE: C2RustUnnamed = 181;
    #[c2rust::src_loc = "454:5"]
    pub const _SC_HOST_NAME_MAX: C2RustUnnamed = 180;
    #[c2rust::src_loc = "451:5"]
    pub const _SC_V6_LPBIG_OFFBIG: C2RustUnnamed = 179;
    #[c2rust::src_loc = "449:5"]
    pub const _SC_V6_LP64_OFF64: C2RustUnnamed = 178;
    #[c2rust::src_loc = "447:5"]
    pub const _SC_V6_ILP32_OFFBIG: C2RustUnnamed = 177;
    #[c2rust::src_loc = "445:5"]
    pub const _SC_V6_ILP32_OFF32: C2RustUnnamed = 176;
    #[c2rust::src_loc = "442:5"]
    pub const _SC_2_PBS_CHECKPOINT: C2RustUnnamed = 175;
    #[c2rust::src_loc = "440:5"]
    pub const _SC_STREAMS: C2RustUnnamed = 174;
    #[c2rust::src_loc = "438:5"]
    pub const _SC_SYMLOOP_MAX: C2RustUnnamed = 173;
    #[c2rust::src_loc = "436:5"]
    pub const _SC_2_PBS_TRACK: C2RustUnnamed = 172;
    #[c2rust::src_loc = "434:5"]
    pub const _SC_2_PBS_MESSAGE: C2RustUnnamed = 171;
    #[c2rust::src_loc = "432:5"]
    pub const _SC_2_PBS_LOCATE: C2RustUnnamed = 170;
    #[c2rust::src_loc = "430:5"]
    pub const _SC_2_PBS_ACCOUNTING: C2RustUnnamed = 169;
    #[c2rust::src_loc = "428:5"]
    pub const _SC_2_PBS: C2RustUnnamed = 168;
    #[c2rust::src_loc = "426:5"]
    pub const _SC_USER_GROUPS_R: C2RustUnnamed = 167;
    #[c2rust::src_loc = "424:5"]
    pub const _SC_USER_GROUPS: C2RustUnnamed = 166;
    #[c2rust::src_loc = "422:5"]
    pub const _SC_TYPED_MEMORY_OBJECTS: C2RustUnnamed = 165;
    #[c2rust::src_loc = "420:5"]
    pub const _SC_TIMEOUTS: C2RustUnnamed = 164;
    #[c2rust::src_loc = "418:5"]
    pub const _SC_SYSTEM_DATABASE_R: C2RustUnnamed = 163;
    #[c2rust::src_loc = "416:5"]
    pub const _SC_SYSTEM_DATABASE: C2RustUnnamed = 162;
    #[c2rust::src_loc = "414:5"]
    pub const _SC_THREAD_SPORADIC_SERVER: C2RustUnnamed = 161;
    #[c2rust::src_loc = "412:5"]
    pub const _SC_SPORADIC_SERVER: C2RustUnnamed = 160;
    #[c2rust::src_loc = "410:5"]
    pub const _SC_SPAWN: C2RustUnnamed = 159;
    #[c2rust::src_loc = "408:5"]
    pub const _SC_SIGNALS: C2RustUnnamed = 158;
    #[c2rust::src_loc = "406:5"]
    pub const _SC_SHELL: C2RustUnnamed = 157;
    #[c2rust::src_loc = "404:5"]
    pub const _SC_REGEX_VERSION: C2RustUnnamed = 156;
    #[c2rust::src_loc = "402:5"]
    pub const _SC_REGEXP: C2RustUnnamed = 155;
    #[c2rust::src_loc = "400:5"]
    pub const _SC_SPIN_LOCKS: C2RustUnnamed = 154;
    #[c2rust::src_loc = "398:5"]
    pub const _SC_READER_WRITER_LOCKS: C2RustUnnamed = 153;
    #[c2rust::src_loc = "396:5"]
    pub const _SC_NETWORKING: C2RustUnnamed = 152;
    #[c2rust::src_loc = "394:5"]
    pub const _SC_SINGLE_PROCESS: C2RustUnnamed = 151;
    #[c2rust::src_loc = "392:5"]
    pub const _SC_MULTI_PROCESS: C2RustUnnamed = 150;
    #[c2rust::src_loc = "390:5"]
    pub const _SC_MONOTONIC_CLOCK: C2RustUnnamed = 149;
    #[c2rust::src_loc = "388:5"]
    pub const _SC_FILE_SYSTEM: C2RustUnnamed = 148;
    #[c2rust::src_loc = "386:5"]
    pub const _SC_FILE_LOCKING: C2RustUnnamed = 147;
    #[c2rust::src_loc = "384:5"]
    pub const _SC_FILE_ATTRIBUTES: C2RustUnnamed = 146;
    #[c2rust::src_loc = "382:5"]
    pub const _SC_PIPE: C2RustUnnamed = 145;
    #[c2rust::src_loc = "380:5"]
    pub const _SC_FIFO: C2RustUnnamed = 144;
    #[c2rust::src_loc = "378:5"]
    pub const _SC_FD_MGMT: C2RustUnnamed = 143;
    #[c2rust::src_loc = "376:5"]
    pub const _SC_DEVICE_SPECIFIC_R: C2RustUnnamed = 142;
    #[c2rust::src_loc = "374:5"]
    pub const _SC_DEVICE_SPECIFIC: C2RustUnnamed = 141;
    #[c2rust::src_loc = "372:5"]
    pub const _SC_DEVICE_IO: C2RustUnnamed = 140;
    #[c2rust::src_loc = "370:5"]
    pub const _SC_THREAD_CPUTIME: C2RustUnnamed = 139;
    #[c2rust::src_loc = "368:5"]
    pub const _SC_CPUTIME: C2RustUnnamed = 138;
    #[c2rust::src_loc = "366:5"]
    pub const _SC_CLOCK_SELECTION: C2RustUnnamed = 137;
    #[c2rust::src_loc = "364:5"]
    pub const _SC_C_LANG_SUPPORT_R: C2RustUnnamed = 136;
    #[c2rust::src_loc = "362:5"]
    pub const _SC_C_LANG_SUPPORT: C2RustUnnamed = 135;
    #[c2rust::src_loc = "360:5"]
    pub const _SC_BASE: C2RustUnnamed = 134;
    #[c2rust::src_loc = "358:5"]
    pub const _SC_BARRIERS: C2RustUnnamed = 133;
    #[c2rust::src_loc = "356:5"]
    pub const _SC_ADVISORY_INFO: C2RustUnnamed = 132;
    #[c2rust::src_loc = "353:5"]
    pub const _SC_XOPEN_REALTIME_THREADS: C2RustUnnamed = 131;
    #[c2rust::src_loc = "351:5"]
    pub const _SC_XOPEN_REALTIME: C2RustUnnamed = 130;
    #[c2rust::src_loc = "349:5"]
    pub const _SC_XOPEN_LEGACY: C2RustUnnamed = 129;
    #[c2rust::src_loc = "346:5"]
    pub const _SC_XBS5_LPBIG_OFFBIG: C2RustUnnamed = 128;
    #[c2rust::src_loc = "344:5"]
    pub const _SC_XBS5_LP64_OFF64: C2RustUnnamed = 127;
    #[c2rust::src_loc = "342:5"]
    pub const _SC_XBS5_ILP32_OFFBIG: C2RustUnnamed = 126;
    #[c2rust::src_loc = "340:5"]
    pub const _SC_XBS5_ILP32_OFF32: C2RustUnnamed = 125;
    #[c2rust::src_loc = "337:5"]
    pub const _SC_NL_TEXTMAX: C2RustUnnamed = 124;
    #[c2rust::src_loc = "335:5"]
    pub const _SC_NL_SETMAX: C2RustUnnamed = 123;
    #[c2rust::src_loc = "333:5"]
    pub const _SC_NL_NMAX: C2RustUnnamed = 122;
    #[c2rust::src_loc = "331:5"]
    pub const _SC_NL_MSGMAX: C2RustUnnamed = 121;
    #[c2rust::src_loc = "329:5"]
    pub const _SC_NL_LANGMAX: C2RustUnnamed = 120;
    #[c2rust::src_loc = "327:5"]
    pub const _SC_NL_ARGMAX: C2RustUnnamed = 119;
    #[c2rust::src_loc = "324:5"]
    pub const _SC_USHRT_MAX: C2RustUnnamed = 118;
    #[c2rust::src_loc = "322:5"]
    pub const _SC_ULONG_MAX: C2RustUnnamed = 117;
    #[c2rust::src_loc = "320:5"]
    pub const _SC_UINT_MAX: C2RustUnnamed = 116;
    #[c2rust::src_loc = "318:5"]
    pub const _SC_UCHAR_MAX: C2RustUnnamed = 115;
    #[c2rust::src_loc = "316:5"]
    pub const _SC_SHRT_MIN: C2RustUnnamed = 114;
    #[c2rust::src_loc = "314:5"]
    pub const _SC_SHRT_MAX: C2RustUnnamed = 113;
    #[c2rust::src_loc = "312:5"]
    pub const _SC_SCHAR_MIN: C2RustUnnamed = 112;
    #[c2rust::src_loc = "310:5"]
    pub const _SC_SCHAR_MAX: C2RustUnnamed = 111;
    #[c2rust::src_loc = "308:5"]
    pub const _SC_SSIZE_MAX: C2RustUnnamed = 110;
    #[c2rust::src_loc = "306:5"]
    pub const _SC_NZERO: C2RustUnnamed = 109;
    #[c2rust::src_loc = "304:5"]
    pub const _SC_MB_LEN_MAX: C2RustUnnamed = 108;
    #[c2rust::src_loc = "302:5"]
    pub const _SC_WORD_BIT: C2RustUnnamed = 107;
    #[c2rust::src_loc = "300:5"]
    pub const _SC_LONG_BIT: C2RustUnnamed = 106;
    #[c2rust::src_loc = "298:5"]
    pub const _SC_INT_MIN: C2RustUnnamed = 105;
    #[c2rust::src_loc = "296:5"]
    pub const _SC_INT_MAX: C2RustUnnamed = 104;
    #[c2rust::src_loc = "294:5"]
    pub const _SC_CHAR_MIN: C2RustUnnamed = 103;
    #[c2rust::src_loc = "292:5"]
    pub const _SC_CHAR_MAX: C2RustUnnamed = 102;
    #[c2rust::src_loc = "290:5"]
    pub const _SC_CHAR_BIT: C2RustUnnamed = 101;
    #[c2rust::src_loc = "287:5"]
    pub const _SC_XOPEN_XPG4: C2RustUnnamed = 100;
    #[c2rust::src_loc = "285:5"]
    pub const _SC_XOPEN_XPG3: C2RustUnnamed = 99;
    #[c2rust::src_loc = "283:5"]
    pub const _SC_XOPEN_XPG2: C2RustUnnamed = 98;
    #[c2rust::src_loc = "280:5"]
    pub const _SC_2_UPE: C2RustUnnamed = 97;
    #[c2rust::src_loc = "278:5"]
    pub const _SC_2_C_VERSION: C2RustUnnamed = 96;
    #[c2rust::src_loc = "276:5"]
    pub const _SC_2_CHAR_TERM: C2RustUnnamed = 95;
    #[c2rust::src_loc = "273:5"]
    pub const _SC_XOPEN_SHM: C2RustUnnamed = 94;
    #[c2rust::src_loc = "271:5"]
    pub const _SC_XOPEN_ENH_I18N: C2RustUnnamed = 93;
    #[c2rust::src_loc = "269:5"]
    pub const _SC_XOPEN_CRYPT: C2RustUnnamed = 92;
    #[c2rust::src_loc = "267:5"]
    pub const _SC_XOPEN_UNIX: C2RustUnnamed = 91;
    #[c2rust::src_loc = "265:5"]
    pub const _SC_XOPEN_XCU_VERSION: C2RustUnnamed = 90;
    #[c2rust::src_loc = "263:5"]
    pub const _SC_XOPEN_VERSION: C2RustUnnamed = 89;
    #[c2rust::src_loc = "260:5"]
    pub const _SC_PASS_MAX: C2RustUnnamed = 88;
    #[c2rust::src_loc = "258:5"]
    pub const _SC_ATEXIT_MAX: C2RustUnnamed = 87;
    #[c2rust::src_loc = "256:5"]
    pub const _SC_AVPHYS_PAGES: C2RustUnnamed = 86;
    #[c2rust::src_loc = "254:5"]
    pub const _SC_PHYS_PAGES: C2RustUnnamed = 85;
    #[c2rust::src_loc = "252:5"]
    pub const _SC_NPROCESSORS_ONLN: C2RustUnnamed = 84;
    #[c2rust::src_loc = "250:5"]
    pub const _SC_NPROCESSORS_CONF: C2RustUnnamed = 83;
    #[c2rust::src_loc = "247:5"]
    pub const _SC_THREAD_PROCESS_SHARED: C2RustUnnamed = 82;
    #[c2rust::src_loc = "245:5"]
    pub const _SC_THREAD_PRIO_PROTECT: C2RustUnnamed = 81;
    #[c2rust::src_loc = "243:5"]
    pub const _SC_THREAD_PRIO_INHERIT: C2RustUnnamed = 80;
    #[c2rust::src_loc = "241:5"]
    pub const _SC_THREAD_PRIORITY_SCHEDULING: C2RustUnnamed = 79;
    #[c2rust::src_loc = "239:5"]
    pub const _SC_THREAD_ATTR_STACKSIZE: C2RustUnnamed = 78;
    #[c2rust::src_loc = "237:5"]
    pub const _SC_THREAD_ATTR_STACKADDR: C2RustUnnamed = 77;
    #[c2rust::src_loc = "235:5"]
    pub const _SC_THREAD_THREADS_MAX: C2RustUnnamed = 76;
    #[c2rust::src_loc = "233:5"]
    pub const _SC_THREAD_STACK_MIN: C2RustUnnamed = 75;
    #[c2rust::src_loc = "231:5"]
    pub const _SC_THREAD_KEYS_MAX: C2RustUnnamed = 74;
    #[c2rust::src_loc = "229:5"]
    pub const _SC_THREAD_DESTRUCTOR_ITERATIONS: C2RustUnnamed = 73;
    #[c2rust::src_loc = "227:5"]
    pub const _SC_TTY_NAME_MAX: C2RustUnnamed = 72;
    #[c2rust::src_loc = "225:5"]
    pub const _SC_LOGIN_NAME_MAX: C2RustUnnamed = 71;
    #[c2rust::src_loc = "223:5"]
    pub const _SC_GETPW_R_SIZE_MAX: C2RustUnnamed = 70;
    #[c2rust::src_loc = "221:5"]
    pub const _SC_GETGR_R_SIZE_MAX: C2RustUnnamed = 69;
    #[c2rust::src_loc = "219:5"]
    pub const _SC_THREAD_SAFE_FUNCTIONS: C2RustUnnamed = 68;
    #[c2rust::src_loc = "217:5"]
    pub const _SC_THREADS: C2RustUnnamed = 67;
    #[c2rust::src_loc = "213:5"]
    pub const _SC_T_IOV_MAX: C2RustUnnamed = 66;
    #[c2rust::src_loc = "211:5"]
    pub const _SC_PII_OSI_M: C2RustUnnamed = 65;
    #[c2rust::src_loc = "209:5"]
    pub const _SC_PII_OSI_CLTS: C2RustUnnamed = 64;
    #[c2rust::src_loc = "207:5"]
    pub const _SC_PII_OSI_COTS: C2RustUnnamed = 63;
    #[c2rust::src_loc = "205:5"]
    pub const _SC_PII_INTERNET_DGRAM: C2RustUnnamed = 62;
    #[c2rust::src_loc = "203:5"]
    pub const _SC_PII_INTERNET_STREAM: C2RustUnnamed = 61;
    #[c2rust::src_loc = "201:5"]
    pub const _SC_IOV_MAX: C2RustUnnamed = 60;
    #[c2rust::src_loc = "199:5"]
    pub const _SC_UIO_MAXIOV: C2RustUnnamed = 60;
    #[c2rust::src_loc = "197:5"]
    pub const _SC_SELECT: C2RustUnnamed = 59;
    #[c2rust::src_loc = "195:5"]
    pub const _SC_POLL: C2RustUnnamed = 58;
    #[c2rust::src_loc = "193:5"]
    pub const _SC_PII_OSI: C2RustUnnamed = 57;
    #[c2rust::src_loc = "191:5"]
    pub const _SC_PII_INTERNET: C2RustUnnamed = 56;
    #[c2rust::src_loc = "189:5"]
    pub const _SC_PII_SOCKET: C2RustUnnamed = 55;
    #[c2rust::src_loc = "187:5"]
    pub const _SC_PII_XTI: C2RustUnnamed = 54;
    #[c2rust::src_loc = "185:5"]
    pub const _SC_PII: C2RustUnnamed = 53;
    #[c2rust::src_loc = "182:5"]
    pub const _SC_2_LOCALEDEF: C2RustUnnamed = 52;
    #[c2rust::src_loc = "180:5"]
    pub const _SC_2_SW_DEV: C2RustUnnamed = 51;
    #[c2rust::src_loc = "178:5"]
    pub const _SC_2_FORT_RUN: C2RustUnnamed = 50;
    #[c2rust::src_loc = "176:5"]
    pub const _SC_2_FORT_DEV: C2RustUnnamed = 49;
    #[c2rust::src_loc = "174:5"]
    pub const _SC_2_C_DEV: C2RustUnnamed = 48;
    #[c2rust::src_loc = "172:5"]
    pub const _SC_2_C_BIND: C2RustUnnamed = 47;
    #[c2rust::src_loc = "170:5"]
    pub const _SC_2_VERSION: C2RustUnnamed = 46;
    #[c2rust::src_loc = "167:5"]
    pub const _SC_CHARCLASS_NAME_MAX: C2RustUnnamed = 45;
    #[c2rust::src_loc = "165:5"]
    pub const _SC_RE_DUP_MAX: C2RustUnnamed = 44;
    #[c2rust::src_loc = "163:5"]
    pub const _SC_LINE_MAX: C2RustUnnamed = 43;
    #[c2rust::src_loc = "161:5"]
    pub const _SC_EXPR_NEST_MAX: C2RustUnnamed = 42;
    #[c2rust::src_loc = "159:5"]
    pub const _SC_EQUIV_CLASS_MAX: C2RustUnnamed = 41;
    #[c2rust::src_loc = "157:5"]
    pub const _SC_COLL_WEIGHTS_MAX: C2RustUnnamed = 40;
    #[c2rust::src_loc = "155:5"]
    pub const _SC_BC_STRING_MAX: C2RustUnnamed = 39;
    #[c2rust::src_loc = "153:5"]
    pub const _SC_BC_SCALE_MAX: C2RustUnnamed = 38;
    #[c2rust::src_loc = "151:5"]
    pub const _SC_BC_DIM_MAX: C2RustUnnamed = 37;
    #[c2rust::src_loc = "149:5"]
    pub const _SC_BC_BASE_MAX: C2RustUnnamed = 36;
    #[c2rust::src_loc = "144:5"]
    pub const _SC_TIMER_MAX: C2RustUnnamed = 35;
    #[c2rust::src_loc = "142:5"]
    pub const _SC_SIGQUEUE_MAX: C2RustUnnamed = 34;
    #[c2rust::src_loc = "140:5"]
    pub const _SC_SEM_VALUE_MAX: C2RustUnnamed = 33;
    #[c2rust::src_loc = "138:5"]
    pub const _SC_SEM_NSEMS_MAX: C2RustUnnamed = 32;
    #[c2rust::src_loc = "136:5"]
    pub const _SC_RTSIG_MAX: C2RustUnnamed = 31;
    #[c2rust::src_loc = "133:5"]
    pub const _SC_PAGESIZE: C2RustUnnamed = 30;
    #[c2rust::src_loc = "131:5"]
    pub const _SC_VERSION: C2RustUnnamed = 29;
    #[c2rust::src_loc = "129:5"]
    pub const _SC_MQ_PRIO_MAX: C2RustUnnamed = 28;
    #[c2rust::src_loc = "127:5"]
    pub const _SC_MQ_OPEN_MAX: C2RustUnnamed = 27;
    #[c2rust::src_loc = "125:5"]
    pub const _SC_DELAYTIMER_MAX: C2RustUnnamed = 26;
    #[c2rust::src_loc = "123:5"]
    pub const _SC_AIO_PRIO_DELTA_MAX: C2RustUnnamed = 25;
    #[c2rust::src_loc = "121:5"]
    pub const _SC_AIO_MAX: C2RustUnnamed = 24;
    #[c2rust::src_loc = "119:5"]
    pub const _SC_AIO_LISTIO_MAX: C2RustUnnamed = 23;
    #[c2rust::src_loc = "117:5"]
    pub const _SC_SHARED_MEMORY_OBJECTS: C2RustUnnamed = 22;
    #[c2rust::src_loc = "115:5"]
    pub const _SC_SEMAPHORES: C2RustUnnamed = 21;
    #[c2rust::src_loc = "113:5"]
    pub const _SC_MESSAGE_PASSING: C2RustUnnamed = 20;
    #[c2rust::src_loc = "111:5"]
    pub const _SC_MEMORY_PROTECTION: C2RustUnnamed = 19;
    #[c2rust::src_loc = "109:5"]
    pub const _SC_MEMLOCK_RANGE: C2RustUnnamed = 18;
    #[c2rust::src_loc = "107:5"]
    pub const _SC_MEMLOCK: C2RustUnnamed = 17;
    #[c2rust::src_loc = "105:5"]
    pub const _SC_MAPPED_FILES: C2RustUnnamed = 16;
    #[c2rust::src_loc = "103:5"]
    pub const _SC_FSYNC: C2RustUnnamed = 15;
    #[c2rust::src_loc = "101:5"]
    pub const _SC_SYNCHRONIZED_IO: C2RustUnnamed = 14;
    #[c2rust::src_loc = "99:5"]
    pub const _SC_PRIORITIZED_IO: C2RustUnnamed = 13;
    #[c2rust::src_loc = "97:5"]
    pub const _SC_ASYNCHRONOUS_IO: C2RustUnnamed = 12;
    #[c2rust::src_loc = "95:5"]
    pub const _SC_TIMERS: C2RustUnnamed = 11;
    #[c2rust::src_loc = "93:5"]
    pub const _SC_PRIORITY_SCHEDULING: C2RustUnnamed = 10;
    #[c2rust::src_loc = "91:5"]
    pub const _SC_REALTIME_SIGNALS: C2RustUnnamed = 9;
    #[c2rust::src_loc = "89:5"]
    pub const _SC_SAVED_IDS: C2RustUnnamed = 8;
    #[c2rust::src_loc = "87:5"]
    pub const _SC_JOB_CONTROL: C2RustUnnamed = 7;
    #[c2rust::src_loc = "85:5"]
    pub const _SC_TZNAME_MAX: C2RustUnnamed = 6;
    #[c2rust::src_loc = "83:5"]
    pub const _SC_STREAM_MAX: C2RustUnnamed = 5;
    #[c2rust::src_loc = "81:5"]
    pub const _SC_OPEN_MAX: C2RustUnnamed = 4;
    #[c2rust::src_loc = "79:5"]
    pub const _SC_NGROUPS_MAX: C2RustUnnamed = 3;
    #[c2rust::src_loc = "77:5"]
    pub const _SC_CLK_TCK: C2RustUnnamed = 2;
    #[c2rust::src_loc = "75:5"]
    pub const _SC_CHILD_MAX: C2RustUnnamed = 1;
    #[c2rust::src_loc = "73:5"]
    pub const _SC_ARG_MAX: C2RustUnnamed = 0;
}
#[c2rust::header_src = "/var/home/oro/bubblewrap/utils.h:21"]
pub mod utils_h {
    #[c2rust::src_loc = "46:1"]
    pub type bool_0 = libc::c_int;
    #[inline]
    #[c2rust::src_loc = "169:1"]
    pub unsafe extern "C" fn steal_pointer(mut pp: *mut libc::c_void) -> *mut libc::c_void {
        let mut ptr: *mut *mut libc::c_void = pp as *mut *mut libc::c_void;
        let mut ref_0: *mut libc::c_void = 0 as *mut libc::c_void;
        ref_0 = *ptr;
        *ptr = 0 as *mut libc::c_void;
        return ref_0;
    }
}
#[c2rust::header_src = "/usr/include/bits/socket_type.h:23"]
pub mod socket_type_h {
    #[c2rust::src_loc = "49:3"]
    pub const SOCK_CLOEXEC: __socket_type = 524288;
    #[c2rust::src_loc = "36:3"]
    pub const SOCK_SEQPACKET: __socket_type = 5;
    #[c2rust::src_loc = "24:1"]
    pub type __socket_type = libc::c_uint;
    #[c2rust::src_loc = "52:3"]
    pub const SOCK_NONBLOCK: __socket_type = 2048;
    #[c2rust::src_loc = "41:3"]
    pub const SOCK_PACKET: __socket_type = 10;
    #[c2rust::src_loc = "39:3"]
    pub const SOCK_DCCP: __socket_type = 6;
    #[c2rust::src_loc = "34:3"]
    pub const SOCK_RDM: __socket_type = 4;
    #[c2rust::src_loc = "32:3"]
    pub const SOCK_RAW: __socket_type = 3;
    #[c2rust::src_loc = "29:3"]
    pub const SOCK_DGRAM: __socket_type = 2;
    #[c2rust::src_loc = "26:3"]
    pub const SOCK_STREAM: __socket_type = 1;
}
#[c2rust::header_src = "/usr/include/bits/socket.h:23"]
pub mod socket_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "262:8"]
    pub struct msghdr {
        pub msg_name: *mut libc::c_void,
        pub msg_namelen: socklen_t,
        pub msg_iov: *mut iovec,
        pub msg_iovlen: size_t,
        pub msg_control: *mut libc::c_void,
        pub msg_controllen: size_t,
        pub msg_flags: libc::c_int,
    }
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "375:8"]
    pub struct ucred {
        pub pid: pid_t,
        pub uid: uid_t,
        pub gid: gid_t,
    }
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "280:8"]
    pub struct cmsghdr {
        pub cmsg_len: size_t,
        pub cmsg_level: libc::c_int,
        pub cmsg_type: libc::c_int,
        pub __cmsg_data: [libc::c_uchar; 0],
    }
    #[c2rust::src_loc = "368:7"]
    pub const SCM_CREDENTIALS: C2RustUnnamed_0 = 2;
    #[c2rust::src_loc = "363:1"]
    pub type C2RustUnnamed_0 = libc::c_uint;
    #[c2rust::src_loc = "365:5"]
    pub const SCM_RIGHTS: C2RustUnnamed_0 = 1;
    use super::fcntl_h::pid_t;
    use super::stddef_h::size_t;
    use super::struct_iovec_h::iovec;
    use super::sys_types_h::{gid_t, uid_t};
    use super::unistd_h::socklen_t;
    extern "C" {
        #[c2rust::src_loc = "316:1"]
        pub fn __cmsg_nxthdr(__mhdr: *mut msghdr, __cmsg: *mut cmsghdr) -> *mut cmsghdr;
    }
}
#[c2rust::header_src = "/usr/include/stdlib.h:21"]
pub mod stdlib_h {
    extern "C" {
        #[c2rust::src_loc = "177:17"]
        pub fn strtol(
            _: *const libc::c_char,
            _: *mut *mut libc::c_char,
            _: libc::c_int,
        ) -> libc::c_long;
        #[c2rust::src_loc = "553:14"]
        pub fn malloc(_: libc::c_ulong) -> *mut libc::c_void;
        #[c2rust::src_loc = "556:14"]
        pub fn calloc(_: libc::c_ulong, _: libc::c_ulong) -> *mut libc::c_void;
        #[c2rust::src_loc = "564:14"]
        pub fn realloc(_: *mut libc::c_void, _: libc::c_ulong) -> *mut libc::c_void;
        #[c2rust::src_loc = "568:13"]
        pub fn free(_: *mut libc::c_void);
        #[c2rust::src_loc = "637:13"]
        pub fn exit(_: libc::c_int) -> !;
        #[c2rust::src_loc = "673:1"]
        pub fn setenv(
            __name: *const libc::c_char,
            __value: *const libc::c_char,
            __replace: libc::c_int,
        ) -> libc::c_int;
        #[c2rust::src_loc = "677:1"]
        pub fn unsetenv(__name: *const libc::c_char) -> libc::c_int;
        #[c2rust::src_loc = "684:1"]
        pub fn clearenv() -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/errno.h:21"]
pub mod errno_h {
    extern "C" {
        #[c2rust::src_loc = "37:1"]
        pub fn __errno_location() -> *mut libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/assert.h:21"]
pub mod assert_h {
    extern "C" {
        #[c2rust::src_loc = "67:1"]
        pub fn __assert_fail(
            __assertion: *const libc::c_char,
            __file: *const libc::c_char,
            __line: libc::c_uint,
            __function: *const libc::c_char,
        ) -> !;
    }
}
#[c2rust::header_src = "/usr/include/string.h:21"]
pub mod string_h {
    extern "C" {
        #[c2rust::src_loc = "149:14"]
        pub fn strcat(_: *mut libc::c_char, _: *const libc::c_char) -> *mut libc::c_char;
        #[c2rust::src_loc = "159:12"]
        pub fn strncmp(
            _: *const libc::c_char,
            _: *const libc::c_char,
            _: libc::c_ulong,
        ) -> libc::c_int;
        #[c2rust::src_loc = "187:14"]
        pub fn strdup(_: *const libc::c_char) -> *mut libc::c_char;
        #[c2rust::src_loc = "407:15"]
        pub fn strlen(_: *const libc::c_char) -> libc::c_ulong;
        #[c2rust::src_loc = "419:14"]
        pub fn strerror(_: libc::c_int) -> *mut libc::c_char;
    }
}
#[c2rust::header_src = "/usr/include/sys/stat.h:21"]
pub mod stat_h {
    use super::struct_stat_h::stat;
    use super::types_h::__mode_t;
    extern "C" {
        #[c2rust::src_loc = "227:1"]
        pub fn stat(__file: *const libc::c_char, __buf: *mut stat) -> libc::c_int;
        #[c2rust::src_loc = "389:1"]
        pub fn mkdir(__path: *const libc::c_char, __mode: __mode_t) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/sys/socket.h:23"]
pub mod sys_socket_h {
    use super::socket_h::msghdr;
    use super::stdio_h::ssize_t;
    use super::unistd_h::socklen_t;
    extern "C" {
        #[c2rust::src_loc = "216:1"]
        pub fn recvmsg(__fd: libc::c_int, __message: *mut msghdr, __flags: libc::c_int) -> ssize_t;
        #[c2rust::src_loc = "174:1"]
        pub fn sendmsg(
            __fd: libc::c_int,
            __message: *const msghdr,
            __flags: libc::c_int,
        ) -> ssize_t;
        #[c2rust::src_loc = "108:1"]
        pub fn socketpair(
            __domain: libc::c_int,
            __type: libc::c_int,
            __protocol: libc::c_int,
            __fds: *mut libc::c_int,
        ) -> libc::c_int;
        #[c2rust::src_loc = "277:1"]
        pub fn setsockopt(
            __fd: libc::c_int,
            __level: libc::c_int,
            __optname: libc::c_int,
            __optval: *const libc::c_void,
            __optlen: socklen_t,
        ) -> libc::c_int;
    }
}
use self::assert_h::__assert_fail;
pub use self::confname_h::{
    C2RustUnnamed, _SC_2_CHAR_TERM, _SC_2_C_BIND, _SC_2_C_DEV, _SC_2_C_VERSION, _SC_2_FORT_DEV,
    _SC_2_FORT_RUN, _SC_2_LOCALEDEF, _SC_2_PBS, _SC_2_PBS_ACCOUNTING, _SC_2_PBS_CHECKPOINT,
    _SC_2_PBS_LOCATE, _SC_2_PBS_MESSAGE, _SC_2_PBS_TRACK, _SC_2_SW_DEV, _SC_2_UPE, _SC_2_VERSION,
    _SC_ADVISORY_INFO, _SC_AIO_LISTIO_MAX, _SC_AIO_MAX, _SC_AIO_PRIO_DELTA_MAX, _SC_ARG_MAX,
    _SC_ASYNCHRONOUS_IO, _SC_ATEXIT_MAX, _SC_AVPHYS_PAGES, _SC_BARRIERS, _SC_BASE, _SC_BC_BASE_MAX,
    _SC_BC_DIM_MAX, _SC_BC_SCALE_MAX, _SC_BC_STRING_MAX, _SC_CHARCLASS_NAME_MAX, _SC_CHAR_BIT,
    _SC_CHAR_MAX, _SC_CHAR_MIN, _SC_CHILD_MAX, _SC_CLK_TCK, _SC_CLOCK_SELECTION,
    _SC_COLL_WEIGHTS_MAX, _SC_CPUTIME, _SC_C_LANG_SUPPORT, _SC_C_LANG_SUPPORT_R,
    _SC_DELAYTIMER_MAX, _SC_DEVICE_IO, _SC_DEVICE_SPECIFIC, _SC_DEVICE_SPECIFIC_R,
    _SC_EQUIV_CLASS_MAX, _SC_EXPR_NEST_MAX, _SC_FD_MGMT, _SC_FIFO, _SC_FILE_ATTRIBUTES,
    _SC_FILE_LOCKING, _SC_FILE_SYSTEM, _SC_FSYNC, _SC_GETGR_R_SIZE_MAX, _SC_GETPW_R_SIZE_MAX,
    _SC_HOST_NAME_MAX, _SC_INT_MAX, _SC_INT_MIN, _SC_IOV_MAX, _SC_IPV6, _SC_JOB_CONTROL,
    _SC_LEVEL1_DCACHE_ASSOC, _SC_LEVEL1_DCACHE_LINESIZE, _SC_LEVEL1_DCACHE_SIZE,
    _SC_LEVEL1_ICACHE_ASSOC, _SC_LEVEL1_ICACHE_LINESIZE, _SC_LEVEL1_ICACHE_SIZE,
    _SC_LEVEL2_CACHE_ASSOC, _SC_LEVEL2_CACHE_LINESIZE, _SC_LEVEL2_CACHE_SIZE,
    _SC_LEVEL3_CACHE_ASSOC, _SC_LEVEL3_CACHE_LINESIZE, _SC_LEVEL3_CACHE_SIZE,
    _SC_LEVEL4_CACHE_ASSOC, _SC_LEVEL4_CACHE_LINESIZE, _SC_LEVEL4_CACHE_SIZE, _SC_LINE_MAX,
    _SC_LOGIN_NAME_MAX, _SC_LONG_BIT, _SC_MAPPED_FILES, _SC_MB_LEN_MAX, _SC_MEMLOCK,
    _SC_MEMLOCK_RANGE, _SC_MEMORY_PROTECTION, _SC_MESSAGE_PASSING, _SC_MINSIGSTKSZ,
    _SC_MONOTONIC_CLOCK, _SC_MQ_OPEN_MAX, _SC_MQ_PRIO_MAX, _SC_MULTI_PROCESS, _SC_NETWORKING,
    _SC_NGROUPS_MAX, _SC_NL_ARGMAX, _SC_NL_LANGMAX, _SC_NL_MSGMAX, _SC_NL_NMAX, _SC_NL_SETMAX,
    _SC_NL_TEXTMAX, _SC_NPROCESSORS_CONF, _SC_NPROCESSORS_ONLN, _SC_NZERO, _SC_OPEN_MAX,
    _SC_PAGESIZE, _SC_PASS_MAX, _SC_PHYS_PAGES, _SC_PII, _SC_PII_INTERNET, _SC_PII_INTERNET_DGRAM,
    _SC_PII_INTERNET_STREAM, _SC_PII_OSI, _SC_PII_OSI_CLTS, _SC_PII_OSI_COTS, _SC_PII_OSI_M,
    _SC_PII_SOCKET, _SC_PII_XTI, _SC_PIPE, _SC_POLL, _SC_PRIORITIZED_IO, _SC_PRIORITY_SCHEDULING,
    _SC_RAW_SOCKETS, _SC_READER_WRITER_LOCKS, _SC_REALTIME_SIGNALS, _SC_REGEXP, _SC_REGEX_VERSION,
    _SC_RE_DUP_MAX, _SC_RTSIG_MAX, _SC_SAVED_IDS, _SC_SCHAR_MAX, _SC_SCHAR_MIN, _SC_SELECT,
    _SC_SEMAPHORES, _SC_SEM_NSEMS_MAX, _SC_SEM_VALUE_MAX, _SC_SHARED_MEMORY_OBJECTS, _SC_SHELL,
    _SC_SHRT_MAX, _SC_SHRT_MIN, _SC_SIGNALS, _SC_SIGQUEUE_MAX, _SC_SIGSTKSZ, _SC_SINGLE_PROCESS,
    _SC_SPAWN, _SC_SPIN_LOCKS, _SC_SPORADIC_SERVER, _SC_SSIZE_MAX, _SC_SS_REPL_MAX, _SC_STREAMS,
    _SC_STREAM_MAX, _SC_SYMLOOP_MAX, _SC_SYNCHRONIZED_IO, _SC_SYSTEM_DATABASE,
    _SC_SYSTEM_DATABASE_R, _SC_THREADS, _SC_THREAD_ATTR_STACKADDR, _SC_THREAD_ATTR_STACKSIZE,
    _SC_THREAD_CPUTIME, _SC_THREAD_DESTRUCTOR_ITERATIONS, _SC_THREAD_KEYS_MAX,
    _SC_THREAD_PRIORITY_SCHEDULING, _SC_THREAD_PRIO_INHERIT, _SC_THREAD_PRIO_PROTECT,
    _SC_THREAD_PROCESS_SHARED, _SC_THREAD_ROBUST_PRIO_INHERIT, _SC_THREAD_ROBUST_PRIO_PROTECT,
    _SC_THREAD_SAFE_FUNCTIONS, _SC_THREAD_SPORADIC_SERVER, _SC_THREAD_STACK_MIN,
    _SC_THREAD_THREADS_MAX, _SC_TIMEOUTS, _SC_TIMERS, _SC_TIMER_MAX, _SC_TRACE,
    _SC_TRACE_EVENT_FILTER, _SC_TRACE_EVENT_NAME_MAX, _SC_TRACE_INHERIT, _SC_TRACE_LOG,
    _SC_TRACE_NAME_MAX, _SC_TRACE_SYS_MAX, _SC_TRACE_USER_EVENT_MAX, _SC_TTY_NAME_MAX,
    _SC_TYPED_MEMORY_OBJECTS, _SC_TZNAME_MAX, _SC_T_IOV_MAX, _SC_UCHAR_MAX, _SC_UINT_MAX,
    _SC_UIO_MAXIOV, _SC_ULONG_MAX, _SC_USER_GROUPS, _SC_USER_GROUPS_R, _SC_USHRT_MAX,
    _SC_V6_ILP32_OFF32, _SC_V6_ILP32_OFFBIG, _SC_V6_LP64_OFF64, _SC_V6_LPBIG_OFFBIG,
    _SC_V7_ILP32_OFF32, _SC_V7_ILP32_OFFBIG, _SC_V7_LP64_OFF64, _SC_V7_LPBIG_OFFBIG, _SC_VERSION,
    _SC_WORD_BIT, _SC_XBS5_ILP32_OFF32, _SC_XBS5_ILP32_OFFBIG, _SC_XBS5_LP64_OFF64,
    _SC_XBS5_LPBIG_OFFBIG, _SC_XOPEN_CRYPT, _SC_XOPEN_ENH_I18N, _SC_XOPEN_LEGACY,
    _SC_XOPEN_REALTIME, _SC_XOPEN_REALTIME_THREADS, _SC_XOPEN_SHM, _SC_XOPEN_STREAMS,
    _SC_XOPEN_UNIX, _SC_XOPEN_VERSION, _SC_XOPEN_XCU_VERSION, _SC_XOPEN_XPG2, _SC_XOPEN_XPG3,
    _SC_XOPEN_XPG4,
};
pub use self::dirent_h::dirent;
use self::errno_h::__errno_location;
pub use self::fcntl_h::{creat, mode_t, open, openat, pid_t};
pub use self::include_dirent_h::{__dirstream, closedir, dirfd, fdopendir, readdir, DIR};
pub use self::internal::{__builtin_va_list, __va_list_tag};
pub use self::socket_h::{
    cmsghdr, msghdr, ucred, C2RustUnnamed_0, __cmsg_nxthdr, SCM_CREDENTIALS, SCM_RIGHTS,
};
pub use self::socket_type_h::{
    __socket_type, SOCK_CLOEXEC, SOCK_DCCP, SOCK_DGRAM, SOCK_NONBLOCK, SOCK_PACKET, SOCK_RAW,
    SOCK_RDM, SOCK_SEQPACKET, SOCK_STREAM,
};
use self::stat_h::{mkdir, stat};
pub use self::stdarg_h::va_list;
pub use self::stddef_h::size_t;
pub use self::stdio_h::{fprintf, fputs, ssize_t, stderr, vasprintf, vfprintf};
use self::stdlib_h::{calloc, clearenv, exit, free, malloc, realloc, setenv, strtol, unsetenv};
use self::string_h::{strcat, strdup, strerror, strlen, strncmp};
pub use self::struct_FILE_h::{_IO_codecvt, _IO_lock_t, _IO_marker, _IO_wide_data, _IO_FILE};
pub use self::struct_iovec_h::iovec;
pub use self::struct_stat_h::stat;
pub use self::struct_timespec_h::timespec;
use self::sys_socket_h::{recvmsg, sendmsg, setsockopt, socketpair};
pub use self::sys_types_h::{gid_t, uid_t};
pub use self::types_h::{
    __blkcnt_t, __blksize_t, __dev_t, __gid_t, __ino64_t, __ino_t, __mode_t, __nlink_t, __off64_t,
    __off_t, __pid_t, __socklen_t, __ssize_t, __syscall_slong_t, __time_t, __uid_t,
};
pub use self::unistd_h::{
    close, fork, getegid, geteuid, getpid, read, readlink, socklen_t, syscall, sysconf, write,
};
pub use self::utils_h::{bool_0, steal_pointer};
pub use self::FILE_h::FILE;
#[c2rust::src_loc = "35:1"]
unsafe extern "C" fn warnv(mut format: *const libc::c_char, mut args: ::core::ffi::VaList) {
    fprintf(stderr, b"bwrap: \0" as *const u8 as *const libc::c_char);
    vfprintf(stderr, format, args.as_va_list());
    fprintf(stderr, b"\n\0" as *const u8 as *const libc::c_char);
}
#[no_mangle]
#[c2rust::src_loc = "43:1"]
pub unsafe extern "C" fn warn(mut format: *const libc::c_char, mut args: ...) {
    let mut args_0: ::core::ffi::VaListImpl;
    args_0 = args.clone();
    warnv(format, args_0.as_va_list());
}
#[no_mangle]
#[c2rust::src_loc = "53:1"]
pub unsafe extern "C" fn die_with_error(mut format: *const libc::c_char, mut args: ...) -> ! {
    let mut args_0: ::core::ffi::VaListImpl;
    let mut errsv: libc::c_int = 0;
    errsv = *__errno_location();
    fprintf(stderr, b"bwrap: \0" as *const u8 as *const libc::c_char);
    args_0 = args.clone();
    vfprintf(stderr, format, args_0.as_va_list());
    fprintf(
        stderr,
        b": %s\n\0" as *const u8 as *const libc::c_char,
        strerror(errsv),
    );
    exit(1 as libc::c_int);
}
#[no_mangle]
#[c2rust::src_loc = "72:1"]
pub unsafe extern "C" fn die(mut format: *const libc::c_char, mut args: ...) -> ! {
    let mut args_0: ::core::ffi::VaListImpl;
    args_0 = args.clone();
    warnv(format, args_0.as_va_list());
    exit(1 as libc::c_int);
}
#[no_mangle]
#[c2rust::src_loc = "84:1"]
pub unsafe extern "C" fn die_unless_label_valid(mut label: *const libc::c_char) {
    die(b"labeling not supported on this system\0" as *const u8 as *const libc::c_char);
}
#[no_mangle]
#[c2rust::src_loc = "98:1"]
pub unsafe extern "C" fn die_oom() -> ! {
    fputs(
        b"Out of memory\n\0" as *const u8 as *const libc::c_char,
        stderr,
    );
    exit(1 as libc::c_int);
}
#[no_mangle]
#[c2rust::src_loc = "106:1"]
pub unsafe extern "C" fn fork_intermediate_child() {
    let mut pid: libc::c_int = fork();
    if pid == -(1 as libc::c_int) {
        die_with_error(b"Can't fork for --pidns\0" as *const u8 as *const libc::c_char);
    }
    if pid != 0 as libc::c_int {
        exit(0 as libc::c_int);
    }
}
#[no_mangle]
#[c2rust::src_loc = "118:1"]
pub unsafe extern "C" fn xmalloc(mut size: size_t) -> *mut libc::c_void {
    let mut res: *mut libc::c_void = malloc(size);
    if res.is_null() {
        die_oom();
    }
    return res;
}
#[no_mangle]
#[c2rust::src_loc = "128:1"]
pub unsafe extern "C" fn xcalloc(mut size: size_t) -> *mut libc::c_void {
    let mut res: *mut libc::c_void = calloc(1 as libc::c_int as libc::c_ulong, size);
    if res.is_null() {
        die_oom();
    }
    return res;
}
#[no_mangle]
#[c2rust::src_loc = "138:1"]
pub unsafe extern "C" fn xrealloc(
    mut ptr: *mut libc::c_void,
    mut size: size_t,
) -> *mut libc::c_void {
    let mut res: *mut libc::c_void = realloc(ptr, size);
    if size != 0 as libc::c_int as libc::c_ulong && res.is_null() {
        die_oom();
    }
    return res;
}
#[no_mangle]
#[c2rust::src_loc = "148:1"]
pub unsafe extern "C" fn xstrdup(mut str: *const libc::c_char) -> *mut libc::c_char {
    let mut res: *mut libc::c_char = 0 as *mut libc::c_char;
    if !str.is_null() {
    } else {
        __assert_fail(
            b"str != NULL\0" as *const u8 as *const libc::c_char,
            b"../utils.c\0" as *const u8 as *const libc::c_char,
            153 as libc::c_int as libc::c_uint,
            (*::core::mem::transmute::<&[u8; 28], &[libc::c_char; 28]>(
                b"char *xstrdup(const char *)\0",
            ))
            .as_ptr(),
        );
    }
    'c_3700: {
        if !str.is_null() {
        } else {
            __assert_fail(
                b"str != NULL\0" as *const u8 as *const libc::c_char,
                b"../utils.c\0" as *const u8 as *const libc::c_char,
                153 as libc::c_int as libc::c_uint,
                (*::core::mem::transmute::<&[u8; 28], &[libc::c_char; 28]>(
                    b"char *xstrdup(const char *)\0",
                ))
                .as_ptr(),
            );
        }
    };
    res = strdup(str);
    if res.is_null() {
        die_oom();
    }
    return res;
}
#[no_mangle]
#[c2rust::src_loc = "162:1"]
pub unsafe extern "C" fn strfreev(mut str_array: *mut *mut libc::c_char) {
    if !str_array.is_null() {
        let mut i: libc::c_int = 0;
        i = 0 as libc::c_int;
        while !(*str_array.offset(i as isize)).is_null() {
            free(*str_array.offset(i as isize) as *mut libc::c_void);
            i += 1;
            i;
        }
        free(str_array as *mut libc::c_void);
    }
}
#[no_mangle]
#[c2rust::src_loc = "182:1"]
pub unsafe extern "C" fn has_path_prefix(
    mut str: *const libc::c_char,
    mut prefix: *const libc::c_char,
) -> bool_0 {
    loop {
        while *str as libc::c_int == '/' as i32 {
            str = str.offset(1);
            str;
        }
        while *prefix as libc::c_int == '/' as i32 {
            prefix = prefix.offset(1);
            prefix;
        }
        if *prefix as libc::c_int == 0 as libc::c_int {
            return 1 as libc::c_int;
        }
        while *prefix as libc::c_int != 0 as libc::c_int && *prefix as libc::c_int != '/' as i32 {
            if *str as libc::c_int != *prefix as libc::c_int {
                return 0 as libc::c_int;
            }
            str = str.offset(1);
            str;
            prefix = prefix.offset(1);
            prefix;
        }
        if *str as libc::c_int != '/' as i32 && *str as libc::c_int != 0 as libc::c_int {
            return 0 as libc::c_int;
        }
    }
}
#[no_mangle]
#[c2rust::src_loc = "215:1"]
pub unsafe extern "C" fn path_equal(
    mut path1: *const libc::c_char,
    mut path2: *const libc::c_char,
) -> bool_0 {
    loop {
        while *path1 as libc::c_int == '/' as i32 {
            path1 = path1.offset(1);
            path1;
        }
        while *path2 as libc::c_int == '/' as i32 {
            path2 = path2.offset(1);
            path2;
        }
        if *path1 as libc::c_int == 0 as libc::c_int || *path2 as libc::c_int == 0 as libc::c_int {
            return (*path1 as libc::c_int == 0 as libc::c_int
                && *path2 as libc::c_int == 0 as libc::c_int) as libc::c_int;
        }
        while *path1 as libc::c_int != 0 as libc::c_int && *path1 as libc::c_int != '/' as i32 {
            if *path1 as libc::c_int != *path2 as libc::c_int {
                return 0 as libc::c_int;
            }
            path1 = path1.offset(1);
            path1;
            path2 = path2.offset(1);
            path2;
        }
        if *path2 as libc::c_int != '/' as i32 && *path2 as libc::c_int != 0 as libc::c_int {
            return 0 as libc::c_int;
        }
    }
}
#[no_mangle]
#[c2rust::src_loc = "248:1"]
pub unsafe extern "C" fn has_prefix(
    mut str: *const libc::c_char,
    mut prefix: *const libc::c_char,
) -> bool_0 {
    return (strncmp(str, prefix, strlen(prefix)) == 0 as libc::c_int) as libc::c_int;
}
#[no_mangle]
#[c2rust::src_loc = "255:1"]
pub unsafe extern "C" fn xclearenv() {
    if clearenv() != 0 as libc::c_int {
        die_with_error(b"clearenv failed\0" as *const u8 as *const libc::c_char);
    }
}
#[no_mangle]
#[c2rust::src_loc = "262:1"]
pub unsafe extern "C" fn xsetenv(
    mut name: *const libc::c_char,
    mut value: *const libc::c_char,
    mut overwrite: libc::c_int,
) {
    if setenv(name, value, overwrite) != 0 {
        die(b"setenv failed\0" as *const u8 as *const libc::c_char);
    }
}
#[no_mangle]
#[c2rust::src_loc = "269:1"]
pub unsafe extern "C" fn xunsetenv(mut name: *const libc::c_char) {
    if unsetenv(name) != 0 {
        die(b"unsetenv failed\0" as *const u8 as *const libc::c_char);
    }
}
#[no_mangle]
#[c2rust::src_loc = "276:1"]
pub unsafe extern "C" fn strconcat(
    mut s1: *const libc::c_char,
    mut s2: *const libc::c_char,
) -> *mut libc::c_char {
    let mut len: size_t = 0 as libc::c_int as size_t;
    let mut res: *mut libc::c_char = 0 as *mut libc::c_char;
    if !s1.is_null() {
        len = (len as libc::c_ulong).wrapping_add(strlen(s1)) as size_t as size_t;
    }
    if !s2.is_null() {
        len = (len as libc::c_ulong).wrapping_add(strlen(s2)) as size_t as size_t;
    }
    res = xmalloc(len.wrapping_add(1 as libc::c_int as libc::c_ulong)) as *mut libc::c_char;
    *res = 0 as libc::c_int as libc::c_char;
    if !s1.is_null() {
        strcat(res, s1);
    }
    if !s2.is_null() {
        strcat(res, s2);
    }
    return res;
}
#[no_mangle]
#[c2rust::src_loc = "298:1"]
pub unsafe extern "C" fn strconcat3(
    mut s1: *const libc::c_char,
    mut s2: *const libc::c_char,
    mut s3: *const libc::c_char,
) -> *mut libc::c_char {
    let mut len: size_t = 0 as libc::c_int as size_t;
    let mut res: *mut libc::c_char = 0 as *mut libc::c_char;
    if !s1.is_null() {
        len = (len as libc::c_ulong).wrapping_add(strlen(s1)) as size_t as size_t;
    }
    if !s2.is_null() {
        len = (len as libc::c_ulong).wrapping_add(strlen(s2)) as size_t as size_t;
    }
    if !s3.is_null() {
        len = (len as libc::c_ulong).wrapping_add(strlen(s3)) as size_t as size_t;
    }
    res = xmalloc(len.wrapping_add(1 as libc::c_int as libc::c_ulong)) as *mut libc::c_char;
    *res = 0 as libc::c_int as libc::c_char;
    if !s1.is_null() {
        strcat(res, s1);
    }
    if !s2.is_null() {
        strcat(res, s2);
    }
    if !s3.is_null() {
        strcat(res, s3);
    }
    return res;
}
#[no_mangle]
#[c2rust::src_loc = "325:1"]
pub unsafe extern "C" fn xasprintf(
    mut format: *const libc::c_char,
    mut args: ...
) -> *mut libc::c_char {
    let mut buffer: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut args_0: ::core::ffi::VaListImpl;
    args_0 = args.clone();
    if vasprintf(&mut buffer, format, args_0.as_va_list()) == -(1 as libc::c_int) {
        die_oom();
    }
    return buffer;
}
#[no_mangle]
#[c2rust::src_loc = "340:1"]
pub unsafe extern "C" fn fdwalk(
    mut proc_fd: libc::c_int,
    mut cb: Option<unsafe extern "C" fn(*mut libc::c_void, libc::c_int) -> libc::c_int>,
    mut data: *mut libc::c_void,
) -> libc::c_int {
    let mut open_max: libc::c_int = 0;
    let mut fd: libc::c_int = 0;
    let mut dfd: libc::c_int = 0;
    let mut res: libc::c_int = 0 as libc::c_int;
    let mut d: *mut DIR = 0 as *mut DIR;
    dfd = openat(
        proc_fd,
        b"self/fd\0" as *const u8 as *const libc::c_char,
        0o200000 as libc::c_int
            | 0 as libc::c_int
            | 0o4000 as libc::c_int
            | 0o2000000 as libc::c_int
            | 0o400 as libc::c_int,
    );
    if dfd == -(1 as libc::c_int) {
        return res;
    }
    d = fdopendir(dfd);
    if !d.is_null() {
        let mut de: *mut dirent = 0 as *mut dirent;
        loop {
            de = readdir(d);
            if de.is_null() {
                break;
            }
            let mut l: libc::c_long = 0;
            let mut e: *mut libc::c_char = 0 as *mut libc::c_char;
            if (*de).d_name[0 as libc::c_int as usize] as libc::c_int == '.' as i32 {
                continue;
            }
            *__errno_location() = 0 as libc::c_int;
            l = strtol(((*de).d_name).as_mut_ptr(), &mut e, 10 as libc::c_int);
            if *__errno_location() != 0 as libc::c_int || e.is_null() || *e as libc::c_int != 0 {
                continue;
            }
            fd = l as libc::c_int;
            if fd as libc::c_long != l {
                continue;
            }
            if fd == dirfd(d) {
                continue;
            }
            res = cb.expect("non-null function pointer")(data, fd);
            if res != 0 as libc::c_int {
                break;
            }
        }
        closedir(d);
        return res;
    }
    open_max = sysconf(_SC_OPEN_MAX as libc::c_int) as libc::c_int;
    fd = 0 as libc::c_int;
    while fd < open_max {
        res = cb.expect("non-null function pointer")(data, fd);
        if res != 0 as libc::c_int {
            break;
        }
        fd += 1;
        fd;
    }
    return res;
}
#[no_mangle]
#[c2rust::src_loc = "397:1"]
pub unsafe extern "C" fn write_to_fd(
    mut fd: libc::c_int,
    mut content: *const libc::c_char,
    mut len: ssize_t,
) -> libc::c_int {
    let mut res: ssize_t = 0;
    while len > 0 as libc::c_int as libc::c_long {
        res = write(fd, content as *const libc::c_void, len as size_t);
        if res < 0 as libc::c_int as libc::c_long && *__errno_location() == 4 as libc::c_int {
            continue;
        }
        if res <= 0 as libc::c_int as libc::c_long {
            if res == 0 as libc::c_int as libc::c_long {
                *__errno_location() = 28 as libc::c_int;
            }
            return -(1 as libc::c_int);
        }
        len -= res;
        content = content.offset(res as isize);
    }
    return 0 as libc::c_int;
}
#[no_mangle]
#[c2rust::src_loc = "423:1"]
pub unsafe extern "C" fn write_file_at(
    mut dirfd_0: libc::c_int,
    mut path: *const libc::c_char,
    mut content: *const libc::c_char,
) -> libc::c_int {
    let mut fd: libc::c_int = 0;
    let mut res: bool_0 = 0;
    let mut errsv: libc::c_int = 0;
    fd = openat(
        dirfd_0,
        path,
        0o2 as libc::c_int | 0o2000000 as libc::c_int,
        0 as libc::c_int,
    );
    if fd == -(1 as libc::c_int) {
        return -(1 as libc::c_int);
    }
    res = 0 as libc::c_int;
    if !content.is_null() {
        res = write_to_fd(fd, content, strlen(content) as ssize_t);
    }
    errsv = *__errno_location();
    close(fd);
    *__errno_location() = errsv;
    return res;
}
#[no_mangle]
#[c2rust::src_loc = "448:1"]
pub unsafe extern "C" fn create_file(
    mut path: *const libc::c_char,
    mut mode: mode_t,
    mut content: *const libc::c_char,
) -> libc::c_int {
    let mut fd: libc::c_int = 0;
    let mut res: libc::c_int = 0;
    let mut errsv: libc::c_int = 0;
    fd = creat(path, mode);
    if fd == -(1 as libc::c_int) {
        return -(1 as libc::c_int);
    }
    res = 0 as libc::c_int;
    if !content.is_null() {
        res = write_to_fd(fd, content, strlen(content) as ssize_t);
    }
    errsv = *__errno_location();
    close(fd);
    *__errno_location() = errsv;
    return res;
}
#[no_mangle]
#[c2rust::src_loc = "472:1"]
pub unsafe extern "C" fn ensure_file(
    mut path: *const libc::c_char,
    mut mode: mode_t,
) -> libc::c_int {
    let mut buf: stat = stat {
        st_dev: 0,
        st_ino: 0,
        st_nlink: 0,
        st_mode: 0,
        st_uid: 0,
        st_gid: 0,
        __pad0: 0,
        st_rdev: 0,
        st_size: 0,
        st_blksize: 0,
        st_blocks: 0,
        st_atim: timespec {
            tv_sec: 0,
            tv_nsec: 0,
        },
        st_mtim: timespec {
            tv_sec: 0,
            tv_nsec: 0,
        },
        st_ctim: timespec {
            tv_sec: 0,
            tv_nsec: 0,
        },
        __glibc_reserved: [0; 3],
    };
    if stat(path, &mut buf) == 0 as libc::c_int
        && !(buf.st_mode & 0o170000 as libc::c_int as libc::c_uint
            == 0o40000 as libc::c_int as libc::c_uint)
        && !(buf.st_mode & 0o170000 as libc::c_int as libc::c_uint
            == 0o120000 as libc::c_int as libc::c_uint)
    {
        return 0 as libc::c_int;
    }
    if create_file(path, mode, 0 as *const libc::c_char) != 0 as libc::c_int
        && *__errno_location() != 17 as libc::c_int
    {
        return -(1 as libc::c_int);
    }
    return 0 as libc::c_int;
}
#[no_mangle]
#[c2rust::src_loc = "499:1"]
pub unsafe extern "C" fn copy_file_data(mut sfd: libc::c_int, mut dfd: libc::c_int) -> libc::c_int {
    let mut buffer: [libc::c_char; 8192] = [0; 8192];
    let mut bytes_read: ssize_t = 0;
    loop {
        bytes_read = read(
            sfd,
            buffer.as_mut_ptr() as *mut libc::c_void,
            8192 as libc::c_int as size_t,
        );
        if bytes_read == -(1 as libc::c_int) as libc::c_long {
            if *__errno_location() == 4 as libc::c_int {
                continue;
            }
            return -(1 as libc::c_int);
        } else {
            if bytes_read == 0 as libc::c_int as libc::c_long {
                break;
            }
            if write_to_fd(dfd, buffer.as_mut_ptr(), bytes_read) != 0 as libc::c_int {
                return -(1 as libc::c_int);
            }
        }
    }
    return 0 as libc::c_int;
}
#[no_mangle]
#[c2rust::src_loc = "528:1"]
pub unsafe extern "C" fn copy_file(
    mut src_path: *const libc::c_char,
    mut dst_path: *const libc::c_char,
    mut mode: mode_t,
) -> libc::c_int {
    let mut sfd: libc::c_int = 0;
    let mut dfd: libc::c_int = 0;
    let mut res: libc::c_int = 0;
    let mut errsv: libc::c_int = 0;
    sfd = open(src_path, 0o2000000 as libc::c_int | 0 as libc::c_int);
    if sfd == -(1 as libc::c_int) {
        return -(1 as libc::c_int);
    }
    dfd = creat(dst_path, mode);
    if dfd == -(1 as libc::c_int) {
        errsv = *__errno_location();
        close(sfd);
        *__errno_location() = errsv;
        return -(1 as libc::c_int);
    }
    res = copy_file_data(sfd, dfd);
    errsv = *__errno_location();
    close(sfd);
    close(dfd);
    *__errno_location() = errsv;
    return res;
}
#[no_mangle]
#[c2rust::src_loc = "563:1"]
pub unsafe extern "C" fn load_file_data(
    mut fd: libc::c_int,
    mut size: *mut size_t,
) -> *mut libc::c_char {
    let mut data: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut data_read: ssize_t = 0;
    let mut data_len: ssize_t = 0;
    let mut res: ssize_t = 0;
    data_read = 0 as libc::c_int as ssize_t;
    data_len = 4080 as libc::c_int as ssize_t;
    data = xmalloc(data_len as size_t) as *mut libc::c_char;
    loop {
        if data_len == data_read + 1 as libc::c_int as libc::c_long {
            data_len *= 2 as libc::c_int as libc::c_long;
            data = xrealloc(data as *mut libc::c_void, data_len as size_t) as *mut libc::c_char;
        }
        loop {
            res = read(
                fd,
                data.offset(data_read as isize) as *mut libc::c_void,
                (data_len - data_read - 1 as libc::c_int as libc::c_long) as size_t,
            );
            if !(res < 0 as libc::c_int as libc::c_long && *__errno_location() == 4 as libc::c_int)
            {
                break;
            }
        }
        if res < 0 as libc::c_int as libc::c_long {
            return 0 as *mut libc::c_char;
        }
        data_read += res;
        if !(res > 0 as libc::c_int as libc::c_long) {
            break;
        }
    }
    *data.offset(data_read as isize) = 0 as libc::c_int as libc::c_char;
    if !size.is_null() {
        *size = data_read as size_t;
    }
    return (if 0 as libc::c_int != 0 {
        data as *mut libc::c_void
    } else {
        steal_pointer(&mut data as *mut *mut libc::c_char as *mut libc::c_void)
    }) as *mut libc::c_char;
}
#[no_mangle]
#[c2rust::src_loc = "605:1"]
pub unsafe extern "C" fn load_file_at(
    mut dirfd_0: libc::c_int,
    mut path: *const libc::c_char,
) -> *mut libc::c_char {
    let mut fd: libc::c_int = 0;
    let mut data: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut errsv: libc::c_int = 0;
    fd = openat(dirfd_0, path, 0o2000000 as libc::c_int | 0 as libc::c_int);
    if fd == -(1 as libc::c_int) {
        return 0 as *mut libc::c_char;
    }
    data = load_file_data(fd, 0 as *mut size_t);
    errsv = *__errno_location();
    close(fd);
    *__errno_location() = errsv;
    return data;
}
#[no_mangle]
#[c2rust::src_loc = "627:1"]
pub unsafe extern "C" fn get_file_mode(mut pathname: *const libc::c_char) -> libc::c_int {
    let mut buf: stat = stat {
        st_dev: 0,
        st_ino: 0,
        st_nlink: 0,
        st_mode: 0,
        st_uid: 0,
        st_gid: 0,
        __pad0: 0,
        st_rdev: 0,
        st_size: 0,
        st_blksize: 0,
        st_blocks: 0,
        st_atim: timespec {
            tv_sec: 0,
            tv_nsec: 0,
        },
        st_mtim: timespec {
            tv_sec: 0,
            tv_nsec: 0,
        },
        st_ctim: timespec {
            tv_sec: 0,
            tv_nsec: 0,
        },
        __glibc_reserved: [0; 3],
    };
    if stat(pathname, &mut buf) != 0 as libc::c_int {
        return -(1 as libc::c_int);
    }
    return (buf.st_mode & 0o170000 as libc::c_int as libc::c_uint) as libc::c_int;
}
#[no_mangle]
#[c2rust::src_loc = "638:1"]
pub unsafe extern "C" fn ensure_dir(
    mut path: *const libc::c_char,
    mut mode: mode_t,
) -> libc::c_int {
    let mut buf: stat = stat {
        st_dev: 0,
        st_ino: 0,
        st_nlink: 0,
        st_mode: 0,
        st_uid: 0,
        st_gid: 0,
        __pad0: 0,
        st_rdev: 0,
        st_size: 0,
        st_blksize: 0,
        st_blocks: 0,
        st_atim: timespec {
            tv_sec: 0,
            tv_nsec: 0,
        },
        st_mtim: timespec {
            tv_sec: 0,
            tv_nsec: 0,
        },
        st_ctim: timespec {
            tv_sec: 0,
            tv_nsec: 0,
        },
        __glibc_reserved: [0; 3],
    };
    if stat(path, &mut buf) == 0 as libc::c_int {
        if !(buf.st_mode & 0o170000 as libc::c_int as libc::c_uint
            == 0o40000 as libc::c_int as libc::c_uint)
        {
            *__errno_location() = 20 as libc::c_int;
            return -(1 as libc::c_int);
        }
        return 0 as libc::c_int;
    }
    if mkdir(path, mode) == -(1 as libc::c_int) && *__errno_location() != 17 as libc::c_int {
        return -(1 as libc::c_int);
    }
    return 0 as libc::c_int;
}
#[no_mangle]
#[c2rust::src_loc = "667:1"]
pub unsafe extern "C" fn mkdir_with_parents(
    mut pathname: *const libc::c_char,
    mut mode: mode_t,
    mut create_last: bool_0,
) -> libc::c_int {
    let mut fn_0: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut p: *mut libc::c_char = 0 as *mut libc::c_char;
    if pathname.is_null() || *pathname as libc::c_int == '\0' as i32 {
        *__errno_location() = 22 as libc::c_int;
        return -(1 as libc::c_int);
    }
    fn_0 = xstrdup(pathname);
    p = fn_0;
    while *p as libc::c_int == '/' as i32 {
        p = p.offset(1);
        p;
    }
    loop {
        while *p as libc::c_int != 0 && *p as libc::c_int != '/' as i32 {
            p = p.offset(1);
            p;
        }
        if *p == 0 {
            p = 0 as *mut libc::c_char;
        } else {
            *p = '\0' as i32 as libc::c_char;
        }
        if create_last == 0 && p.is_null() {
            break;
        }
        if ensure_dir(fn_0, mode) != 0 as libc::c_int {
            return -(1 as libc::c_int);
        }
        if !p.is_null() {
            let fresh0 = p;
            p = p.offset(1);
            *fresh0 = '/' as i32 as libc::c_char;
            while *p as libc::c_int != 0 && *p as libc::c_int == '/' as i32 {
                p = p.offset(1);
                p;
            }
        }
        if p.is_null() {
            break;
        }
    }
    return 0 as libc::c_int;
}
#[no_mangle]
#[c2rust::src_loc = "718:1"]
pub unsafe extern "C" fn send_pid_on_socket(mut socket: libc::c_int) {
    let mut buf: [libc::c_char; 1] = [0 as libc::c_int as libc::c_char];
    let mut msg: msghdr = {
        let mut init = msghdr {
            msg_name: 0 as *mut libc::c_void,
            msg_namelen: 0,
            msg_iov: 0 as *mut iovec,
            msg_iovlen: 0,
            msg_control: 0 as *mut libc::c_void,
            msg_controllen: 0,
            msg_flags: 0,
        };
        init
    };
    let mut iov: iovec = {
        let mut init = iovec {
            iov_base: buf.as_mut_ptr() as *mut libc::c_void,
            iov_len: ::core::mem::size_of::<[libc::c_char; 1]>() as libc::c_ulong,
        };
        init
    };
    let control_len_snd: ssize_t = ((::core::mem::size_of::<ucred>() as libc::c_ulong)
        .wrapping_add(::core::mem::size_of::<size_t>() as libc::c_ulong)
        .wrapping_sub(1 as libc::c_int as libc::c_ulong)
        & !(::core::mem::size_of::<size_t>() as libc::c_ulong)
            .wrapping_sub(1 as libc::c_int as libc::c_ulong))
    .wrapping_add(
        (::core::mem::size_of::<cmsghdr>() as libc::c_ulong)
            .wrapping_add(::core::mem::size_of::<size_t>() as libc::c_ulong)
            .wrapping_sub(1 as libc::c_int as libc::c_ulong)
            & !(::core::mem::size_of::<size_t>() as libc::c_ulong)
                .wrapping_sub(1 as libc::c_int as libc::c_ulong),
    ) as ssize_t;
    let vla = control_len_snd as usize;
    let mut control_buf_snd: Vec<libc::c_char> = ::std::vec::from_elem(0, vla);
    let mut cmsg: *mut cmsghdr = 0 as *mut cmsghdr;
    let mut cred: *mut ucred = 0 as *mut ucred;
    msg.msg_iov = &mut iov;
    msg.msg_iovlen = 1 as libc::c_int as size_t;
    msg.msg_control = control_buf_snd.as_mut_ptr() as *mut libc::c_void;
    msg.msg_controllen = control_len_snd as size_t;
    cmsg = if msg.msg_controllen >= ::core::mem::size_of::<cmsghdr>() as libc::c_ulong {
        msg.msg_control as *mut cmsghdr
    } else {
        0 as *mut cmsghdr
    };
    (*cmsg).cmsg_level = 1 as libc::c_int;
    (*cmsg).cmsg_type = SCM_CREDENTIALS as libc::c_int;
    (*cmsg).cmsg_len = ((::core::mem::size_of::<cmsghdr>() as libc::c_ulong)
        .wrapping_add(::core::mem::size_of::<size_t>() as libc::c_ulong)
        .wrapping_sub(1 as libc::c_int as libc::c_ulong)
        & !(::core::mem::size_of::<size_t>() as libc::c_ulong)
            .wrapping_sub(1 as libc::c_int as libc::c_ulong))
    .wrapping_add(::core::mem::size_of::<ucred>() as libc::c_ulong);
    cred = ((*cmsg).__cmsg_data).as_mut_ptr() as *mut ucred;
    (*cred).pid = getpid();
    (*cred).uid = geteuid();
    (*cred).gid = getegid();
    if sendmsg(socket, &mut msg, 0 as libc::c_int) < 0 as libc::c_int as libc::c_long {
        die_with_error(b"Can't send pid\0" as *const u8 as *const libc::c_char);
    }
}
#[no_mangle]
#[c2rust::src_loc = "748:1"]
pub unsafe extern "C" fn create_pid_socketpair(mut sockets: *mut libc::c_int) {
    let mut enable: libc::c_int = 1 as libc::c_int;
    if socketpair(
        1 as libc::c_int,
        SOCK_SEQPACKET as libc::c_int | SOCK_CLOEXEC as libc::c_int,
        0 as libc::c_int,
        sockets,
    ) != 0 as libc::c_int
    {
        die_with_error(
            b"Can't create intermediate pids socket\0" as *const u8 as *const libc::c_char,
        );
    }
    if setsockopt(
        *sockets.offset(0 as libc::c_int as isize),
        1 as libc::c_int,
        16 as libc::c_int,
        &mut enable as *mut libc::c_int as *const libc::c_void,
        ::core::mem::size_of::<libc::c_int>() as libc::c_ulong as socklen_t,
    ) < 0 as libc::c_int
    {
        die_with_error(b"Can't set SO_PASSCRED\0" as *const u8 as *const libc::c_char);
    }
}
#[no_mangle]
#[c2rust::src_loc = "760:1"]
pub unsafe extern "C" fn read_pid_from_socket(mut socket: libc::c_int) -> libc::c_int {
    let mut recv_buf: [libc::c_char; 1] = [0 as libc::c_int as libc::c_char];
    let mut msg: msghdr = {
        let mut init = msghdr {
            msg_name: 0 as *mut libc::c_void,
            msg_namelen: 0,
            msg_iov: 0 as *mut iovec,
            msg_iovlen: 0,
            msg_control: 0 as *mut libc::c_void,
            msg_controllen: 0,
            msg_flags: 0,
        };
        init
    };
    let mut iov: iovec = {
        let mut init = iovec {
            iov_base: recv_buf.as_mut_ptr() as *mut libc::c_void,
            iov_len: ::core::mem::size_of::<[libc::c_char; 1]>() as libc::c_ulong,
        };
        init
    };
    let control_len_rcv: ssize_t = ((::core::mem::size_of::<ucred>() as libc::c_ulong)
        .wrapping_add(::core::mem::size_of::<size_t>() as libc::c_ulong)
        .wrapping_sub(1 as libc::c_int as libc::c_ulong)
        & !(::core::mem::size_of::<size_t>() as libc::c_ulong)
            .wrapping_sub(1 as libc::c_int as libc::c_ulong))
    .wrapping_add(
        (::core::mem::size_of::<cmsghdr>() as libc::c_ulong)
            .wrapping_add(::core::mem::size_of::<size_t>() as libc::c_ulong)
            .wrapping_sub(1 as libc::c_int as libc::c_ulong)
            & !(::core::mem::size_of::<size_t>() as libc::c_ulong)
                .wrapping_sub(1 as libc::c_int as libc::c_ulong),
    ) as ssize_t;
    let vla = control_len_rcv as usize;
    let mut control_buf_rcv: Vec<libc::c_char> = ::std::vec::from_elem(0, vla);
    let mut cmsg: *mut cmsghdr = 0 as *mut cmsghdr;
    msg.msg_iov = &mut iov;
    msg.msg_iovlen = 1 as libc::c_int as size_t;
    msg.msg_control = control_buf_rcv.as_mut_ptr() as *mut libc::c_void;
    msg.msg_controllen = control_len_rcv as size_t;
    if recvmsg(socket, &mut msg, 0 as libc::c_int) < 0 as libc::c_int as libc::c_long {
        die_with_error(b"Can't read pid from socket\0" as *const u8 as *const libc::c_char);
    }
    if msg.msg_controllen <= 0 as libc::c_int as libc::c_ulong {
        die(b"Unexpected short read from pid socket\0" as *const u8 as *const libc::c_char);
    }
    cmsg = if msg.msg_controllen >= ::core::mem::size_of::<cmsghdr>() as libc::c_ulong {
        msg.msg_control as *mut cmsghdr
    } else {
        0 as *mut cmsghdr
    };
    while !cmsg.is_null() {
        let payload_len: libc::c_uint = ((*cmsg).cmsg_len).wrapping_sub(
            ((::core::mem::size_of::<cmsghdr>() as libc::c_ulong)
                .wrapping_add(::core::mem::size_of::<size_t>() as libc::c_ulong)
                .wrapping_sub(1 as libc::c_int as libc::c_ulong)
                & !(::core::mem::size_of::<size_t>() as libc::c_ulong)
                    .wrapping_sub(1 as libc::c_int as libc::c_ulong))
            .wrapping_add(0 as libc::c_int as libc::c_ulong),
        ) as libc::c_uint;
        if (*cmsg).cmsg_level == 1 as libc::c_int
            && (*cmsg).cmsg_type == SCM_CREDENTIALS as libc::c_int
            && payload_len as libc::c_ulong == ::core::mem::size_of::<ucred>() as libc::c_ulong
        {
            let mut cred: *mut ucred = ((*cmsg).__cmsg_data).as_mut_ptr() as *mut ucred;
            return (*cred).pid;
        }
        cmsg = __cmsg_nxthdr(&mut msg, cmsg);
    }
    die(b"No pid returned on socket\0" as *const u8 as *const libc::c_char);
}
#[no_mangle]
#[c2rust::src_loc = "797:1"]
pub unsafe extern "C" fn readlink_malloc(mut pathname: *const libc::c_char) -> *mut libc::c_char {
    let mut size: size_t = 50 as libc::c_int as size_t;
    let mut n: ssize_t = 0;
    let mut value: *mut libc::c_char = 0 as *mut libc::c_char;
    loop {
        size = (size as libc::c_ulong).wrapping_mul(2 as libc::c_int as libc::c_ulong) as size_t
            as size_t;
        value = xrealloc(value as *mut libc::c_void, size) as *mut libc::c_char;
        n = readlink(
            pathname,
            value,
            size.wrapping_sub(1 as libc::c_int as libc::c_ulong),
        );
        if n < 0 as libc::c_int as libc::c_long {
            return 0 as *mut libc::c_char;
        }
        if !(size.wrapping_sub(2 as libc::c_int as libc::c_ulong) < n as size_t) {
            break;
        }
    }
    *value.offset(n as isize) = 0 as libc::c_int as libc::c_char;
    return (if 0 as libc::c_int != 0 {
        value as *mut libc::c_void
    } else {
        steal_pointer(&mut value as *mut *mut libc::c_char as *mut libc::c_void)
    }) as *mut libc::c_char;
}
#[no_mangle]
#[c2rust::src_loc = "818:1"]
pub unsafe extern "C" fn get_oldroot_path(mut path: *const libc::c_char) -> *mut libc::c_char {
    while *path as libc::c_int == '/' as i32 {
        path = path.offset(1);
        path;
    }
    return strconcat(b"/oldroot/\0" as *const u8 as *const libc::c_char, path);
}
#[no_mangle]
#[c2rust::src_loc = "826:1"]
pub unsafe extern "C" fn raw_clone(
    mut flags: libc::c_ulong,
    mut child_stack: *mut libc::c_void,
) -> libc::c_int {
    return syscall(56 as libc::c_int as libc::c_long, flags, child_stack) as libc::c_int;
}
#[no_mangle]
#[c2rust::src_loc = "839:1"]
pub unsafe extern "C" fn pivot_root(
    mut new_root: *const libc::c_char,
    mut put_old: *const libc::c_char,
) -> libc::c_int {
    return syscall(155 as libc::c_int as libc::c_long, new_root, put_old) as libc::c_int;
}
#[no_mangle]
#[c2rust::src_loc = "850:1"]
pub unsafe extern "C" fn label_mount(
    mut opt: *const libc::c_char,
    mut mount_label: *const libc::c_char,
) -> *mut libc::c_char {
    if !opt.is_null() {
        return xstrdup(opt);
    }
    return 0 as *mut libc::c_char;
}
#[no_mangle]
#[c2rust::src_loc = "867:1"]
pub unsafe extern "C" fn label_create_file(mut file_label: *const libc::c_char) -> libc::c_int {
    return 0 as libc::c_int;
}
#[no_mangle]
#[c2rust::src_loc = "877:1"]
pub unsafe extern "C" fn label_exec(mut exec_label: *const libc::c_char) -> libc::c_int {
    return 0 as libc::c_int;
}
