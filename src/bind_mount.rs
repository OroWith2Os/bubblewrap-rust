use ::libc;
#[c2rust::header_src = "internal:0"]
pub mod internal {
    #[c2rust::src_loc = "0:0"]
    pub type __builtin_va_list = [__va_list_tag; 1];
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "0:0"]
    pub struct __va_list_tag {
        pub gp_offset: libc::c_uint,
        pub fp_offset: libc::c_uint,
        pub overflow_arg_area: *mut libc::c_void,
        pub reg_save_area: *mut libc::c_void,
    }
}
#[c2rust::header_src = "/usr/include/bits/types.h:22"]
pub mod types_h {
    #[c2rust::src_loc = "152:1"]
    pub type __off_t = libc::c_long;
    #[c2rust::src_loc = "153:1"]
    pub type __off64_t = libc::c_long;
}
#[c2rust::header_src = "/usr/lib/clang/15.0.7/include/stddef.h:22"]
pub mod stddef_h {
    #[c2rust::src_loc = "46:1"]
    pub type size_t = libc::c_ulong;
}
#[c2rust::header_src = "/usr/include/sys/mount.h:22"]
pub mod mount_h {
    #[c2rust::src_loc = "43:1"]
    pub type C2RustUnnamed = libc::c_int;
    #[c2rust::src_loc = "124:3"]
    pub const MS_NOUSER: C2RustUnnamed = -2147483648;
    #[c2rust::src_loc = "121:3"]
    pub const MS_ACTIVE: C2RustUnnamed = 1073741824;
    #[c2rust::src_loc = "118:3"]
    pub const MS_LAZYTIME: C2RustUnnamed = 33554432;
    #[c2rust::src_loc = "115:3"]
    pub const MS_STRICTATIME: C2RustUnnamed = 16777216;
    #[c2rust::src_loc = "112:3"]
    pub const MS_I_VERSION: C2RustUnnamed = 8388608;
    #[c2rust::src_loc = "109:3"]
    pub const MS_KERNMOUNT: C2RustUnnamed = 4194304;
    #[c2rust::src_loc = "106:3"]
    pub const MS_RELATIME: C2RustUnnamed = 2097152;
    #[c2rust::src_loc = "103:3"]
    pub const MS_SHARED: C2RustUnnamed = 1048576;
    #[c2rust::src_loc = "100:3"]
    pub const MS_SLAVE: C2RustUnnamed = 524288;
    #[c2rust::src_loc = "97:3"]
    pub const MS_PRIVATE: C2RustUnnamed = 262144;
    #[c2rust::src_loc = "94:3"]
    pub const MS_UNBINDABLE: C2RustUnnamed = 131072;
    #[c2rust::src_loc = "91:3"]
    pub const MS_POSIXACL: C2RustUnnamed = 65536;
    #[c2rust::src_loc = "88:3"]
    pub const MS_SILENT: C2RustUnnamed = 32768;
    #[c2rust::src_loc = "85:3"]
    pub const MS_REC: C2RustUnnamed = 16384;
    #[c2rust::src_loc = "82:3"]
    pub const MS_MOVE: C2RustUnnamed = 8192;
    #[c2rust::src_loc = "79:3"]
    pub const MS_BIND: C2RustUnnamed = 4096;
    #[c2rust::src_loc = "76:3"]
    pub const MS_NODIRATIME: C2RustUnnamed = 2048;
    #[c2rust::src_loc = "73:3"]
    pub const MS_NOATIME: C2RustUnnamed = 1024;
    #[c2rust::src_loc = "70:3"]
    pub const MS_NOSYMFOLLOW: C2RustUnnamed = 256;
    #[c2rust::src_loc = "67:3"]
    pub const MS_DIRSYNC: C2RustUnnamed = 128;
    #[c2rust::src_loc = "64:3"]
    pub const MS_MANDLOCK: C2RustUnnamed = 64;
    #[c2rust::src_loc = "61:3"]
    pub const MS_REMOUNT: C2RustUnnamed = 32;
    #[c2rust::src_loc = "58:3"]
    pub const MS_SYNCHRONOUS: C2RustUnnamed = 16;
    #[c2rust::src_loc = "55:3"]
    pub const MS_NOEXEC: C2RustUnnamed = 8;
    #[c2rust::src_loc = "52:3"]
    pub const MS_NODEV: C2RustUnnamed = 4;
    #[c2rust::src_loc = "49:3"]
    pub const MS_NOSUID: C2RustUnnamed = 2;
    #[c2rust::src_loc = "46:3"]
    pub const MS_RDONLY: C2RustUnnamed = 1;
    extern "C" {
        #[c2rust::src_loc = "272:1"]
        pub fn mount(
            __special_file: *const libc::c_char,
            __dir: *const libc::c_char,
            __fstype: *const libc::c_char,
            __rwflag: libc::c_ulong,
            __data: *const libc::c_void,
        ) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/lib/clang/15.0.7/include/stdarg.h:24"]
pub mod stdarg_h {
    #[c2rust::src_loc = "14:1"]
    pub type va_list = __builtin_va_list;
    use super::internal::__builtin_va_list;
}
#[c2rust::header_src = "/usr/include/bits/types/struct_FILE.h:24"]
pub mod struct_FILE_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "49:8"]
    pub struct _IO_FILE {
        pub _flags: libc::c_int,
        pub _IO_read_ptr: *mut libc::c_char,
        pub _IO_read_end: *mut libc::c_char,
        pub _IO_read_base: *mut libc::c_char,
        pub _IO_write_base: *mut libc::c_char,
        pub _IO_write_ptr: *mut libc::c_char,
        pub _IO_write_end: *mut libc::c_char,
        pub _IO_buf_base: *mut libc::c_char,
        pub _IO_buf_end: *mut libc::c_char,
        pub _IO_save_base: *mut libc::c_char,
        pub _IO_backup_base: *mut libc::c_char,
        pub _IO_save_end: *mut libc::c_char,
        pub _markers: *mut _IO_marker,
        pub _chain: *mut _IO_FILE,
        pub _fileno: libc::c_int,
        pub _flags2: libc::c_int,
        pub _old_offset: __off_t,
        pub _cur_column: libc::c_ushort,
        pub _vtable_offset: libc::c_schar,
        pub _shortbuf: [libc::c_char; 1],
        pub _lock: *mut libc::c_void,
        pub _offset: __off64_t,
        pub _codecvt: *mut _IO_codecvt,
        pub _wide_data: *mut _IO_wide_data,
        pub _freeres_list: *mut _IO_FILE,
        pub _freeres_buf: *mut libc::c_void,
        pub __pad5: size_t,
        pub _mode: libc::c_int,
        pub _unused2: [libc::c_char; 20],
    }
    #[c2rust::src_loc = "43:1"]
    pub type _IO_lock_t = ();
    use super::stddef_h::size_t;
    use super::types_h::{__off64_t, __off_t};
    extern "C" {
        #[c2rust::src_loc = "38:8"]
        pub type _IO_wide_data;
        #[c2rust::src_loc = "37:8"]
        pub type _IO_codecvt;
        #[c2rust::src_loc = "36:8"]
        pub type _IO_marker;
    }
}
#[c2rust::header_src = "/usr/include/bits/types/FILE.h:24"]
pub mod FILE_h {
    #[c2rust::src_loc = "7:1"]
    pub type FILE = _IO_FILE;
    use super::struct_FILE_h::_IO_FILE;
}
#[c2rust::header_src = "/var/home/oro/bubblewrap/utils.h:24"]
pub mod utils_h {
    #[c2rust::src_loc = "46:1"]
    pub type bool_0 = libc::c_int;
    #[inline]
    #[c2rust::src_loc = "169:1"]
    pub unsafe extern "C" fn steal_pointer(mut pp: *mut libc::c_void) -> *mut libc::c_void {
        let mut ptr: *mut *mut libc::c_void = pp as *mut *mut libc::c_void;
        let mut ref_0: *mut libc::c_void = 0 as *mut libc::c_void;
        ref_0 = *ptr;
        *ptr = 0 as *mut libc::c_void;
        return ref_0;
    }
    use super::stddef_h::size_t;
    extern "C" {
        #[c2rust::src_loc = "124:1"]
        pub fn readlink_malloc(pathname: *const libc::c_char) -> *mut libc::c_char;
        #[c2rust::src_loc = "123:1"]
        pub fn get_oldroot_path(path: *const libc::c_char) -> *mut libc::c_char;
        #[c2rust::src_loc = "57:1"]
        pub fn die_with_error(format: *const libc::c_char, _: ...) -> !;
        #[c2rust::src_loc = "59:1"]
        pub fn die(format: *const libc::c_char, _: ...) -> !;
        #[c2rust::src_loc = "67:1"]
        pub fn xcalloc(size: size_t) -> *mut libc::c_void;
        #[c2rust::src_loc = "70:1"]
        pub fn xstrdup(str: *const libc::c_char) -> *mut libc::c_char;
        #[c2rust::src_loc = "82:1"]
        pub fn xasprintf(format: *const libc::c_char, _: ...) -> *mut libc::c_char;
        #[c2rust::src_loc = "86:1"]
        pub fn has_path_prefix(str: *const libc::c_char, prefix: *const libc::c_char) -> bool_0;
        #[c2rust::src_loc = "88:1"]
        pub fn path_equal(path1: *const libc::c_char, path2: *const libc::c_char) -> bool_0;
        #[c2rust::src_loc = "96:1"]
        pub fn load_file_at(dirfd: libc::c_int, path: *const libc::c_char) -> *mut libc::c_char;
    }
}
#[c2rust::header_src = "/var/home/oro/bubblewrap/bind-mount.h:25"]
pub mod bind_mount_h {
    #[c2rust::src_loc = "24:9"]
    pub type bind_option_t = libc::c_uint;
    #[c2rust::src_loc = "27:3"]
    pub const BIND_RECURSIVE: bind_option_t = 8;
    #[c2rust::src_loc = "26:3"]
    pub const BIND_DEVICES: bind_option_t = 4;
    #[c2rust::src_loc = "25:3"]
    pub const BIND_READONLY: bind_option_t = 1;
    #[c2rust::src_loc = "30:9"]
    pub type bind_mount_result = libc::c_uint;
    #[c2rust::src_loc = "39:3"]
    pub const BIND_MOUNT_ERROR_REMOUNT_SUBMOUNT: bind_mount_result = 7;
    #[c2rust::src_loc = "38:3"]
    pub const BIND_MOUNT_ERROR_REMOUNT_DEST: bind_mount_result = 6;
    #[c2rust::src_loc = "37:3"]
    pub const BIND_MOUNT_ERROR_FIND_DEST_MOUNT: bind_mount_result = 5;
    #[c2rust::src_loc = "36:3"]
    pub const BIND_MOUNT_ERROR_READLINK_DEST_PROC_FD: bind_mount_result = 4;
    #[c2rust::src_loc = "35:3"]
    pub const BIND_MOUNT_ERROR_REOPEN_DEST: bind_mount_result = 3;
    #[c2rust::src_loc = "34:3"]
    pub const BIND_MOUNT_ERROR_REALPATH_DEST: bind_mount_result = 2;
    #[c2rust::src_loc = "33:3"]
    pub const BIND_MOUNT_ERROR_MOUNT: bind_mount_result = 1;
    #[c2rust::src_loc = "32:3"]
    pub const BIND_MOUNT_SUCCESS: bind_mount_result = 0;
}
#[c2rust::header_src = "/usr/include/fcntl.h:22"]
pub mod fcntl_h {
    extern "C" {
        #[c2rust::src_loc = "212:1"]
        pub fn open(__file: *const libc::c_char, __oflag: libc::c_int, _: ...) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/errno.h:24"]
pub mod errno_h {
    extern "C" {
        #[c2rust::src_loc = "37:1"]
        pub fn __errno_location() -> *mut libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/stdio.h:24"]
pub mod stdio_h {
    use super::internal::__va_list_tag;
    use super::FILE_h::FILE;
    extern "C" {
        #[c2rust::src_loc = "145:14"]
        pub static mut stderr: *mut FILE;
        #[c2rust::src_loc = "350:12"]
        pub fn fprintf(_: *mut FILE, _: *const libc::c_char, _: ...) -> libc::c_int;
        #[c2rust::src_loc = "365:12"]
        pub fn vfprintf(
            _: *mut FILE,
            _: *const libc::c_char,
            _: ::core::ffi::VaList,
        ) -> libc::c_int;
        #[c2rust::src_loc = "423:12"]
        pub fn sscanf(_: *const libc::c_char, _: *const libc::c_char, _: ...) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/stdlib.h:24"]
pub mod stdlib_h {
    extern "C" {
        #[c2rust::src_loc = "637:13"]
        pub fn exit(_: libc::c_int) -> !;
        #[c2rust::src_loc = "821:1"]
        pub fn realpath(
            __name: *const libc::c_char,
            __resolved: *mut libc::c_char,
        ) -> *mut libc::c_char;
    }
}
#[c2rust::header_src = "/usr/include/assert.h:24"]
pub mod assert_h {
    extern "C" {
        #[c2rust::src_loc = "67:1"]
        pub fn __assert_fail(
            __assertion: *const libc::c_char,
            __file: *const libc::c_char,
            __line: libc::c_uint,
            __function: *const libc::c_char,
        ) -> !;
    }
}
#[c2rust::header_src = "/usr/include/string.h:24"]
pub mod string_h {
    extern "C" {
        #[c2rust::src_loc = "156:12"]
        pub fn strcmp(_: *const libc::c_char, _: *const libc::c_char) -> libc::c_int;
        #[c2rust::src_loc = "246:14"]
        pub fn strchr(_: *const libc::c_char, _: libc::c_int) -> *mut libc::c_char;
        #[c2rust::src_loc = "407:15"]
        pub fn strlen(_: *const libc::c_char) -> libc::c_ulong;
        #[c2rust::src_loc = "419:14"]
        pub fn strerror(_: libc::c_int) -> *mut libc::c_char;
    }
}
use self::assert_h::__assert_fail;
pub use self::bind_mount_h::{
    bind_mount_result, bind_option_t, BIND_DEVICES, BIND_MOUNT_ERROR_FIND_DEST_MOUNT,
    BIND_MOUNT_ERROR_MOUNT, BIND_MOUNT_ERROR_READLINK_DEST_PROC_FD, BIND_MOUNT_ERROR_REALPATH_DEST,
    BIND_MOUNT_ERROR_REMOUNT_DEST, BIND_MOUNT_ERROR_REMOUNT_SUBMOUNT, BIND_MOUNT_ERROR_REOPEN_DEST,
    BIND_MOUNT_SUCCESS, BIND_READONLY, BIND_RECURSIVE,
};
use self::errno_h::__errno_location;
use self::fcntl_h::open;
pub use self::internal::{__builtin_va_list, __va_list_tag};
pub use self::mount_h::{
    mount, C2RustUnnamed, MS_ACTIVE, MS_BIND, MS_DIRSYNC, MS_I_VERSION, MS_KERNMOUNT, MS_LAZYTIME,
    MS_MANDLOCK, MS_MOVE, MS_NOATIME, MS_NODEV, MS_NODIRATIME, MS_NOEXEC, MS_NOSUID,
    MS_NOSYMFOLLOW, MS_NOUSER, MS_POSIXACL, MS_PRIVATE, MS_RDONLY, MS_REC, MS_RELATIME, MS_REMOUNT,
    MS_SHARED, MS_SILENT, MS_SLAVE, MS_STRICTATIME, MS_SYNCHRONOUS, MS_UNBINDABLE,
};
pub use self::stdarg_h::va_list;
pub use self::stddef_h::size_t;
use self::stdio_h::{fprintf, sscanf, stderr, vfprintf};
use self::stdlib_h::{exit, realpath};
use self::string_h::{strchr, strcmp, strerror, strlen};
pub use self::struct_FILE_h::{_IO_codecvt, _IO_lock_t, _IO_marker, _IO_wide_data, _IO_FILE};
pub use self::types_h::{__off64_t, __off_t};
pub use self::utils_h::{
    bool_0, die, die_with_error, get_oldroot_path, has_path_prefix, load_file_at, path_equal,
    readlink_malloc, steal_pointer, xasprintf, xcalloc, xstrdup,
};
pub use self::FILE_h::FILE;
#[derive(Copy, Clone)]
#[repr(C)]
#[c2rust::src_loc = "129:8"]
pub struct MountInfo {
    pub mountpoint: *mut libc::c_char,
    pub options: libc::c_ulong,
}
#[c2rust::src_loc = "134:1"]
pub type MountTab = *mut MountInfo;
#[derive(Copy, Clone)]
#[repr(C)]
#[c2rust::src_loc = "158:8"]
pub struct MountInfoLine {
    pub mountpoint: *const libc::c_char,
    pub options: *const libc::c_char,
    pub covered: bool_0,
    pub id: libc::c_int,
    pub parent_id: libc::c_int,
    pub first_child: *mut MountInfoLine,
    pub next_sibling: *mut MountInfoLine,
}
#[derive(Copy, Clone)]
#[repr(C)]
#[c2rust::src_loc = "88:16"]
pub struct C2RustUnnamed_0 {
    pub flag: libc::c_int,
    pub name: *const libc::c_char,
}
#[c2rust::src_loc = "27:1"]
unsafe extern "C" fn skip_token(
    mut line: *mut libc::c_char,
    mut eat_whitespace: bool_0,
) -> *mut libc::c_char {
    while *line as libc::c_int != ' ' as i32 && *line as libc::c_int != '\n' as i32 {
        line = line.offset(1);
        line;
    }
    if eat_whitespace != 0 && *line as libc::c_int == ' ' as i32 {
        line = line.offset(1);
        line;
    }
    return line;
}
#[c2rust::src_loc = "39:1"]
unsafe extern "C" fn unescape_inline(mut escaped: *mut libc::c_char) -> *mut libc::c_char {
    let mut unescaped: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut res: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut end: *const libc::c_char = 0 as *const libc::c_char;
    res = escaped;
    end = escaped.offset(strlen(escaped) as isize);
    unescaped = escaped;
    while escaped < end as *mut libc::c_char {
        if *escaped as libc::c_int == '\\' as i32 {
            let fresh0 = unescaped;
            unescaped = unescaped.offset(1);
            *fresh0 = ((*escaped.offset(1 as libc::c_int as isize) as libc::c_int - '0' as i32)
                << 6 as libc::c_int
                | (*escaped.offset(2 as libc::c_int as isize) as libc::c_int - '0' as i32)
                    << 3 as libc::c_int
                | (*escaped.offset(3 as libc::c_int as isize) as libc::c_int - '0' as i32)
                    << 0 as libc::c_int) as libc::c_char;
            escaped = escaped.offset(4 as libc::c_int as isize);
        } else {
            let fresh1 = escaped;
            escaped = escaped.offset(1);
            let fresh2 = unescaped;
            unescaped = unescaped.offset(1);
            *fresh2 = *fresh1;
        }
    }
    *unescaped = 0 as libc::c_int as libc::c_char;
    return res;
}
#[c2rust::src_loc = "68:1"]
unsafe extern "C" fn match_token(
    mut token: *const libc::c_char,
    mut token_end: *const libc::c_char,
    mut str: *const libc::c_char,
) -> bool_0 {
    while token != token_end && *token as libc::c_int == *str as libc::c_int {
        token = token.offset(1);
        token;
        str = str.offset(1);
        str;
    }
    if token == token_end {
        return (*str as libc::c_int == 0 as libc::c_int) as libc::c_int;
    }
    return 0 as libc::c_int;
}
#[c2rust::src_loc = "82:1"]
unsafe extern "C" fn decode_mountoptions(mut options: *const libc::c_char) -> libc::c_ulong {
    let mut token: *const libc::c_char = 0 as *const libc::c_char;
    let mut end_token: *const libc::c_char = 0 as *const libc::c_char;
    let mut i: libc::c_int = 0;
    let mut flags: libc::c_ulong = 0 as libc::c_int as libc::c_ulong;
    static mut flags_data: [C2RustUnnamed_0; 9] = [
        {
            let mut init = C2RustUnnamed_0 {
                flag: 0 as libc::c_int,
                name: b"rw\0" as *const u8 as *const libc::c_char,
            };
            init
        },
        {
            let mut init = C2RustUnnamed_0 {
                flag: MS_RDONLY as libc::c_int,
                name: b"ro\0" as *const u8 as *const libc::c_char,
            };
            init
        },
        {
            let mut init = C2RustUnnamed_0 {
                flag: MS_NOSUID as libc::c_int,
                name: b"nosuid\0" as *const u8 as *const libc::c_char,
            };
            init
        },
        {
            let mut init = C2RustUnnamed_0 {
                flag: MS_NODEV as libc::c_int,
                name: b"nodev\0" as *const u8 as *const libc::c_char,
            };
            init
        },
        {
            let mut init = C2RustUnnamed_0 {
                flag: MS_NOEXEC as libc::c_int,
                name: b"noexec\0" as *const u8 as *const libc::c_char,
            };
            init
        },
        {
            let mut init = C2RustUnnamed_0 {
                flag: MS_NOATIME as libc::c_int,
                name: b"noatime\0" as *const u8 as *const libc::c_char,
            };
            init
        },
        {
            let mut init = C2RustUnnamed_0 {
                flag: MS_NODIRATIME as libc::c_int,
                name: b"nodiratime\0" as *const u8 as *const libc::c_char,
            };
            init
        },
        {
            let mut init = C2RustUnnamed_0 {
                flag: MS_RELATIME as libc::c_int,
                name: b"relatime\0" as *const u8 as *const libc::c_char,
            };
            init
        },
        {
            let mut init = C2RustUnnamed_0 {
                flag: 0 as libc::c_int,
                name: 0 as *const libc::c_char,
            };
            init
        },
    ];
    token = options;
    loop {
        end_token = strchr(token, ',' as i32);
        if end_token.is_null() {
            end_token = token.offset(strlen(token) as isize);
        }
        i = 0 as libc::c_int;
        while !(flags_data[i as usize].name).is_null() {
            if match_token(token, end_token, flags_data[i as usize].name) != 0 {
                flags |= flags_data[i as usize].flag as libc::c_ulong;
                break;
            } else {
                i += 1;
                i;
            }
        }
        if *end_token as libc::c_int != 0 as libc::c_int {
            token = end_token.offset(1 as libc::c_int as isize);
        } else {
            token = 0 as *const libc::c_char;
        }
        if token.is_null() {
            break;
        }
    }
    return flags;
}
#[c2rust::src_loc = "168:1"]
unsafe extern "C" fn count_lines(mut data: *const libc::c_char) -> libc::c_uint {
    let mut count: libc::c_uint = 0 as libc::c_int as libc::c_uint;
    let mut p: *const libc::c_char = data;
    while *p as libc::c_int != 0 as libc::c_int {
        if *p as libc::c_int == '\n' as i32 {
            count = count.wrapping_add(1);
            count;
        }
        p = p.offset(1);
        p;
    }
    if p > data && *p.offset(-(1 as libc::c_int as isize)) as libc::c_int != '\n' as i32 {
        count = count.wrapping_add(1);
        count;
    }
    return count;
}
#[c2rust::src_loc = "188:1"]
unsafe extern "C" fn count_mounts(mut line: *mut MountInfoLine) -> libc::c_int {
    let mut child: *mut MountInfoLine = 0 as *mut MountInfoLine;
    let mut res: libc::c_int = 0 as libc::c_int;
    if (*line).covered == 0 {
        res += 1 as libc::c_int;
    }
    child = (*line).first_child;
    while !child.is_null() {
        res += count_mounts(child);
        child = (*child).next_sibling;
    }
    return res;
}
#[c2rust::src_loc = "207:1"]
unsafe extern "C" fn collect_mounts(
    mut info: *mut MountInfo,
    mut line: *mut MountInfoLine,
) -> *mut MountInfo {
    let mut child: *mut MountInfoLine = 0 as *mut MountInfoLine;
    if (*line).covered == 0 {
        (*info).mountpoint = xstrdup((*line).mountpoint);
        (*info).options = decode_mountoptions((*line).options);
        info = info.offset(1);
        info;
    }
    child = (*line).first_child;
    while !child.is_null() {
        info = collect_mounts(info, child);
        child = (*child).next_sibling;
    }
    return info;
}
#[c2rust::src_loc = "229:1"]
unsafe extern "C" fn parse_mountinfo(
    mut proc_fd: libc::c_int,
    mut root_mount: *const libc::c_char,
) -> MountTab {
    let mut mountinfo: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut lines: *mut MountInfoLine = 0 as *mut MountInfoLine;
    let mut by_id: *mut *mut MountInfoLine = 0 as *mut *mut MountInfoLine;
    let mut mount_tab: MountTab = 0 as MountTab;
    let mut end_tab: *mut MountInfo = 0 as *mut MountInfo;
    let mut n_mounts: libc::c_int = 0;
    let mut line: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut i: libc::c_uint = 0;
    let mut max_id: libc::c_int = 0;
    let mut n_lines: libc::c_uint = 0;
    let mut root: libc::c_int = 0;
    mountinfo = load_file_at(
        proc_fd,
        b"self/mountinfo\0" as *const u8 as *const libc::c_char,
    );
    if mountinfo.is_null() {
        die_with_error(b"Can't open /proc/self/mountinfo\0" as *const u8 as *const libc::c_char);
    }
    n_lines = count_lines(mountinfo);
    lines = xcalloc(
        (n_lines as libc::c_ulong)
            .wrapping_mul(::core::mem::size_of::<MountInfoLine>() as libc::c_ulong),
    ) as *mut MountInfoLine;
    max_id = 0 as libc::c_int;
    line = mountinfo;
    i = 0 as libc::c_int as libc::c_uint;
    root = -(1 as libc::c_int);
    while *line as libc::c_int != 0 as libc::c_int {
        let mut rc: libc::c_int = 0;
        let mut consumed: libc::c_int = 0 as libc::c_int;
        let mut maj: libc::c_uint = 0;
        let mut min: libc::c_uint = 0;
        let mut end: *mut libc::c_char = 0 as *mut libc::c_char;
        let mut rest: *mut libc::c_char = 0 as *mut libc::c_char;
        let mut mountpoint: *mut libc::c_char = 0 as *mut libc::c_char;
        let mut mountpoint_end: *mut libc::c_char = 0 as *mut libc::c_char;
        let mut options: *mut libc::c_char = 0 as *mut libc::c_char;
        let mut options_end: *mut libc::c_char = 0 as *mut libc::c_char;
        let mut next_line: *mut libc::c_char = 0 as *mut libc::c_char;
        if i < n_lines {
        } else {
            __assert_fail(
                b"i < n_lines\0" as *const u8 as *const libc::c_char,
                b"../bind-mount.c\0" as *const u8 as *const libc::c_char,
                268 as libc::c_int as libc::c_uint,
                (*::core::mem::transmute::<&[u8; 44], &[libc::c_char; 44]>(
                    b"MountTab parse_mountinfo(int, const char *)\0",
                ))
                .as_ptr(),
            );
        }
        'c_5542: {
            if i < n_lines {
            } else {
                __assert_fail(
                    b"i < n_lines\0" as *const u8 as *const libc::c_char,
                    b"../bind-mount.c\0" as *const u8 as *const libc::c_char,
                    268 as libc::c_int as libc::c_uint,
                    (*::core::mem::transmute::<&[u8; 44], &[libc::c_char; 44]>(
                        b"MountTab parse_mountinfo(int, const char *)\0",
                    ))
                    .as_ptr(),
                );
            }
        };
        end = strchr(line, '\n' as i32);
        if !end.is_null() {
            *end = 0 as libc::c_int as libc::c_char;
            next_line = end.offset(1 as libc::c_int as isize);
        } else {
            next_line = line.offset(strlen(line) as isize);
        }
        rc = sscanf(
            line,
            b"%d %d %u:%u %n\0" as *const u8 as *const libc::c_char,
            &mut (*lines.offset(i as isize)).id as *mut libc::c_int,
            &mut (*lines.offset(i as isize)).parent_id as *mut libc::c_int,
            &mut maj as *mut libc::c_uint,
            &mut min as *mut libc::c_uint,
            &mut consumed as *mut libc::c_int,
        );
        if rc != 4 as libc::c_int {
            die(b"Can't parse mountinfo line\0" as *const u8 as *const libc::c_char);
        }
        rest = line.offset(consumed as isize);
        rest = skip_token(rest, 1 as libc::c_int);
        mountpoint = rest;
        rest = skip_token(rest, 0 as libc::c_int);
        let fresh3 = rest;
        rest = rest.offset(1);
        mountpoint_end = fresh3;
        options = rest;
        rest = skip_token(rest, 0 as libc::c_int);
        options_end = rest;
        *mountpoint_end = 0 as libc::c_int as libc::c_char;
        let ref mut fresh4 = (*lines.offset(i as isize)).mountpoint;
        *fresh4 = unescape_inline(mountpoint);
        *options_end = 0 as libc::c_int as libc::c_char;
        let ref mut fresh5 = (*lines.offset(i as isize)).options;
        *fresh5 = options;
        if (*lines.offset(i as isize)).id > max_id {
            max_id = (*lines.offset(i as isize)).id;
        }
        if (*lines.offset(i as isize)).parent_id > max_id {
            max_id = (*lines.offset(i as isize)).parent_id;
        }
        if path_equal((*lines.offset(i as isize)).mountpoint, root_mount) != 0 {
            root = i as libc::c_int;
        }
        i = i.wrapping_add(1);
        i;
        line = next_line;
    }
    if i == n_lines {
    } else {
        __assert_fail(
            b"i == n_lines\0" as *const u8 as *const libc::c_char,
            b"../bind-mount.c\0" as *const u8 as *const libc::c_char,
            309 as libc::c_int as libc::c_uint,
            (*::core::mem::transmute::<&[u8; 44], &[libc::c_char; 44]>(
                b"MountTab parse_mountinfo(int, const char *)\0",
            ))
            .as_ptr(),
        );
    }
    'c_5065: {
        if i == n_lines {
        } else {
            __assert_fail(
                b"i == n_lines\0" as *const u8 as *const libc::c_char,
                b"../bind-mount.c\0" as *const u8 as *const libc::c_char,
                309 as libc::c_int as libc::c_uint,
                (*::core::mem::transmute::<&[u8; 44], &[libc::c_char; 44]>(
                    b"MountTab parse_mountinfo(int, const char *)\0",
                ))
                .as_ptr(),
            );
        }
    };
    if root == -(1 as libc::c_int) {
        mount_tab = xcalloc(
            (::core::mem::size_of::<MountInfo>() as libc::c_ulong)
                .wrapping_mul(1 as libc::c_int as libc::c_ulong),
        ) as MountTab;
        return (if 0 as libc::c_int != 0 {
            mount_tab as *mut libc::c_void
        } else {
            steal_pointer(&mut mount_tab as *mut MountTab as *mut libc::c_void)
        }) as MountTab;
    }
    by_id = xcalloc(
        ((max_id + 1 as libc::c_int) as libc::c_ulong)
            .wrapping_mul(::core::mem::size_of::<*mut MountInfoLine>() as libc::c_ulong),
    ) as *mut *mut MountInfoLine;
    i = 0 as libc::c_int as libc::c_uint;
    while i < n_lines {
        let ref mut fresh6 = *by_id.offset((*lines.offset(i as isize)).id as isize);
        *fresh6 = &mut *lines.offset(i as isize) as *mut MountInfoLine;
        i = i.wrapping_add(1);
        i;
    }
    i = 0 as libc::c_int as libc::c_uint;
    while i < n_lines {
        let mut this: *mut MountInfoLine = &mut *lines.offset(i as isize) as *mut MountInfoLine;
        let mut parent: *mut MountInfoLine = *by_id.offset((*this).parent_id as isize);
        let mut to_sibling: *mut *mut MountInfoLine = 0 as *mut *mut MountInfoLine;
        let mut sibling: *mut MountInfoLine = 0 as *mut MountInfoLine;
        let mut covered: bool_0 = 0 as libc::c_int;
        if !(has_path_prefix((*this).mountpoint, root_mount) == 0) {
            if !parent.is_null() {
                if strcmp((*parent).mountpoint, (*this).mountpoint) == 0 as libc::c_int {
                    (*parent).covered = 1 as libc::c_int;
                }
                to_sibling = &mut (*parent).first_child;
                sibling = (*parent).first_child;
                while !sibling.is_null() {
                    if has_path_prefix((*this).mountpoint, (*sibling).mountpoint) != 0 {
                        covered = 1 as libc::c_int;
                        break;
                    } else {
                        if has_path_prefix((*sibling).mountpoint, (*this).mountpoint) != 0 {
                            *to_sibling = (*sibling).next_sibling;
                        } else {
                            to_sibling = &mut (*sibling).next_sibling;
                        }
                        sibling = (*sibling).next_sibling;
                    }
                }
                if !(covered != 0) {
                    *to_sibling = this;
                }
            }
        }
        i = i.wrapping_add(1);
        i;
    }
    n_mounts = count_mounts(&mut *lines.offset(root as isize));
    mount_tab = xcalloc(
        (::core::mem::size_of::<MountInfo>() as libc::c_ulong)
            .wrapping_mul((n_mounts + 1 as libc::c_int) as libc::c_ulong),
    ) as MountTab;
    end_tab = collect_mounts(
        &mut *mount_tab.offset(0 as libc::c_int as isize),
        &mut *lines.offset(root as isize),
    );
    if end_tab == &mut *mount_tab.offset(n_mounts as isize) as *mut MountInfo {
    } else {
        __assert_fail(
            b"end_tab == &mount_tab[n_mounts]\0" as *const u8 as *const libc::c_char,
            b"../bind-mount.c\0" as *const u8 as *const libc::c_char,
            372 as libc::c_int as libc::c_uint,
            (*::core::mem::transmute::<&[u8; 44], &[libc::c_char; 44]>(
                b"MountTab parse_mountinfo(int, const char *)\0",
            ))
            .as_ptr(),
        );
    }
    'c_4318: {
        if end_tab == &mut *mount_tab.offset(n_mounts as isize) as *mut MountInfo {
        } else {
            __assert_fail(
                b"end_tab == &mount_tab[n_mounts]\0" as *const u8 as *const libc::c_char,
                b"../bind-mount.c\0" as *const u8 as *const libc::c_char,
                372 as libc::c_int as libc::c_uint,
                (*::core::mem::transmute::<&[u8; 44], &[libc::c_char; 44]>(
                    b"MountTab parse_mountinfo(int, const char *)\0",
                ))
                .as_ptr(),
            );
        }
    };
    return (if 0 as libc::c_int != 0 {
        mount_tab as *mut libc::c_void
    } else {
        steal_pointer(&mut mount_tab as *mut MountTab as *mut libc::c_void)
    }) as MountTab;
}
#[no_mangle]
#[c2rust::src_loc = "377:1"]
pub unsafe extern "C" fn bind_mount(
    mut proc_fd: libc::c_int,
    mut src: *const libc::c_char,
    mut dest: *const libc::c_char,
    mut options: bind_option_t,
    mut failing_path: *mut *mut libc::c_char,
) -> bind_mount_result {
    let mut readonly: bool_0 = (options as libc::c_uint
        & BIND_READONLY as libc::c_int as libc::c_uint
        != 0 as libc::c_int as libc::c_uint) as libc::c_int;
    let mut devices: bool_0 = (options as libc::c_uint
        & BIND_DEVICES as libc::c_int as libc::c_uint
        != 0 as libc::c_int as libc::c_uint) as libc::c_int;
    let mut recursive: bool_0 = (options as libc::c_uint
        & BIND_RECURSIVE as libc::c_int as libc::c_uint
        != 0 as libc::c_int as libc::c_uint) as libc::c_int;
    let mut current_flags: libc::c_ulong = 0;
    let mut new_flags: libc::c_ulong = 0;
    let mut mount_tab: MountTab = 0 as MountTab;
    let mut resolved_dest: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut dest_proc: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut oldroot_dest_proc: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut kernel_case_combination: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut dest_fd: libc::c_int = -(1 as libc::c_int);
    let mut i: libc::c_int = 0;
    if !src.is_null() {
        if mount(
            src,
            dest,
            0 as *const libc::c_char,
            (MS_SILENT as libc::c_int
                | MS_BIND as libc::c_int
                | (if recursive != 0 {
                    MS_REC as libc::c_int
                } else {
                    0 as libc::c_int
                })) as libc::c_ulong,
            0 as *const libc::c_void,
        ) != 0 as libc::c_int
        {
            return BIND_MOUNT_ERROR_MOUNT;
        }
    }
    resolved_dest = realpath(dest, 0 as *mut libc::c_char);
    if resolved_dest.is_null() {
        return BIND_MOUNT_ERROR_REALPATH_DEST;
    }
    dest_fd = open(
        resolved_dest,
        0o10000000 as libc::c_int | 0o2000000 as libc::c_int,
    );
    if dest_fd < 0 as libc::c_int {
        if !failing_path.is_null() {
            *failing_path = (if 0 as libc::c_int != 0 {
                resolved_dest as *mut libc::c_void
            } else {
                steal_pointer(&mut resolved_dest as *mut *mut libc::c_char as *mut libc::c_void)
            }) as *mut libc::c_char;
        }
        return BIND_MOUNT_ERROR_REOPEN_DEST;
    }
    dest_proc = xasprintf(
        b"/proc/self/fd/%d\0" as *const u8 as *const libc::c_char,
        dest_fd,
    );
    oldroot_dest_proc = get_oldroot_path(dest_proc);
    kernel_case_combination = readlink_malloc(oldroot_dest_proc);
    if kernel_case_combination.is_null() {
        if !failing_path.is_null() {
            *failing_path = (if 0 as libc::c_int != 0 {
                resolved_dest as *mut libc::c_void
            } else {
                steal_pointer(&mut resolved_dest as *mut *mut libc::c_char as *mut libc::c_void)
            }) as *mut libc::c_char;
        }
        return BIND_MOUNT_ERROR_READLINK_DEST_PROC_FD;
    }
    mount_tab = parse_mountinfo(proc_fd, kernel_case_combination);
    if ((*mount_tab.offset(0 as libc::c_int as isize)).mountpoint).is_null() {
        if !failing_path.is_null() {
            *failing_path = (if 0 as libc::c_int != 0 {
                kernel_case_combination as *mut libc::c_void
            } else {
                steal_pointer(
                    &mut kernel_case_combination as *mut *mut libc::c_char as *mut libc::c_void,
                )
            }) as *mut libc::c_char;
        }
        *__errno_location() = 22 as libc::c_int;
        return BIND_MOUNT_ERROR_FIND_DEST_MOUNT;
    }
    if path_equal(
        (*mount_tab.offset(0 as libc::c_int as isize)).mountpoint,
        kernel_case_combination,
    ) != 0
    {
    } else {
        __assert_fail(
            b"path_equal (mount_tab[0].mountpoint, kernel_case_combination)\0"
                as *const u8 as *const libc::c_char,
            b"../bind-mount.c\0" as *const u8 as *const libc::c_char,
            448 as libc::c_int as libc::c_uint,
            (*::core::mem::transmute::<
                &[u8; 86],
                &[libc::c_char; 86],
            >(
                b"bind_mount_result bind_mount(int, const char *, const char *, bind_option_t, char **)\0",
            ))
                .as_ptr(),
        );
    }
    'c_4130: {
        if path_equal(
            (*mount_tab.offset(0 as libc::c_int as isize)).mountpoint,
            kernel_case_combination,
        ) != 0
        {
        } else {
            __assert_fail(
                b"path_equal (mount_tab[0].mountpoint, kernel_case_combination)\0"
                    as *const u8 as *const libc::c_char,
                b"../bind-mount.c\0" as *const u8 as *const libc::c_char,
                448 as libc::c_int as libc::c_uint,
                (*::core::mem::transmute::<
                    &[u8; 86],
                    &[libc::c_char; 86],
                >(
                    b"bind_mount_result bind_mount(int, const char *, const char *, bind_option_t, char **)\0",
                ))
                    .as_ptr(),
            );
        }
    };
    current_flags = (*mount_tab.offset(0 as libc::c_int as isize)).options;
    new_flags = current_flags
        | (if devices != 0 {
            0 as libc::c_int
        } else {
            MS_NODEV as libc::c_int
        }) as libc::c_ulong
        | MS_NOSUID as libc::c_int as libc::c_ulong
        | (if readonly != 0 {
            MS_RDONLY as libc::c_int
        } else {
            0 as libc::c_int
        }) as libc::c_ulong;
    if new_flags != current_flags
        && mount(
            b"none\0" as *const u8 as *const libc::c_char,
            resolved_dest,
            0 as *const libc::c_char,
            (MS_SILENT as libc::c_int | MS_BIND as libc::c_int | MS_REMOUNT as libc::c_int)
                as libc::c_ulong
                | new_flags,
            0 as *const libc::c_void,
        ) != 0 as libc::c_int
    {
        if !failing_path.is_null() {
            *failing_path = (if 0 as libc::c_int != 0 {
                resolved_dest as *mut libc::c_void
            } else {
                steal_pointer(&mut resolved_dest as *mut *mut libc::c_char as *mut libc::c_void)
            }) as *mut libc::c_char;
        }
        return BIND_MOUNT_ERROR_REMOUNT_DEST;
    }
    if recursive != 0 {
        i = 1 as libc::c_int;
        while !((*mount_tab.offset(i as isize)).mountpoint).is_null() {
            current_flags = (*mount_tab.offset(i as isize)).options;
            new_flags = current_flags
                | (if devices != 0 {
                    0 as libc::c_int
                } else {
                    MS_NODEV as libc::c_int
                }) as libc::c_ulong
                | MS_NOSUID as libc::c_int as libc::c_ulong
                | (if readonly != 0 {
                    MS_RDONLY as libc::c_int
                } else {
                    0 as libc::c_int
                }) as libc::c_ulong;
            if new_flags != current_flags
                && mount(
                    b"none\0" as *const u8 as *const libc::c_char,
                    (*mount_tab.offset(i as isize)).mountpoint,
                    0 as *const libc::c_char,
                    (MS_SILENT as libc::c_int | MS_BIND as libc::c_int | MS_REMOUNT as libc::c_int)
                        as libc::c_ulong
                        | new_flags,
                    0 as *const libc::c_void,
                ) != 0 as libc::c_int
            {
                if *__errno_location() != 13 as libc::c_int {
                    if !failing_path.is_null() {
                        *failing_path = xstrdup((*mount_tab.offset(i as isize)).mountpoint);
                    }
                    return BIND_MOUNT_ERROR_REMOUNT_SUBMOUNT;
                }
            }
            i += 1;
            i;
        }
    }
    return BIND_MOUNT_SUCCESS;
}
#[c2rust::src_loc = "496:1"]
unsafe extern "C" fn bind_mount_result_to_string(
    mut res: bind_mount_result,
    mut failing_path: *const libc::c_char,
    mut want_errno_p: *mut bool_0,
) -> *mut libc::c_char {
    let mut string: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut want_errno: bool_0 = 1 as libc::c_int;
    match res as libc::c_uint {
        1 => {
            string = xstrdup(
                b"Unable to mount source on destination\0" as *const u8 as *const libc::c_char,
            );
        }
        2 => {
            string = xstrdup(b"realpath(destination)\0" as *const u8 as *const libc::c_char);
        }
        3 => {
            string = xasprintf(
                b"open(\"%s\", O_PATH)\0" as *const u8 as *const libc::c_char,
                failing_path,
            );
        }
        4 => {
            string = xasprintf(
                b"readlink(/proc/self/fd/N) for \"%s\"\0" as *const u8 as *const libc::c_char,
                failing_path,
            );
        }
        5 => {
            string = xasprintf(
                b"Unable to find \"%s\" in mount table\0" as *const u8 as *const libc::c_char,
                failing_path,
            );
            want_errno = 0 as libc::c_int;
        }
        6 => {
            string = xasprintf(
                b"Unable to remount destination \"%s\" with correct flags\0" as *const u8
                    as *const libc::c_char,
                failing_path,
            );
        }
        7 => {
            string = xasprintf(
                b"Unable to apply mount flags: remount \"%s\"\0" as *const u8
                    as *const libc::c_char,
                failing_path,
            );
        }
        0 => {
            string = xstrdup(b"Success\0" as *const u8 as *const libc::c_char);
        }
        _ => {
            string = xstrdup(
                b"(unknown/invalid bind_mount_result)\0" as *const u8 as *const libc::c_char,
            );
        }
    }
    if !want_errno_p.is_null() {
        *want_errno_p = want_errno;
    }
    return string;
}
#[no_mangle]
#[c2rust::src_loc = "552:1"]
pub unsafe extern "C" fn die_with_bind_result(
    mut res: bind_mount_result,
    mut saved_errno: libc::c_int,
    mut failing_path: *const libc::c_char,
    mut format: *const libc::c_char,
    mut args: ...
) -> ! {
    let mut args_0: ::core::ffi::VaListImpl;
    let mut want_errno: bool_0 = 1 as libc::c_int;
    let mut message: *mut libc::c_char = 0 as *mut libc::c_char;
    fprintf(stderr, b"bwrap: \0" as *const u8 as *const libc::c_char);
    args_0 = args.clone();
    vfprintf(stderr, format, args_0.as_va_list());
    message = bind_mount_result_to_string(res, failing_path, &mut want_errno);
    fprintf(
        stderr,
        b": %s\0" as *const u8 as *const libc::c_char,
        message,
    );
    if want_errno != 0 {
        fprintf(
            stderr,
            b": %s\0" as *const u8 as *const libc::c_char,
            strerror(saved_errno),
        );
    }
    fprintf(stderr, b"\n\0" as *const u8 as *const libc::c_char);
    exit(1 as libc::c_int);
}
