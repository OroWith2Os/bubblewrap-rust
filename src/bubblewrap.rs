#![allow(
    dead_code,
    mutable_transmutes,
    non_camel_case_types,
    non_snake_case,
    non_upper_case_globals,
    unused_assignments,
    unused_mut
)]
#![register_tool(c2rust)]
#![feature(extern_types, label_break_value, register_tool)]
use ::c2rust_out::*;
#[c2rust::header_src = "/usr/include/bits/types/__sigset_t.h:22"]
pub mod __sigset_t_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "5:9"]
    pub struct __sigset_t {
        pub __val: [libc::c_ulong; 16],
    }
}
#[c2rust::header_src = "/usr/include/bits/types.h:22"]
pub mod types_h {
    #[c2rust::src_loc = "38:1"]
    pub type __uint8_t = libc::c_uchar;
    #[c2rust::src_loc = "40:1"]
    pub type __uint16_t = libc::c_ushort;
    #[c2rust::src_loc = "41:1"]
    pub type __int32_t = libc::c_int;
    #[c2rust::src_loc = "42:1"]
    pub type __uint32_t = libc::c_uint;
    #[c2rust::src_loc = "45:1"]
    pub type __uint64_t = libc::c_ulong;
    #[c2rust::src_loc = "73:1"]
    pub type __uintmax_t = libc::c_ulong;
    #[c2rust::src_loc = "145:1"]
    pub type __dev_t = libc::c_ulong;
    #[c2rust::src_loc = "146:1"]
    pub type __uid_t = libc::c_uint;
    #[c2rust::src_loc = "147:1"]
    pub type __gid_t = libc::c_uint;
    #[c2rust::src_loc = "148:1"]
    pub type __ino_t = libc::c_ulong;
    #[c2rust::src_loc = "149:1"]
    pub type __ino64_t = libc::c_ulong;
    #[c2rust::src_loc = "150:1"]
    pub type __mode_t = libc::c_uint;
    #[c2rust::src_loc = "151:1"]
    pub type __nlink_t = libc::c_ulong;
    #[c2rust::src_loc = "152:1"]
    pub type __off_t = libc::c_long;
    #[c2rust::src_loc = "153:1"]
    pub type __off64_t = libc::c_long;
    #[c2rust::src_loc = "154:1"]
    pub type __pid_t = libc::c_int;
    #[c2rust::src_loc = "160:1"]
    pub type __time_t = libc::c_long;
    #[c2rust::src_loc = "175:1"]
    pub type __blksize_t = libc::c_long;
    #[c2rust::src_loc = "180:1"]
    pub type __blkcnt_t = libc::c_long;
    #[c2rust::src_loc = "194:1"]
    pub type __ssize_t = libc::c_long;
    #[c2rust::src_loc = "197:1"]
    pub type __syscall_slong_t = libc::c_long;
}
#[c2rust::header_src = "/usr/include/bits/types/struct_timespec.h:22"]
pub mod struct_timespec_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "11:8"]
    pub struct timespec {
        pub tv_sec: __time_t,
        pub tv_nsec: __syscall_slong_t,
    }
    use super::types_h::{__syscall_slong_t, __time_t};
}
#[c2rust::header_src = "/usr/include/sys/poll.h:22"]
pub mod poll_h {
    #[c2rust::src_loc = "33:1"]
    pub type nfds_t = libc::c_ulong;
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "36:8"]
    pub struct pollfd {
        pub fd: libc::c_int,
        pub events: libc::c_short,
        pub revents: libc::c_short,
    }
    extern "C" {
        #[c2rust::src_loc = "54:1"]
        pub fn poll(__fds: *mut pollfd, __nfds: nfds_t, __timeout: libc::c_int) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/lib/clang/15.0.7/include/stddef.h:23"]
pub mod stddef_h {
    #[c2rust::src_loc = "46:1"]
    pub type size_t = libc::c_ulong;
}
#[c2rust::header_src = "/usr/include/sched.h:23"]
pub mod sched_h {
    #[c2rust::src_loc = "38:1"]
    pub type pid_t = __pid_t;
    use super::types_h::__pid_t;
}
#[c2rust::header_src = "/usr/include/pwd.h:24"]
pub mod pwd_h {
    #[c2rust::src_loc = "38:1"]
    pub type gid_t = __gid_t;
    #[c2rust::src_loc = "43:1"]
    pub type uid_t = __uid_t;
    use super::types_h::{__gid_t, __uid_t};
}
#[c2rust::header_src = "/usr/include/bits/types/struct_FILE.h:39"]
pub mod struct_FILE_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "49:8"]
    pub struct _IO_FILE {
        pub _flags: libc::c_int,
        pub _IO_read_ptr: *mut libc::c_char,
        pub _IO_read_end: *mut libc::c_char,
        pub _IO_read_base: *mut libc::c_char,
        pub _IO_write_base: *mut libc::c_char,
        pub _IO_write_ptr: *mut libc::c_char,
        pub _IO_write_end: *mut libc::c_char,
        pub _IO_buf_base: *mut libc::c_char,
        pub _IO_buf_end: *mut libc::c_char,
        pub _IO_save_base: *mut libc::c_char,
        pub _IO_backup_base: *mut libc::c_char,
        pub _IO_save_end: *mut libc::c_char,
        pub _markers: *mut _IO_marker,
        pub _chain: *mut _IO_FILE,
        pub _fileno: libc::c_int,
        pub _flags2: libc::c_int,
        pub _old_offset: __off_t,
        pub _cur_column: libc::c_ushort,
        pub _vtable_offset: libc::c_schar,
        pub _shortbuf: [libc::c_char; 1],
        pub _lock: *mut libc::c_void,
        pub _offset: __off64_t,
        pub _codecvt: *mut _IO_codecvt,
        pub _wide_data: *mut _IO_wide_data,
        pub _freeres_list: *mut _IO_FILE,
        pub _freeres_buf: *mut libc::c_void,
        pub __pad5: size_t,
        pub _mode: libc::c_int,
        pub _unused2: [libc::c_char; 20],
    }
    #[c2rust::src_loc = "43:1"]
    pub type _IO_lock_t = ();
    use super::stddef_h::size_t;
    use super::types_h::{__off64_t, __off_t};
    extern "C" {
        #[c2rust::src_loc = "38:8"]
        pub type _IO_wide_data;
        #[c2rust::src_loc = "37:8"]
        pub type _IO_codecvt;
        #[c2rust::src_loc = "36:8"]
        pub type _IO_marker;
    }
}
#[c2rust::header_src = "/usr/include/bits/types/FILE.h:24"]
pub mod FILE_h {
    #[c2rust::src_loc = "7:1"]
    pub type FILE = _IO_FILE;
    use super::struct_FILE_h::_IO_FILE;
}
#[c2rust::header_src = "/usr/include/ctype.h:26"]
pub mod ctype_h {
    #[c2rust::src_loc = "46:1"]
    pub type C2RustUnnamed = libc::c_uint;
    #[c2rust::src_loc = "59:3"]
    pub const _ISalnum: C2RustUnnamed = 8;
    #[c2rust::src_loc = "58:3"]
    pub const _ISpunct: C2RustUnnamed = 4;
    #[c2rust::src_loc = "57:3"]
    pub const _IScntrl: C2RustUnnamed = 2;
    #[c2rust::src_loc = "56:3"]
    pub const _ISblank: C2RustUnnamed = 1;
    #[c2rust::src_loc = "55:3"]
    pub const _ISgraph: C2RustUnnamed = 32768;
    #[c2rust::src_loc = "54:3"]
    pub const _ISprint: C2RustUnnamed = 16384;
    #[c2rust::src_loc = "53:3"]
    pub const _ISspace: C2RustUnnamed = 8192;
    #[c2rust::src_loc = "52:3"]
    pub const _ISxdigit: C2RustUnnamed = 4096;
    #[c2rust::src_loc = "51:3"]
    pub const _ISdigit: C2RustUnnamed = 2048;
    #[c2rust::src_loc = "50:3"]
    pub const _ISalpha: C2RustUnnamed = 1024;
    #[c2rust::src_loc = "49:3"]
    pub const _ISlower: C2RustUnnamed = 512;
    #[c2rust::src_loc = "48:3"]
    pub const _ISupper: C2RustUnnamed = 256;
    extern "C" {
        #[c2rust::src_loc = "79:1"]
        pub fn __ctype_b_loc() -> *mut *const libc::c_ushort;
    }
}
#[c2rust::header_src = "/usr/include/bits/fcntl.h:27"]
pub mod bits_fcntl_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "35:8"]
    pub struct flock {
        pub l_type: libc::c_short,
        pub l_whence: libc::c_short,
        pub l_start: __off64_t,
        pub l_len: __off64_t,
        pub l_pid: __pid_t,
    }
    use super::types_h::{__off64_t, __pid_t};
}
#[c2rust::header_src = "/usr/include/fcntl.h:27"]
pub mod fcntl_h {
    #[c2rust::src_loc = "50:1"]
    pub type mode_t = __mode_t;
    use super::types_h::__mode_t;
    extern "C" {
        #[c2rust::src_loc = "212:1"]
        pub fn open(__file: *const libc::c_char, __oflag: libc::c_int, _: ...) -> libc::c_int;
        #[c2rust::src_loc = "237:1"]
        pub fn openat(
            __fd: libc::c_int,
            __file: *const libc::c_char,
            __oflag: libc::c_int,
            _: ...
        ) -> libc::c_int;
        #[c2rust::src_loc = "258:1"]
        pub fn creat(__file: *const libc::c_char, __mode: mode_t) -> libc::c_int;
        #[c2rust::src_loc = "180:1"]
        pub fn fcntl(__fd: libc::c_int, __cmd: libc::c_int, _: ...) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/bits/struct_stat.h:27"]
pub mod struct_stat_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "26:8"]
    pub struct stat {
        pub st_dev: __dev_t,
        pub st_ino: __ino_t,
        pub st_nlink: __nlink_t,
        pub st_mode: __mode_t,
        pub st_uid: __uid_t,
        pub st_gid: __gid_t,
        pub __pad0: libc::c_int,
        pub st_rdev: __dev_t,
        pub st_size: __off_t,
        pub st_blksize: __blksize_t,
        pub st_blocks: __blkcnt_t,
        pub st_atim: timespec,
        pub st_mtim: timespec,
        pub st_ctim: timespec,
        pub __glibc_reserved: [__syscall_slong_t; 3],
    }
    use super::struct_timespec_h::timespec;
    use super::types_h::{
        __blkcnt_t, __blksize_t, __dev_t, __gid_t, __ino_t, __mode_t, __nlink_t, __off_t,
        __syscall_slong_t, __uid_t,
    };
}
#[c2rust::header_src = "/usr/include/bits/stdint-intn.h:27"]
pub mod stdint_intn_h {
    #[c2rust::src_loc = "26:1"]
    pub type int32_t = __int32_t;
    use super::types_h::__int32_t;
}
#[c2rust::header_src = "/usr/include/bits/stdint-uintn.h:27"]
pub mod stdint_uintn_h {
    #[c2rust::src_loc = "24:1"]
    pub type uint8_t = __uint8_t;
    #[c2rust::src_loc = "25:1"]
    pub type uint16_t = __uint16_t;
    #[c2rust::src_loc = "26:1"]
    pub type uint32_t = __uint32_t;
    #[c2rust::src_loc = "27:1"]
    pub type uint64_t = __uint64_t;
    use super::types_h::{__uint16_t, __uint32_t, __uint64_t, __uint8_t};
}
#[c2rust::header_src = "/usr/include/stdint.h:27"]
pub mod stdint_h {
    #[c2rust::src_loc = "102:1"]
    pub type uintmax_t = __uintmax_t;
    use super::types_h::__uintmax_t;
}
#[c2rust::header_src = "/usr/include/asm-generic/int-ll64.h:27"]
pub mod int_ll64_h {
    #[c2rust::src_loc = "21:1"]
    pub type __u8 = libc::c_uchar;
    #[c2rust::src_loc = "24:1"]
    pub type __u16 = libc::c_ushort;
    #[c2rust::src_loc = "27:1"]
    pub type __u32 = libc::c_uint;
}
#[c2rust::header_src = "/usr/include/sys/mount.h:27"]
pub mod mount_h {
    #[c2rust::src_loc = "43:1"]
    pub type C2RustUnnamed_0 = libc::c_int;
    #[c2rust::src_loc = "124:3"]
    pub const MS_NOUSER: C2RustUnnamed_0 = -2147483648;
    #[c2rust::src_loc = "121:3"]
    pub const MS_ACTIVE: C2RustUnnamed_0 = 1073741824;
    #[c2rust::src_loc = "118:3"]
    pub const MS_LAZYTIME: C2RustUnnamed_0 = 33554432;
    #[c2rust::src_loc = "115:3"]
    pub const MS_STRICTATIME: C2RustUnnamed_0 = 16777216;
    #[c2rust::src_loc = "112:3"]
    pub const MS_I_VERSION: C2RustUnnamed_0 = 8388608;
    #[c2rust::src_loc = "109:3"]
    pub const MS_KERNMOUNT: C2RustUnnamed_0 = 4194304;
    #[c2rust::src_loc = "106:3"]
    pub const MS_RELATIME: C2RustUnnamed_0 = 2097152;
    #[c2rust::src_loc = "103:3"]
    pub const MS_SHARED: C2RustUnnamed_0 = 1048576;
    #[c2rust::src_loc = "100:3"]
    pub const MS_SLAVE: C2RustUnnamed_0 = 524288;
    #[c2rust::src_loc = "97:3"]
    pub const MS_PRIVATE: C2RustUnnamed_0 = 262144;
    #[c2rust::src_loc = "94:3"]
    pub const MS_UNBINDABLE: C2RustUnnamed_0 = 131072;
    #[c2rust::src_loc = "91:3"]
    pub const MS_POSIXACL: C2RustUnnamed_0 = 65536;
    #[c2rust::src_loc = "88:3"]
    pub const MS_SILENT: C2RustUnnamed_0 = 32768;
    #[c2rust::src_loc = "85:3"]
    pub const MS_REC: C2RustUnnamed_0 = 16384;
    #[c2rust::src_loc = "82:3"]
    pub const MS_MOVE: C2RustUnnamed_0 = 8192;
    #[c2rust::src_loc = "79:3"]
    pub const MS_BIND: C2RustUnnamed_0 = 4096;
    #[c2rust::src_loc = "76:3"]
    pub const MS_NODIRATIME: C2RustUnnamed_0 = 2048;
    #[c2rust::src_loc = "73:3"]
    pub const MS_NOATIME: C2RustUnnamed_0 = 1024;
    #[c2rust::src_loc = "70:3"]
    pub const MS_NOSYMFOLLOW: C2RustUnnamed_0 = 256;
    #[c2rust::src_loc = "67:3"]
    pub const MS_DIRSYNC: C2RustUnnamed_0 = 128;
    #[c2rust::src_loc = "64:3"]
    pub const MS_MANDLOCK: C2RustUnnamed_0 = 64;
    #[c2rust::src_loc = "61:3"]
    pub const MS_REMOUNT: C2RustUnnamed_0 = 32;
    #[c2rust::src_loc = "58:3"]
    pub const MS_SYNCHRONOUS: C2RustUnnamed_0 = 16;
    #[c2rust::src_loc = "55:3"]
    pub const MS_NOEXEC: C2RustUnnamed_0 = 8;
    #[c2rust::src_loc = "52:3"]
    pub const MS_NODEV: C2RustUnnamed_0 = 4;
    #[c2rust::src_loc = "49:3"]
    pub const MS_NOSUID: C2RustUnnamed_0 = 2;
    #[c2rust::src_loc = "46:3"]
    pub const MS_RDONLY: C2RustUnnamed_0 = 1;
    #[c2rust::src_loc = "178:1"]
    pub type C2RustUnnamed_1 = libc::c_uint;
    #[c2rust::src_loc = "186:3"]
    pub const UMOUNT_NOFOLLOW: C2RustUnnamed_1 = 8;
    #[c2rust::src_loc = "184:3"]
    pub const MNT_EXPIRE: C2RustUnnamed_1 = 4;
    #[c2rust::src_loc = "182:3"]
    pub const MNT_DETACH: C2RustUnnamed_1 = 2;
    #[c2rust::src_loc = "180:3"]
    pub const MNT_FORCE: C2RustUnnamed_1 = 1;
    extern "C" {
        #[c2rust::src_loc = "272:1"]
        pub fn mount(
            __special_file: *const libc::c_char,
            __dir: *const libc::c_char,
            __fstype: *const libc::c_char,
            __rwflag: libc::c_ulong,
            __data: *const libc::c_void,
        ) -> libc::c_int;
        #[c2rust::src_loc = "280:1"]
        pub fn umount2(__special_file: *const libc::c_char, __flags: libc::c_int) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/sys/types.h:28"]
pub mod sys_types_h {
    #[c2rust::src_loc = "49:1"]
    pub type ino_t = __ino64_t;
    #[c2rust::src_loc = "108:1"]
    pub type ssize_t = __ssize_t;
    use super::types_h::{__ino64_t, __ssize_t};
}
#[c2rust::header_src = "/usr/include/bits/types/sigset_t.h:28"]
pub mod sigset_t_h {
    #[c2rust::src_loc = "7:1"]
    pub type sigset_t = __sigset_t;
    use super::__sigset_t_h::__sigset_t;
}
#[c2rust::header_src = "/usr/include/bits/socket_type.h:28"]
pub mod socket_type_h {
    #[c2rust::src_loc = "24:1"]
    pub type __socket_type = libc::c_uint;
    #[c2rust::src_loc = "52:3"]
    pub const SOCK_NONBLOCK: __socket_type = 2048;
    #[c2rust::src_loc = "49:3"]
    pub const SOCK_CLOEXEC: __socket_type = 524288;
    #[c2rust::src_loc = "41:3"]
    pub const SOCK_PACKET: __socket_type = 10;
    #[c2rust::src_loc = "39:3"]
    pub const SOCK_DCCP: __socket_type = 6;
    #[c2rust::src_loc = "36:3"]
    pub const SOCK_SEQPACKET: __socket_type = 5;
    #[c2rust::src_loc = "34:3"]
    pub const SOCK_RDM: __socket_type = 4;
    #[c2rust::src_loc = "32:3"]
    pub const SOCK_RAW: __socket_type = 3;
    #[c2rust::src_loc = "29:3"]
    pub const SOCK_DGRAM: __socket_type = 2;
    #[c2rust::src_loc = "26:3"]
    pub const SOCK_STREAM: __socket_type = 1;
}
#[c2rust::header_src = "/usr/include/bits/eventfd.h:30"]
pub mod eventfd_h {
    #[c2rust::src_loc = "23:1"]
    pub type C2RustUnnamed_2 = libc::c_uint;
    #[c2rust::src_loc = "29:5"]
    pub const EFD_NONBLOCK: C2RustUnnamed_2 = 2048;
    #[c2rust::src_loc = "27:5"]
    pub const EFD_CLOEXEC: C2RustUnnamed_2 = 524288;
    #[c2rust::src_loc = "25:5"]
    pub const EFD_SEMAPHORE: C2RustUnnamed_2 = 1;
}
#[c2rust::header_src = "/usr/include/bits/signalfd.h:32"]
pub mod signalfd_h {
    #[c2rust::src_loc = "23:1"]
    pub type C2RustUnnamed_3 = libc::c_uint;
    #[c2rust::src_loc = "27:5"]
    pub const SFD_NONBLOCK: C2RustUnnamed_3 = 2048;
    #[c2rust::src_loc = "25:5"]
    pub const SFD_CLOEXEC: C2RustUnnamed_3 = 524288;
}
#[c2rust::header_src = "/usr/include/sys/signalfd.h:32"]
pub mod sys_signalfd_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "27:8"]
    pub struct signalfd_siginfo {
        pub ssi_signo: uint32_t,
        pub ssi_errno: int32_t,
        pub ssi_code: int32_t,
        pub ssi_pid: uint32_t,
        pub ssi_uid: uint32_t,
        pub ssi_fd: int32_t,
        pub ssi_tid: uint32_t,
        pub ssi_band: uint32_t,
        pub ssi_overrun: uint32_t,
        pub ssi_trapno: uint32_t,
        pub ssi_status: int32_t,
        pub ssi_int: int32_t,
        pub ssi_ptr: uint64_t,
        pub ssi_utime: uint64_t,
        pub ssi_stime: uint64_t,
        pub ssi_addr: uint64_t,
        pub ssi_addr_lsb: uint16_t,
        pub __pad2: uint16_t,
        pub ssi_syscall: int32_t,
        pub ssi_call_addr: uint64_t,
        pub ssi_arch: uint32_t,
        pub __pad: [uint8_t; 28],
    }
    use super::sigset_t_h::sigset_t;
    use super::stdint_intn_h::int32_t;
    use super::stdint_uintn_h::{uint16_t, uint32_t, uint64_t, uint8_t};
    extern "C" {
        #[c2rust::src_loc = "57:1"]
        pub fn signalfd(
            __fd: libc::c_int,
            __mask: *const sigset_t,
            __flags: libc::c_int,
        ) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/linux/capability.h:33"]
pub mod capability_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "39:16"]
    pub struct __user_cap_header_struct {
        pub version: __u32,
        pub pid: libc::c_int,
    }
    #[c2rust::src_loc = "39:1"]
    pub type cap_user_header_t = *mut __user_cap_header_struct;
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "44:16"]
    pub struct __user_cap_data_struct {
        pub effective: __u32,
        pub permitted: __u32,
        pub inheritable: __u32,
    }
    #[c2rust::src_loc = "44:1"]
    pub type cap_user_data_t = *mut __user_cap_data_struct;
    use super::int_ll64_h::__u32;
}
#[c2rust::header_src = "/usr/include/sys/capability.h:33"]
pub mod sys_capability_h {
    #[c2rust::src_loc = "52:1"]
    pub type cap_value_t = libc::c_int;
    use super::capability_h::{
        __user_cap_data_struct, __user_cap_header_struct, cap_user_data_t, cap_user_header_t,
    };
    extern "C" {
        #[c2rust::src_loc = "191:1"]
        pub fn cap_from_name(_: *const libc::c_char, _: *mut cap_value_t) -> libc::c_int;
        #[c2rust::src_loc = "242:1"]
        pub fn capget(header: cap_user_header_t, data: cap_user_data_t) -> libc::c_int;
        #[c2rust::src_loc = "243:1"]
        pub fn capset(header: cap_user_header_t, data: cap_user_data_t) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/linux/filter.h:37"]
pub mod filter_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "24:8"]
    pub struct sock_filter {
        pub code: __u16,
        pub jt: __u8,
        pub jf: __u8,
        pub k: __u32,
    }
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "31:8"]
    pub struct sock_fprog {
        pub len: libc::c_ushort,
        pub filter: *mut sock_filter,
    }
    use super::int_ll64_h::{__u16, __u32, __u8};
}
#[c2rust::header_src = "/var/home/oro/bubblewrap/utils.h:39"]
pub mod utils_h {
    #[c2rust::src_loc = "46:1"]
    pub type bool_0 = libc::c_int;
    #[inline]
    #[c2rust::src_loc = "169:1"]
    pub unsafe extern "C" fn steal_pointer(mut pp: *mut libc::c_void) -> *mut libc::c_void {
        let mut ptr: *mut *mut libc::c_void = pp as *mut *mut libc::c_void;
        let mut ref_0: *mut libc::c_void = 0 as *mut libc::c_void;
        ref_0 = *ptr;
        *ptr = 0 as *mut libc::c_void;
        return ref_0;
    }
    use super::fcntl_h::mode_t;
    use super::stddef_h::size_t;
    use super::sys_types_h::ssize_t;
    extern "C" {
        #[c2rust::src_loc = "55:1"]
        pub fn warn(format: *const libc::c_char, _: ...);
        #[c2rust::src_loc = "57:1"]
        pub fn die_with_error(format: *const libc::c_char, _: ...) -> !;
        #[c2rust::src_loc = "59:1"]
        pub fn die(format: *const libc::c_char, _: ...) -> !;
        #[c2rust::src_loc = "62:1"]
        pub fn die_unless_label_valid(label: *const libc::c_char);
        #[c2rust::src_loc = "64:1"]
        pub fn fork_intermediate_child();
        #[c2rust::src_loc = "67:1"]
        pub fn xcalloc(size: size_t) -> *mut libc::c_void;
        #[c2rust::src_loc = "70:1"]
        pub fn xstrdup(str: *const libc::c_char) -> *mut libc::c_char;
        #[c2rust::src_loc = "72:1"]
        pub fn xclearenv();
        #[c2rust::src_loc = "73:1"]
        pub fn xsetenv(
            name: *const libc::c_char,
            value: *const libc::c_char,
            overwrite: libc::c_int,
        );
        #[c2rust::src_loc = "76:1"]
        pub fn xunsetenv(name: *const libc::c_char);
        #[c2rust::src_loc = "77:1"]
        pub fn strconcat(s1: *const libc::c_char, s2: *const libc::c_char) -> *mut libc::c_char;
        #[c2rust::src_loc = "79:1"]
        pub fn strconcat3(
            s1: *const libc::c_char,
            s2: *const libc::c_char,
            s3: *const libc::c_char,
        ) -> *mut libc::c_char;
        #[c2rust::src_loc = "82:1"]
        pub fn xasprintf(format: *const libc::c_char, _: ...) -> *mut libc::c_char;
        #[c2rust::src_loc = "90:1"]
        pub fn fdwalk(
            proc_fd_0: libc::c_int,
            cb: Option<unsafe extern "C" fn(*mut libc::c_void, libc::c_int) -> libc::c_int>,
            data: *mut libc::c_void,
        ) -> libc::c_int;
        #[c2rust::src_loc = "94:1"]
        pub fn load_file_data(fd: libc::c_int, size: *mut size_t) -> *mut libc::c_char;
        #[c2rust::src_loc = "96:1"]
        pub fn load_file_at(dirfd: libc::c_int, path: *const libc::c_char) -> *mut libc::c_char;
        #[c2rust::src_loc = "98:1"]
        pub fn write_file_at(
            dirfd: libc::c_int,
            path: *const libc::c_char,
            content: *const libc::c_char,
        ) -> libc::c_int;
        #[c2rust::src_loc = "101:1"]
        pub fn write_to_fd(
            fd: libc::c_int,
            content: *const libc::c_char,
            len: ssize_t,
        ) -> libc::c_int;
        #[c2rust::src_loc = "104:1"]
        pub fn copy_file_data(sfd: libc::c_int, dfd: libc::c_int) -> libc::c_int;
        #[c2rust::src_loc = "109:1"]
        pub fn create_file(
            path: *const libc::c_char,
            mode: mode_t,
            content: *const libc::c_char,
        ) -> libc::c_int;
        #[c2rust::src_loc = "112:1"]
        pub fn ensure_file(path: *const libc::c_char, mode: mode_t) -> libc::c_int;
        #[c2rust::src_loc = "114:1"]
        pub fn ensure_dir(path: *const libc::c_char, mode: mode_t) -> libc::c_int;
        #[c2rust::src_loc = "116:1"]
        pub fn get_file_mode(pathname: *const libc::c_char) -> libc::c_int;
        #[c2rust::src_loc = "117:1"]
        pub fn mkdir_with_parents(
            pathname: *const libc::c_char,
            mode: mode_t,
            create_last: bool_0,
        ) -> libc::c_int;
        #[c2rust::src_loc = "120:1"]
        pub fn create_pid_socketpair(sockets: *mut libc::c_int);
        #[c2rust::src_loc = "121:1"]
        pub fn send_pid_on_socket(socket: libc::c_int);
        #[c2rust::src_loc = "122:1"]
        pub fn read_pid_from_socket(socket: libc::c_int) -> libc::c_int;
        #[c2rust::src_loc = "123:1"]
        pub fn get_oldroot_path(path: *const libc::c_char) -> *mut libc::c_char;
        #[c2rust::src_loc = "127:1"]
        pub fn raw_clone(flags: libc::c_ulong, child_stack: *mut libc::c_void) -> libc::c_int;
        #[c2rust::src_loc = "129:1"]
        pub fn pivot_root(
            new_root: *const libc::c_char,
            put_old: *const libc::c_char,
        ) -> libc::c_int;
        #[c2rust::src_loc = "131:1"]
        pub fn label_mount(
            opt: *const libc::c_char,
            mount_label: *const libc::c_char,
        ) -> *mut libc::c_char;
        #[c2rust::src_loc = "133:1"]
        pub fn label_exec(exec_label: *const libc::c_char) -> libc::c_int;
        #[c2rust::src_loc = "134:1"]
        pub fn label_create_file(file_label: *const libc::c_char) -> libc::c_int;
    }
}
#[c2rust::header_src = "/var/home/oro/bubblewrap/bind-mount.h:41"]
pub mod bind_mount_h {
    #[c2rust::src_loc = "24:9"]
    pub type bind_option_t = libc::c_uint;
    #[c2rust::src_loc = "27:3"]
    pub const BIND_RECURSIVE: bind_option_t = 8;
    #[c2rust::src_loc = "26:3"]
    pub const BIND_DEVICES: bind_option_t = 4;
    #[c2rust::src_loc = "25:3"]
    pub const BIND_READONLY: bind_option_t = 1;
    #[c2rust::src_loc = "30:9"]
    pub type bind_mount_result = libc::c_uint;
    #[c2rust::src_loc = "39:3"]
    pub const BIND_MOUNT_ERROR_REMOUNT_SUBMOUNT: bind_mount_result = 7;
    #[c2rust::src_loc = "38:3"]
    pub const BIND_MOUNT_ERROR_REMOUNT_DEST: bind_mount_result = 6;
    #[c2rust::src_loc = "37:3"]
    pub const BIND_MOUNT_ERROR_FIND_DEST_MOUNT: bind_mount_result = 5;
    #[c2rust::src_loc = "36:3"]
    pub const BIND_MOUNT_ERROR_READLINK_DEST_PROC_FD: bind_mount_result = 4;
    #[c2rust::src_loc = "35:3"]
    pub const BIND_MOUNT_ERROR_REOPEN_DEST: bind_mount_result = 3;
    #[c2rust::src_loc = "34:3"]
    pub const BIND_MOUNT_ERROR_REALPATH_DEST: bind_mount_result = 2;
    #[c2rust::src_loc = "33:3"]
    pub const BIND_MOUNT_ERROR_MOUNT: bind_mount_result = 1;
    #[c2rust::src_loc = "32:3"]
    pub const BIND_MOUNT_SUCCESS: bind_mount_result = 0;
    extern "C" {
        #[c2rust::src_loc = "42:1"]
        pub fn bind_mount(
            proc_fd_0: libc::c_int,
            src: *const libc::c_char,
            dest: *const libc::c_char,
            options: bind_option_t,
            failing_path: *mut *mut libc::c_char,
        ) -> bind_mount_result;
        #[c2rust::src_loc = "48:1"]
        pub fn die_with_bind_result(
            res: bind_mount_result,
            saved_errno: libc::c_int,
            failing_path: *const libc::c_char,
            format: *const libc::c_char,
            _: ...
        ) -> !;
    }
}
#[c2rust::header_src = "/usr/include/bits/sched.h:23"]
pub mod bits_sched_h {
    extern "C" {
        #[c2rust::src_loc = "90:1"]
        pub fn unshare(__flags: libc::c_int) -> libc::c_int;
        #[c2rust::src_loc = "99:1"]
        pub fn setns(__fd: libc::c_int, __nstype: libc::c_int) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/sys/socket.h:28"]
pub mod socket_h {
    extern "C" {
        #[c2rust::src_loc = "108:1"]
        pub fn socketpair(
            __domain: libc::c_int,
            __type: libc::c_int,
            __protocol: libc::c_int,
            __fds: *mut libc::c_int,
        ) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/unistd.h:29"]
pub mod unistd_h {
    use super::stddef_h::size_t;
    use super::sys_types_h::ssize_t;
    use super::types_h::{__gid_t, __pid_t, __uid_t};
    extern "C" {
        #[c2rust::src_loc = "287:1"]
        pub fn access(__name: *const libc::c_char, __type: libc::c_int) -> libc::c_int;
        #[c2rust::src_loc = "358:1"]
        pub fn close(__fd: libc::c_int) -> libc::c_int;
        #[c2rust::src_loc = "371:1"]
        pub fn read(__fd: libc::c_int, __buf: *mut libc::c_void, __nbytes: size_t) -> ssize_t;
        #[c2rust::src_loc = "378:1"]
        pub fn write(__fd: libc::c_int, __buf: *const libc::c_void, __n: size_t) -> ssize_t;
        #[c2rust::src_loc = "442:1"]
        pub fn pipe2(__pipedes: *mut libc::c_int, __flags: libc::c_int) -> libc::c_int;
        #[c2rust::src_loc = "517:1"]
        pub fn chdir(__path: *const libc::c_char) -> libc::c_int;
        #[c2rust::src_loc = "521:1"]
        pub fn fchdir(__fd: libc::c_int) -> libc::c_int;
        #[c2rust::src_loc = "537:1"]
        pub fn get_current_dir_name() -> *mut libc::c_char;
        #[c2rust::src_loc = "599:1"]
        pub fn execvp(__file: *const libc::c_char, __argv: *const *mut libc::c_char)
            -> libc::c_int;
        #[c2rust::src_loc = "689:1"]
        pub fn setsid() -> __pid_t;
        #[c2rust::src_loc = "697:1"]
        pub fn getuid() -> __uid_t;
        #[c2rust::src_loc = "700:1"]
        pub fn geteuid() -> __uid_t;
        #[c2rust::src_loc = "703:1"]
        pub fn getgid() -> __gid_t;
        #[c2rust::src_loc = "722:1"]
        pub fn setuid(__uid: __uid_t) -> libc::c_int;
        #[c2rust::src_loc = "739:1"]
        pub fn setgid(__gid: __gid_t) -> libc::c_int;
        #[c2rust::src_loc = "778:1"]
        pub fn fork() -> __pid_t;
        #[c2rust::src_loc = "799:1"]
        pub fn ttyname(__fd: libc::c_int) -> *mut libc::c_char;
        #[c2rust::src_loc = "809:1"]
        pub fn isatty(__fd: libc::c_int) -> libc::c_int;
        #[c2rust::src_loc = "832:1"]
        pub fn symlink(__from: *const libc::c_char, __to: *const libc::c_char) -> libc::c_int;
        #[c2rust::src_loc = "858:1"]
        pub fn unlink(__name: *const libc::c_char) -> libc::c_int;
        #[c2rust::src_loc = "919:1"]
        pub fn sethostname(__name: *const libc::c_char, __len: size_t) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/signal.h:29"]
pub mod signal_h {
    use super::sigset_t_h::sigset_t;
    extern "C" {
        #[c2rust::src_loc = "232:1"]
        pub fn sigprocmask(
            __how: libc::c_int,
            __set: *const sigset_t,
            __oset: *mut sigset_t,
        ) -> libc::c_int;
        #[c2rust::src_loc = "199:1"]
        pub fn sigemptyset(__set: *mut sigset_t) -> libc::c_int;
        #[c2rust::src_loc = "205:1"]
        pub fn sigaddset(__set: *mut sigset_t, __signo: libc::c_int) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/sys/wait.h:29"]
pub mod wait_h {
    use super::types_h::__pid_t;
    extern "C" {
        #[c2rust::src_loc = "83:1"]
        pub fn wait(__stat_loc: *mut libc::c_int) -> __pid_t;
        #[c2rust::src_loc = "106:1"]
        pub fn waitpid(
            __pid: __pid_t,
            __stat_loc: *mut libc::c_int,
            __options: libc::c_int,
        ) -> __pid_t;
    }
}
#[c2rust::header_src = "/usr/include/sys/eventfd.h:30"]
pub mod sys_eventfd_h {
    extern "C" {
        #[c2rust::src_loc = "34:1"]
        pub fn eventfd(__count: libc::c_uint, __flags: libc::c_int) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/sys/fsuid.h:31"]
pub mod fsuid_h {
    use super::types_h::__uid_t;
    extern "C" {
        #[c2rust::src_loc = "28:1"]
        pub fn setfsuid(__uid: __uid_t) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/sys/prctl.h:34"]
pub mod prctl_h {
    extern "C" {
        #[c2rust::src_loc = "42:1"]
        pub fn prctl(__option: libc::c_int, _: ...) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/string.h:39"]
pub mod string_h {
    extern "C" {
        #[c2rust::src_loc = "107:14"]
        pub fn memchr(
            _: *const libc::c_void,
            _: libc::c_int,
            _: libc::c_ulong,
        ) -> *mut libc::c_void;
        #[c2rust::src_loc = "141:14"]
        pub fn strcpy(_: *mut libc::c_char, _: *const libc::c_char) -> *mut libc::c_char;
        #[c2rust::src_loc = "156:12"]
        pub fn strcmp(_: *const libc::c_char, _: *const libc::c_char) -> libc::c_int;
        #[c2rust::src_loc = "407:15"]
        pub fn strlen(_: *const libc::c_char) -> libc::c_ulong;
    }
}
#[c2rust::header_src = "/usr/include/errno.h:39"]
pub mod errno_h {
    extern "C" {
        #[c2rust::src_loc = "37:1"]
        pub fn __errno_location() -> *mut libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/stdio.h:39"]
pub mod stdio_h {
    use super::FILE_h::FILE;
    extern "C" {
        #[c2rust::src_loc = "144:14"]
        pub static mut stdout: *mut FILE;
        #[c2rust::src_loc = "145:14"]
        pub static mut stderr: *mut FILE;
        #[c2rust::src_loc = "356:12"]
        pub fn printf(_: *const libc::c_char, _: ...) -> libc::c_int;
        #[c2rust::src_loc = "350:12"]
        pub fn fprintf(_: *mut FILE, _: *const libc::c_char, _: ...) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/assert.h:39"]
pub mod assert_h {
    extern "C" {
        #[c2rust::src_loc = "67:1"]
        pub fn __assert_fail(
            __assertion: *const libc::c_char,
            __file: *const libc::c_char,
            __line: libc::c_uint,
            __function: *const libc::c_char,
        ) -> !;
    }
}
#[c2rust::header_src = "/usr/include/stdlib.h:39"]
pub mod stdlib_h {
    extern "C" {
        #[c2rust::src_loc = "177:17"]
        pub fn strtol(
            _: *const libc::c_char,
            _: *mut *mut libc::c_char,
            _: libc::c_int,
        ) -> libc::c_long;
        #[c2rust::src_loc = "181:26"]
        pub fn strtoul(
            _: *const libc::c_char,
            _: *mut *mut libc::c_char,
            _: libc::c_int,
        ) -> libc::c_ulong;
        #[c2rust::src_loc = "206:31"]
        pub fn strtoull(
            _: *const libc::c_char,
            _: *mut *mut libc::c_char,
            _: libc::c_int,
        ) -> libc::c_ulonglong;
        #[c2rust::src_loc = "568:13"]
        pub fn free(_: *mut libc::c_void);
        #[c2rust::src_loc = "637:13"]
        pub fn exit(_: libc::c_int) -> !;
        #[c2rust::src_loc = "654:1"]
        pub fn getenv(__name: *const libc::c_char) -> *mut libc::c_char;
        #[c2rust::src_loc = "711:1"]
        pub fn mkstemp(__template: *mut libc::c_char) -> libc::c_int;
        #[c2rust::src_loc = "821:1"]
        pub fn realpath(
            __name: *const libc::c_char,
            __resolved: *mut libc::c_char,
        ) -> *mut libc::c_char;
    }
}
#[c2rust::header_src = "/usr/include/strings.h:39"]
pub mod strings_h {
    extern "C" {
        #[c2rust::src_loc = "116:12"]
        pub fn strcasecmp(_: *const libc::c_char, _: *const libc::c_char) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/sys/stat.h:39"]
pub mod stat_h {
    use super::struct_stat_h::stat;
    use super::types_h::__mode_t;
    extern "C" {
        #[c2rust::src_loc = "227:1"]
        pub fn stat(__file: *const libc::c_char, __buf: *mut stat) -> libc::c_int;
        #[c2rust::src_loc = "279:1"]
        pub fn fstatat(
            __fd: libc::c_int,
            __file: *const libc::c_char,
            __buf: *mut stat,
            __flag: libc::c_int,
        ) -> libc::c_int;
        #[c2rust::src_loc = "352:1"]
        pub fn chmod(__file: *const libc::c_char, __mode: __mode_t) -> libc::c_int;
        #[c2rust::src_loc = "365:1"]
        pub fn fchmod(__fd: libc::c_int, __mode: __mode_t) -> libc::c_int;
        #[c2rust::src_loc = "380:1"]
        pub fn umask(__mask: __mode_t) -> __mode_t;
        #[c2rust::src_loc = "389:1"]
        pub fn mkdir(__path: *const libc::c_char, __mode: __mode_t) -> libc::c_int;
    }
}
#[c2rust::header_src = "/var/home/oro/bubblewrap/network.h:40"]
pub mod network_h {
    extern "C" {
        #[c2rust::src_loc = "22:1"]
        pub fn loopback_setup();
    }
}
pub use self::__sigset_t_h::__sigset_t;
use self::assert_h::__assert_fail;
pub use self::bind_mount_h::{
    bind_mount, bind_mount_result, bind_option_t, die_with_bind_result, BIND_DEVICES,
    BIND_MOUNT_ERROR_FIND_DEST_MOUNT, BIND_MOUNT_ERROR_MOUNT,
    BIND_MOUNT_ERROR_READLINK_DEST_PROC_FD, BIND_MOUNT_ERROR_REALPATH_DEST,
    BIND_MOUNT_ERROR_REMOUNT_DEST, BIND_MOUNT_ERROR_REMOUNT_SUBMOUNT, BIND_MOUNT_ERROR_REOPEN_DEST,
    BIND_MOUNT_SUCCESS, BIND_READONLY, BIND_RECURSIVE,
};
pub use self::bits_fcntl_h::flock;
use self::bits_sched_h::{setns, unshare};
pub use self::capability_h::{
    __user_cap_data_struct, __user_cap_header_struct, cap_user_data_t, cap_user_header_t,
};
pub use self::ctype_h::{
    C2RustUnnamed, _ISalnum, _ISalpha, _ISblank, _IScntrl, _ISdigit, _ISgraph, _ISlower, _ISprint,
    _ISpunct, _ISspace, _ISupper, _ISxdigit, __ctype_b_loc,
};
use self::errno_h::__errno_location;
pub use self::eventfd_h::{C2RustUnnamed_2, EFD_CLOEXEC, EFD_NONBLOCK, EFD_SEMAPHORE};
pub use self::fcntl_h::{creat, fcntl, mode_t, open, openat};
pub use self::filter_h::{sock_filter, sock_fprog};
use self::fsuid_h::setfsuid;
pub use self::int_ll64_h::{__u16, __u32, __u8};
pub use self::mount_h::{
    mount, umount2, C2RustUnnamed_0, C2RustUnnamed_1, MNT_DETACH, MNT_EXPIRE, MNT_FORCE, MS_ACTIVE,
    MS_BIND, MS_DIRSYNC, MS_I_VERSION, MS_KERNMOUNT, MS_LAZYTIME, MS_MANDLOCK, MS_MOVE, MS_NOATIME,
    MS_NODEV, MS_NODIRATIME, MS_NOEXEC, MS_NOSUID, MS_NOSYMFOLLOW, MS_NOUSER, MS_POSIXACL,
    MS_PRIVATE, MS_RDONLY, MS_REC, MS_RELATIME, MS_REMOUNT, MS_SHARED, MS_SILENT, MS_SLAVE,
    MS_STRICTATIME, MS_SYNCHRONOUS, MS_UNBINDABLE, UMOUNT_NOFOLLOW,
};
use self::network_h::loopback_setup;
pub use self::poll_h::{nfds_t, poll, pollfd};
use self::prctl_h::prctl;
pub use self::pwd_h::{gid_t, uid_t};
pub use self::sched_h::pid_t;
use self::signal_h::{sigaddset, sigemptyset, sigprocmask};
pub use self::signalfd_h::{C2RustUnnamed_3, SFD_CLOEXEC, SFD_NONBLOCK};
pub use self::sigset_t_h::sigset_t;
use self::socket_h::socketpair;
pub use self::socket_type_h::{
    __socket_type, SOCK_CLOEXEC, SOCK_DCCP, SOCK_DGRAM, SOCK_NONBLOCK, SOCK_PACKET, SOCK_RAW,
    SOCK_RDM, SOCK_SEQPACKET, SOCK_STREAM,
};
use self::stat_h::{chmod, fchmod, fstatat, mkdir, stat, umask};
pub use self::stddef_h::size_t;
pub use self::stdint_h::uintmax_t;
pub use self::stdint_intn_h::int32_t;
pub use self::stdint_uintn_h::{uint16_t, uint32_t, uint64_t, uint8_t};
use self::stdio_h::{fprintf, printf, stderr, stdout};
use self::stdlib_h::{exit, free, getenv, mkstemp, realpath, strtol, strtoul, strtoull};
use self::string_h::{memchr, strcmp, strcpy, strlen};
use self::strings_h::strcasecmp;
pub use self::struct_FILE_h::{_IO_codecvt, _IO_lock_t, _IO_marker, _IO_wide_data, _IO_FILE};
pub use self::struct_stat_h::stat;
pub use self::struct_timespec_h::timespec;
pub use self::sys_capability_h::{cap_from_name, cap_value_t, capget, capset};
use self::sys_eventfd_h::eventfd;
pub use self::sys_signalfd_h::{signalfd, signalfd_siginfo};
pub use self::sys_types_h::{ino_t, ssize_t};
pub use self::types_h::{
    __blkcnt_t, __blksize_t, __dev_t, __gid_t, __ino64_t, __ino_t, __int32_t, __mode_t, __nlink_t,
    __off64_t, __off_t, __pid_t, __ssize_t, __syscall_slong_t, __time_t, __uid_t, __uint16_t,
    __uint32_t, __uint64_t, __uint8_t, __uintmax_t,
};
use self::unistd_h::{
    access, chdir, close, execvp, fchdir, fork, get_current_dir_name, geteuid, getgid, getuid,
    isatty, pipe2, read, setgid, sethostname, setsid, setuid, symlink, ttyname, unlink, write,
};
pub use self::utils_h::{
    bool_0, copy_file_data, create_file, create_pid_socketpair, die, die_unless_label_valid,
    die_with_error, ensure_dir, ensure_file, fdwalk, fork_intermediate_child, get_file_mode,
    get_oldroot_path, label_create_file, label_exec, label_mount, load_file_at, load_file_data,
    mkdir_with_parents, pivot_root, raw_clone, read_pid_from_socket, send_pid_on_socket,
    steal_pointer, strconcat, strconcat3, warn, write_file_at, write_to_fd, xasprintf, xcalloc,
    xclearenv, xsetenv, xstrdup, xunsetenv,
};
use self::wait_h::{wait, waitpid};
pub use self::FILE_h::FILE;
#[derive(Copy, Clone)]
#[repr(C)]
#[c2rust::src_loc = "110:8"]
pub struct _NsInfo {
    pub name: *const libc::c_char,
    pub do_unshare: *mut bool_0,
    pub id: ino_t,
}
#[c2rust::src_loc = "108:1"]
pub type NsInfo = _NsInfo;
#[c2rust::src_loc = "129:9"]
pub type SetupOpType = libc::c_uint;
#[c2rust::src_loc = "144:3"]
pub const SETUP_CHMOD: SetupOpType = 14;
#[c2rust::src_loc = "143:3"]
pub const SETUP_SET_HOSTNAME: SetupOpType = 13;
#[c2rust::src_loc = "142:3"]
pub const SETUP_REMOUNT_RO_NO_RECURSIVE: SetupOpType = 12;
#[c2rust::src_loc = "141:3"]
pub const SETUP_MAKE_SYMLINK: SetupOpType = 11;
#[c2rust::src_loc = "140:3"]
pub const SETUP_MAKE_RO_BIND_FILE: SetupOpType = 10;
#[c2rust::src_loc = "139:3"]
pub const SETUP_MAKE_BIND_FILE: SetupOpType = 9;
#[c2rust::src_loc = "138:3"]
pub const SETUP_MAKE_FILE: SetupOpType = 8;
#[c2rust::src_loc = "137:3"]
pub const SETUP_MAKE_DIR: SetupOpType = 7;
#[c2rust::src_loc = "136:3"]
pub const SETUP_MOUNT_MQUEUE: SetupOpType = 6;
#[c2rust::src_loc = "135:3"]
pub const SETUP_MOUNT_TMPFS: SetupOpType = 5;
#[c2rust::src_loc = "134:3"]
pub const SETUP_MOUNT_DEV: SetupOpType = 4;
#[c2rust::src_loc = "133:3"]
pub const SETUP_MOUNT_PROC: SetupOpType = 3;
#[c2rust::src_loc = "132:3"]
pub const SETUP_DEV_BIND_MOUNT: SetupOpType = 2;
#[c2rust::src_loc = "131:3"]
pub const SETUP_RO_BIND_MOUNT: SetupOpType = 1;
#[c2rust::src_loc = "130:3"]
pub const SETUP_BIND_MOUNT: SetupOpType = 0;
#[c2rust::src_loc = "147:9"]
pub type SetupOpFlag = libc::c_uint;
#[c2rust::src_loc = "149:3"]
pub const ALLOW_NOTEXIST: SetupOpFlag = 2;
#[c2rust::src_loc = "148:3"]
pub const NO_CREATE_DEST: SetupOpFlag = 1;
#[derive(Copy, Clone)]
#[repr(C)]
#[c2rust::src_loc = "154:8"]
pub struct _SetupOp {
    pub type_0: SetupOpType,
    pub source: *const libc::c_char,
    pub dest: *const libc::c_char,
    pub fd: libc::c_int,
    pub flags: SetupOpFlag,
    pub perms: libc::c_int,
    pub size: size_t,
    pub next: *mut SetupOp,
}
#[c2rust::src_loc = "152:1"]
pub type SetupOp = _SetupOp;
#[derive(Copy, Clone)]
#[repr(C)]
#[c2rust::src_loc = "168:8"]
pub struct _LockFile {
    pub path: *const libc::c_char,
    pub fd: libc::c_int,
    pub next: *mut LockFile,
}
#[c2rust::src_loc = "166:1"]
pub type LockFile = _LockFile;
#[c2rust::src_loc = "175:1"]
pub type C2RustUnnamed_4 = libc::c_uint;
#[c2rust::src_loc = "183:3"]
pub const PRIV_SEP_OP_SET_HOSTNAME: C2RustUnnamed_4 = 7;
#[c2rust::src_loc = "182:3"]
pub const PRIV_SEP_OP_REMOUNT_RO_NO_RECURSIVE: C2RustUnnamed_4 = 6;
#[c2rust::src_loc = "181:3"]
pub const PRIV_SEP_OP_MQUEUE_MOUNT: C2RustUnnamed_4 = 5;
#[c2rust::src_loc = "180:3"]
pub const PRIV_SEP_OP_DEVPTS_MOUNT: C2RustUnnamed_4 = 4;
#[c2rust::src_loc = "179:3"]
pub const PRIV_SEP_OP_TMPFS_MOUNT: C2RustUnnamed_4 = 3;
#[c2rust::src_loc = "178:3"]
pub const PRIV_SEP_OP_PROC_MOUNT: C2RustUnnamed_4 = 2;
#[c2rust::src_loc = "177:3"]
pub const PRIV_SEP_OP_BIND_MOUNT: C2RustUnnamed_4 = 1;
#[c2rust::src_loc = "176:3"]
pub const PRIV_SEP_OP_DONE: C2RustUnnamed_4 = 0;
#[derive(Copy, Clone)]
#[repr(C)]
#[c2rust::src_loc = "186:9"]
pub struct PrivSepOp {
    pub op: uint32_t,
    pub flags: uint32_t,
    pub perms: uint32_t,
    pub size_arg: size_t,
    pub arg1_offset: uint32_t,
    pub arg2_offset: uint32_t,
}
#[derive(Copy, Clone)]
#[repr(C)]
#[c2rust::src_loc = "252:8"]
pub struct _SeccompProgram {
    pub program: sock_fprog,
    pub next: *mut SeccompProgram,
}
#[c2rust::src_loc = "250:1"]
pub type SeccompProgram = _SeccompProgram;
#[c2rust::src_loc = "63:14"]
static mut real_uid: uid_t = 0;
#[c2rust::src_loc = "64:14"]
static mut real_gid: gid_t = 0;
#[c2rust::src_loc = "65:14"]
static mut overflow_uid: uid_t = 0;
#[c2rust::src_loc = "66:14"]
static mut overflow_gid: gid_t = 0;
#[c2rust::src_loc = "67:13"]
static mut is_privileged: bool_0 = 0;
#[c2rust::src_loc = "68:20"]
static mut argv0: *const libc::c_char = 0 as *const libc::c_char;
#[c2rust::src_loc = "69:20"]
static mut host_tty_dev: *const libc::c_char = 0 as *const libc::c_char;
#[c2rust::src_loc = "70:12"]
static mut proc_fd: libc::c_int = -(1 as libc::c_int);
#[c2rust::src_loc = "71:20"]
static mut opt_exec_label: *const libc::c_char = 0 as *const libc::c_char;
#[c2rust::src_loc = "72:20"]
static mut opt_file_label: *const libc::c_char = 0 as *const libc::c_char;
#[c2rust::src_loc = "73:13"]
static mut opt_as_pid_1: bool_0 = 0;
#[c2rust::src_loc = "75:20"]
static mut opt_chdir_path: *const libc::c_char = 0 as *const libc::c_char;
#[c2rust::src_loc = "76:13"]
static mut opt_assert_userns_disabled: bool_0 = 0 as libc::c_int;
#[c2rust::src_loc = "77:13"]
static mut opt_disable_userns: bool_0 = 0 as libc::c_int;
#[c2rust::src_loc = "78:13"]
static mut opt_unshare_user: bool_0 = 0 as libc::c_int;
#[c2rust::src_loc = "79:13"]
static mut opt_unshare_user_try: bool_0 = 0 as libc::c_int;
#[c2rust::src_loc = "80:13"]
static mut opt_unshare_pid: bool_0 = 0 as libc::c_int;
#[c2rust::src_loc = "81:13"]
static mut opt_unshare_ipc: bool_0 = 0 as libc::c_int;
#[c2rust::src_loc = "82:13"]
static mut opt_unshare_net: bool_0 = 0 as libc::c_int;
#[c2rust::src_loc = "83:13"]
static mut opt_unshare_uts: bool_0 = 0 as libc::c_int;
#[c2rust::src_loc = "84:13"]
static mut opt_unshare_cgroup: bool_0 = 0 as libc::c_int;
#[c2rust::src_loc = "85:13"]
static mut opt_unshare_cgroup_try: bool_0 = 0 as libc::c_int;
#[c2rust::src_loc = "86:13"]
static mut opt_needs_devpts: bool_0 = 0 as libc::c_int;
#[c2rust::src_loc = "87:13"]
static mut opt_new_session: bool_0 = 0 as libc::c_int;
#[c2rust::src_loc = "88:13"]
static mut opt_die_with_parent: bool_0 = 0 as libc::c_int;
#[c2rust::src_loc = "89:14"]
static mut opt_sandbox_uid: uid_t = -(1 as libc::c_int) as uid_t;
#[c2rust::src_loc = "90:14"]
static mut opt_sandbox_gid: gid_t = -(1 as libc::c_int) as gid_t;
#[c2rust::src_loc = "91:12"]
static mut opt_sync_fd: libc::c_int = -(1 as libc::c_int);
#[c2rust::src_loc = "92:12"]
static mut opt_block_fd: libc::c_int = -(1 as libc::c_int);
#[c2rust::src_loc = "93:12"]
static mut opt_userns_block_fd: libc::c_int = -(1 as libc::c_int);
#[c2rust::src_loc = "94:12"]
static mut opt_info_fd: libc::c_int = -(1 as libc::c_int);
#[c2rust::src_loc = "95:12"]
static mut opt_json_status_fd: libc::c_int = -(1 as libc::c_int);
#[c2rust::src_loc = "96:12"]
static mut opt_seccomp_fd: libc::c_int = -(1 as libc::c_int);
#[c2rust::src_loc = "97:20"]
static mut opt_sandbox_hostname: *const libc::c_char = 0 as *const libc::c_char;
#[c2rust::src_loc = "98:14"]
static mut opt_args_data: *mut libc::c_char = 0 as *const libc::c_char as *mut libc::c_char;
#[c2rust::src_loc = "99:12"]
static mut opt_userns_fd: libc::c_int = -(1 as libc::c_int);
#[c2rust::src_loc = "100:12"]
static mut opt_userns2_fd: libc::c_int = -(1 as libc::c_int);
#[c2rust::src_loc = "101:12"]
static mut opt_pidns_fd: libc::c_int = -(1 as libc::c_int);
#[c2rust::src_loc = "102:12"]
static mut next_perms: libc::c_int = -(1 as libc::c_int);
#[c2rust::src_loc = "103:15"]
static mut next_size_arg: size_t = 0 as libc::c_int as size_t;
#[c2rust::src_loc = "116:15"]
static mut ns_infos: [NsInfo; 7] = unsafe {
    [
        {
            let mut init = _NsInfo {
                name: b"cgroup\0" as *const u8 as *const libc::c_char,
                do_unshare: &opt_unshare_cgroup as *const bool_0 as *mut bool_0,
                id: 0 as libc::c_int as ino_t,
            };
            init
        },
        {
            let mut init = _NsInfo {
                name: b"ipc\0" as *const u8 as *const libc::c_char,
                do_unshare: &opt_unshare_ipc as *const bool_0 as *mut bool_0,
                id: 0 as libc::c_int as ino_t,
            };
            init
        },
        {
            let mut init = _NsInfo {
                name: b"mnt\0" as *const u8 as *const libc::c_char,
                do_unshare: 0 as *const bool_0 as *mut bool_0,
                id: 0 as libc::c_int as ino_t,
            };
            init
        },
        {
            let mut init = _NsInfo {
                name: b"net\0" as *const u8 as *const libc::c_char,
                do_unshare: &opt_unshare_net as *const bool_0 as *mut bool_0,
                id: 0 as libc::c_int as ino_t,
            };
            init
        },
        {
            let mut init = _NsInfo {
                name: b"pid\0" as *const u8 as *const libc::c_char,
                do_unshare: &opt_unshare_pid as *const bool_0 as *mut bool_0,
                id: 0 as libc::c_int as ino_t,
            };
            init
        },
        {
            let mut init = _NsInfo {
                name: b"uts\0" as *const u8 as *const libc::c_char,
                do_unshare: &opt_unshare_uts as *const bool_0 as *mut bool_0,
                id: 0 as libc::c_int as ino_t,
            };
            init
        },
        {
            let mut init = _NsInfo {
                name: 0 as *const libc::c_char,
                do_unshare: 0 as *const bool_0 as *mut bool_0,
                id: 0 as libc::c_int as ino_t,
            };
            init
        },
    ]
};
#[inline]
#[c2rust::src_loc = "226:1"]
unsafe extern "C" fn _op_append_new() -> *mut SetupOp {
    let mut self_0: *mut SetupOp =
        xcalloc(::core::mem::size_of::<SetupOp>() as libc::c_ulong) as *mut SetupOp;
    if !last_op.is_null() {
        (*last_op).next = self_0;
    } else {
        ops = self_0;
    }
    last_op = self_0;
    return self_0;
}
#[c2rust::src_loc = "226:1"]
static mut last_op: *mut SetupOp = 0 as *const SetupOp as *mut SetupOp;
#[c2rust::src_loc = "226:1"]
static mut ops: *mut SetupOp = 0 as *const SetupOp as *mut SetupOp;
#[c2rust::src_loc = "228:1"]
unsafe extern "C" fn setup_op_new(mut type_0: SetupOpType) -> *mut SetupOp {
    let mut op: *mut SetupOp = _op_append_new();
    (*op).type_0 = type_0;
    (*op).fd = -(1 as libc::c_int);
    (*op).flags = 0 as SetupOpFlag;
    return op;
}
#[c2rust::src_loc = "239:1"]
static mut lock_files: *mut LockFile = 0 as *const LockFile as *mut LockFile;
#[inline]
#[c2rust::src_loc = "239:1"]
unsafe extern "C" fn _lock_file_append_new() -> *mut LockFile {
    let mut self_0: *mut LockFile =
        xcalloc(::core::mem::size_of::<LockFile>() as libc::c_ulong) as *mut LockFile;
    if !last_lock_file.is_null() {
        (*last_lock_file).next = self_0;
    } else {
        lock_files = self_0;
    }
    last_lock_file = self_0;
    return self_0;
}
#[c2rust::src_loc = "239:1"]
static mut last_lock_file: *mut LockFile = 0 as *const LockFile as *mut LockFile;
#[c2rust::src_loc = "241:1"]
unsafe extern "C" fn lock_file_new(mut path: *const libc::c_char) -> *mut LockFile {
    let mut lock: *mut LockFile = _lock_file_append_new();
    (*lock).path = path;
    return lock;
}
#[inline]
#[c2rust::src_loc = "258:1"]
unsafe extern "C" fn _seccomp_program_append_new() -> *mut SeccompProgram {
    let mut self_0: *mut SeccompProgram =
        xcalloc(::core::mem::size_of::<SeccompProgram>() as libc::c_ulong) as *mut SeccompProgram;
    if !last_seccomp_program.is_null() {
        (*last_seccomp_program).next = self_0;
    } else {
        seccomp_programs = self_0;
    }
    last_seccomp_program = self_0;
    return self_0;
}
#[c2rust::src_loc = "258:1"]
static mut last_seccomp_program: *mut SeccompProgram =
    0 as *const SeccompProgram as *mut SeccompProgram;
#[c2rust::src_loc = "258:1"]
static mut seccomp_programs: *mut SeccompProgram =
    0 as *const SeccompProgram as *mut SeccompProgram;
#[c2rust::src_loc = "260:1"]
unsafe extern "C" fn seccomp_program_new(mut fd: *mut libc::c_int) -> *mut SeccompProgram {
    let mut self_0: *mut SeccompProgram = _seccomp_program_append_new();
    let mut data: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut len: size_t = 0;
    data = load_file_data(*fd, &mut len);
    if data.is_null() {
        die_with_error(b"Can't read seccomp data\0" as *const u8 as *const libc::c_char);
    }
    close(*fd);
    *fd = -(1 as libc::c_int);
    if len.wrapping_rem(8 as libc::c_int as libc::c_ulong) != 0 as libc::c_int as libc::c_ulong {
        die(b"Invalid seccomp data, must be multiple of 8\0" as *const u8 as *const libc::c_char);
    }
    (*self_0).program.len = len.wrapping_div(8 as libc::c_int as libc::c_ulong) as libc::c_ushort;
    (*self_0).program.filter = (if 0 as libc::c_int != 0 {
        data as *mut libc::c_void
    } else {
        steal_pointer(&mut data as *mut *mut libc::c_char as *mut libc::c_void)
    }) as *mut sock_filter;
    return self_0;
}
#[c2rust::src_loc = "283:1"]
unsafe extern "C" fn seccomp_programs_apply() {
    let mut program: *mut SeccompProgram = 0 as *mut SeccompProgram;
    program = seccomp_programs;
    while !program.is_null() {
        if prctl(
            22 as libc::c_int,
            2 as libc::c_int,
            &mut (*program).program as *mut sock_fprog,
        ) != 0 as libc::c_int
        {
            if *__errno_location() == 22 as libc::c_int {
                die(
                    b"Unable to set up system call filtering as requested: prctl(PR_SET_SECCOMP) reported EINVAL. (Hint: this requires a kernel configured with CONFIG_SECCOMP and CONFIG_SECCOMP_FILTER.)\0"
                        as *const u8 as *const libc::c_char,
                );
            }
            die_with_error(b"prctl(PR_SET_SECCOMP)\0" as *const u8 as *const libc::c_char);
        }
        program = (*program).next;
    }
}
#[c2rust::src_loc = "303:1"]
unsafe extern "C" fn usage(mut ecode: libc::c_int, mut out: *mut FILE) {
    fprintf(
        out,
        b"usage: %s [OPTIONS...] [--] COMMAND [ARGS...]\n\n\0" as *const u8 as *const libc::c_char,
        if !argv0.is_null() {
            argv0
        } else {
            b"bwrap\0" as *const u8 as *const libc::c_char
        },
    );
    fprintf(
        out,
        b"    --help                       Print this help\n    --version                    Print version\n    --args FD                    Parse NUL-separated args from FD\n    --unshare-all                Unshare every namespace we support by default\n    --share-net                  Retain the network namespace (can only combine with --unshare-all)\n    --unshare-user               Create new user namespace (may be automatically implied if not setuid)\n    --unshare-user-try           Create new user namespace if possible else continue by skipping it\n    --unshare-ipc                Create new ipc namespace\n    --unshare-pid                Create new pid namespace\n    --unshare-net                Create new network namespace\n    --unshare-uts                Create new uts namespace\n    --unshare-cgroup             Create new cgroup namespace\n    --unshare-cgroup-try         Create new cgroup namespace if possible else continue by skipping it\n    --userns FD                  Use this user namespace (cannot combine with --unshare-user)\n    --userns2 FD                 After setup switch to this user namespace, only useful with --userns\n    --disable-userns             Disable further use of user namespaces inside sandbox\n    --assert-userns-disabled     Fail unless further use of user namespace inside sandbox is disabled\n    --pidns FD                   Use this pid namespace (as parent namespace if using --unshare-pid)\n    --uid UID                    Custom uid in the sandbox (requires --unshare-user or --userns)\n    --gid GID                    Custom gid in the sandbox (requires --unshare-user or --userns)\n    --hostname NAME              Custom hostname in the sandbox (requires --unshare-uts)\n    --chdir DIR                  Change directory to DIR\n    --clearenv                   Unset all environment variables\n    --setenv VAR VALUE           Set an environment variable\n    --unsetenv VAR               Unset an environment variable\n    --lock-file DEST             Take a lock on DEST while sandbox is running\n    --sync-fd FD                 Keep this fd open while sandbox is running\n    --bind SRC DEST              Bind mount the host path SRC on DEST\n    --bind-try SRC DEST          Equal to --bind but ignores non-existent SRC\n    --dev-bind SRC DEST          Bind mount the host path SRC on DEST, allowing device access\n    --dev-bind-try SRC DEST      Equal to --dev-bind but ignores non-existent SRC\n    --ro-bind SRC DEST           Bind mount the host path SRC readonly on DEST\n    --ro-bind-try SRC DEST       Equal to --ro-bind but ignores non-existent SRC\n    --remount-ro DEST            Remount DEST as readonly; does not recursively remount\n    --exec-label LABEL           Exec label for the sandbox\n    --file-label LABEL           File label for temporary sandbox content\n    --proc DEST                  Mount new procfs on DEST\n    --dev DEST                   Mount new dev on DEST\n    --tmpfs DEST                 Mount new tmpfs on DEST\n    --mqueue DEST                Mount new mqueue on DEST\n    --dir DEST                   Create dir at DEST\n    --file FD DEST               Copy from FD to destination DEST\n    --bind-data FD DEST          Copy from FD to file which is bind-mounted on DEST\n    --ro-bind-data FD DEST       Copy from FD to file which is readonly bind-mounted on DEST\n    --symlink SRC DEST           Create symlink at DEST with target SRC\n    --seccomp FD                 Load and use seccomp rules from FD (not repeatable)\n    --add-seccomp-fd FD          Load and use seccomp rules from FD (repeatable)\n    --block-fd FD                Block on FD until some data to read is available\n    --userns-block-fd FD         Block on FD until the user namespace is ready\n    --info-fd FD                 Write information about the running container to FD\n    --json-status-fd FD          Write container status to FD as multiple JSON documents\n    --new-session                Create a new terminal session\n    --die-with-parent            Kills with SIGKILL child process (COMMAND) when bwrap or bwrap's parent dies.\n    --as-pid-1                   Do not install a reaper process with PID=1\n    --cap-add CAP                Add cap CAP when running as privileged user\n    --cap-drop CAP               Drop cap CAP when running as privileged user\n    --perms OCTAL                Set permissions of next argument (--bind-data, --file, etc.)\n    --size BYTES                 Set size of next argument (only for --tmpfs)\n    --chmod OCTAL PATH           Change permissions of PATH (must already exist)\n\0"
            as *const u8 as *const libc::c_char,
    );
    exit(ecode);
}
#[c2rust::src_loc = "375:1"]
unsafe extern "C" fn handle_die_with_parent() {
    if opt_die_with_parent != 0
        && prctl(
            1 as libc::c_int,
            9 as libc::c_int,
            0 as libc::c_int,
            0 as libc::c_int,
            0 as libc::c_int,
        ) != 0 as libc::c_int
    {
        die_with_error(b"prctl\0" as *const u8 as *const libc::c_char);
    }
}
#[c2rust::src_loc = "382:1"]
unsafe extern "C" fn block_sigchild() {
    let mut mask: sigset_t = sigset_t { __val: [0; 16] };
    let mut status: libc::c_int = 0;
    sigemptyset(&mut mask);
    sigaddset(&mut mask, 17 as libc::c_int);
    if sigprocmask(0 as libc::c_int, &mut mask, 0 as *mut sigset_t) == -(1 as libc::c_int) {
        die_with_error(b"sigprocmask\0" as *const u8 as *const libc::c_char);
    }
    while waitpid(-(1 as libc::c_int), &mut status, 1 as libc::c_int) > 0 as libc::c_int {}
}
#[c2rust::src_loc = "399:1"]
unsafe extern "C" fn unblock_sigchild() {
    let mut mask: sigset_t = sigset_t { __val: [0; 16] };
    sigemptyset(&mut mask);
    sigaddset(&mut mask, 17 as libc::c_int);
    if sigprocmask(1 as libc::c_int, &mut mask, 0 as *mut sigset_t) == -(1 as libc::c_int) {
        die_with_error(b"sigprocmask\0" as *const u8 as *const libc::c_char);
    }
}
#[c2rust::src_loc = "412:1"]
unsafe extern "C" fn close_extra_fds(
    mut data: *mut libc::c_void,
    mut fd: libc::c_int,
) -> libc::c_int {
    let mut extra_fds: *mut libc::c_int = data as *mut libc::c_int;
    let mut i: libc::c_int = 0;
    i = 0 as libc::c_int;
    while *extra_fds.offset(i as isize) != -(1 as libc::c_int) {
        if fd == *extra_fds.offset(i as isize) {
            return 0 as libc::c_int;
        }
        i += 1;
        i;
    }
    if fd <= 2 as libc::c_int {
        return 0 as libc::c_int;
    }
    close(fd);
    return 0 as libc::c_int;
}
#[c2rust::src_loc = "429:1"]
unsafe extern "C" fn propagate_exit_status(mut status: libc::c_int) -> libc::c_int {
    if status & 0x7f as libc::c_int == 0 as libc::c_int {
        return (status & 0xff00 as libc::c_int) >> 8 as libc::c_int;
    }
    if ((status & 0x7f as libc::c_int) + 1 as libc::c_int) as libc::c_schar as libc::c_int
        >> 1 as libc::c_int
        > 0 as libc::c_int
    {
        return 128 as libc::c_int + (status & 0x7f as libc::c_int);
    }
    return 255 as libc::c_int;
}
#[c2rust::src_loc = "448:1"]
unsafe extern "C" fn dump_info(
    mut fd: libc::c_int,
    mut output: *const libc::c_char,
    mut exit_on_error: bool_0,
) {
    let mut len: size_t = strlen(output);
    if write_to_fd(fd, output, len as ssize_t) != 0 {
        if exit_on_error != 0 {
            die_with_error(b"Write to info_fd\0" as *const u8 as *const libc::c_char);
        }
    }
}
#[c2rust::src_loc = "459:1"]
unsafe extern "C" fn report_child_exit_status(
    mut exitc: libc::c_int,
    mut setup_finished_fd: libc::c_int,
) {
    let mut s: ssize_t = 0;
    let mut data: [libc::c_char; 2] = [0; 2];
    let mut output: *mut libc::c_char = 0 as *mut libc::c_char;
    if opt_json_status_fd == -(1 as libc::c_int) || setup_finished_fd == -(1 as libc::c_int) {
        return;
    }
    s = ({
        let mut __result: libc::c_long = 0;
        loop {
            __result = read(
                setup_finished_fd,
                data.as_mut_ptr() as *mut libc::c_void,
                ::core::mem::size_of::<[libc::c_char; 2]>() as libc::c_ulong,
            );
            if !(__result == -(1 as libc::c_long) && *__errno_location() == 4 as libc::c_int) {
                break;
            }
        }
        __result
    });
    if s == -(1 as libc::c_int) as libc::c_long && *__errno_location() != 11 as libc::c_int {
        die_with_error(b"read eventfd\0" as *const u8 as *const libc::c_char);
    }
    if s != 1 as libc::c_int as libc::c_long {
        return;
    }
    output = xasprintf(
        b"{ \"exit-code\": %i }\n\0" as *const u8 as *const libc::c_char,
        exitc,
    );
    dump_info(opt_json_status_fd, output, 0 as libc::c_int);
    close(opt_json_status_fd);
    opt_json_status_fd = -(1 as libc::c_int);
    close(setup_finished_fd);
}
#[c2rust::src_loc = "487:1"]
unsafe extern "C" fn monitor_child(
    mut event_fd: libc::c_int,
    mut child_pid: pid_t,
    mut setup_finished_fd: libc::c_int,
) -> libc::c_int {
    let mut res: libc::c_int = 0;
    let mut val: uint64_t = 0;
    let mut s: ssize_t = 0;
    let mut signal_fd: libc::c_int = 0;
    let mut mask: sigset_t = sigset_t { __val: [0; 16] };
    let mut fds: [pollfd; 2] = [pollfd {
        fd: 0,
        events: 0,
        revents: 0,
    }; 2];
    let mut num_fds: libc::c_int = 0;
    let mut fdsi: signalfd_siginfo = signalfd_siginfo {
        ssi_signo: 0,
        ssi_errno: 0,
        ssi_code: 0,
        ssi_pid: 0,
        ssi_uid: 0,
        ssi_fd: 0,
        ssi_tid: 0,
        ssi_band: 0,
        ssi_overrun: 0,
        ssi_trapno: 0,
        ssi_status: 0,
        ssi_int: 0,
        ssi_ptr: 0,
        ssi_utime: 0,
        ssi_stime: 0,
        ssi_addr: 0,
        ssi_addr_lsb: 0,
        __pad2: 0,
        ssi_syscall: 0,
        ssi_call_addr: 0,
        ssi_arch: 0,
        __pad: [0; 28],
    };
    let mut dont_close: [libc::c_int; 4] = [
        -(1 as libc::c_int),
        -(1 as libc::c_int),
        -(1 as libc::c_int),
        -(1 as libc::c_int),
    ];
    let mut j: libc::c_uint = 0 as libc::c_int as libc::c_uint;
    let mut exitc: libc::c_int = 0;
    let mut died_pid: pid_t = 0;
    let mut died_status: libc::c_int = 0;
    if event_fd != -(1 as libc::c_int) {
        let fresh0 = j;
        j = j.wrapping_add(1);
        dont_close[fresh0 as usize] = event_fd;
    }
    if opt_json_status_fd != -(1 as libc::c_int) {
        let fresh1 = j;
        j = j.wrapping_add(1);
        dont_close[fresh1 as usize] = opt_json_status_fd;
    }
    if setup_finished_fd != -(1 as libc::c_int) {
        let fresh2 = j;
        j = j.wrapping_add(1);
        dont_close[fresh2 as usize] = setup_finished_fd;
    }
    if (j as libc::c_ulong)
        < (::core::mem::size_of::<[libc::c_int; 4]>() as libc::c_ulong)
            .wrapping_div(::core::mem::size_of::<libc::c_int>() as libc::c_ulong)
    {
    } else {
        __assert_fail(
            b"j < sizeof(dont_close)/sizeof(*dont_close)\0" as *const u8 as *const libc::c_char,
            b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
            512 as libc::c_int as libc::c_uint,
            (*::core::mem::transmute::<&[u8; 35], &[libc::c_char; 35]>(
                b"int monitor_child(int, pid_t, int)\0",
            ))
            .as_ptr(),
        );
    }
    'c_7026: {
        if (j as libc::c_ulong)
            < (::core::mem::size_of::<[libc::c_int; 4]>() as libc::c_ulong)
                .wrapping_div(::core::mem::size_of::<libc::c_int>() as libc::c_ulong)
        {
        } else {
            __assert_fail(
                b"j < sizeof(dont_close)/sizeof(*dont_close)\0" as *const u8 as *const libc::c_char,
                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                512 as libc::c_int as libc::c_uint,
                (*::core::mem::transmute::<&[u8; 35], &[libc::c_char; 35]>(
                    b"int monitor_child(int, pid_t, int)\0",
                ))
                .as_ptr(),
            );
        }
    };
    fdwalk(
        proc_fd,
        Some(
            close_extra_fds as unsafe extern "C" fn(*mut libc::c_void, libc::c_int) -> libc::c_int,
        ),
        dont_close.as_mut_ptr() as *mut libc::c_void,
    );
    sigemptyset(&mut mask);
    sigaddset(&mut mask, 17 as libc::c_int);
    signal_fd = signalfd(
        -(1 as libc::c_int),
        &mut mask,
        SFD_CLOEXEC as libc::c_int | SFD_NONBLOCK as libc::c_int,
    );
    if signal_fd == -(1 as libc::c_int) {
        die_with_error(b"Can't create signalfd\0" as *const u8 as *const libc::c_char);
    }
    num_fds = 1 as libc::c_int;
    fds[0 as libc::c_int as usize].fd = signal_fd;
    fds[0 as libc::c_int as usize].events = 0x1 as libc::c_int as libc::c_short;
    if event_fd != -(1 as libc::c_int) {
        fds[1 as libc::c_int as usize].fd = event_fd;
        fds[1 as libc::c_int as usize].events = 0x1 as libc::c_int as libc::c_short;
        num_fds += 1;
        num_fds;
    }
    loop {
        fds[1 as libc::c_int as usize].revents = 0 as libc::c_int as libc::c_short;
        fds[0 as libc::c_int as usize].revents = fds[1 as libc::c_int as usize].revents;
        res = poll(fds.as_mut_ptr(), num_fds as nfds_t, -(1 as libc::c_int));
        if res == -(1 as libc::c_int) && *__errno_location() != 4 as libc::c_int {
            die_with_error(b"poll\0" as *const u8 as *const libc::c_char);
        }
        if event_fd != -(1 as libc::c_int) {
            s = read(
                event_fd,
                &mut val as *mut uint64_t as *mut libc::c_void,
                8 as libc::c_int as size_t,
            );
            if s == -(1 as libc::c_int) as libc::c_long
                && *__errno_location() != 4 as libc::c_int
                && *__errno_location() != 11 as libc::c_int
            {
                die_with_error(b"read eventfd\0" as *const u8 as *const libc::c_char);
            } else if s == 8 as libc::c_int as libc::c_long {
                exitc = val as libc::c_int - 1 as libc::c_int;
                report_child_exit_status(exitc, setup_finished_fd);
                return exitc;
            }
        }
        s = read(
            signal_fd,
            &mut fdsi as *mut signalfd_siginfo as *mut libc::c_void,
            ::core::mem::size_of::<signalfd_siginfo>() as libc::c_ulong,
        );
        if s == -(1 as libc::c_int) as libc::c_long
            && *__errno_location() != 4 as libc::c_int
            && *__errno_location() != 11 as libc::c_int
        {
            die_with_error(b"read signalfd\0" as *const u8 as *const libc::c_char);
        }
        loop {
            died_pid = waitpid(-(1 as libc::c_int), &mut died_status, 1 as libc::c_int);
            if !(died_pid > 0 as libc::c_int) {
                break;
            }
            if died_pid == child_pid {
                exitc = propagate_exit_status(died_status);
                report_child_exit_status(exitc, setup_finished_fd);
                return exitc;
            }
        }
    }
}
#[c2rust::src_loc = "589:1"]
unsafe extern "C" fn do_init(mut event_fd: libc::c_int, mut initial_pid: pid_t) -> libc::c_int {
    let mut initial_exit_status: libc::c_int = 1 as libc::c_int;
    let mut lock: *mut LockFile = 0 as *mut LockFile;
    lock = lock_files;
    while !lock.is_null() {
        let mut fd: libc::c_int = open((*lock).path, 0 as libc::c_int | 0o2000000 as libc::c_int);
        if fd == -(1 as libc::c_int) {
            die_with_error(
                b"Unable to open lock file %s\0" as *const u8 as *const libc::c_char,
                (*lock).path,
            );
        }
        let mut l: flock = {
            let mut init = flock {
                l_type: 0 as libc::c_int as libc::c_short,
                l_whence: 0 as libc::c_int as libc::c_short,
                l_start: 0 as libc::c_int as __off64_t,
                l_len: 0 as libc::c_int as __off64_t,
                l_pid: 0,
            };
            init
        };
        if fcntl(fd, 6 as libc::c_int, &mut l as *mut flock) < 0 as libc::c_int {
            die_with_error(
                b"Unable to lock file %s\0" as *const u8 as *const libc::c_char,
                (*lock).path,
            );
        }
        (*lock).fd = fd;
        lock = (*lock).next;
    }
    handle_die_with_parent();
    seccomp_programs_apply();
    loop {
        let mut child: pid_t = 0;
        let mut status: libc::c_int = 0;
        child = wait(&mut status);
        if child == initial_pid {
            initial_exit_status = propagate_exit_status(status);
            if event_fd != -(1 as libc::c_int) {
                let mut val: uint64_t = 0;
                let mut res: libc::c_int = 0;
                val = (initial_exit_status + 1 as libc::c_int) as uint64_t;
                res = write(
                    event_fd,
                    &mut val as *mut uint64_t as *const libc::c_void,
                    8 as libc::c_int as size_t,
                ) as libc::c_int;
            }
        }
        if !(child == -(1 as libc::c_int) && *__errno_location() != 4 as libc::c_int) {
            continue;
        }
        if *__errno_location() != 10 as libc::c_int {
            die_with_error(b"init wait()\0" as *const u8 as *const libc::c_char);
        }
        break;
    }
    lock = lock_files;
    while !lock.is_null() {
        if (*lock).fd >= 0 as libc::c_int {
            close((*lock).fd);
            (*lock).fd = -(1 as libc::c_int);
        }
        lock = (*lock).next;
    }
    return initial_exit_status;
}
#[c2rust::src_loc = "667:13"]
static mut opt_cap_add_or_drop_used: bool_0 = 0;
#[c2rust::src_loc = "669:17"]
static mut requested_caps: [uint32_t; 2] =
    [0 as libc::c_int as uint32_t, 0 as libc::c_int as uint32_t];
#[c2rust::src_loc = "677:1"]
unsafe extern "C" fn set_required_caps() {
    let mut hdr: __user_cap_header_struct = {
        let mut init = __user_cap_header_struct {
            version: 0x20080522 as libc::c_int as __u32,
            pid: 0 as libc::c_int,
        };
        init
    };
    let mut data: [__user_cap_data_struct; 2] = [
        {
            let mut init = __user_cap_data_struct {
                effective: 0 as libc::c_int as __u32,
                permitted: 0,
                inheritable: 0,
            };
            init
        },
        __user_cap_data_struct {
            effective: 0,
            permitted: 0,
            inheritable: 0,
        },
    ];
    data[0 as libc::c_int as usize].effective =
        ((1 as libc::c_long) << (21 as libc::c_int & 31 as libc::c_int)
            | (1 as libc::c_long) << (18 as libc::c_int & 31 as libc::c_int)
            | (1 as libc::c_long) << (12 as libc::c_int & 31 as libc::c_int)
            | (1 as libc::c_long) << (7 as libc::c_int & 31 as libc::c_int)
            | (1 as libc::c_long) << (6 as libc::c_int & 31 as libc::c_int)
            | (1 as libc::c_long) << (19 as libc::c_int & 31 as libc::c_int)) as __u32;
    data[0 as libc::c_int as usize].permitted =
        ((1 as libc::c_long) << (21 as libc::c_int & 31 as libc::c_int)
            | (1 as libc::c_long) << (18 as libc::c_int & 31 as libc::c_int)
            | (1 as libc::c_long) << (12 as libc::c_int & 31 as libc::c_int)
            | (1 as libc::c_long) << (7 as libc::c_int & 31 as libc::c_int)
            | (1 as libc::c_long) << (6 as libc::c_int & 31 as libc::c_int)
            | (1 as libc::c_long) << (19 as libc::c_int & 31 as libc::c_int)) as __u32;
    data[0 as libc::c_int as usize].inheritable = 0 as libc::c_int as __u32;
    data[1 as libc::c_int as usize].effective = 0 as libc::c_int as __u32;
    data[1 as libc::c_int as usize].permitted = 0 as libc::c_int as __u32;
    data[1 as libc::c_int as usize].inheritable = 0 as libc::c_int as __u32;
    if capset(&mut hdr, data.as_mut_ptr()) < 0 as libc::c_int {
        die_with_error(b"capset failed\0" as *const u8 as *const libc::c_char);
    }
}
#[c2rust::src_loc = "694:1"]
unsafe extern "C" fn drop_all_caps(mut keep_requested_caps: bool_0) {
    let mut hdr: __user_cap_header_struct = {
        let mut init = __user_cap_header_struct {
            version: 0x20080522 as libc::c_int as __u32,
            pid: 0 as libc::c_int,
        };
        init
    };
    let mut data: [__user_cap_data_struct; 2] = [
        {
            let mut init = __user_cap_data_struct {
                effective: 0 as libc::c_int as __u32,
                permitted: 0,
                inheritable: 0,
            };
            init
        },
        __user_cap_data_struct {
            effective: 0,
            permitted: 0,
            inheritable: 0,
        },
    ];
    if keep_requested_caps != 0 {
        if opt_cap_add_or_drop_used == 0 && real_uid == 0 as libc::c_int as libc::c_uint {
            if is_privileged == 0 {
            } else {
                __assert_fail(
                    b"!is_privileged\0" as *const u8 as *const libc::c_char,
                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                    709 as libc::c_int as libc::c_uint,
                    (*::core::mem::transmute::<&[u8; 25], &[libc::c_char; 25]>(
                        b"void drop_all_caps(bool)\0",
                    ))
                    .as_ptr(),
                );
            }
            'c_7753: {
                if is_privileged == 0 {
                } else {
                    __assert_fail(
                        b"!is_privileged\0" as *const u8 as *const libc::c_char,
                        b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                        709 as libc::c_int as libc::c_uint,
                        (*::core::mem::transmute::<&[u8; 25], &[libc::c_char; 25]>(
                            b"void drop_all_caps(bool)\0",
                        ))
                        .as_ptr(),
                    );
                }
            };
            return;
        }
        data[0 as libc::c_int as usize].effective = requested_caps[0 as libc::c_int as usize];
        data[0 as libc::c_int as usize].permitted = requested_caps[0 as libc::c_int as usize];
        data[0 as libc::c_int as usize].inheritable = requested_caps[0 as libc::c_int as usize];
        data[1 as libc::c_int as usize].effective = requested_caps[1 as libc::c_int as usize];
        data[1 as libc::c_int as usize].permitted = requested_caps[1 as libc::c_int as usize];
        data[1 as libc::c_int as usize].inheritable = requested_caps[1 as libc::c_int as usize];
    }
    if capset(&mut hdr, data.as_mut_ptr()) < 0 as libc::c_int {
        if *__errno_location() == 1 as libc::c_int
            && real_uid == 0 as libc::c_int as libc::c_uint
            && is_privileged == 0
        {
            return;
        } else {
            die_with_error(b"capset failed\0" as *const u8 as *const libc::c_char);
        }
    }
}
#[c2rust::src_loc = "734:1"]
unsafe extern "C" fn has_caps() -> bool_0 {
    let mut hdr: __user_cap_header_struct = {
        let mut init = __user_cap_header_struct {
            version: 0x20080522 as libc::c_int as __u32,
            pid: 0 as libc::c_int,
        };
        init
    };
    let mut data: [__user_cap_data_struct; 2] = [
        {
            let mut init = __user_cap_data_struct {
                effective: 0 as libc::c_int as __u32,
                permitted: 0,
                inheritable: 0,
            };
            init
        },
        __user_cap_data_struct {
            effective: 0,
            permitted: 0,
            inheritable: 0,
        },
    ];
    if capget(&mut hdr, data.as_mut_ptr()) < 0 as libc::c_int {
        die_with_error(b"capget failed\0" as *const u8 as *const libc::c_char);
    }
    return (data[0 as libc::c_int as usize].permitted != 0 as libc::c_int as libc::c_uint
        || data[1 as libc::c_int as usize].permitted != 0 as libc::c_int as libc::c_uint)
        as libc::c_int;
}
#[c2rust::src_loc = "750:1"]
unsafe extern "C" fn prctl_caps(
    mut caps: *mut uint32_t,
    mut do_cap_bounding: bool_0,
    mut do_set_ambient: bool_0,
) {
    let mut cap: libc::c_ulong = 0;
    cap = 0 as libc::c_int as libc::c_ulong;
    while cap <= 40 as libc::c_int as libc::c_ulong {
        let mut keep: bool_0 = 0 as libc::c_int;
        if cap < 32 as libc::c_int as libc::c_ulong {
            if (1 as libc::c_long) << (cap & 31 as libc::c_int as libc::c_ulong)
                & *caps.offset(0 as libc::c_int as isize) as libc::c_long
                != 0
            {
                keep = 1 as libc::c_int;
            }
        } else if (1 as libc::c_long)
            << (cap.wrapping_sub(32 as libc::c_int as libc::c_ulong)
                & 31 as libc::c_int as libc::c_ulong)
            & *caps.offset(1 as libc::c_int as isize) as libc::c_long
            != 0
        {
            keep = 1 as libc::c_int;
        }
        if keep != 0 && do_set_ambient != 0 {
            let mut res: libc::c_int = prctl(
                47 as libc::c_int,
                2 as libc::c_int,
                cap,
                0 as libc::c_int,
                0 as libc::c_int,
            );
            if res == -(1 as libc::c_int)
                && !(*__errno_location() == 22 as libc::c_int
                    || *__errno_location() == 1 as libc::c_int)
            {
                die_with_error(
                    b"Adding ambient capability %ld\0" as *const u8 as *const libc::c_char,
                    cap,
                );
            }
        }
        if keep == 0 && do_cap_bounding != 0 {
            let mut res_0: libc::c_int = prctl(
                24 as libc::c_int,
                cap,
                0 as libc::c_int,
                0 as libc::c_int,
                0 as libc::c_int,
            );
            if res_0 == -(1 as libc::c_int)
                && !(*__errno_location() == 22 as libc::c_int
                    || *__errno_location() == 1 as libc::c_int)
            {
                die_with_error(
                    b"Dropping capability %ld from bounds\0" as *const u8 as *const libc::c_char,
                    cap,
                );
            }
        }
        cap = cap.wrapping_add(1);
        cap;
    }
}
#[c2rust::src_loc = "798:1"]
unsafe extern "C" fn drop_cap_bounding_set(mut drop_all: bool_0) {
    if drop_all == 0 {
        prctl_caps(
            requested_caps.as_mut_ptr(),
            1 as libc::c_int,
            0 as libc::c_int,
        );
    } else {
        let mut no_caps: [uint32_t; 2] =
            [0 as libc::c_int as uint32_t, 0 as libc::c_int as uint32_t];
        prctl_caps(no_caps.as_mut_ptr(), 1 as libc::c_int, 0 as libc::c_int);
    };
}
#[c2rust::src_loc = "810:1"]
unsafe extern "C" fn set_ambient_capabilities() {
    if is_privileged != 0 {
        return;
    }
    prctl_caps(
        requested_caps.as_mut_ptr(),
        0 as libc::c_int,
        1 as libc::c_int,
    );
}
#[c2rust::src_loc = "832:1"]
unsafe extern "C" fn acquire_privs() {
    let mut euid: uid_t = 0;
    let mut new_fsuid: uid_t = 0;
    euid = geteuid();
    if real_uid != euid {
        if euid != 0 as libc::c_int as libc::c_uint {
            die(
                b"Unexpected setuid user %d, should be 0\0" as *const u8 as *const libc::c_char,
                euid,
            );
        }
        is_privileged = 1 as libc::c_int;
        if setfsuid(real_uid) < 0 as libc::c_int {
            die_with_error(b"Unable to set fsuid\0" as *const u8 as *const libc::c_char);
        }
        new_fsuid = setfsuid(-(1 as libc::c_int) as __uid_t) as uid_t;
        if new_fsuid != real_uid {
            die(
                b"Unable to set fsuid (was %d)\0" as *const u8 as *const libc::c_char,
                new_fsuid as libc::c_int,
            );
        }
        drop_cap_bounding_set(1 as libc::c_int);
        set_required_caps();
    } else if real_uid != 0 as libc::c_int as libc::c_uint && has_caps() != 0 {
        die(
            b"Unexpected capabilities but not setuid, old file caps config?\0" as *const u8
                as *const libc::c_char,
        );
    } else if real_uid == 0 as libc::c_int as libc::c_uint {
        let mut hdr: __user_cap_header_struct = {
            let mut init = __user_cap_header_struct {
                version: 0x20080522 as libc::c_int as __u32,
                pid: 0 as libc::c_int,
            };
            init
        };
        let mut data: [__user_cap_data_struct; 2] = [
            {
                let mut init = __user_cap_data_struct {
                    effective: 0 as libc::c_int as __u32,
                    permitted: 0,
                    inheritable: 0,
                };
                init
            },
            __user_cap_data_struct {
                effective: 0,
                permitted: 0,
                inheritable: 0,
            },
        ];
        if capget(&mut hdr, data.as_mut_ptr()) < 0 as libc::c_int {
            die_with_error(b"capget (for uid == 0) failed\0" as *const u8 as *const libc::c_char);
        }
        requested_caps[0 as libc::c_int as usize] = data[0 as libc::c_int as usize].effective;
        requested_caps[1 as libc::c_int as usize] = data[1 as libc::c_int as usize].effective;
    }
}
#[c2rust::src_loc = "898:1"]
unsafe extern "C" fn switch_to_user_with_privs() {
    if opt_unshare_user != 0 || opt_userns_fd != -(1 as libc::c_int) {
        drop_cap_bounding_set(0 as libc::c_int);
    }
    if opt_userns_fd != -(1 as libc::c_int) {
        if opt_sandbox_uid != real_uid && setuid(opt_sandbox_uid) < 0 as libc::c_int {
            die_with_error(
                b"unable to switch to uid %d\0" as *const u8 as *const libc::c_char,
                opt_sandbox_uid,
            );
        }
        if opt_sandbox_gid != real_gid && setgid(opt_sandbox_gid) < 0 as libc::c_int {
            die_with_error(
                b"unable to switch to gid %d\0" as *const u8 as *const libc::c_char,
                opt_sandbox_gid,
            );
        }
    }
    if is_privileged == 0 {
        return;
    }
    if prctl(
        8 as libc::c_int,
        1 as libc::c_int,
        0 as libc::c_int,
        0 as libc::c_int,
        0 as libc::c_int,
    ) < 0 as libc::c_int
    {
        die_with_error(b"prctl(PR_SET_KEEPCAPS) failed\0" as *const u8 as *const libc::c_char);
    }
    if setuid(opt_sandbox_uid) < 0 as libc::c_int {
        die_with_error(b"unable to drop root uid\0" as *const u8 as *const libc::c_char);
    }
    set_required_caps();
}
#[c2rust::src_loc = "930:1"]
unsafe extern "C" fn drop_privs(mut keep_requested_caps: bool_0, mut already_changed_uid: bool_0) {
    if keep_requested_caps == 0 || is_privileged == 0 {
    } else {
        __assert_fail(
            b"!keep_requested_caps || !is_privileged\0" as *const u8 as *const libc::c_char,
            b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
            934 as libc::c_int as libc::c_uint,
            (*::core::mem::transmute::<&[u8; 28], &[libc::c_char; 28]>(
                b"void drop_privs(bool, bool)\0",
            ))
            .as_ptr(),
        );
    }
    'c_8430: {
        if keep_requested_caps == 0 || is_privileged == 0 {
        } else {
            __assert_fail(
                b"!keep_requested_caps || !is_privileged\0" as *const u8 as *const libc::c_char,
                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                934 as libc::c_int as libc::c_uint,
                (*::core::mem::transmute::<&[u8; 28], &[libc::c_char; 28]>(
                    b"void drop_privs(bool, bool)\0",
                ))
                .as_ptr(),
            );
        }
    };
    if is_privileged != 0 && already_changed_uid == 0 && setuid(opt_sandbox_uid) < 0 as libc::c_int
    {
        die_with_error(b"unable to drop root uid\0" as *const u8 as *const libc::c_char);
    }
    drop_all_caps(keep_requested_caps);
    if prctl(
        4 as libc::c_int,
        1 as libc::c_int,
        0 as libc::c_int,
        0 as libc::c_int,
        0 as libc::c_int,
    ) != 0 as libc::c_int
    {
        die_with_error(b"can't set dumpable\0" as *const u8 as *const libc::c_char);
    }
}
#[c2rust::src_loc = "947:1"]
unsafe extern "C" fn get_newroot_path(mut path: *const libc::c_char) -> *mut libc::c_char {
    while *path as libc::c_int == '/' as i32 {
        path = path.offset(1);
        path;
    }
    return strconcat(b"/newroot/\0" as *const u8 as *const libc::c_char, path);
}
#[c2rust::src_loc = "955:1"]
unsafe extern "C" fn write_uid_gid_map(
    mut sandbox_uid: uid_t,
    mut parent_uid: uid_t,
    mut sandbox_gid: uid_t,
    mut parent_gid: uid_t,
    mut pid: pid_t,
    mut deny_groups: bool_0,
    mut map_root: bool_0,
) {
    let mut uid_map: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut gid_map: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut dir: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut dir_fd: libc::c_int = -(1 as libc::c_int);
    let mut old_fsuid: uid_t = -(1 as libc::c_int) as uid_t;
    if pid == -(1 as libc::c_int) {
        dir = xstrdup(b"self\0" as *const u8 as *const libc::c_char);
    } else {
        dir = xasprintf(b"%d\0" as *const u8 as *const libc::c_char, pid);
    }
    dir_fd = openat(proc_fd, dir, 0o10000000 as libc::c_int);
    if dir_fd < 0 as libc::c_int {
        die_with_error(
            b"open /proc/%s failed\0" as *const u8 as *const libc::c_char,
            dir,
        );
    }
    if map_root != 0
        && parent_uid != 0 as libc::c_int as libc::c_uint
        && sandbox_uid != 0 as libc::c_int as libc::c_uint
    {
        uid_map = xasprintf(
            b"0 %d 1\n%d %d 1\n\0" as *const u8 as *const libc::c_char,
            overflow_uid,
            sandbox_uid,
            parent_uid,
        );
    } else {
        uid_map = xasprintf(
            b"%d %d 1\n\0" as *const u8 as *const libc::c_char,
            sandbox_uid,
            parent_uid,
        );
    }
    if map_root != 0
        && parent_gid != 0 as libc::c_int as libc::c_uint
        && sandbox_gid != 0 as libc::c_int as libc::c_uint
    {
        gid_map = xasprintf(
            b"0 %d 1\n%d %d 1\n\0" as *const u8 as *const libc::c_char,
            overflow_gid,
            sandbox_gid,
            parent_gid,
        );
    } else {
        gid_map = xasprintf(
            b"%d %d 1\n\0" as *const u8 as *const libc::c_char,
            sandbox_gid,
            parent_gid,
        );
    }
    if is_privileged != 0 {
        old_fsuid = setfsuid(0 as libc::c_int as __uid_t) as uid_t;
    }
    if write_file_at(
        dir_fd,
        b"uid_map\0" as *const u8 as *const libc::c_char,
        uid_map,
    ) != 0 as libc::c_int
    {
        die_with_error(b"setting up uid map\0" as *const u8 as *const libc::c_char);
    }
    if deny_groups != 0
        && write_file_at(
            dir_fd,
            b"setgroups\0" as *const u8 as *const libc::c_char,
            b"deny\n\0" as *const u8 as *const libc::c_char,
        ) != 0 as libc::c_int
    {
        if *__errno_location() != 2 as libc::c_int {
            die_with_error(b"error writing to setgroups\0" as *const u8 as *const libc::c_char);
        }
    }
    if write_file_at(
        dir_fd,
        b"gid_map\0" as *const u8 as *const libc::c_char,
        gid_map,
    ) != 0 as libc::c_int
    {
        die_with_error(b"setting up gid map\0" as *const u8 as *const libc::c_char);
    }
    if is_privileged != 0 {
        setfsuid(old_fsuid);
        if setfsuid(-(1 as libc::c_int) as __uid_t) as uid_t != real_uid {
            die(b"Unable to re-set fsuid\0" as *const u8 as *const libc::c_char);
        }
    }
}
#[c2rust::src_loc = "1022:1"]
unsafe extern "C" fn privileged_op(
    mut privileged_op_socket: libc::c_int,
    mut op: uint32_t,
    mut flags: uint32_t,
    mut perms: uint32_t,
    mut size_arg: size_t,
    mut arg1: *const libc::c_char,
    mut arg2: *const libc::c_char,
) {
    let mut bind_result: bind_mount_result = BIND_MOUNT_SUCCESS;
    let mut failing_path: *mut libc::c_char = 0 as *mut libc::c_char;
    if privileged_op_socket != -(1 as libc::c_int) {
        let mut buffer: [uint32_t; 2048] = [0; 2048];
        let mut op_buffer: *mut PrivSepOp = buffer.as_mut_ptr() as *mut PrivSepOp;
        let mut buffer_size: size_t = ::core::mem::size_of::<PrivSepOp>() as libc::c_ulong;
        let mut arg1_offset: uint32_t = 0 as libc::c_int as uint32_t;
        let mut arg2_offset: uint32_t = 0 as libc::c_int as uint32_t;
        if !arg1.is_null() {
            arg1_offset = buffer_size as uint32_t;
            buffer_size = (buffer_size as libc::c_ulong)
                .wrapping_add((strlen(arg1)).wrapping_add(1 as libc::c_int as libc::c_ulong))
                as size_t as size_t;
        }
        if !arg2.is_null() {
            arg2_offset = buffer_size as uint32_t;
            buffer_size = (buffer_size as libc::c_ulong)
                .wrapping_add((strlen(arg2)).wrapping_add(1 as libc::c_int as libc::c_ulong))
                as size_t as size_t;
        }
        if buffer_size >= ::core::mem::size_of::<[uint32_t; 2048]>() as libc::c_ulong {
            die(b"privilege separation operation to large\0" as *const u8 as *const libc::c_char);
        }
        (*op_buffer).op = op;
        (*op_buffer).flags = flags;
        (*op_buffer).perms = perms;
        (*op_buffer).size_arg = size_arg;
        (*op_buffer).arg1_offset = arg1_offset;
        (*op_buffer).arg2_offset = arg2_offset;
        if !arg1.is_null() {
            strcpy(
                (buffer.as_mut_ptr() as *mut libc::c_char).offset(arg1_offset as isize),
                arg1,
            );
        }
        if !arg2.is_null() {
            strcpy(
                (buffer.as_mut_ptr() as *mut libc::c_char).offset(arg2_offset as isize),
                arg2,
            );
        }
        if write(
            privileged_op_socket,
            buffer.as_mut_ptr() as *const libc::c_void,
            buffer_size,
        ) != buffer_size as ssize_t
        {
            die(b"Can't write to privileged_op_socket\0" as *const u8 as *const libc::c_char);
        }
        if read(
            privileged_op_socket,
            buffer.as_mut_ptr() as *mut libc::c_void,
            1 as libc::c_int as size_t,
        ) != 1 as libc::c_int as libc::c_long
        {
            die(b"Can't read from privileged_op_socket\0" as *const u8 as *const libc::c_char);
        }
        return;
    }
    match op {
        0 => {}
        6 => {
            bind_result = bind_mount(
                proc_fd,
                0 as *const libc::c_char,
                arg2,
                BIND_READONLY,
                &mut failing_path,
            );
            if bind_result as libc::c_uint != BIND_MOUNT_SUCCESS as libc::c_int as libc::c_uint {
                die_with_bind_result(
                    bind_result,
                    *__errno_location(),
                    failing_path,
                    b"Can't remount readonly on %s\0" as *const u8 as *const libc::c_char,
                    arg2,
                );
            }
            if failing_path.is_null() {
            } else {
                __assert_fail(
                    b"failing_path == NULL\0" as *const u8 as *const libc::c_char,
                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                    1105 as libc::c_int as libc::c_uint,
                    (*::core::mem::transmute::<
                        &[u8; 90],
                        &[libc::c_char; 90],
                    >(
                        b"void privileged_op(int, uint32_t, uint32_t, uint32_t, size_t, const char *, const char *)\0",
                    ))
                        .as_ptr(),
                );
            }
            'c_9163: {
                if failing_path.is_null() {
                } else {
                    __assert_fail(
                        b"failing_path == NULL\0" as *const u8 as *const libc::c_char,
                        b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                        1105 as libc::c_int as libc::c_uint,
                        (*::core::mem::transmute::<
                            &[u8; 90],
                            &[libc::c_char; 90],
                        >(
                            b"void privileged_op(int, uint32_t, uint32_t, uint32_t, size_t, const char *, const char *)\0",
                        ))
                            .as_ptr(),
                    );
                }
            };
        }
        1 => {
            bind_result = bind_mount(
                proc_fd,
                arg1,
                arg2,
                (BIND_RECURSIVE as libc::c_int as libc::c_uint | flags) as bind_option_t,
                &mut failing_path,
            );
            if bind_result as libc::c_uint != BIND_MOUNT_SUCCESS as libc::c_int as libc::c_uint {
                die_with_bind_result(
                    bind_result,
                    *__errno_location(),
                    failing_path,
                    b"Can't bind mount %s on %s\0" as *const u8 as *const libc::c_char,
                    arg1,
                    arg2,
                );
            }
            if failing_path.is_null() {
            } else {
                __assert_fail(
                    b"failing_path == NULL\0" as *const u8 as *const libc::c_char,
                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                    1117 as libc::c_int as libc::c_uint,
                    (*::core::mem::transmute::<
                        &[u8; 90],
                        &[libc::c_char; 90],
                    >(
                        b"void privileged_op(int, uint32_t, uint32_t, uint32_t, size_t, const char *, const char *)\0",
                    ))
                        .as_ptr(),
                );
            }
            'c_9061: {
                if failing_path.is_null() {
                } else {
                    __assert_fail(
                        b"failing_path == NULL\0" as *const u8 as *const libc::c_char,
                        b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                        1117 as libc::c_int as libc::c_uint,
                        (*::core::mem::transmute::<
                            &[u8; 90],
                            &[libc::c_char; 90],
                        >(
                            b"void privileged_op(int, uint32_t, uint32_t, uint32_t, size_t, const char *, const char *)\0",
                        ))
                            .as_ptr(),
                    );
                }
            };
        }
        2 => {
            if mount(
                b"proc\0" as *const u8 as *const libc::c_char,
                arg1,
                b"proc\0" as *const u8 as *const libc::c_char,
                (MS_NOSUID as libc::c_int | MS_NOEXEC as libc::c_int | MS_NODEV as libc::c_int)
                    as libc::c_ulong,
                0 as *const libc::c_void,
            ) != 0 as libc::c_int
            {
                die_with_error(
                    b"Can't mount proc on %s\0" as *const u8 as *const libc::c_char,
                    arg1,
                );
            }
        }
        3 => {
            let mut mode: *mut libc::c_char = 0 as *mut libc::c_char;
            if size_arg > 18446744073709551615 as libc::c_ulong >> 1 as libc::c_int {
                die_with_error(
                    b"Specified tmpfs size too large (%zu > %zu)\0" as *const u8
                        as *const libc::c_char,
                    size_arg,
                    18446744073709551615 as libc::c_ulong >> 1 as libc::c_int,
                );
            }
            if size_arg != 0 as libc::c_int as libc::c_ulong {
                mode = xasprintf(
                    b"mode=%#o,size=%zu\0" as *const u8 as *const libc::c_char,
                    perms,
                    size_arg,
                );
            } else {
                mode = xasprintf(b"mode=%#o\0" as *const u8 as *const libc::c_char, perms);
            }
            let mut opt: *mut libc::c_char = label_mount(mode, opt_file_label);
            if mount(
                b"tmpfs\0" as *const u8 as *const libc::c_char,
                arg1,
                b"tmpfs\0" as *const u8 as *const libc::c_char,
                (MS_NOSUID as libc::c_int | MS_NODEV as libc::c_int) as libc::c_ulong,
                opt as *const libc::c_void,
            ) != 0 as libc::c_int
            {
                die_with_error(
                    b"Can't mount tmpfs on %s\0" as *const u8 as *const libc::c_char,
                    arg1,
                );
            }
        }
        4 => {
            if mount(
                b"devpts\0" as *const u8 as *const libc::c_char,
                arg1,
                b"devpts\0" as *const u8 as *const libc::c_char,
                (MS_NOSUID as libc::c_int | MS_NOEXEC as libc::c_int) as libc::c_ulong,
                b"newinstance,ptmxmode=0666,mode=620\0" as *const u8 as *const libc::c_char
                    as *const libc::c_void,
            ) != 0 as libc::c_int
            {
                die_with_error(
                    b"Can't mount devpts on %s\0" as *const u8 as *const libc::c_char,
                    arg1,
                );
            }
        }
        5 => {
            if mount(
                b"mqueue\0" as *const u8 as *const libc::c_char,
                arg1,
                b"mqueue\0" as *const u8 as *const libc::c_char,
                0 as libc::c_int as libc::c_ulong,
                0 as *const libc::c_void,
            ) != 0 as libc::c_int
            {
                die_with_error(
                    b"Can't mount mqueue on %s\0" as *const u8 as *const libc::c_char,
                    arg1,
                );
            }
        }
        7 => {
            if opt_unshare_uts == 0 {
                die(
                    b"Refusing to set hostname in original namespace\0" as *const u8
                        as *const libc::c_char,
                );
            }
            if sethostname(arg1, strlen(arg1)) != 0 as libc::c_int {
                die_with_error(
                    b"Can't set hostname to %s\0" as *const u8 as *const libc::c_char,
                    arg1,
                );
            }
        }
        _ => {
            die(
                b"Unexpected privileged op %d\0" as *const u8 as *const libc::c_char,
                op,
            );
        }
    };
}
#[c2rust::src_loc = "1174:1"]
unsafe extern "C" fn setup_newroot(mut unshare_pid: bool_0, mut privileged_op_socket: libc::c_int) {
    let mut op: *mut SetupOp = 0 as *mut SetupOp;
    let mut current_block_122: u64;
    op = ops;
    while !op.is_null() {
        let mut source: *mut libc::c_char = 0 as *mut libc::c_char;
        let mut dest: *mut libc::c_char = 0 as *mut libc::c_char;
        let mut source_mode: libc::c_int = 0 as libc::c_int;
        let mut i: libc::c_uint = 0;
        if !((*op).source).is_null()
            && (*op).type_0 as libc::c_uint != SETUP_MAKE_SYMLINK as libc::c_int as libc::c_uint
        {
            source = get_oldroot_path((*op).source);
            source_mode = get_file_mode(source);
            if source_mode < 0 as libc::c_int {
                if (*op).flags as libc::c_uint & ALLOW_NOTEXIST as libc::c_int as libc::c_uint != 0
                    && *__errno_location() == 2 as libc::c_int
                {
                    current_block_122 = 11174649648027449784;
                } else {
                    die_with_error(
                        b"Can't get type of source %s\0" as *const u8 as *const libc::c_char,
                        (*op).source,
                    );
                }
            } else {
                current_block_122 = 13513818773234778473;
            }
        } else {
            current_block_122 = 13513818773234778473;
        }
        match current_block_122 {
            13513818773234778473 => {
                if !((*op).dest).is_null()
                    && (*op).flags as libc::c_uint & NO_CREATE_DEST as libc::c_int as libc::c_uint
                        == 0 as libc::c_int as libc::c_uint
                {
                    let mut parent_mode: libc::c_uint = 0o755 as libc::c_int as libc::c_uint;
                    if (*op).perms >= 0 as libc::c_int
                        && (*op).perms & 0o70 as libc::c_int == 0 as libc::c_int
                    {
                        parent_mode &= !(0o50 as libc::c_uint);
                    }
                    if (*op).perms >= 0 as libc::c_int
                        && (*op).perms & 0o7 as libc::c_int == 0 as libc::c_int
                    {
                        parent_mode &= !(0o5 as libc::c_uint);
                    }
                    dest = get_newroot_path((*op).dest);
                    if mkdir_with_parents(dest, parent_mode, 0 as libc::c_int) != 0 as libc::c_int {
                        die_with_error(
                            b"Can't mkdir parents for %s\0" as *const u8 as *const libc::c_char,
                            (*op).dest,
                        );
                    }
                }
                static mut cover_proc_dirs: [*const libc::c_char; 4] = [
                    b"sys\0" as *const u8 as *const libc::c_char,
                    b"sysrq-trigger\0" as *const u8 as *const libc::c_char,
                    b"irq\0" as *const u8 as *const libc::c_char,
                    b"bus\0" as *const u8 as *const libc::c_char,
                ];
                static mut devnodes: [*const libc::c_char; 6] = [
                    b"null\0" as *const u8 as *const libc::c_char,
                    b"zero\0" as *const u8 as *const libc::c_char,
                    b"full\0" as *const u8 as *const libc::c_char,
                    b"random\0" as *const u8 as *const libc::c_char,
                    b"urandom\0" as *const u8 as *const libc::c_char,
                    b"tty\0" as *const u8 as *const libc::c_char,
                ];
                static mut stdionodes: [*const libc::c_char; 3] = [
                    b"stdin\0" as *const u8 as *const libc::c_char,
                    b"stdout\0" as *const u8 as *const libc::c_char,
                    b"stderr\0" as *const u8 as *const libc::c_char,
                ];
                match (*op).type_0 as libc::c_uint {
                    1 | 2 | 0 => {
                        if source_mode == 0o40000 as libc::c_int {
                            if ensure_dir(dest, 0o755 as libc::c_int as mode_t) != 0 as libc::c_int
                            {
                                die_with_error(
                                    b"Can't mkdir %s\0" as *const u8 as *const libc::c_char,
                                    (*op).dest,
                                );
                            }
                        } else if ensure_file(dest, 0o444 as libc::c_int as mode_t)
                            != 0 as libc::c_int
                        {
                            die_with_error(
                                b"Can't create file at %s\0" as *const u8 as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                        privileged_op(
                            privileged_op_socket,
                            PRIV_SEP_OP_BIND_MOUNT as libc::c_int as uint32_t,
                            ((if (*op).type_0 as libc::c_uint
                                == SETUP_RO_BIND_MOUNT as libc::c_int as libc::c_uint
                            {
                                BIND_READONLY as libc::c_int
                            } else {
                                0 as libc::c_int
                            }) | (if (*op).type_0 as libc::c_uint
                                == SETUP_DEV_BIND_MOUNT as libc::c_int as libc::c_uint
                            {
                                BIND_DEVICES as libc::c_int
                            } else {
                                0 as libc::c_int
                            })) as uint32_t,
                            0 as libc::c_int as uint32_t,
                            0 as libc::c_int as size_t,
                            source,
                            dest,
                        );
                    }
                    12 => {
                        privileged_op(
                            privileged_op_socket,
                            PRIV_SEP_OP_REMOUNT_RO_NO_RECURSIVE as libc::c_int as uint32_t,
                            0 as libc::c_int as uint32_t,
                            0 as libc::c_int as uint32_t,
                            0 as libc::c_int as size_t,
                            0 as *const libc::c_char,
                            dest,
                        );
                    }
                    3 => {
                        if ensure_dir(dest, 0o755 as libc::c_int as mode_t) != 0 as libc::c_int {
                            die_with_error(
                                b"Can't mkdir %s\0" as *const u8 as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                        if unshare_pid != 0 || opt_pidns_fd != -(1 as libc::c_int) {
                            privileged_op(
                                privileged_op_socket,
                                PRIV_SEP_OP_PROC_MOUNT as libc::c_int as uint32_t,
                                0 as libc::c_int as uint32_t,
                                0 as libc::c_int as uint32_t,
                                0 as libc::c_int as size_t,
                                dest,
                                0 as *const libc::c_char,
                            );
                        } else {
                            privileged_op(
                                privileged_op_socket,
                                PRIV_SEP_OP_BIND_MOUNT as libc::c_int as uint32_t,
                                0 as libc::c_int as uint32_t,
                                0 as libc::c_int as uint32_t,
                                0 as libc::c_int as size_t,
                                b"oldroot/proc\0" as *const u8 as *const libc::c_char,
                                dest,
                            );
                        }
                        i = 0 as libc::c_int as libc::c_uint;
                        while (i as libc::c_ulong)
                            < (::core::mem::size_of::<[*const libc::c_char; 4]>() as libc::c_ulong)
                                .wrapping_div(
                                    ::core::mem::size_of::<*const libc::c_char>() as libc::c_ulong
                                )
                        {
                            let mut subdir: *mut libc::c_char = strconcat3(
                                dest,
                                b"/\0" as *const u8 as *const libc::c_char,
                                cover_proc_dirs[i as usize],
                            );
                            if access(subdir, 2 as libc::c_int) < 0 as libc::c_int {
                                if !(*__errno_location() == 13 as libc::c_int
                                    || *__errno_location() == 2 as libc::c_int
                                    || *__errno_location() == 30 as libc::c_int)
                                {
                                    die_with_error(
                                        b"Can't access %s\0" as *const u8 as *const libc::c_char,
                                        subdir,
                                    );
                                }
                            } else {
                                privileged_op(
                                    privileged_op_socket,
                                    PRIV_SEP_OP_BIND_MOUNT as libc::c_int as uint32_t,
                                    BIND_READONLY as libc::c_int as uint32_t,
                                    0 as libc::c_int as uint32_t,
                                    0 as libc::c_int as size_t,
                                    subdir,
                                    subdir,
                                );
                            }
                            i = i.wrapping_add(1);
                            i;
                        }
                    }
                    4 => {
                        if ensure_dir(dest, 0o755 as libc::c_int as mode_t) != 0 as libc::c_int {
                            die_with_error(
                                b"Can't mkdir %s\0" as *const u8 as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                        privileged_op(
                            privileged_op_socket,
                            PRIV_SEP_OP_TMPFS_MOUNT as libc::c_int as uint32_t,
                            0 as libc::c_int as uint32_t,
                            0o755 as libc::c_int as uint32_t,
                            0 as libc::c_int as size_t,
                            dest,
                            0 as *const libc::c_char,
                        );
                        i = 0 as libc::c_int as libc::c_uint;
                        while (i as libc::c_ulong)
                            < (::core::mem::size_of::<[*const libc::c_char; 6]>() as libc::c_ulong)
                                .wrapping_div(
                                    ::core::mem::size_of::<*const libc::c_char>() as libc::c_ulong
                                )
                        {
                            let mut node_dest: *mut libc::c_char = strconcat3(
                                dest,
                                b"/\0" as *const u8 as *const libc::c_char,
                                devnodes[i as usize],
                            );
                            let mut node_src: *mut libc::c_char = strconcat(
                                b"/oldroot/dev/\0" as *const u8 as *const libc::c_char,
                                devnodes[i as usize],
                            );
                            if create_file(
                                node_dest,
                                0o444 as libc::c_int as mode_t,
                                0 as *const libc::c_char,
                            ) != 0 as libc::c_int
                            {
                                die_with_error(
                                    b"Can't create file %s/%s\0" as *const u8
                                        as *const libc::c_char,
                                    (*op).dest,
                                    devnodes[i as usize],
                                );
                            }
                            privileged_op(
                                privileged_op_socket,
                                PRIV_SEP_OP_BIND_MOUNT as libc::c_int as uint32_t,
                                BIND_DEVICES as libc::c_int as uint32_t,
                                0 as libc::c_int as uint32_t,
                                0 as libc::c_int as size_t,
                                node_src,
                                node_dest,
                            );
                            i = i.wrapping_add(1);
                            i;
                        }
                        i = 0 as libc::c_int as libc::c_uint;
                        while (i as libc::c_ulong)
                            < (::core::mem::size_of::<[*const libc::c_char; 3]>() as libc::c_ulong)
                                .wrapping_div(
                                    ::core::mem::size_of::<*const libc::c_char>() as libc::c_ulong
                                )
                        {
                            let mut target: *mut libc::c_char = xasprintf(
                                b"/proc/self/fd/%d\0" as *const u8 as *const libc::c_char,
                                i,
                            );
                            let mut node_dest_0: *mut libc::c_char = strconcat3(
                                dest,
                                b"/\0" as *const u8 as *const libc::c_char,
                                stdionodes[i as usize],
                            );
                            if symlink(target, node_dest_0) < 0 as libc::c_int {
                                die_with_error(
                                    b"Can't create symlink %s/%s\0" as *const u8
                                        as *const libc::c_char,
                                    (*op).dest,
                                    stdionodes[i as usize],
                                );
                            }
                            i = i.wrapping_add(1);
                            i;
                        }
                        let mut dev_fd: *mut libc::c_char =
                            strconcat(dest, b"/fd\0" as *const u8 as *const libc::c_char);
                        if symlink(
                            b"/proc/self/fd\0" as *const u8 as *const libc::c_char,
                            dev_fd,
                        ) < 0 as libc::c_int
                        {
                            die_with_error(
                                b"Can't create symlink %s\0" as *const u8 as *const libc::c_char,
                                dev_fd,
                            );
                        }
                        let mut dev_core: *mut libc::c_char =
                            strconcat(dest, b"/core\0" as *const u8 as *const libc::c_char);
                        if symlink(
                            b"/proc/kcore\0" as *const u8 as *const libc::c_char,
                            dev_core,
                        ) < 0 as libc::c_int
                        {
                            die_with_error(
                                b"Can't create symlink %s\0" as *const u8 as *const libc::c_char,
                                dev_core,
                            );
                        }
                        let mut pts: *mut libc::c_char =
                            strconcat(dest, b"/pts\0" as *const u8 as *const libc::c_char);
                        let mut ptmx: *mut libc::c_char =
                            strconcat(dest, b"/ptmx\0" as *const u8 as *const libc::c_char);
                        let mut shm: *mut libc::c_char =
                            strconcat(dest, b"/shm\0" as *const u8 as *const libc::c_char);
                        if mkdir(shm, 0o755 as libc::c_int as __mode_t) == -(1 as libc::c_int) {
                            die_with_error(
                                b"Can't create %s/shm\0" as *const u8 as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                        if mkdir(pts, 0o755 as libc::c_int as __mode_t) == -(1 as libc::c_int) {
                            die_with_error(
                                b"Can't create %s/devpts\0" as *const u8 as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                        privileged_op(
                            privileged_op_socket,
                            PRIV_SEP_OP_DEVPTS_MOUNT as libc::c_int as uint32_t,
                            0 as libc::c_int as uint32_t,
                            0 as libc::c_int as uint32_t,
                            0 as libc::c_int as size_t,
                            pts,
                            0 as *const libc::c_char,
                        );
                        if symlink(b"pts/ptmx\0" as *const u8 as *const libc::c_char, ptmx)
                            != 0 as libc::c_int
                        {
                            die_with_error(
                                b"Can't make symlink at %s/ptmx\0" as *const u8
                                    as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                        if !host_tty_dev.is_null()
                            && *host_tty_dev as libc::c_int != 0 as libc::c_int
                        {
                            let mut src_tty_dev: *mut libc::c_char = strconcat(
                                b"/oldroot\0" as *const u8 as *const libc::c_char,
                                host_tty_dev,
                            );
                            let mut dest_console: *mut libc::c_char =
                                strconcat(dest, b"/console\0" as *const u8 as *const libc::c_char);
                            if create_file(
                                dest_console,
                                0o444 as libc::c_int as mode_t,
                                0 as *const libc::c_char,
                            ) != 0 as libc::c_int
                            {
                                die_with_error(
                                    b"creating %s/console\0" as *const u8 as *const libc::c_char,
                                    (*op).dest,
                                );
                            }
                            privileged_op(
                                privileged_op_socket,
                                PRIV_SEP_OP_BIND_MOUNT as libc::c_int as uint32_t,
                                BIND_DEVICES as libc::c_int as uint32_t,
                                0 as libc::c_int as uint32_t,
                                0 as libc::c_int as size_t,
                                src_tty_dev,
                                dest_console,
                            );
                        }
                    }
                    5 => {
                        if !dest.is_null() {
                        } else {
                            __assert_fail(
                                b"dest != NULL\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1367 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10840: {
                            if !dest.is_null() {
                            } else {
                                __assert_fail(
                                    b"dest != NULL\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1367 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if (*op).perms >= 0 as libc::c_int {
                        } else {
                            __assert_fail(
                                b"op->perms >= 0\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1368 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10798: {
                            if (*op).perms >= 0 as libc::c_int {
                            } else {
                                __assert_fail(
                                    b"op->perms >= 0\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1368 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if (*op).perms <= 0o7777 as libc::c_int {
                        } else {
                            __assert_fail(
                                b"op->perms <= 07777\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1369 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10758: {
                            if (*op).perms <= 0o7777 as libc::c_int {
                            } else {
                                __assert_fail(
                                    b"op->perms <= 07777\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1369 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if ensure_dir(dest, 0o755 as libc::c_int as mode_t) != 0 as libc::c_int {
                            die_with_error(
                                b"Can't mkdir %s\0" as *const u8 as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                        privileged_op(
                            privileged_op_socket,
                            PRIV_SEP_OP_TMPFS_MOUNT as libc::c_int as uint32_t,
                            0 as libc::c_int as uint32_t,
                            (*op).perms as uint32_t,
                            (*op).size,
                            dest,
                            0 as *const libc::c_char,
                        );
                    }
                    6 => {
                        if ensure_dir(dest, 0o755 as libc::c_int as mode_t) != 0 as libc::c_int {
                            die_with_error(
                                b"Can't mkdir %s\0" as *const u8 as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                        privileged_op(
                            privileged_op_socket,
                            PRIV_SEP_OP_MQUEUE_MOUNT as libc::c_int as uint32_t,
                            0 as libc::c_int as uint32_t,
                            0 as libc::c_int as uint32_t,
                            0 as libc::c_int as size_t,
                            dest,
                            0 as *const libc::c_char,
                        );
                    }
                    7 => {
                        if !dest.is_null() {
                        } else {
                            __assert_fail(
                                b"dest != NULL\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1389 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10625: {
                            if !dest.is_null() {
                            } else {
                                __assert_fail(
                                    b"dest != NULL\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1389 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if (*op).perms >= 0 as libc::c_int {
                        } else {
                            __assert_fail(
                                b"op->perms >= 0\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1390 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10583: {
                            if (*op).perms >= 0 as libc::c_int {
                            } else {
                                __assert_fail(
                                    b"op->perms >= 0\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1390 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if (*op).perms <= 0o7777 as libc::c_int {
                        } else {
                            __assert_fail(
                                b"op->perms <= 07777\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1391 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10543: {
                            if (*op).perms <= 0o7777 as libc::c_int {
                            } else {
                                __assert_fail(
                                    b"op->perms <= 07777\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1391 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if ensure_dir(dest, (*op).perms as mode_t) != 0 as libc::c_int {
                            die_with_error(
                                b"Can't mkdir %s\0" as *const u8 as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                    }
                    14 => {
                        if !((*op).dest).is_null() {
                        } else {
                            __assert_fail(
                                b"op->dest != NULL\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1399 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10472: {
                            if !((*op).dest).is_null() {
                            } else {
                                __assert_fail(
                                    b"op->dest != NULL\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1399 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if dest.is_null() {
                        } else {
                            __assert_fail(
                                b"dest == NULL\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1402 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10428: {
                            if dest.is_null() {
                            } else {
                                __assert_fail(
                                    b"dest == NULL\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1402 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        dest = get_newroot_path((*op).dest);
                        if !dest.is_null() {
                        } else {
                            __assert_fail(
                                b"dest != NULL\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1404 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10377: {
                            if !dest.is_null() {
                            } else {
                                __assert_fail(
                                    b"dest != NULL\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1404 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if (*op).perms >= 0 as libc::c_int {
                        } else {
                            __assert_fail(
                                b"op->perms >= 0\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1405 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10337: {
                            if (*op).perms >= 0 as libc::c_int {
                            } else {
                                __assert_fail(
                                    b"op->perms >= 0\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1405 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if (*op).perms <= 0o7777 as libc::c_int {
                        } else {
                            __assert_fail(
                                b"op->perms <= 07777\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1406 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10297: {
                            if (*op).perms <= 0o7777 as libc::c_int {
                            } else {
                                __assert_fail(
                                    b"op->perms <= 07777\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1406 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if chmod(dest, (*op).perms as __mode_t) != 0 as libc::c_int {
                            die_with_error(
                                b"Can't chmod %#o %s\0" as *const u8 as *const libc::c_char,
                                (*op).perms,
                                (*op).dest,
                            );
                        }
                    }
                    8 => {
                        let mut dest_fd: libc::c_int = -(1 as libc::c_int);
                        if !dest.is_null() {
                        } else {
                            __assert_fail(
                                b"dest != NULL\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1417 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10225: {
                            if !dest.is_null() {
                            } else {
                                __assert_fail(
                                    b"dest != NULL\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1417 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if (*op).perms >= 0 as libc::c_int {
                        } else {
                            __assert_fail(
                                b"op->perms >= 0\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1418 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10185: {
                            if (*op).perms >= 0 as libc::c_int {
                            } else {
                                __assert_fail(
                                    b"op->perms >= 0\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1418 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if (*op).perms <= 0o7777 as libc::c_int {
                        } else {
                            __assert_fail(
                                b"op->perms <= 07777\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1419 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10145: {
                            if (*op).perms <= 0o7777 as libc::c_int {
                            } else {
                                __assert_fail(
                                    b"op->perms <= 07777\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1419 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        dest_fd = creat(dest, (*op).perms as mode_t);
                        if dest_fd == -(1 as libc::c_int) {
                            die_with_error(
                                b"Can't create file %s\0" as *const u8 as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                        if copy_file_data((*op).fd, dest_fd) != 0 as libc::c_int {
                            die_with_error(
                                b"Can't write data to file %s\0" as *const u8
                                    as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                        close((*op).fd);
                        (*op).fd = -(1 as libc::c_int);
                    }
                    9 | 10 => {
                        let mut dest_fd_0: libc::c_int = -(1 as libc::c_int);
                        let mut tempfile: [libc::c_char; 16] =
                            *::core::mem::transmute::<&[u8; 16], &mut [libc::c_char; 16]>(
                                b"/bindfileXXXXXX\0",
                            );
                        if !dest.is_null() {
                        } else {
                            __assert_fail(
                                b"dest != NULL\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1439 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_10023: {
                            if !dest.is_null() {
                            } else {
                                __assert_fail(
                                    b"dest != NULL\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1439 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if (*op).perms >= 0 as libc::c_int {
                        } else {
                            __assert_fail(
                                b"op->perms >= 0\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1440 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_9983: {
                            if (*op).perms >= 0 as libc::c_int {
                            } else {
                                __assert_fail(
                                    b"op->perms >= 0\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1440 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if (*op).perms <= 0o7777 as libc::c_int {
                        } else {
                            __assert_fail(
                                b"op->perms <= 07777\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1441 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_9943: {
                            if (*op).perms <= 0o7777 as libc::c_int {
                            } else {
                                __assert_fail(
                                    b"op->perms <= 07777\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1441 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        dest_fd_0 = mkstemp(tempfile.as_mut_ptr());
                        if dest_fd_0 == -(1 as libc::c_int) {
                            die_with_error(
                                b"Can't create tmpfile for %s\0" as *const u8
                                    as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                        if fchmod(dest_fd_0, (*op).perms as __mode_t) != 0 as libc::c_int {
                            die_with_error(
                                b"Can't set mode %#o on file to be used for %s\0" as *const u8
                                    as *const libc::c_char,
                                (*op).perms,
                                (*op).dest,
                            );
                        }
                        if copy_file_data((*op).fd, dest_fd_0) != 0 as libc::c_int {
                            die_with_error(
                                b"Can't write data to file %s\0" as *const u8
                                    as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                        close((*op).fd);
                        (*op).fd = -(1 as libc::c_int);
                        if !dest.is_null() {
                        } else {
                            __assert_fail(
                                b"dest != NULL\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1457 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_9812: {
                            if !dest.is_null() {
                            } else {
                                __assert_fail(
                                    b"dest != NULL\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1457 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if ensure_file(dest, 0o444 as libc::c_int as mode_t) != 0 as libc::c_int {
                            die_with_error(
                                b"Can't create file at %s\0" as *const u8 as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                        privileged_op(
                            privileged_op_socket,
                            PRIV_SEP_OP_BIND_MOUNT as libc::c_int as uint32_t,
                            (if (*op).type_0 as libc::c_uint
                                == SETUP_MAKE_RO_BIND_FILE as libc::c_int as libc::c_uint
                            {
                                BIND_READONLY as libc::c_int
                            } else {
                                0 as libc::c_int
                            }) as uint32_t,
                            0 as libc::c_int as uint32_t,
                            0 as libc::c_int as size_t,
                            tempfile.as_mut_ptr(),
                            dest,
                        );
                        unlink(tempfile.as_mut_ptr());
                    }
                    11 => {
                        if !((*op).source).is_null() {
                        } else {
                            __assert_fail(
                                b"op->source != NULL\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1475 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_9684: {
                            if !((*op).source).is_null() {
                            } else {
                                __assert_fail(
                                    b"op->source != NULL\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1475 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        if symlink((*op).source, dest) != 0 as libc::c_int {
                            die_with_error(
                                b"Can't make symlink at %s\0" as *const u8 as *const libc::c_char,
                                (*op).dest,
                            );
                        }
                    }
                    13 => {
                        if !((*op).dest).is_null() {
                        } else {
                            __assert_fail(
                                b"op->dest != NULL\0" as *const u8 as *const libc::c_char,
                                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                1481 as libc::c_int as libc::c_uint,
                                (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                    b"void setup_newroot(bool, int)\0",
                                ))
                                .as_ptr(),
                            );
                        }
                        'c_9606: {
                            if !((*op).dest).is_null() {
                            } else {
                                __assert_fail(
                                    b"op->dest != NULL\0" as *const u8 as *const libc::c_char,
                                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                                    1481 as libc::c_int as libc::c_uint,
                                    (*::core::mem::transmute::<&[u8; 30], &[libc::c_char; 30]>(
                                        b"void setup_newroot(bool, int)\0",
                                    ))
                                    .as_ptr(),
                                );
                            }
                        };
                        privileged_op(
                            privileged_op_socket,
                            PRIV_SEP_OP_SET_HOSTNAME as libc::c_int as uint32_t,
                            0 as libc::c_int as uint32_t,
                            0 as libc::c_int as uint32_t,
                            0 as libc::c_int as size_t,
                            (*op).dest,
                            0 as *const libc::c_char,
                        );
                    }
                    _ => {
                        die(
                            b"Unexpected type %d\0" as *const u8 as *const libc::c_char,
                            (*op).type_0 as libc::c_uint,
                        );
                    }
                }
            }
            _ => {}
        }
        op = (*op).next;
    }
    privileged_op(
        privileged_op_socket,
        PRIV_SEP_OP_DONE as libc::c_int as uint32_t,
        0 as libc::c_int as uint32_t,
        0 as libc::c_int as uint32_t,
        0 as libc::c_int as size_t,
        0 as *const libc::c_char,
        0 as *const libc::c_char,
    );
}
#[c2rust::src_loc = "1496:1"]
unsafe extern "C" fn close_ops_fd() {
    let mut op: *mut SetupOp = 0 as *mut SetupOp;
    op = ops;
    while !op.is_null() {
        if (*op).fd != -(1 as libc::c_int) {
            close((*op).fd);
            (*op).fd = -(1 as libc::c_int);
        }
        op = (*op).next;
    }
}
#[c2rust::src_loc = "1515:1"]
unsafe extern "C" fn resolve_symlinks_in_ops() {
    let mut op: *mut SetupOp = 0 as *mut SetupOp;
    op = ops;
    while !op.is_null() {
        let mut old_source: *const libc::c_char = 0 as *const libc::c_char;
        match (*op).type_0 as libc::c_uint {
            1 | 2 | 0 => {
                old_source = (*op).source;
                (*op).source = realpath(old_source, 0 as *mut libc::c_char);
                if ((*op).source).is_null() {
                    if (*op).flags as libc::c_uint & ALLOW_NOTEXIST as libc::c_int as libc::c_uint
                        != 0
                        && *__errno_location() == 2 as libc::c_int
                    {
                        (*op).source = old_source;
                    } else {
                        die_with_error(
                            b"Can't find source path %s\0" as *const u8 as *const libc::c_char,
                            old_source,
                        );
                    }
                }
            }
            3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | _ => {}
        }
        op = (*op).next;
    }
}
#[c2rust::src_loc = "1559:1"]
unsafe extern "C" fn resolve_string_offset(
    mut buffer: *mut libc::c_void,
    mut buffer_size: size_t,
    mut offset: uint32_t,
) -> *const libc::c_char {
    if offset == 0 as libc::c_int as libc::c_uint {
        return 0 as *const libc::c_char;
    }
    if offset as libc::c_ulong > buffer_size {
        die(
            b"Invalid string offset %d (buffer size %zd)\0" as *const u8 as *const libc::c_char,
            offset,
            buffer_size,
        );
    }
    return (buffer as *const libc::c_char).offset(offset as isize);
}
#[c2rust::src_loc = "1573:1"]
unsafe extern "C" fn read_priv_sec_op(
    mut read_socket: libc::c_int,
    mut buffer: *mut libc::c_void,
    mut buffer_size: size_t,
    mut flags: *mut uint32_t,
    mut perms: *mut uint32_t,
    mut size_arg: *mut size_t,
    mut arg1: *mut *const libc::c_char,
    mut arg2: *mut *const libc::c_char,
) -> uint32_t {
    let mut op: *const PrivSepOp = buffer as *const PrivSepOp;
    let mut rec_len: ssize_t = 0;
    loop {
        rec_len = read(
            read_socket,
            buffer,
            buffer_size.wrapping_sub(1 as libc::c_int as libc::c_ulong),
        );
        if !(rec_len == -(1 as libc::c_int) as libc::c_long
            && *__errno_location() == 4 as libc::c_int)
        {
            break;
        }
    }
    if rec_len < 0 as libc::c_int as libc::c_long {
        die_with_error(
            b"Can't read from unprivileged helper\0" as *const u8 as *const libc::c_char,
        );
    }
    if rec_len == 0 as libc::c_int as libc::c_long {
        exit(1 as libc::c_int);
    }
    if (rec_len as size_t) < ::core::mem::size_of::<PrivSepOp>() as libc::c_ulong {
        die(
            b"Invalid size %zd from unprivileged helper\0" as *const u8 as *const libc::c_char,
            rec_len,
        );
    }
    *(buffer as *mut libc::c_char).offset(rec_len as isize) = 0 as libc::c_int as libc::c_char;
    *flags = (*op).flags;
    *perms = (*op).perms;
    *size_arg = (*op).size_arg;
    *arg1 = resolve_string_offset(buffer, rec_len as size_t, (*op).arg1_offset);
    *arg2 = resolve_string_offset(buffer, rec_len as size_t, (*op).arg2_offset);
    return (*op).op;
}
#[c2rust::src_loc = "1611:1"]
unsafe extern "C" fn print_version_and_exit() -> ! {
    printf(
        b"%s\n\0" as *const u8 as *const libc::c_char,
        b"bubblewrap 0.8.0\0" as *const u8 as *const libc::c_char,
    );
    exit(0 as libc::c_int);
}
#[c2rust::src_loc = "1618:1"]
unsafe extern "C" fn is_modifier_option(mut option: *const libc::c_char) -> libc::c_int {
    return (strcmp(option, b"--perms\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int
        || strcmp(option, b"--size\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int)
        as libc::c_int;
}
#[c2rust::src_loc = "1625:1"]
unsafe extern "C" fn warn_only_last_option(mut name: *const libc::c_char) {
    warn(
        b"Only the last %s option will take effect\0" as *const u8 as *const libc::c_char,
        name,
    );
}
#[c2rust::src_loc = "1631:1"]
unsafe extern "C" fn parse_args_recurse(
    mut argcp: *mut libc::c_int,
    mut argvp: *mut *mut *const libc::c_char,
    mut in_file: bool_0,
    mut total_parsed_argc_p: *mut libc::c_int,
) {
    let mut op: *mut SetupOp = 0 as *mut SetupOp;
    let mut argc: libc::c_int = *argcp;
    let mut argv: *mut *const libc::c_char = *argvp;
    static mut MAX_ARGS: int32_t = 9000 as libc::c_int;
    if *total_parsed_argc_p > MAX_ARGS {
        die(
            b"Exceeded maximum number of arguments %u\0" as *const u8 as *const libc::c_char,
            MAX_ARGS,
        );
    }
    while argc > 0 as libc::c_int {
        let mut arg: *const libc::c_char = *argv.offset(0 as libc::c_int as isize);
        if strcmp(arg, b"--help\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            usage(0 as libc::c_int, stdout);
        } else if strcmp(arg, b"--version\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            print_version_and_exit();
        } else if strcmp(arg, b"--args\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            let mut the_fd: libc::c_int = 0;
            let mut endptr: *mut libc::c_char = 0 as *mut libc::c_char;
            let mut p: *const libc::c_char = 0 as *const libc::c_char;
            let mut data_end: *const libc::c_char = 0 as *const libc::c_char;
            let mut data_len: size_t = 0;
            let mut data_argv: *mut *const libc::c_char = 0 as *mut *const libc::c_char;
            let mut data_argv_copy: *mut *const libc::c_char = 0 as *mut *const libc::c_char;
            let mut data_argc: libc::c_int = 0;
            let mut i: libc::c_int = 0;
            if in_file != 0 {
                die(b"--args not supported in arguments file\0" as *const u8 as *const libc::c_char);
            }
            if argc < 2 as libc::c_int {
                die(b"--args takes an argument\0" as *const u8 as *const libc::c_char);
            }
            the_fd = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || the_fd < 0 as libc::c_int
            {
                die(
                    b"Invalid fd: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            opt_args_data = load_file_data(the_fd, &mut data_len);
            if opt_args_data.is_null() {
                die_with_error(b"Can't read --args data\0" as *const u8 as *const libc::c_char);
            }
            close(the_fd);
            data_end = opt_args_data.offset(data_len as isize);
            data_argc = 0 as libc::c_int;
            p = opt_args_data;
            while !p.is_null() && p < data_end {
                data_argc += 1;
                data_argc;
                *total_parsed_argc_p += 1;
                *total_parsed_argc_p;
                if *total_parsed_argc_p > MAX_ARGS {
                    die(
                        b"Exceeded maximum number of arguments %u\0" as *const u8
                            as *const libc::c_char,
                        MAX_ARGS,
                    );
                }
                p = memchr(
                    p as *const libc::c_void,
                    0 as libc::c_int,
                    data_end.offset_from(p) as libc::c_long as libc::c_ulong,
                ) as *const libc::c_char;
                if !p.is_null() {
                    p = p.offset(1);
                    p;
                }
            }
            data_argv = xcalloc(
                (::core::mem::size_of::<*mut libc::c_char>() as libc::c_ulong)
                    .wrapping_mul((data_argc + 1 as libc::c_int) as libc::c_ulong),
            ) as *mut *const libc::c_char;
            i = 0 as libc::c_int;
            p = opt_args_data;
            while !p.is_null() && p < data_end {
                let fresh3 = i;
                i = i + 1;
                let ref mut fresh4 = *data_argv.offset(fresh3 as isize);
                *fresh4 = p;
                p = memchr(
                    p as *const libc::c_void,
                    0 as libc::c_int,
                    data_end.offset_from(p) as libc::c_long as libc::c_ulong,
                ) as *const libc::c_char;
                if !p.is_null() {
                    p = p.offset(1);
                    p;
                }
            }
            data_argv_copy = data_argv;
            parse_args_recurse(
                &mut data_argc,
                &mut data_argv_copy,
                1 as libc::c_int,
                total_parsed_argc_p,
            );
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--unshare-all\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            opt_unshare_net = 1 as libc::c_int;
            opt_unshare_cgroup_try = opt_unshare_net;
            opt_unshare_uts = opt_unshare_cgroup_try;
            opt_unshare_pid = opt_unshare_uts;
            opt_unshare_ipc = opt_unshare_pid;
            opt_unshare_user_try = opt_unshare_ipc;
        } else if strcmp(arg, b"--unshare-user\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            opt_unshare_user = 1 as libc::c_int;
        } else if strcmp(
            arg,
            b"--unshare-user-try\0" as *const u8 as *const libc::c_char,
        ) == 0 as libc::c_int
        {
            opt_unshare_user_try = 1 as libc::c_int;
        } else if strcmp(arg, b"--unshare-ipc\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            opt_unshare_ipc = 1 as libc::c_int;
        } else if strcmp(arg, b"--unshare-pid\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            opt_unshare_pid = 1 as libc::c_int;
        } else if strcmp(arg, b"--unshare-net\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            opt_unshare_net = 1 as libc::c_int;
        } else if strcmp(arg, b"--unshare-uts\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            opt_unshare_uts = 1 as libc::c_int;
        } else if strcmp(
            arg,
            b"--unshare-cgroup\0" as *const u8 as *const libc::c_char,
        ) == 0 as libc::c_int
        {
            opt_unshare_cgroup = 1 as libc::c_int;
        } else if strcmp(
            arg,
            b"--unshare-cgroup-try\0" as *const u8 as *const libc::c_char,
        ) == 0 as libc::c_int
        {
            opt_unshare_cgroup_try = 1 as libc::c_int;
        } else if strcmp(arg, b"--share-net\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            opt_unshare_net = 0 as libc::c_int;
        } else if strcmp(arg, b"--chdir\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int
        {
            if argc < 2 as libc::c_int {
                die(b"--chdir takes one argument\0" as *const u8 as *const libc::c_char);
            }
            if !opt_chdir_path.is_null() {
                warn_only_last_option(b"--chdir\0" as *const u8 as *const libc::c_char);
            }
            opt_chdir_path = *argv.offset(1 as libc::c_int as isize);
            argv = argv.offset(1);
            argv;
            argc -= 1;
            argc;
        } else if strcmp(
            arg,
            b"--disable-userns\0" as *const u8 as *const libc::c_char,
        ) == 0 as libc::c_int
        {
            opt_disable_userns = 1 as libc::c_int;
        } else if strcmp(
            arg,
            b"--assert-userns-disabled\0" as *const u8 as *const libc::c_char,
        ) == 0 as libc::c_int
        {
            opt_assert_userns_disabled = 1 as libc::c_int;
        } else if strcmp(arg, b"--remount-ro\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            if argc < 2 as libc::c_int {
                die(b"--remount-ro takes one argument\0" as *const u8 as *const libc::c_char);
            }
            op = setup_op_new(SETUP_REMOUNT_RO_NO_RECURSIVE);
            (*op).dest = *argv.offset(1 as libc::c_int as isize);
            argv = argv.offset(1);
            argv;
            argc -= 1;
            argc;
        } else if strcmp(arg, b"--bind\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int
            || strcmp(arg, b"--bind-try\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int
        {
            if argc < 3 as libc::c_int {
                die(
                    b"%s takes two arguments\0" as *const u8 as *const libc::c_char,
                    arg,
                );
            }
            op = setup_op_new(SETUP_BIND_MOUNT);
            (*op).source = *argv.offset(1 as libc::c_int as isize);
            (*op).dest = *argv.offset(2 as libc::c_int as isize);
            if strcmp(arg, b"--bind-try\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int
            {
                (*op).flags = ALLOW_NOTEXIST;
            }
            argv = argv.offset(2 as libc::c_int as isize);
            argc -= 2 as libc::c_int;
        } else if strcmp(arg, b"--ro-bind\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
            || strcmp(arg, b"--ro-bind-try\0" as *const u8 as *const libc::c_char)
                == 0 as libc::c_int
        {
            if argc < 3 as libc::c_int {
                die(
                    b"%s takes two arguments\0" as *const u8 as *const libc::c_char,
                    arg,
                );
            }
            op = setup_op_new(SETUP_RO_BIND_MOUNT);
            (*op).source = *argv.offset(1 as libc::c_int as isize);
            (*op).dest = *argv.offset(2 as libc::c_int as isize);
            if strcmp(arg, b"--ro-bind-try\0" as *const u8 as *const libc::c_char)
                == 0 as libc::c_int
            {
                (*op).flags = ALLOW_NOTEXIST;
            }
            argv = argv.offset(2 as libc::c_int as isize);
            argc -= 2 as libc::c_int;
        } else if strcmp(arg, b"--dev-bind\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
            || strcmp(arg, b"--dev-bind-try\0" as *const u8 as *const libc::c_char)
                == 0 as libc::c_int
        {
            if argc < 3 as libc::c_int {
                die(
                    b"%s takes two arguments\0" as *const u8 as *const libc::c_char,
                    arg,
                );
            }
            op = setup_op_new(SETUP_DEV_BIND_MOUNT);
            (*op).source = *argv.offset(1 as libc::c_int as isize);
            (*op).dest = *argv.offset(2 as libc::c_int as isize);
            if strcmp(arg, b"--dev-bind-try\0" as *const u8 as *const libc::c_char)
                == 0 as libc::c_int
            {
                (*op).flags = ALLOW_NOTEXIST;
            }
            argv = argv.offset(2 as libc::c_int as isize);
            argc -= 2 as libc::c_int;
        } else if strcmp(arg, b"--proc\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            if argc < 2 as libc::c_int {
                die(b"--proc takes an argument\0" as *const u8 as *const libc::c_char);
            }
            op = setup_op_new(SETUP_MOUNT_PROC);
            (*op).dest = *argv.offset(1 as libc::c_int as isize);
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--exec-label\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            if argc < 2 as libc::c_int {
                die(b"--exec-label takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if !opt_exec_label.is_null() {
                warn_only_last_option(b"--exec-label\0" as *const u8 as *const libc::c_char);
            }
            opt_exec_label = *argv.offset(1 as libc::c_int as isize);
            die_unless_label_valid(opt_exec_label);
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--file-label\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            if argc < 2 as libc::c_int {
                die(b"--file-label takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if !opt_file_label.is_null() {
                warn_only_last_option(b"--file-label\0" as *const u8 as *const libc::c_char);
            }
            opt_file_label = *argv.offset(1 as libc::c_int as isize);
            die_unless_label_valid(opt_file_label);
            if label_create_file(opt_file_label) != 0 {
                die_with_error(b"--file-label setup failed\0" as *const u8 as *const libc::c_char);
            }
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--dev\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            if argc < 2 as libc::c_int {
                die(b"--dev takes an argument\0" as *const u8 as *const libc::c_char);
            }
            op = setup_op_new(SETUP_MOUNT_DEV);
            (*op).dest = *argv.offset(1 as libc::c_int as isize);
            opt_needs_devpts = 1 as libc::c_int;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--tmpfs\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int
        {
            if argc < 2 as libc::c_int {
                die(b"--tmpfs takes an argument\0" as *const u8 as *const libc::c_char);
            }
            op = setup_op_new(SETUP_MOUNT_TMPFS);
            (*op).dest = *argv.offset(1 as libc::c_int as isize);
            if next_perms >= 0 as libc::c_int {
                (*op).perms = next_perms;
            } else {
                (*op).perms = 0o755 as libc::c_int;
            }
            next_perms = -(1 as libc::c_int);
            (*op).size = next_size_arg;
            next_size_arg = 0 as libc::c_int as size_t;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--mqueue\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int
        {
            if argc < 2 as libc::c_int {
                die(b"--mqueue takes an argument\0" as *const u8 as *const libc::c_char);
            }
            op = setup_op_new(SETUP_MOUNT_MQUEUE);
            (*op).dest = *argv.offset(1 as libc::c_int as isize);
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--dir\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            if argc < 2 as libc::c_int {
                die(b"--dir takes an argument\0" as *const u8 as *const libc::c_char);
            }
            op = setup_op_new(SETUP_MAKE_DIR);
            (*op).dest = *argv.offset(1 as libc::c_int as isize);
            if next_perms >= 0 as libc::c_int {
                (*op).perms = next_perms;
            } else {
                (*op).perms = 0o755 as libc::c_int;
            }
            next_perms = -(1 as libc::c_int);
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--file\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            let mut file_fd: libc::c_int = 0;
            let mut endptr_0: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 3 as libc::c_int {
                die(b"--file takes two arguments\0" as *const u8 as *const libc::c_char);
            }
            file_fd = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_0,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_0.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || file_fd < 0 as libc::c_int
            {
                die(
                    b"Invalid fd: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            op = setup_op_new(SETUP_MAKE_FILE);
            (*op).fd = file_fd;
            (*op).dest = *argv.offset(2 as libc::c_int as isize);
            if next_perms >= 0 as libc::c_int {
                (*op).perms = next_perms;
            } else {
                (*op).perms = 0o666 as libc::c_int;
            }
            next_perms = -(1 as libc::c_int);
            argv = argv.offset(2 as libc::c_int as isize);
            argc -= 2 as libc::c_int;
        } else if strcmp(arg, b"--bind-data\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            let mut file_fd_0: libc::c_int = 0;
            let mut endptr_1: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 3 as libc::c_int {
                die(b"--bind-data takes two arguments\0" as *const u8 as *const libc::c_char);
            }
            file_fd_0 = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_1,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_1.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || file_fd_0 < 0 as libc::c_int
            {
                die(
                    b"Invalid fd: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            op = setup_op_new(SETUP_MAKE_BIND_FILE);
            (*op).fd = file_fd_0;
            (*op).dest = *argv.offset(2 as libc::c_int as isize);
            if next_perms >= 0 as libc::c_int {
                (*op).perms = next_perms;
            } else {
                (*op).perms = 0o600 as libc::c_int;
            }
            next_perms = -(1 as libc::c_int);
            argv = argv.offset(2 as libc::c_int as isize);
            argc -= 2 as libc::c_int;
        } else if strcmp(arg, b"--ro-bind-data\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            let mut file_fd_1: libc::c_int = 0;
            let mut endptr_2: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 3 as libc::c_int {
                die(b"--ro-bind-data takes two arguments\0" as *const u8 as *const libc::c_char);
            }
            file_fd_1 = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_2,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_2.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || file_fd_1 < 0 as libc::c_int
            {
                die(
                    b"Invalid fd: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            op = setup_op_new(SETUP_MAKE_RO_BIND_FILE);
            (*op).fd = file_fd_1;
            (*op).dest = *argv.offset(2 as libc::c_int as isize);
            if next_perms >= 0 as libc::c_int {
                (*op).perms = next_perms;
            } else {
                (*op).perms = 0o600 as libc::c_int;
            }
            next_perms = -(1 as libc::c_int);
            argv = argv.offset(2 as libc::c_int as isize);
            argc -= 2 as libc::c_int;
        } else if strcmp(arg, b"--symlink\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            if argc < 3 as libc::c_int {
                die(b"--symlink takes two arguments\0" as *const u8 as *const libc::c_char);
            }
            op = setup_op_new(SETUP_MAKE_SYMLINK);
            (*op).source = *argv.offset(1 as libc::c_int as isize);
            (*op).dest = *argv.offset(2 as libc::c_int as isize);
            argv = argv.offset(2 as libc::c_int as isize);
            argc -= 2 as libc::c_int;
        } else if strcmp(arg, b"--lock-file\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            if argc < 2 as libc::c_int {
                die(b"--lock-file takes an argument\0" as *const u8 as *const libc::c_char);
            }
            lock_file_new(*argv.offset(1 as libc::c_int as isize));
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--sync-fd\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            let mut the_fd_0: libc::c_int = 0;
            let mut endptr_3: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 2 as libc::c_int {
                die(b"--sync-fd takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if opt_sync_fd != -(1 as libc::c_int) {
                warn_only_last_option(b"--sync-fd\0" as *const u8 as *const libc::c_char);
            }
            the_fd_0 = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_3,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_3.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || the_fd_0 < 0 as libc::c_int
            {
                die(
                    b"Invalid fd: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            opt_sync_fd = the_fd_0;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--block-fd\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            let mut the_fd_1: libc::c_int = 0;
            let mut endptr_4: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 2 as libc::c_int {
                die(b"--block-fd takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if opt_block_fd != -(1 as libc::c_int) {
                warn_only_last_option(b"--block-fd\0" as *const u8 as *const libc::c_char);
            }
            the_fd_1 = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_4,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_4.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || the_fd_1 < 0 as libc::c_int
            {
                die(
                    b"Invalid fd: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            opt_block_fd = the_fd_1;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(
            arg,
            b"--userns-block-fd\0" as *const u8 as *const libc::c_char,
        ) == 0 as libc::c_int
        {
            let mut the_fd_2: libc::c_int = 0;
            let mut endptr_5: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 2 as libc::c_int {
                die(b"--userns-block-fd takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if opt_userns_block_fd != -(1 as libc::c_int) {
                warn_only_last_option(b"--userns-block-fd\0" as *const u8 as *const libc::c_char);
            }
            the_fd_2 = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_5,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_5.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || the_fd_2 < 0 as libc::c_int
            {
                die(
                    b"Invalid fd: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            opt_userns_block_fd = the_fd_2;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--info-fd\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            let mut the_fd_3: libc::c_int = 0;
            let mut endptr_6: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 2 as libc::c_int {
                die(b"--info-fd takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if opt_info_fd != -(1 as libc::c_int) {
                warn_only_last_option(b"--info-fd\0" as *const u8 as *const libc::c_char);
            }
            the_fd_3 = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_6,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_6.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || the_fd_3 < 0 as libc::c_int
            {
                die(
                    b"Invalid fd: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            opt_info_fd = the_fd_3;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(
            arg,
            b"--json-status-fd\0" as *const u8 as *const libc::c_char,
        ) == 0 as libc::c_int
        {
            let mut the_fd_4: libc::c_int = 0;
            let mut endptr_7: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 2 as libc::c_int {
                die(b"--json-status-fd takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if opt_json_status_fd != -(1 as libc::c_int) {
                warn_only_last_option(b"--json-status-fd\0" as *const u8 as *const libc::c_char);
            }
            the_fd_4 = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_7,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_7.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || the_fd_4 < 0 as libc::c_int
            {
                die(
                    b"Invalid fd: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            opt_json_status_fd = the_fd_4;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--seccomp\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            let mut the_fd_5: libc::c_int = 0;
            let mut endptr_8: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 2 as libc::c_int {
                die(b"--seccomp takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if !seccomp_programs.is_null() {
                die(
                    b"--seccomp cannot be combined with --add-seccomp-fd\0" as *const u8
                        as *const libc::c_char,
                );
            }
            if opt_seccomp_fd != -(1 as libc::c_int) {
                warn_only_last_option(b"--seccomp\0" as *const u8 as *const libc::c_char);
            }
            the_fd_5 = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_8,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_8.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || the_fd_5 < 0 as libc::c_int
            {
                die(
                    b"Invalid fd: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            opt_seccomp_fd = the_fd_5;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(
            arg,
            b"--add-seccomp-fd\0" as *const u8 as *const libc::c_char,
        ) == 0 as libc::c_int
        {
            let mut the_fd_6: libc::c_int = 0;
            let mut endptr_9: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 2 as libc::c_int {
                die(b"--add-seccomp-fd takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if opt_seccomp_fd != -(1 as libc::c_int) {
                die(
                    b"--add-seccomp-fd cannot be combined with --seccomp\0" as *const u8
                        as *const libc::c_char,
                );
            }
            the_fd_6 = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_9,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_9.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || the_fd_6 < 0 as libc::c_int
            {
                die(
                    b"Invalid fd: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            seccomp_program_new(&mut the_fd_6);
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--userns\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int
        {
            let mut the_fd_7: libc::c_int = 0;
            let mut endptr_10: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 2 as libc::c_int {
                die(b"--userns takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if opt_userns_fd != -(1 as libc::c_int) {
                warn_only_last_option(b"--userns\0" as *const u8 as *const libc::c_char);
            }
            the_fd_7 = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_10,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_10.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || the_fd_7 < 0 as libc::c_int
            {
                die(
                    b"Invalid fd: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            opt_userns_fd = the_fd_7;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--userns2\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            let mut the_fd_8: libc::c_int = 0;
            let mut endptr_11: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 2 as libc::c_int {
                die(b"--userns2 takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if opt_userns2_fd != -(1 as libc::c_int) {
                warn_only_last_option(b"--userns2\0" as *const u8 as *const libc::c_char);
            }
            the_fd_8 = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_11,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_11.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || the_fd_8 < 0 as libc::c_int
            {
                die(
                    b"Invalid fd: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            opt_userns2_fd = the_fd_8;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--pidns\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int
        {
            let mut the_fd_9: libc::c_int = 0;
            let mut endptr_12: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 2 as libc::c_int {
                die(b"--pidns takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if opt_pidns_fd != -(1 as libc::c_int) {
                warn_only_last_option(b"--pidns\0" as *const u8 as *const libc::c_char);
            }
            the_fd_9 = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_12,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_12.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || the_fd_9 < 0 as libc::c_int
            {
                die(
                    b"Invalid fd: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            opt_pidns_fd = the_fd_9;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--clearenv\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            xclearenv();
        } else if strcmp(arg, b"--setenv\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int
        {
            if argc < 3 as libc::c_int {
                die(b"--setenv takes two arguments\0" as *const u8 as *const libc::c_char);
            }
            xsetenv(
                *argv.offset(1 as libc::c_int as isize),
                *argv.offset(2 as libc::c_int as isize),
                1 as libc::c_int,
            );
            argv = argv.offset(2 as libc::c_int as isize);
            argc -= 2 as libc::c_int;
        } else if strcmp(arg, b"--unsetenv\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            if argc < 2 as libc::c_int {
                die(b"--unsetenv takes an argument\0" as *const u8 as *const libc::c_char);
            }
            xunsetenv(*argv.offset(1 as libc::c_int as isize));
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--uid\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            let mut the_uid: libc::c_int = 0;
            let mut endptr_13: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 2 as libc::c_int {
                die(b"--uid takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if opt_sandbox_uid != -(1 as libc::c_int) as uid_t {
                warn_only_last_option(b"--uid\0" as *const u8 as *const libc::c_char);
            }
            the_uid = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_13,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_13.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || the_uid < 0 as libc::c_int
            {
                die(
                    b"Invalid uid: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            opt_sandbox_uid = the_uid as uid_t;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--gid\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            let mut the_gid: libc::c_int = 0;
            let mut endptr_14: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 2 as libc::c_int {
                die(b"--gid takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if opt_sandbox_gid != -(1 as libc::c_int) as gid_t {
                warn_only_last_option(b"--gid\0" as *const u8 as *const libc::c_char);
            }
            the_gid = strtol(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_14,
                10 as libc::c_int,
            ) as libc::c_int;
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == 0 as libc::c_int
                || *endptr_14.offset(0 as libc::c_int as isize) as libc::c_int != 0 as libc::c_int
                || the_gid < 0 as libc::c_int
            {
                die(
                    b"Invalid gid: %s\0" as *const u8 as *const libc::c_char,
                    *argv.offset(1 as libc::c_int as isize),
                );
            }
            opt_sandbox_gid = the_gid as gid_t;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--hostname\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            if argc < 2 as libc::c_int {
                die(b"--hostname takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if !opt_sandbox_hostname.is_null() {
                warn_only_last_option(b"--hostname\0" as *const u8 as *const libc::c_char);
            }
            op = setup_op_new(SETUP_SET_HOSTNAME);
            (*op).dest = *argv.offset(1 as libc::c_int as isize);
            (*op).flags = NO_CREATE_DEST;
            opt_sandbox_hostname = *argv.offset(1 as libc::c_int as isize);
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--new-session\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            opt_new_session = 1 as libc::c_int;
        } else if strcmp(
            arg,
            b"--die-with-parent\0" as *const u8 as *const libc::c_char,
        ) == 0 as libc::c_int
        {
            opt_die_with_parent = 1 as libc::c_int;
        } else if strcmp(arg, b"--as-pid-1\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            opt_as_pid_1 = 1 as libc::c_int;
        } else if strcmp(arg, b"--cap-add\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            let mut cap: cap_value_t = 0;
            if argc < 2 as libc::c_int {
                die(b"--cap-add takes an argument\0" as *const u8 as *const libc::c_char);
            }
            opt_cap_add_or_drop_used = 1 as libc::c_int;
            if strcasecmp(
                *argv.offset(1 as libc::c_int as isize),
                b"ALL\0" as *const u8 as *const libc::c_char,
            ) == 0 as libc::c_int
            {
                requested_caps[1 as libc::c_int as usize] = 0xffffffff as libc::c_uint;
                requested_caps[0 as libc::c_int as usize] =
                    requested_caps[1 as libc::c_int as usize];
            } else {
                if cap_from_name(*argv.offset(1 as libc::c_int as isize), &mut cap)
                    < 0 as libc::c_int
                {
                    die(
                        b"unknown cap: %s\0" as *const u8 as *const libc::c_char,
                        *argv.offset(1 as libc::c_int as isize),
                    );
                }
                if cap < 32 as libc::c_int {
                    requested_caps[0 as libc::c_int as usize] =
                        (requested_caps[0 as libc::c_int as usize] as libc::c_long
                            | (1 as libc::c_long) << (cap & 31 as libc::c_int))
                            as uint32_t;
                } else {
                    requested_caps[1 as libc::c_int as usize] =
                        (requested_caps[1 as libc::c_int as usize] as libc::c_long
                            | (1 as libc::c_long)
                                << (cap - 32 as libc::c_int - 32 as libc::c_int
                                    & 31 as libc::c_int)) as uint32_t;
                }
            }
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--cap-drop\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            let mut cap_0: cap_value_t = 0;
            if argc < 2 as libc::c_int {
                die(b"--cap-drop takes an argument\0" as *const u8 as *const libc::c_char);
            }
            opt_cap_add_or_drop_used = 1 as libc::c_int;
            if strcasecmp(
                *argv.offset(1 as libc::c_int as isize),
                b"ALL\0" as *const u8 as *const libc::c_char,
            ) == 0 as libc::c_int
            {
                requested_caps[1 as libc::c_int as usize] = 0 as libc::c_int as uint32_t;
                requested_caps[0 as libc::c_int as usize] =
                    requested_caps[1 as libc::c_int as usize];
            } else {
                if cap_from_name(*argv.offset(1 as libc::c_int as isize), &mut cap_0)
                    < 0 as libc::c_int
                {
                    die(
                        b"unknown cap: %s\0" as *const u8 as *const libc::c_char,
                        *argv.offset(1 as libc::c_int as isize),
                    );
                }
                if cap_0 < 32 as libc::c_int {
                    requested_caps[0 as libc::c_int as usize] =
                        (requested_caps[0 as libc::c_int as usize] as libc::c_long
                            & !((1 as libc::c_long) << (cap_0 & 31 as libc::c_int)))
                            as uint32_t;
                } else {
                    requested_caps[1 as libc::c_int as usize] =
                        (requested_caps[1 as libc::c_int as usize] as libc::c_long
                            & !((1 as libc::c_long)
                                << (cap_0 - 32 as libc::c_int - 32 as libc::c_int
                                    & 31 as libc::c_int))) as uint32_t;
                }
            }
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--perms\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int
        {
            let mut perms: libc::c_ulong = 0;
            let mut endptr_15: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 2 as libc::c_int {
                die(b"--perms takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if next_perms != -(1 as libc::c_int) {
                die(b"--perms given twice for the same action\0" as *const u8
                    as *const libc::c_char);
            }
            perms = strtoul(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_15,
                8 as libc::c_int,
            );
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == '\0' as i32
                || endptr_15.is_null()
                || *endptr_15 as libc::c_int != '\0' as i32
                || perms > 0o7777 as libc::c_int as libc::c_ulong
            {
                die(b"--perms takes an octal argument <= 07777\0" as *const u8
                    as *const libc::c_char);
            }
            next_perms = perms as libc::c_int;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--size\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            let mut size: libc::c_ulonglong = 0;
            let mut endptr_16: *mut libc::c_char = 0 as *mut libc::c_char;
            if is_privileged != 0 {
                die(
                    b"The --size option is not permitted in setuid mode\0" as *const u8
                        as *const libc::c_char,
                );
            }
            if argc < 2 as libc::c_int {
                die(b"--size takes an argument\0" as *const u8 as *const libc::c_char);
            }
            if next_size_arg != 0 as libc::c_int as libc::c_ulong {
                die(b"--size given twice for the same action\0" as *const u8 as *const libc::c_char);
            }
            *__errno_location() = 0 as libc::c_int;
            size = strtoull(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_16,
                0 as libc::c_int,
            );
            if *__errno_location() != 0 as libc::c_int
                || *(*__ctype_b_loc()).offset(
                    *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                        as libc::c_int as isize,
                ) as libc::c_int
                    & _ISdigit as libc::c_int as libc::c_ushort as libc::c_int
                    == 0
                || endptr_16.is_null()
                || *endptr_16 as libc::c_int != '\0' as i32
                || size == 0 as libc::c_int as libc::c_ulonglong
            {
                die(b"--size takes a non-zero number of bytes\0" as *const u8
                    as *const libc::c_char);
            }
            if size
                > (18446744073709551615 as libc::c_ulong >> 1 as libc::c_int) as libc::c_ulonglong
            {
                die(
                    b"--size (for tmpfs) is limited to %zu\0" as *const u8 as *const libc::c_char,
                    18446744073709551615 as libc::c_ulong >> 1 as libc::c_int,
                );
            }
            next_size_arg = size as size_t;
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
        } else if strcmp(arg, b"--chmod\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int
        {
            let mut perms_0: libc::c_ulong = 0;
            let mut endptr_17: *mut libc::c_char = 0 as *mut libc::c_char;
            if argc < 3 as libc::c_int {
                die(b"--chmod takes two arguments\0" as *const u8 as *const libc::c_char);
            }
            perms_0 = strtoul(
                *argv.offset(1 as libc::c_int as isize),
                &mut endptr_17,
                8 as libc::c_int,
            );
            if *(*argv.offset(1 as libc::c_int as isize)).offset(0 as libc::c_int as isize)
                as libc::c_int
                == '\0' as i32
                || endptr_17.is_null()
                || *endptr_17 as libc::c_int != '\0' as i32
                || perms_0 > 0o7777 as libc::c_int as libc::c_ulong
            {
                die(b"--chmod takes an octal argument <= 07777\0" as *const u8
                    as *const libc::c_char);
            }
            op = setup_op_new(SETUP_CHMOD);
            (*op).flags = NO_CREATE_DEST;
            (*op).perms = perms_0 as libc::c_int;
            (*op).dest = *argv.offset(2 as libc::c_int as isize);
            argv = argv.offset(2 as libc::c_int as isize);
            argc -= 2 as libc::c_int;
        } else if strcmp(arg, b"--\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            argv = argv.offset(1 as libc::c_int as isize);
            argc -= 1 as libc::c_int;
            break;
        } else {
            if !(*arg as libc::c_int == '-' as i32) {
                break;
            }
            die(
                b"Unknown option %s\0" as *const u8 as *const libc::c_char,
                arg,
            );
        }
        if is_modifier_option(arg) == 0 && next_perms >= 0 as libc::c_int {
            die(
                b"--perms must be followed by an option that creates a file\0" as *const u8
                    as *const libc::c_char,
            );
        }
        if is_modifier_option(arg) == 0 && next_size_arg != 0 as libc::c_int as libc::c_ulong {
            die(b"--size must be followed by --tmpfs\0" as *const u8 as *const libc::c_char);
        }
        argv = argv.offset(1);
        argv;
        argc -= 1;
        argc;
    }
    *argcp = argc;
    *argvp = argv;
}
#[c2rust::src_loc = "2533:1"]
unsafe extern "C" fn parse_args(
    mut argcp: *mut libc::c_int,
    mut argvp: *mut *mut *const libc::c_char,
) {
    let mut total_parsed_argc: libc::c_int = *argcp;
    parse_args_recurse(argcp, argvp, 0 as libc::c_int, &mut total_parsed_argc);
}
#[c2rust::src_loc = "2542:1"]
unsafe extern "C" fn read_overflowids() {
    let mut uid_data: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut gid_data: *mut libc::c_char = 0 as *mut libc::c_char;
    uid_data = load_file_at(
        -(100 as libc::c_int),
        b"/proc/sys/kernel/overflowuid\0" as *const u8 as *const libc::c_char,
    );
    if uid_data.is_null() {
        die_with_error(
            b"Can't read /proc/sys/kernel/overflowuid\0" as *const u8 as *const libc::c_char,
        );
    }
    overflow_uid = strtol(uid_data, 0 as *mut *mut libc::c_char, 10 as libc::c_int) as uid_t;
    if overflow_uid == 0 as libc::c_int as libc::c_uint {
        die(b"Can't parse /proc/sys/kernel/overflowuid\0" as *const u8 as *const libc::c_char);
    }
    gid_data = load_file_at(
        -(100 as libc::c_int),
        b"/proc/sys/kernel/overflowgid\0" as *const u8 as *const libc::c_char,
    );
    if gid_data.is_null() {
        die_with_error(
            b"Can't read /proc/sys/kernel/overflowgid\0" as *const u8 as *const libc::c_char,
        );
    }
    overflow_gid = strtol(gid_data, 0 as *mut *mut libc::c_char, 10 as libc::c_int) as gid_t;
    if overflow_gid == 0 as libc::c_int as libc::c_uint {
        die(b"Can't parse /proc/sys/kernel/overflowgid\0" as *const u8 as *const libc::c_char);
    }
}
#[c2rust::src_loc = "2565:1"]
unsafe extern "C" fn namespace_ids_read(mut pid: pid_t) {
    let mut dir: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut ns_fd: libc::c_int = -(1 as libc::c_int);
    let mut info: *mut NsInfo = 0 as *mut NsInfo;
    dir = xasprintf(b"%d/ns\0" as *const u8 as *const libc::c_char, pid);
    ns_fd = openat(proc_fd, dir, 0o10000000 as libc::c_int);
    if ns_fd < 0 as libc::c_int {
        die_with_error(
            b"open /proc/%s/ns failed\0" as *const u8 as *const libc::c_char,
            dir,
        );
    }
    info = ns_infos.as_mut_ptr();
    while !((*info).name).is_null() {
        let mut do_unshare: *mut bool_0 = (*info).do_unshare;
        let mut st: stat = stat {
            st_dev: 0,
            st_ino: 0,
            st_nlink: 0,
            st_mode: 0,
            st_uid: 0,
            st_gid: 0,
            __pad0: 0,
            st_rdev: 0,
            st_size: 0,
            st_blksize: 0,
            st_blocks: 0,
            st_atim: timespec {
                tv_sec: 0,
                tv_nsec: 0,
            },
            st_mtim: timespec {
                tv_sec: 0,
                tv_nsec: 0,
            },
            st_ctim: timespec {
                tv_sec: 0,
                tv_nsec: 0,
            },
            __glibc_reserved: [0; 3],
        };
        let mut r: libc::c_int = 0;
        if !(!do_unshare.is_null() && *do_unshare == 0 as libc::c_int) {
            r = fstatat(ns_fd, (*info).name, &mut st, 0 as libc::c_int);
            if !(r != 0 as libc::c_int) {
                (*info).id = st.st_ino;
            }
        }
        info = info.offset(1);
        info;
    }
}
#[c2rust::src_loc = "2598:1"]
unsafe extern "C" fn namespace_ids_write(mut fd: libc::c_int, mut in_json: bool_0) {
    let mut info: *mut NsInfo = 0 as *mut NsInfo;
    info = ns_infos.as_mut_ptr();
    while !((*info).name).is_null() {
        let mut output: *mut libc::c_char = 0 as *mut libc::c_char;
        let mut indent: *const libc::c_char = 0 as *const libc::c_char;
        let mut nsid: uintmax_t = 0;
        nsid = (*info).id;
        if !(nsid == 0 as libc::c_int as libc::c_ulong) {
            indent = if in_json != 0 {
                b" \0" as *const u8 as *const libc::c_char
            } else {
                b"\n    \0" as *const u8 as *const libc::c_char
            };
            output = xasprintf(
                b",%s\"%s-namespace\": %ju\0" as *const u8 as *const libc::c_char,
                indent,
                (*info).name,
                nsid,
            );
            dump_info(fd, output, 1 as libc::c_int);
        }
        info = info.offset(1);
        info;
    }
}
#[c2rust::src_loc = "2624:1"]
unsafe fn main_0(mut argc: libc::c_int, mut argv: *mut *mut libc::c_char) -> libc::c_int {
    let mut old_umask: mode_t = 0;
    let mut base_path: *const libc::c_char = 0 as *const libc::c_char;
    let mut clone_flags: libc::c_int = 0;
    let mut old_cwd: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut pid: pid_t = 0;
    let mut event_fd: libc::c_int = -(1 as libc::c_int);
    let mut child_wait_fd: libc::c_int = -(1 as libc::c_int);
    let mut setup_finished_pipe: [libc::c_int; 2] = [-(1 as libc::c_int), -(1 as libc::c_int)];
    let mut new_cwd: *const libc::c_char = 0 as *const libc::c_char;
    let mut ns_uid: uid_t = 0;
    let mut ns_gid: gid_t = 0;
    let mut sbuf: stat = stat {
        st_dev: 0,
        st_ino: 0,
        st_nlink: 0,
        st_mode: 0,
        st_uid: 0,
        st_gid: 0,
        __pad0: 0,
        st_rdev: 0,
        st_size: 0,
        st_blksize: 0,
        st_blocks: 0,
        st_atim: timespec {
            tv_sec: 0,
            tv_nsec: 0,
        },
        st_mtim: timespec {
            tv_sec: 0,
            tv_nsec: 0,
        },
        st_ctim: timespec {
            tv_sec: 0,
            tv_nsec: 0,
        },
        __glibc_reserved: [0; 3],
    };
    let mut val: uint64_t = 0;
    let mut res: libc::c_int = 0;
    let mut args_data: *mut libc::c_char = 0 as *mut libc::c_char;
    let mut intermediate_pids_sockets: [libc::c_int; 2] =
        [-(1 as libc::c_int), -(1 as libc::c_int)];
    if argc == 2 as libc::c_int
        && strcmp(
            *argv.offset(1 as libc::c_int as isize),
            b"--version\0" as *const u8 as *const libc::c_char,
        ) == 0 as libc::c_int
    {
        print_version_and_exit();
    }
    real_uid = getuid();
    real_gid = getgid();
    acquire_privs();
    if prctl(
        38 as libc::c_int,
        1 as libc::c_int,
        0 as libc::c_int,
        0 as libc::c_int,
        0 as libc::c_int,
    ) < 0 as libc::c_int
    {
        die_with_error(b"prctl(PR_SET_NO_NEW_PRIVS) failed\0" as *const u8 as *const libc::c_char);
    }
    read_overflowids();
    argv0 = *argv.offset(0 as libc::c_int as isize);
    if isatty(1 as libc::c_int) != 0 {
        host_tty_dev = ttyname(1 as libc::c_int);
    }
    argv = argv.offset(1);
    argv;
    argc -= 1;
    argc;
    if argc <= 0 as libc::c_int {
        usage(1 as libc::c_int, stderr);
    }
    parse_args(
        &mut argc,
        &mut argv as *mut *mut *mut libc::c_char as *mut *mut *const libc::c_char,
    );
    args_data = opt_args_data;
    opt_args_data = 0 as *mut libc::c_char;
    if (requested_caps[0 as libc::c_int as usize] != 0
        || requested_caps[1 as libc::c_int as usize] != 0)
        && is_privileged != 0
    {
        die(
            b"--cap-add in setuid mode can be used only by root\0" as *const u8
                as *const libc::c_char,
        );
    }
    if opt_userns_block_fd != -(1 as libc::c_int) && opt_unshare_user == 0 {
        die(b"--userns-block-fd requires --unshare-user\0" as *const u8 as *const libc::c_char);
    }
    if opt_userns_block_fd != -(1 as libc::c_int) && opt_info_fd == -(1 as libc::c_int) {
        die(b"--userns-block-fd requires --info-fd\0" as *const u8 as *const libc::c_char);
    }
    if opt_userns_fd != -(1 as libc::c_int) && opt_unshare_user != 0 {
        die(b"--userns not compatible --unshare-user\0" as *const u8 as *const libc::c_char);
    }
    if opt_userns_fd != -(1 as libc::c_int) && opt_unshare_user_try != 0 {
        die(b"--userns not compatible --unshare-user-try\0" as *const u8 as *const libc::c_char);
    }
    if opt_disable_userns != 0 && opt_unshare_user == 0 {
        die(b"--disable-userns requires --unshare-user\0" as *const u8 as *const libc::c_char);
    }
    if opt_disable_userns != 0 && opt_userns_block_fd != -(1 as libc::c_int) {
        die(
            b"--disable-userns is not compatible with  --userns-block-fd\0" as *const u8
                as *const libc::c_char,
        );
    }
    if opt_userns_fd != -(1 as libc::c_int) && is_privileged != 0 {
        die(b"--userns doesn't work in setuid mode\0" as *const u8 as *const libc::c_char);
    }
    if opt_userns2_fd != -(1 as libc::c_int) && is_privileged != 0 {
        die(b"--userns2 doesn't work in setuid mode\0" as *const u8 as *const libc::c_char);
    }
    if is_privileged == 0
        && getuid() != 0 as libc::c_int as libc::c_uint
        && opt_userns_fd == -(1 as libc::c_int)
    {
        opt_unshare_user = 1 as libc::c_int;
    }
    if opt_unshare_user_try != 0
        && stat(
            b"/proc/self/ns/user\0" as *const u8 as *const libc::c_char,
            &mut sbuf,
        ) == 0 as libc::c_int
    {
        let mut disabled: bool_0 = 0 as libc::c_int;
        if stat(
            b"/sys/module/user_namespace/parameters/enable\0" as *const u8 as *const libc::c_char,
            &mut sbuf,
        ) == 0 as libc::c_int
        {
            let mut enable: *mut libc::c_char = 0 as *mut libc::c_char;
            enable = load_file_at(
                -(100 as libc::c_int),
                b"/sys/module/user_namespace/parameters/enable\0" as *const u8
                    as *const libc::c_char,
            );
            if !enable.is_null()
                && *enable.offset(0 as libc::c_int as isize) as libc::c_int == 'N' as i32
            {
                disabled = 1 as libc::c_int;
            }
        }
        if stat(
            b"/proc/sys/user/max_user_namespaces\0" as *const u8 as *const libc::c_char,
            &mut sbuf,
        ) == 0 as libc::c_int
        {
            let mut max_user_ns: *mut libc::c_char = 0 as *mut libc::c_char;
            max_user_ns = load_file_at(
                -(100 as libc::c_int),
                b"/proc/sys/user/max_user_namespaces\0" as *const u8 as *const libc::c_char,
            );
            if !max_user_ns.is_null()
                && strcmp(max_user_ns, b"0\n\0" as *const u8 as *const libc::c_char)
                    == 0 as libc::c_int
            {
                disabled = 1 as libc::c_int;
            }
        }
        if disabled == 0 {
            opt_unshare_user = 1 as libc::c_int;
        }
    }
    if argc <= 0 as libc::c_int {
        usage(1 as libc::c_int, stderr);
    }
    if opt_sandbox_uid == -(1 as libc::c_int) as uid_t {
        opt_sandbox_uid = real_uid;
    }
    if opt_sandbox_gid == -(1 as libc::c_int) as gid_t {
        opt_sandbox_gid = real_gid;
    }
    if opt_unshare_user == 0 && opt_userns_fd == -(1 as libc::c_int) && opt_sandbox_uid != real_uid
    {
        die(
            b"Specifying --uid requires --unshare-user or --userns\0" as *const u8
                as *const libc::c_char,
        );
    }
    if opt_unshare_user == 0 && opt_userns_fd == -(1 as libc::c_int) && opt_sandbox_gid != real_gid
    {
        die(
            b"Specifying --gid requires --unshare-user or --userns\0" as *const u8
                as *const libc::c_char,
        );
    }
    if opt_unshare_uts == 0 && !opt_sandbox_hostname.is_null() {
        die(b"Specifying --hostname requires --unshare-uts\0" as *const u8 as *const libc::c_char);
    }
    if opt_as_pid_1 != 0 && opt_unshare_pid == 0 {
        die(b"Specifying --as-pid-1 requires --unshare-pid\0" as *const u8 as *const libc::c_char);
    }
    if opt_as_pid_1 != 0 && !lock_files.is_null() {
        die(
            b"Specifying --as-pid-1 and --lock-file is not permitted\0" as *const u8
                as *const libc::c_char,
        );
    }
    proc_fd = open(
        b"/proc\0" as *const u8 as *const libc::c_char,
        0o10000000 as libc::c_int,
    );
    if proc_fd == -(1 as libc::c_int) {
        die_with_error(b"Can't open /proc\0" as *const u8 as *const libc::c_char);
    }
    base_path = b"/tmp\0" as *const u8 as *const libc::c_char;
    if opt_unshare_pid != 0 && opt_as_pid_1 == 0 {
        event_fd = eventfd(
            0 as libc::c_int as libc::c_uint,
            EFD_CLOEXEC as libc::c_int | EFD_NONBLOCK as libc::c_int,
        );
        if event_fd == -(1 as libc::c_int) {
            die_with_error(b"eventfd()\0" as *const u8 as *const libc::c_char);
        }
    }
    block_sigchild();
    clone_flags = 17 as libc::c_int | 0x20000 as libc::c_int;
    if opt_unshare_user != 0 {
        clone_flags |= 0x10000000 as libc::c_int;
    }
    if opt_unshare_pid != 0 && opt_pidns_fd == -(1 as libc::c_int) {
        clone_flags |= 0x20000000 as libc::c_int;
    }
    if opt_unshare_net != 0 {
        clone_flags |= 0x40000000 as libc::c_int;
    }
    if opt_unshare_ipc != 0 {
        clone_flags |= 0x8000000 as libc::c_int;
    }
    if opt_unshare_uts != 0 {
        clone_flags |= 0x4000000 as libc::c_int;
    }
    if opt_unshare_cgroup != 0 {
        if stat(
            b"/proc/self/ns/cgroup\0" as *const u8 as *const libc::c_char,
            &mut sbuf,
        ) != 0
        {
            if *__errno_location() == 2 as libc::c_int {
                die(
                    b"Cannot create new cgroup namespace because the kernel does not support it\0"
                        as *const u8 as *const libc::c_char,
                );
            } else {
                die_with_error(
                    b"stat on /proc/self/ns/cgroup failed\0" as *const u8 as *const libc::c_char,
                );
            }
        }
        clone_flags |= 0x2000000 as libc::c_int;
    }
    if opt_unshare_cgroup_try != 0 {
        opt_unshare_cgroup = (stat(
            b"/proc/self/ns/cgroup\0" as *const u8 as *const libc::c_char,
            &mut sbuf,
        ) == 0) as libc::c_int;
        if opt_unshare_cgroup != 0 {
            clone_flags |= 0x2000000 as libc::c_int;
        }
    }
    child_wait_fd = eventfd(0 as libc::c_int as libc::c_uint, EFD_CLOEXEC as libc::c_int);
    if child_wait_fd == -(1 as libc::c_int) {
        die_with_error(b"eventfd()\0" as *const u8 as *const libc::c_char);
    }
    if opt_json_status_fd != -(1 as libc::c_int) {
        let mut ret: libc::c_int = 0;
        ret = pipe2(setup_finished_pipe.as_mut_ptr(), 0o2000000 as libc::c_int);
        if ret == -(1 as libc::c_int) {
            die_with_error(b"pipe2()\0" as *const u8 as *const libc::c_char);
        }
    }
    if opt_userns_fd > 0 as libc::c_int
        && setns(opt_userns_fd, 0x10000000 as libc::c_int) != 0 as libc::c_int
    {
        if *__errno_location() == 22 as libc::c_int {
            die(
                b"Joining the specified user namespace failed, it might not be a descendant of the current user namespace.\0"
                    as *const u8 as *const libc::c_char,
            );
        }
        die_with_error(
            b"Joining specified user namespace failed\0" as *const u8 as *const libc::c_char,
        );
    }
    if opt_pidns_fd != -(1 as libc::c_int) {
        prctl(
            36 as libc::c_int,
            1 as libc::c_int,
            0 as libc::c_int,
            0 as libc::c_int,
            0 as libc::c_int,
        );
        create_pid_socketpair(intermediate_pids_sockets.as_mut_ptr());
    }
    pid = raw_clone(clone_flags as libc::c_ulong, 0 as *mut libc::c_void);
    if pid == -(1 as libc::c_int) {
        if opt_unshare_user != 0 {
            if *__errno_location() == 22 as libc::c_int {
                die(
                    b"Creating new namespace failed, likely because the kernel does not support user namespaces.  bwrap must be installed setuid on such systems.\0"
                        as *const u8 as *const libc::c_char,
                );
            } else if *__errno_location() == 1 as libc::c_int && is_privileged == 0 {
                die(
                    b"No permissions to creating new namespace, likely because the kernel does not allow non-privileged user namespaces. On e.g. debian this can be enabled with 'sysctl kernel.unprivileged_userns_clone=1'.\0"
                        as *const u8 as *const libc::c_char,
                );
            }
        }
        if *__errno_location() == 28 as libc::c_int {
            die(
                b"Creating new namespace failed: nesting depth or /proc/sys/user/max_*_namespaces exceeded (ENOSPC)\0"
                    as *const u8 as *const libc::c_char,
            );
        }
        die_with_error(b"Creating new namespace failed\0" as *const u8 as *const libc::c_char);
    }
    ns_uid = opt_sandbox_uid;
    ns_gid = opt_sandbox_gid;
    if pid != 0 as libc::c_int {
        if intermediate_pids_sockets[0 as libc::c_int as usize] != -(1 as libc::c_int) {
            close(intermediate_pids_sockets[1 as libc::c_int as usize]);
            pid = read_pid_from_socket(intermediate_pids_sockets[0 as libc::c_int as usize]);
            close(intermediate_pids_sockets[0 as libc::c_int as usize]);
        }
        namespace_ids_read(pid);
        if is_privileged != 0 && opt_unshare_user != 0 && opt_userns_block_fd == -(1 as libc::c_int)
        {
            write_uid_gid_map(
                ns_uid,
                real_uid,
                ns_gid,
                real_gid,
                pid,
                1 as libc::c_int,
                opt_needs_devpts,
            );
        }
        if opt_userns2_fd > 0 as libc::c_int
            && setns(opt_userns2_fd, 0x10000000 as libc::c_int) != 0 as libc::c_int
        {
            die_with_error(b"Setting userns2 failed\0" as *const u8 as *const libc::c_char);
        }
        drop_privs(0 as libc::c_int, 0 as libc::c_int);
        handle_die_with_parent();
        if opt_info_fd != -(1 as libc::c_int) {
            let mut output: *mut libc::c_char = xasprintf(
                b"{\n    \"child-pid\": %i\0" as *const u8 as *const libc::c_char,
                pid,
            );
            dump_info(opt_info_fd, output, 1 as libc::c_int);
            namespace_ids_write(opt_info_fd, 0 as libc::c_int);
            dump_info(
                opt_info_fd,
                b"\n}\n\0" as *const u8 as *const libc::c_char,
                1 as libc::c_int,
            );
            close(opt_info_fd);
        }
        if opt_json_status_fd != -(1 as libc::c_int) {
            let mut output_0: *mut libc::c_char = xasprintf(
                b"{ \"child-pid\": %i\0" as *const u8 as *const libc::c_char,
                pid,
            );
            dump_info(opt_json_status_fd, output_0, 1 as libc::c_int);
            namespace_ids_write(opt_json_status_fd, 1 as libc::c_int);
            dump_info(
                opt_json_status_fd,
                b" }\n\0" as *const u8 as *const libc::c_char,
                1 as libc::c_int,
            );
        }
        if opt_userns_block_fd != -(1 as libc::c_int) {
            let mut b: [libc::c_char; 1] = [0; 1];
            ({
                let mut __result: libc::c_long = 0;
                loop {
                    __result = read(
                        opt_userns_block_fd,
                        b.as_mut_ptr() as *mut libc::c_void,
                        1 as libc::c_int as size_t,
                    );
                    if !(__result == -(1 as libc::c_long)
                        && *__errno_location() == 4 as libc::c_int)
                    {
                        break;
                    }
                }
                __result
            });
            ({
                let mut __result: libc::c_long = 0;
                loop {
                    __result = read(
                        opt_userns_block_fd,
                        b.as_mut_ptr() as *mut libc::c_void,
                        1 as libc::c_int as size_t,
                    );
                    if !(__result == -(1 as libc::c_long)
                        && *__errno_location() == 4 as libc::c_int)
                    {
                        break;
                    }
                }
                __result
            });
            close(opt_userns_block_fd);
        }
        val = 1 as libc::c_int as uint64_t;
        res = write(
            child_wait_fd,
            &mut val as *mut uint64_t as *const libc::c_void,
            8 as libc::c_int as size_t,
        ) as libc::c_int;
        close(child_wait_fd);
        return monitor_child(
            event_fd,
            pid,
            setup_finished_pipe[0 as libc::c_int as usize],
        );
    }
    if opt_pidns_fd > 0 as libc::c_int {
        if setns(opt_pidns_fd, 0x20000000 as libc::c_int) != 0 as libc::c_int {
            die_with_error(b"Setting pidns failed\0" as *const u8 as *const libc::c_char);
        }
        fork_intermediate_child();
        if opt_unshare_pid != 0 {
            if unshare(0x20000000 as libc::c_int) != 0 {
                die_with_error(b"unshare pid ns\0" as *const u8 as *const libc::c_char);
            }
            fork_intermediate_child();
        }
        close(intermediate_pids_sockets[0 as libc::c_int as usize]);
        send_pid_on_socket(intermediate_pids_sockets[1 as libc::c_int as usize]);
        close(intermediate_pids_sockets[1 as libc::c_int as usize]);
    }
    if opt_info_fd != -(1 as libc::c_int) {
        close(opt_info_fd);
    }
    if opt_json_status_fd != -(1 as libc::c_int) {
        close(opt_json_status_fd);
    }
    res = read(
        child_wait_fd,
        &mut val as *mut uint64_t as *mut libc::c_void,
        8 as libc::c_int as size_t,
    ) as libc::c_int;
    close(child_wait_fd);
    switch_to_user_with_privs();
    if opt_unshare_net != 0 {
        loopback_setup();
    }
    ns_uid = opt_sandbox_uid;
    ns_gid = opt_sandbox_gid;
    if is_privileged == 0 && opt_unshare_user != 0 && opt_userns_block_fd == -(1 as libc::c_int) {
        if opt_needs_devpts != 0 {
            ns_uid = 0 as libc::c_int as uid_t;
            ns_gid = 0 as libc::c_int as gid_t;
        }
        write_uid_gid_map(
            ns_uid,
            real_uid,
            ns_gid,
            real_gid,
            -(1 as libc::c_int),
            1 as libc::c_int,
            0 as libc::c_int,
        );
    }
    old_umask = umask(0 as libc::c_int as __mode_t);
    resolve_symlinks_in_ops();
    if mount(
        0 as *const libc::c_char,
        b"/\0" as *const u8 as *const libc::c_char,
        0 as *const libc::c_char,
        (MS_SILENT as libc::c_int | MS_SLAVE as libc::c_int | MS_REC as libc::c_int)
            as libc::c_ulong,
        0 as *const libc::c_void,
    ) < 0 as libc::c_int
    {
        die_with_error(b"Failed to make / slave\0" as *const u8 as *const libc::c_char);
    }
    if mount(
        b"tmpfs\0" as *const u8 as *const libc::c_char,
        base_path,
        b"tmpfs\0" as *const u8 as *const libc::c_char,
        (MS_NODEV as libc::c_int | MS_NOSUID as libc::c_int) as libc::c_ulong,
        0 as *const libc::c_void,
    ) != 0 as libc::c_int
    {
        die_with_error(b"Failed to mount tmpfs\0" as *const u8 as *const libc::c_char);
    }
    old_cwd = get_current_dir_name();
    if chdir(base_path) != 0 as libc::c_int {
        die_with_error(b"chdir base_path\0" as *const u8 as *const libc::c_char);
    }
    if mkdir(
        b"newroot\0" as *const u8 as *const libc::c_char,
        0o755 as libc::c_int as __mode_t,
    ) != 0
    {
        die_with_error(b"Creating newroot failed\0" as *const u8 as *const libc::c_char);
    }
    if mount(
        b"newroot\0" as *const u8 as *const libc::c_char,
        b"newroot\0" as *const u8 as *const libc::c_char,
        0 as *const libc::c_char,
        (MS_SILENT as libc::c_int as libc::c_uint
            | 0xc0ed0000 as libc::c_uint
            | MS_BIND as libc::c_int as libc::c_uint
            | MS_REC as libc::c_int as libc::c_uint) as libc::c_ulong,
        0 as *const libc::c_void,
    ) < 0 as libc::c_int
    {
        die_with_error(b"setting up newroot bind\0" as *const u8 as *const libc::c_char);
    }
    if mkdir(
        b"oldroot\0" as *const u8 as *const libc::c_char,
        0o755 as libc::c_int as __mode_t,
    ) != 0
    {
        die_with_error(b"Creating oldroot failed\0" as *const u8 as *const libc::c_char);
    }
    if pivot_root(base_path, b"oldroot\0" as *const u8 as *const libc::c_char) != 0 {
        die_with_error(b"pivot_root\0" as *const u8 as *const libc::c_char);
    }
    if chdir(b"/\0" as *const u8 as *const libc::c_char) != 0 as libc::c_int {
        die_with_error(b"chdir / (base path)\0" as *const u8 as *const libc::c_char);
    }
    if is_privileged != 0 {
        let mut child: pid_t = 0;
        let mut privsep_sockets: [libc::c_int; 2] = [0; 2];
        if socketpair(
            1 as libc::c_int,
            SOCK_SEQPACKET as libc::c_int | SOCK_CLOEXEC as libc::c_int,
            0 as libc::c_int,
            privsep_sockets.as_mut_ptr(),
        ) != 0 as libc::c_int
        {
            die_with_error(b"Can't create privsep socket\0" as *const u8 as *const libc::c_char);
        }
        child = fork();
        if child == -(1 as libc::c_int) {
            die_with_error(b"Can't fork unprivileged helper\0" as *const u8 as *const libc::c_char);
        }
        if child == 0 as libc::c_int {
            drop_privs(0 as libc::c_int, 1 as libc::c_int);
            close(privsep_sockets[0 as libc::c_int as usize]);
            setup_newroot(opt_unshare_pid, privsep_sockets[1 as libc::c_int as usize]);
            exit(0 as libc::c_int);
        } else {
            let mut status: libc::c_int = 0;
            let mut buffer: [uint32_t; 2048] = [0; 2048];
            let mut op: uint32_t = 0;
            let mut flags: uint32_t = 0;
            let mut perms: uint32_t = 0;
            let mut size_arg: size_t = 0;
            let mut arg1: *const libc::c_char = 0 as *const libc::c_char;
            let mut arg2: *const libc::c_char = 0 as *const libc::c_char;
            let mut unpriv_socket: libc::c_int = -(1 as libc::c_int);
            unpriv_socket = privsep_sockets[0 as libc::c_int as usize];
            close(privsep_sockets[1 as libc::c_int as usize]);
            loop {
                op = read_priv_sec_op(
                    unpriv_socket,
                    buffer.as_mut_ptr() as *mut libc::c_void,
                    ::core::mem::size_of::<[uint32_t; 2048]>() as libc::c_ulong,
                    &mut flags,
                    &mut perms,
                    &mut size_arg,
                    &mut arg1,
                    &mut arg2,
                );
                privileged_op(-(1 as libc::c_int), op, flags, perms, size_arg, arg1, arg2);
                if write(
                    unpriv_socket,
                    buffer.as_mut_ptr() as *const libc::c_void,
                    1 as libc::c_int as size_t,
                ) != 1 as libc::c_int as libc::c_long
                {
                    die(b"Can't write to op_socket\0" as *const u8 as *const libc::c_char);
                }
                if !(op != PRIV_SEP_OP_DONE as libc::c_int as libc::c_uint) {
                    break;
                }
            }
            waitpid(child, &mut status, 0 as libc::c_int);
        }
    } else {
        setup_newroot(opt_unshare_pid, -(1 as libc::c_int));
    }
    close_ops_fd();
    if mount(
        b"oldroot\0" as *const u8 as *const libc::c_char,
        b"oldroot\0" as *const u8 as *const libc::c_char,
        0 as *const libc::c_char,
        (MS_SILENT as libc::c_int | MS_REC as libc::c_int | MS_PRIVATE as libc::c_int)
            as libc::c_ulong,
        0 as *const libc::c_void,
    ) != 0 as libc::c_int
    {
        die_with_error(b"Failed to make old root rprivate\0" as *const u8 as *const libc::c_char);
    }
    if umount2(
        b"oldroot\0" as *const u8 as *const libc::c_char,
        MNT_DETACH as libc::c_int,
    ) != 0
    {
        die_with_error(b"unmount old root\0" as *const u8 as *const libc::c_char);
    }
    let mut oldrootfd: libc::c_int = open(
        b"/\0" as *const u8 as *const libc::c_char,
        0o200000 as libc::c_int | 0 as libc::c_int,
    );
    if oldrootfd < 0 as libc::c_int {
        die_with_error(b"can't open /\0" as *const u8 as *const libc::c_char);
    }
    if chdir(b"/newroot\0" as *const u8 as *const libc::c_char) != 0 as libc::c_int {
        die_with_error(b"chdir /newroot\0" as *const u8 as *const libc::c_char);
    }
    if pivot_root(
        b".\0" as *const u8 as *const libc::c_char,
        b".\0" as *const u8 as *const libc::c_char,
    ) != 0 as libc::c_int
    {
        die_with_error(b"pivot_root(/newroot)\0" as *const u8 as *const libc::c_char);
    }
    if fchdir(oldrootfd) < 0 as libc::c_int {
        die_with_error(b"fchdir to oldroot\0" as *const u8 as *const libc::c_char);
    }
    if umount2(
        b".\0" as *const u8 as *const libc::c_char,
        MNT_DETACH as libc::c_int,
    ) < 0 as libc::c_int
    {
        die_with_error(b"umount old root\0" as *const u8 as *const libc::c_char);
    }
    if chdir(b"/\0" as *const u8 as *const libc::c_char) != 0 as libc::c_int {
        die_with_error(b"chdir /\0" as *const u8 as *const libc::c_char);
    }
    if opt_userns2_fd > 0 as libc::c_int
        && setns(opt_userns2_fd, 0x10000000 as libc::c_int) != 0 as libc::c_int
    {
        die_with_error(b"Setting userns2 failed\0" as *const u8 as *const libc::c_char);
    }
    if opt_unshare_user != 0
        && opt_userns_block_fd == -(1 as libc::c_int)
        && (ns_uid != opt_sandbox_uid || ns_gid != opt_sandbox_gid || opt_disable_userns != 0)
    {
        if opt_disable_userns != 0 {
            let mut sysctl_fd: libc::c_int = -(1 as libc::c_int);
            sysctl_fd = openat(
                proc_fd,
                b"sys/user/max_user_namespaces\0" as *const u8 as *const libc::c_char,
                0o1 as libc::c_int,
            );
            if sysctl_fd < 0 as libc::c_int {
                die_with_error(
                    b"cannot open /proc/sys/user/max_user_namespaces\0" as *const u8
                        as *const libc::c_char,
                );
            }
            if write_to_fd(
                sysctl_fd,
                b"1\0" as *const u8 as *const libc::c_char,
                1 as libc::c_int as ssize_t,
            ) < 0 as libc::c_int
            {
                die_with_error(
                    b"sysctl user.max_user_namespaces = 1\0" as *const u8 as *const libc::c_char,
                );
            }
        }
        if unshare(0x10000000 as libc::c_int) != 0 {
            die_with_error(b"unshare user ns\0" as *const u8 as *const libc::c_char);
        }
        drop_cap_bounding_set(0 as libc::c_int);
        write_uid_gid_map(
            opt_sandbox_uid,
            ns_uid,
            opt_sandbox_gid,
            ns_gid,
            -(1 as libc::c_int),
            0 as libc::c_int,
            0 as libc::c_int,
        );
    }
    if opt_disable_userns != 0 || opt_assert_userns_disabled != 0 {
        res = unshare(0x10000000 as libc::c_int);
        if res == 0 as libc::c_int {
            die(
                b"creation of new user namespaces was not disabled as requested\0" as *const u8
                    as *const libc::c_char,
            );
        }
    }
    drop_privs((is_privileged == 0) as libc::c_int, 1 as libc::c_int);
    if opt_block_fd != -(1 as libc::c_int) {
        let mut b_0: [libc::c_char; 1] = [0; 1];
        ({
            let mut __result: libc::c_long = 0;
            loop {
                __result = read(
                    opt_block_fd,
                    b_0.as_mut_ptr() as *mut libc::c_void,
                    1 as libc::c_int as size_t,
                );
                if !(__result == -(1 as libc::c_long) && *__errno_location() == 4 as libc::c_int) {
                    break;
                }
            }
            __result
        });
        ({
            let mut __result: libc::c_long = 0;
            loop {
                __result = read(
                    opt_block_fd,
                    b_0.as_mut_ptr() as *mut libc::c_void,
                    1 as libc::c_int as size_t,
                );
                if !(__result == -(1 as libc::c_long) && *__errno_location() == 4 as libc::c_int) {
                    break;
                }
            }
            __result
        });
        close(opt_block_fd);
    }
    if opt_seccomp_fd != -(1 as libc::c_int) {
        if seccomp_programs.is_null() {
        } else {
            __assert_fail(
                b"seccomp_programs == NULL\0" as *const u8 as *const libc::c_char,
                b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                3245 as libc::c_int as libc::c_uint,
                (*::core::mem::transmute::<&[u8; 23], &[libc::c_char; 23]>(
                    b"int main(int, char **)\0",
                ))
                .as_ptr(),
            );
        }
        'c_17422: {
            if seccomp_programs.is_null() {
            } else {
                __assert_fail(
                    b"seccomp_programs == NULL\0" as *const u8 as *const libc::c_char,
                    b"../bubblewrap.c\0" as *const u8 as *const libc::c_char,
                    3245 as libc::c_int as libc::c_uint,
                    (*::core::mem::transmute::<&[u8; 23], &[libc::c_char; 23]>(
                        b"int main(int, char **)\0",
                    ))
                    .as_ptr(),
                );
            }
        };
        seccomp_program_new(&mut opt_seccomp_fd);
    }
    umask(old_umask);
    new_cwd = b"/\0" as *const u8 as *const libc::c_char;
    if !opt_chdir_path.is_null() {
        if chdir(opt_chdir_path) != 0 {
            die_with_error(
                b"Can't chdir to %s\0" as *const u8 as *const libc::c_char,
                opt_chdir_path,
            );
        }
        new_cwd = opt_chdir_path;
    } else if chdir(old_cwd) == 0 as libc::c_int {
        new_cwd = old_cwd;
    } else {
        let mut home: *const libc::c_char = getenv(b"HOME\0" as *const u8 as *const libc::c_char);
        if !home.is_null() && chdir(home) == 0 as libc::c_int {
            new_cwd = home;
        }
    }
    xsetenv(
        b"PWD\0" as *const u8 as *const libc::c_char,
        new_cwd,
        1 as libc::c_int,
    );
    free(old_cwd as *mut libc::c_void);
    if opt_new_session != 0 && setsid() == -(1 as libc::c_int) {
        die_with_error(b"setsid\0" as *const u8 as *const libc::c_char);
    }
    if label_exec(opt_exec_label) == -(1 as libc::c_int) {
        die_with_error(
            b"label_exec %s\0" as *const u8 as *const libc::c_char,
            *argv.offset(0 as libc::c_int as isize),
        );
    }
    if opt_as_pid_1 == 0
        && (opt_unshare_pid != 0 || !lock_files.is_null() || opt_sync_fd != -(1 as libc::c_int))
    {
        pid = fork();
        if pid == -(1 as libc::c_int) {
            die_with_error(b"Can't fork for pid 1\0" as *const u8 as *const libc::c_char);
        }
        if pid != 0 as libc::c_int {
            drop_all_caps(0 as libc::c_int);
            let mut dont_close: [libc::c_int; 3] = [0; 3];
            let mut j: libc::c_int = 0 as libc::c_int;
            if event_fd != -(1 as libc::c_int) {
                let fresh5 = j;
                j = j + 1;
                dont_close[fresh5 as usize] = event_fd;
            }
            if opt_sync_fd != -(1 as libc::c_int) {
                let fresh6 = j;
                j = j + 1;
                dont_close[fresh6 as usize] = opt_sync_fd;
            }
            let fresh7 = j;
            j = j + 1;
            dont_close[fresh7 as usize] = -(1 as libc::c_int);
            fdwalk(
                proc_fd,
                Some(
                    close_extra_fds
                        as unsafe extern "C" fn(*mut libc::c_void, libc::c_int) -> libc::c_int,
                ),
                dont_close.as_mut_ptr() as *mut libc::c_void,
            );
            return do_init(event_fd, pid);
        }
    }
    if proc_fd != -(1 as libc::c_int) {
        close(proc_fd);
    }
    if opt_as_pid_1 == 0 {
        if opt_sync_fd != -(1 as libc::c_int) {
            close(opt_sync_fd);
        }
    }
    unblock_sigchild();
    handle_die_with_parent();
    if is_privileged == 0 {
        set_ambient_capabilities();
    }
    seccomp_programs_apply();
    if setup_finished_pipe[1 as libc::c_int as usize] != -(1 as libc::c_int) {
        let mut data: libc::c_char = 0 as libc::c_int as libc::c_char;
        res = write_to_fd(
            setup_finished_pipe[1 as libc::c_int as usize],
            &mut data,
            1 as libc::c_int as ssize_t,
        );
    }
    if execvp(
        *argv.offset(0 as libc::c_int as isize),
        argv as *const *mut libc::c_char,
    ) == -(1 as libc::c_int)
    {
        if setup_finished_pipe[1 as libc::c_int as usize] != -(1 as libc::c_int) {
            let mut saved_errno: libc::c_int = *__errno_location();
            let mut data_0: libc::c_char = 0 as libc::c_int as libc::c_char;
            res = write_to_fd(
                setup_finished_pipe[1 as libc::c_int as usize],
                &mut data_0,
                1 as libc::c_int as ssize_t,
            );
            *__errno_location() = saved_errno;
        }
        die_with_error(
            b"execvp %s\0" as *const u8 as *const libc::c_char,
            *argv.offset(0 as libc::c_int as isize),
        );
    }
    return 0 as libc::c_int;
}
pub fn main() {
    let mut args: Vec<*mut libc::c_char> = Vec::new();
    for arg in ::std::env::args() {
        args.push(
            (::std::ffi::CString::new(arg))
                .expect("Failed to convert argument into CString.")
                .into_raw(),
        );
    }
    args.push(::core::ptr::null_mut());
    unsafe {
        ::std::process::exit(main_0(
            (args.len() - 1) as libc::c_int,
            args.as_mut_ptr() as *mut *mut libc::c_char,
        ) as i32)
    }
}
