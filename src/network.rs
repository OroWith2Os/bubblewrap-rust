use ::libc;
#[c2rust::header_src = "/usr/include/bits/types.h:22"]
pub mod types_h {
    #[c2rust::src_loc = "38:1"]
    pub type __uint8_t = libc::c_uchar;
    #[c2rust::src_loc = "40:1"]
    pub type __uint16_t = libc::c_ushort;
    #[c2rust::src_loc = "42:1"]
    pub type __uint32_t = libc::c_uint;
    #[c2rust::src_loc = "154:1"]
    pub type __pid_t = libc::c_int;
    #[c2rust::src_loc = "194:1"]
    pub type __ssize_t = libc::c_long;
    #[c2rust::src_loc = "210:1"]
    pub type __socklen_t = libc::c_uint;
}
#[c2rust::header_src = "/usr/include/bits/stdint-uintn.h:22"]
pub mod stdint_uintn_h {
    #[c2rust::src_loc = "24:1"]
    pub type uint8_t = __uint8_t;
    #[c2rust::src_loc = "25:1"]
    pub type uint16_t = __uint16_t;
    #[c2rust::src_loc = "26:1"]
    pub type uint32_t = __uint32_t;
    use super::types_h::{__uint16_t, __uint32_t, __uint8_t};
}
#[c2rust::header_src = "/usr/lib/clang/15.0.7/include/stddef.h:22"]
pub mod stddef_h {
    #[c2rust::src_loc = "46:1"]
    pub type size_t = libc::c_ulong;
}
#[c2rust::header_src = "/usr/include/sys/types.h:22"]
pub mod sys_types_h {
    #[c2rust::src_loc = "97:1"]
    pub type pid_t = __pid_t;
    #[c2rust::src_loc = "108:1"]
    pub type ssize_t = __ssize_t;
    use super::types_h::{__pid_t, __ssize_t};
}
#[c2rust::header_src = "/usr/include/bits/socket.h:22"]
pub mod socket_h {
    #[c2rust::src_loc = "33:1"]
    pub type socklen_t = __socklen_t;
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "183:8"]
    pub struct sockaddr {
        pub sa_family: sa_family_t,
        pub sa_data: [libc::c_char; 14],
    }
    use super::sockaddr_h::sa_family_t;
    use super::types_h::__socklen_t;
}
#[c2rust::header_src = "/usr/include/bits/socket_type.h:22"]
pub mod socket_type_h {
    #[c2rust::src_loc = "24:1"]
    pub type __socket_type = libc::c_uint;
    #[c2rust::src_loc = "52:3"]
    pub const SOCK_NONBLOCK: __socket_type = 2048;
    #[c2rust::src_loc = "49:3"]
    pub const SOCK_CLOEXEC: __socket_type = 524288;
    #[c2rust::src_loc = "41:3"]
    pub const SOCK_PACKET: __socket_type = 10;
    #[c2rust::src_loc = "39:3"]
    pub const SOCK_DCCP: __socket_type = 6;
    #[c2rust::src_loc = "36:3"]
    pub const SOCK_SEQPACKET: __socket_type = 5;
    #[c2rust::src_loc = "34:3"]
    pub const SOCK_RDM: __socket_type = 4;
    #[c2rust::src_loc = "32:3"]
    pub const SOCK_RAW: __socket_type = 3;
    #[c2rust::src_loc = "29:3"]
    pub const SOCK_DGRAM: __socket_type = 2;
    #[c2rust::src_loc = "26:3"]
    pub const SOCK_STREAM: __socket_type = 1;
}
#[c2rust::header_src = "/usr/include/bits/sockaddr.h:22"]
pub mod sockaddr_h {
    #[c2rust::src_loc = "28:1"]
    pub type sa_family_t = libc::c_ushort;
}
#[c2rust::header_src = "/usr/include/sys/socket.h:22"]
pub mod sys_socket_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "83:9"]
    pub union __CONST_SOCKADDR_ARG {
        pub __sockaddr__: *const sockaddr,
        pub __sockaddr_at__: *const sockaddr_at,
        pub __sockaddr_ax25__: *const sockaddr_ax25,
        pub __sockaddr_dl__: *const sockaddr_dl,
        pub __sockaddr_eon__: *const sockaddr_eon,
        pub __sockaddr_in__: *const sockaddr_in,
        pub __sockaddr_in6__: *const sockaddr_in6,
        pub __sockaddr_inarp__: *const sockaddr_inarp,
        pub __sockaddr_ipx__: *const sockaddr_ipx,
        pub __sockaddr_iso__: *const sockaddr_iso,
        pub __sockaddr_ns__: *const sockaddr_ns,
        pub __sockaddr_un__: *const sockaddr_un,
        pub __sockaddr_x25__: *const sockaddr_x25,
    }
    use super::in_h::{sockaddr_in, sockaddr_in6};
    use super::socket_h::{sockaddr, socklen_t};
    use super::stddef_h::size_t;
    use super::sys_types_h::ssize_t;
    extern "C" {
        #[c2rust::src_loc = "79:17"]
        pub type sockaddr_x25;
        #[c2rust::src_loc = "79:17"]
        pub type sockaddr_un;
        #[c2rust::src_loc = "79:17"]
        pub type sockaddr_ns;
        #[c2rust::src_loc = "79:17"]
        pub type sockaddr_iso;
        #[c2rust::src_loc = "79:17"]
        pub type sockaddr_ipx;
        #[c2rust::src_loc = "79:17"]
        pub type sockaddr_inarp;
        #[c2rust::src_loc = "79:17"]
        pub type sockaddr_eon;
        #[c2rust::src_loc = "79:17"]
        pub type sockaddr_dl;
        #[c2rust::src_loc = "79:17"]
        pub type sockaddr_ax25;
        #[c2rust::src_loc = "79:17"]
        pub type sockaddr_at;
        #[c2rust::src_loc = "102:1"]
        pub fn socket(
            __domain: libc::c_int,
            __type: libc::c_int,
            __protocol: libc::c_int,
        ) -> libc::c_int;
        #[c2rust::src_loc = "112:1"]
        pub fn bind(
            __fd: libc::c_int,
            __addr: __CONST_SOCKADDR_ARG,
            __len: socklen_t,
        ) -> libc::c_int;
        #[c2rust::src_loc = "145:1"]
        pub fn recv(
            __fd: libc::c_int,
            __buf: *mut libc::c_void,
            __n: size_t,
            __flags: libc::c_int,
        ) -> ssize_t;
        #[c2rust::src_loc = "152:1"]
        pub fn sendto(
            __fd: libc::c_int,
            __buf: *const libc::c_void,
            __n: size_t,
            __flags: libc::c_int,
            __addr: __CONST_SOCKADDR_ARG,
            __addr_len: socklen_t,
        ) -> ssize_t;
    }
}
#[c2rust::header_src = "/usr/include/netinet/in.h:22"]
pub mod in_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "262:8"]
    pub struct sockaddr_in6 {
        pub sin6_family: sa_family_t,
        pub sin6_port: in_port_t,
        pub sin6_flowinfo: uint32_t,
        pub sin6_addr: in6_addr,
        pub sin6_scope_id: uint32_t,
    }
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "221:8"]
    pub struct in6_addr {
        pub __in6_u: C2RustUnnamed,
    }
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "223:5"]
    pub union C2RustUnnamed {
        pub __u6_addr8: [uint8_t; 16],
        pub __u6_addr16: [uint16_t; 8],
        pub __u6_addr32: [uint32_t; 4],
    }
    #[c2rust::src_loc = "125:1"]
    pub type in_port_t = uint16_t;
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "247:8"]
    pub struct sockaddr_in {
        pub sin_family: sa_family_t,
        pub sin_port: in_port_t,
        pub sin_addr: in_addr,
        pub sin_zero: [libc::c_uchar; 8],
    }
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "31:8"]
    pub struct in_addr {
        pub s_addr: in_addr_t,
    }
    #[c2rust::src_loc = "30:1"]
    pub type in_addr_t = uint32_t;
    use super::sockaddr_h::sa_family_t;
    use super::stdint_uintn_h::{uint16_t, uint32_t, uint8_t};
    extern "C" {
        #[c2rust::src_loc = "400:1"]
        pub fn htonl(__hostlong: uint32_t) -> uint32_t;
    }
}
#[c2rust::header_src = "/usr/include/net/if.h:23"]
pub mod if_h {
    #[c2rust::src_loc = "42:1"]
    pub type C2RustUnnamed_0 = libc::c_uint;
    #[c2rust::src_loc = "79:5"]
    pub const IFF_DYNAMIC: C2RustUnnamed_0 = 32768;
    #[c2rust::src_loc = "77:5"]
    pub const IFF_AUTOMEDIA: C2RustUnnamed_0 = 16384;
    #[c2rust::src_loc = "75:5"]
    pub const IFF_PORTSEL: C2RustUnnamed_0 = 8192;
    #[c2rust::src_loc = "72:5"]
    pub const IFF_MULTICAST: C2RustUnnamed_0 = 4096;
    #[c2rust::src_loc = "69:5"]
    pub const IFF_SLAVE: C2RustUnnamed_0 = 2048;
    #[c2rust::src_loc = "67:5"]
    pub const IFF_MASTER: C2RustUnnamed_0 = 1024;
    #[c2rust::src_loc = "64:5"]
    pub const IFF_ALLMULTI: C2RustUnnamed_0 = 512;
    #[c2rust::src_loc = "60:5"]
    pub const IFF_PROMISC: C2RustUnnamed_0 = 256;
    #[c2rust::src_loc = "58:5"]
    pub const IFF_NOARP: C2RustUnnamed_0 = 128;
    #[c2rust::src_loc = "56:5"]
    pub const IFF_RUNNING: C2RustUnnamed_0 = 64;
    #[c2rust::src_loc = "54:5"]
    pub const IFF_NOTRAILERS: C2RustUnnamed_0 = 32;
    #[c2rust::src_loc = "52:5"]
    pub const IFF_POINTOPOINT: C2RustUnnamed_0 = 16;
    #[c2rust::src_loc = "50:5"]
    pub const IFF_LOOPBACK: C2RustUnnamed_0 = 8;
    #[c2rust::src_loc = "48:5"]
    pub const IFF_DEBUG: C2RustUnnamed_0 = 4;
    #[c2rust::src_loc = "46:5"]
    pub const IFF_BROADCAST: C2RustUnnamed_0 = 2;
    #[c2rust::src_loc = "44:5"]
    pub const IFF_UP: C2RustUnnamed_0 = 1;
    extern "C" {
        #[c2rust::src_loc = "193:1"]
        pub fn if_nametoindex(__ifname: *const libc::c_char) -> libc::c_uint;
    }
}
#[c2rust::header_src = "/usr/include/asm-generic/int-ll64.h:25"]
pub mod int_ll64_h {
    #[c2rust::src_loc = "21:1"]
    pub type __u8 = libc::c_uchar;
    #[c2rust::src_loc = "24:1"]
    pub type __u16 = libc::c_ushort;
    #[c2rust::src_loc = "27:1"]
    pub type __u32 = libc::c_uint;
}
#[c2rust::header_src = "/usr/include/linux/socket.h:26"]
pub mod linux_socket_h {
    #[c2rust::src_loc = "10:1"]
    pub type __kernel_sa_family_t = libc::c_ushort;
}
#[c2rust::header_src = "/usr/include/linux/netlink.h:26"]
pub mod netlink_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "37:8"]
    pub struct sockaddr_nl {
        pub nl_family: __kernel_sa_family_t,
        pub nl_pad: libc::c_ushort,
        pub nl_pid: __u32,
        pub nl_groups: __u32,
    }
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "52:8"]
    pub struct nlmsghdr {
        pub nlmsg_len: __u32,
        pub nlmsg_type: __u16,
        pub nlmsg_flags: __u16,
        pub nlmsg_seq: __u32,
        pub nlmsg_pid: __u32,
    }
    use super::int_ll64_h::{__u16, __u32};
    use super::linux_socket_h::__kernel_sa_family_t;
}
#[c2rust::header_src = "/usr/include/linux/if_addr.h:27"]
pub mod if_addr_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "8:8"]
    pub struct ifaddrmsg {
        pub ifa_family: __u8,
        pub ifa_prefixlen: __u8,
        pub ifa_flags: __u8,
        pub ifa_scope: __u8,
        pub ifa_index: __u32,
    }
    #[c2rust::src_loc = "26:1"]
    pub type C2RustUnnamed_1 = libc::c_uint;
    #[c2rust::src_loc = "39:2"]
    pub const __IFA_MAX: C2RustUnnamed_1 = 12;
    #[c2rust::src_loc = "38:2"]
    pub const IFA_PROTO: C2RustUnnamed_1 = 11;
    #[c2rust::src_loc = "37:2"]
    pub const IFA_TARGET_NETNSID: C2RustUnnamed_1 = 10;
    #[c2rust::src_loc = "36:2"]
    pub const IFA_RT_PRIORITY: C2RustUnnamed_1 = 9;
    #[c2rust::src_loc = "35:2"]
    pub const IFA_FLAGS: C2RustUnnamed_1 = 8;
    #[c2rust::src_loc = "34:2"]
    pub const IFA_MULTICAST: C2RustUnnamed_1 = 7;
    #[c2rust::src_loc = "33:2"]
    pub const IFA_CACHEINFO: C2RustUnnamed_1 = 6;
    #[c2rust::src_loc = "32:2"]
    pub const IFA_ANYCAST: C2RustUnnamed_1 = 5;
    #[c2rust::src_loc = "31:2"]
    pub const IFA_BROADCAST: C2RustUnnamed_1 = 4;
    #[c2rust::src_loc = "30:2"]
    pub const IFA_LABEL: C2RustUnnamed_1 = 3;
    #[c2rust::src_loc = "29:2"]
    pub const IFA_LOCAL: C2RustUnnamed_1 = 2;
    #[c2rust::src_loc = "28:2"]
    pub const IFA_ADDRESS: C2RustUnnamed_1 = 1;
    #[c2rust::src_loc = "27:2"]
    pub const IFA_UNSPEC: C2RustUnnamed_1 = 0;
    use super::int_ll64_h::{__u32, __u8};
}
#[c2rust::header_src = "/usr/include/linux/rtnetlink.h:27"]
pub mod rtnetlink_h {
    #[c2rust::src_loc = "24:1"]
    pub type C2RustUnnamed_2 = libc::c_uint;
    #[c2rust::src_loc = "197:2"]
    pub const __RTM_MAX: C2RustUnnamed_2 = 123;
    #[c2rust::src_loc = "194:2"]
    pub const RTM_GETTUNNEL: C2RustUnnamed_2 = 122;
    #[c2rust::src_loc = "192:2"]
    pub const RTM_DELTUNNEL: C2RustUnnamed_2 = 121;
    #[c2rust::src_loc = "190:2"]
    pub const RTM_NEWTUNNEL: C2RustUnnamed_2 = 120;
    #[c2rust::src_loc = "187:2"]
    pub const RTM_GETNEXTHOPBUCKET: C2RustUnnamed_2 = 118;
    #[c2rust::src_loc = "185:2"]
    pub const RTM_DELNEXTHOPBUCKET: C2RustUnnamed_2 = 117;
    #[c2rust::src_loc = "183:2"]
    pub const RTM_NEWNEXTHOPBUCKET: C2RustUnnamed_2 = 116;
    #[c2rust::src_loc = "180:2"]
    pub const RTM_GETVLAN: C2RustUnnamed_2 = 114;
    #[c2rust::src_loc = "178:2"]
    pub const RTM_DELVLAN: C2RustUnnamed_2 = 113;
    #[c2rust::src_loc = "176:2"]
    pub const RTM_NEWVLAN: C2RustUnnamed_2 = 112;
    #[c2rust::src_loc = "173:2"]
    pub const RTM_GETLINKPROP: C2RustUnnamed_2 = 110;
    #[c2rust::src_loc = "171:2"]
    pub const RTM_DELLINKPROP: C2RustUnnamed_2 = 109;
    #[c2rust::src_loc = "169:2"]
    pub const RTM_NEWLINKPROP: C2RustUnnamed_2 = 108;
    #[c2rust::src_loc = "166:2"]
    pub const RTM_GETNEXTHOP: C2RustUnnamed_2 = 106;
    #[c2rust::src_loc = "164:2"]
    pub const RTM_DELNEXTHOP: C2RustUnnamed_2 = 105;
    #[c2rust::src_loc = "162:2"]
    pub const RTM_NEWNEXTHOP: C2RustUnnamed_2 = 104;
    #[c2rust::src_loc = "159:2"]
    pub const RTM_GETCHAIN: C2RustUnnamed_2 = 102;
    #[c2rust::src_loc = "157:2"]
    pub const RTM_DELCHAIN: C2RustUnnamed_2 = 101;
    #[c2rust::src_loc = "155:2"]
    pub const RTM_NEWCHAIN: C2RustUnnamed_2 = 100;
    #[c2rust::src_loc = "152:2"]
    pub const RTM_NEWCACHEREPORT: C2RustUnnamed_2 = 96;
    #[c2rust::src_loc = "149:2"]
    pub const RTM_SETSTATS: C2RustUnnamed_2 = 95;
    #[c2rust::src_loc = "147:2"]
    pub const RTM_GETSTATS: C2RustUnnamed_2 = 94;
    #[c2rust::src_loc = "145:2"]
    pub const RTM_NEWSTATS: C2RustUnnamed_2 = 92;
    #[c2rust::src_loc = "142:2"]
    pub const RTM_GETNSID: C2RustUnnamed_2 = 90;
    #[c2rust::src_loc = "140:2"]
    pub const RTM_DELNSID: C2RustUnnamed_2 = 89;
    #[c2rust::src_loc = "138:2"]
    pub const RTM_NEWNSID: C2RustUnnamed_2 = 88;
    #[c2rust::src_loc = "135:2"]
    pub const RTM_GETMDB: C2RustUnnamed_2 = 86;
    #[c2rust::src_loc = "133:2"]
    pub const RTM_DELMDB: C2RustUnnamed_2 = 85;
    #[c2rust::src_loc = "131:2"]
    pub const RTM_NEWMDB: C2RustUnnamed_2 = 84;
    #[c2rust::src_loc = "128:2"]
    pub const RTM_GETNETCONF: C2RustUnnamed_2 = 82;
    #[c2rust::src_loc = "126:2"]
    pub const RTM_DELNETCONF: C2RustUnnamed_2 = 81;
    #[c2rust::src_loc = "124:2"]
    pub const RTM_NEWNETCONF: C2RustUnnamed_2 = 80;
    #[c2rust::src_loc = "121:2"]
    pub const RTM_SETDCB: C2RustUnnamed_2 = 79;
    #[c2rust::src_loc = "119:2"]
    pub const RTM_GETDCB: C2RustUnnamed_2 = 78;
    #[c2rust::src_loc = "116:2"]
    pub const RTM_GETADDRLABEL: C2RustUnnamed_2 = 74;
    #[c2rust::src_loc = "114:2"]
    pub const RTM_DELADDRLABEL: C2RustUnnamed_2 = 73;
    #[c2rust::src_loc = "112:2"]
    pub const RTM_NEWADDRLABEL: C2RustUnnamed_2 = 72;
    #[c2rust::src_loc = "109:2"]
    pub const RTM_NEWNDUSEROPT: C2RustUnnamed_2 = 68;
    #[c2rust::src_loc = "106:2"]
    pub const RTM_SETNEIGHTBL: C2RustUnnamed_2 = 67;
    #[c2rust::src_loc = "104:2"]
    pub const RTM_GETNEIGHTBL: C2RustUnnamed_2 = 66;
    #[c2rust::src_loc = "102:2"]
    pub const RTM_NEWNEIGHTBL: C2RustUnnamed_2 = 64;
    #[c2rust::src_loc = "99:2"]
    pub const RTM_GETANYCAST: C2RustUnnamed_2 = 62;
    #[c2rust::src_loc = "96:2"]
    pub const RTM_GETMULTICAST: C2RustUnnamed_2 = 58;
    #[c2rust::src_loc = "93:2"]
    pub const RTM_NEWPREFIX: C2RustUnnamed_2 = 52;
    #[c2rust::src_loc = "90:2"]
    pub const RTM_GETACTION: C2RustUnnamed_2 = 50;
    #[c2rust::src_loc = "88:2"]
    pub const RTM_DELACTION: C2RustUnnamed_2 = 49;
    #[c2rust::src_loc = "86:2"]
    pub const RTM_NEWACTION: C2RustUnnamed_2 = 48;
    #[c2rust::src_loc = "83:2"]
    pub const RTM_GETTFILTER: C2RustUnnamed_2 = 46;
    #[c2rust::src_loc = "81:2"]
    pub const RTM_DELTFILTER: C2RustUnnamed_2 = 45;
    #[c2rust::src_loc = "79:2"]
    pub const RTM_NEWTFILTER: C2RustUnnamed_2 = 44;
    #[c2rust::src_loc = "76:2"]
    pub const RTM_GETTCLASS: C2RustUnnamed_2 = 42;
    #[c2rust::src_loc = "74:2"]
    pub const RTM_DELTCLASS: C2RustUnnamed_2 = 41;
    #[c2rust::src_loc = "72:2"]
    pub const RTM_NEWTCLASS: C2RustUnnamed_2 = 40;
    #[c2rust::src_loc = "69:2"]
    pub const RTM_GETQDISC: C2RustUnnamed_2 = 38;
    #[c2rust::src_loc = "67:2"]
    pub const RTM_DELQDISC: C2RustUnnamed_2 = 37;
    #[c2rust::src_loc = "65:2"]
    pub const RTM_NEWQDISC: C2RustUnnamed_2 = 36;
    #[c2rust::src_loc = "62:2"]
    pub const RTM_GETRULE: C2RustUnnamed_2 = 34;
    #[c2rust::src_loc = "60:2"]
    pub const RTM_DELRULE: C2RustUnnamed_2 = 33;
    #[c2rust::src_loc = "58:2"]
    pub const RTM_NEWRULE: C2RustUnnamed_2 = 32;
    #[c2rust::src_loc = "55:2"]
    pub const RTM_GETNEIGH: C2RustUnnamed_2 = 30;
    #[c2rust::src_loc = "53:2"]
    pub const RTM_DELNEIGH: C2RustUnnamed_2 = 29;
    #[c2rust::src_loc = "51:2"]
    pub const RTM_NEWNEIGH: C2RustUnnamed_2 = 28;
    #[c2rust::src_loc = "48:2"]
    pub const RTM_GETROUTE: C2RustUnnamed_2 = 26;
    #[c2rust::src_loc = "46:2"]
    pub const RTM_DELROUTE: C2RustUnnamed_2 = 25;
    #[c2rust::src_loc = "44:2"]
    pub const RTM_NEWROUTE: C2RustUnnamed_2 = 24;
    #[c2rust::src_loc = "41:2"]
    pub const RTM_GETADDR: C2RustUnnamed_2 = 22;
    #[c2rust::src_loc = "39:2"]
    pub const RTM_DELADDR: C2RustUnnamed_2 = 21;
    #[c2rust::src_loc = "37:2"]
    pub const RTM_NEWADDR: C2RustUnnamed_2 = 20;
    #[c2rust::src_loc = "34:2"]
    pub const RTM_SETLINK: C2RustUnnamed_2 = 19;
    #[c2rust::src_loc = "32:2"]
    pub const RTM_GETLINK: C2RustUnnamed_2 = 18;
    #[c2rust::src_loc = "30:2"]
    pub const RTM_DELLINK: C2RustUnnamed_2 = 17;
    #[c2rust::src_loc = "28:2"]
    pub const RTM_NEWLINK: C2RustUnnamed_2 = 16;
    #[c2rust::src_loc = "25:2"]
    pub const RTM_BASE: C2RustUnnamed_2 = 16;
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "211:8"]
    pub struct rtattr {
        pub rta_len: libc::c_ushort,
        pub rta_type: libc::c_ushort,
    }
    #[c2rust::src_loc = "320:1"]
    pub type rt_scope_t = libc::c_uint;
    #[c2rust::src_loc = "326:2"]
    pub const RT_SCOPE_NOWHERE: rt_scope_t = 255;
    #[c2rust::src_loc = "325:2"]
    pub const RT_SCOPE_HOST: rt_scope_t = 254;
    #[c2rust::src_loc = "324:2"]
    pub const RT_SCOPE_LINK: rt_scope_t = 253;
    #[c2rust::src_loc = "323:2"]
    pub const RT_SCOPE_SITE: rt_scope_t = 200;
    #[c2rust::src_loc = "321:2"]
    pub const RT_SCOPE_UNIVERSE: rt_scope_t = 0;
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "557:8"]
    pub struct ifinfomsg {
        pub ifi_family: libc::c_uchar,
        pub __ifi_pad: libc::c_uchar,
        pub ifi_type: libc::c_ushort,
        pub ifi_index: libc::c_int,
        pub ifi_flags: libc::c_uint,
        pub ifi_change: libc::c_uint,
    }
}
#[c2rust::header_src = "/usr/include/string.h:29"]
pub mod string_h {
    extern "C" {
        #[c2rust::src_loc = "61:14"]
        pub fn memset(_: *mut libc::c_void, _: libc::c_int, _: libc::c_ulong) -> *mut libc::c_void;
    }
}
#[c2rust::header_src = "/usr/include/assert.h:29"]
pub mod assert_h {
    extern "C" {
        #[c2rust::src_loc = "67:1"]
        pub fn __assert_fail(
            __assertion: *const libc::c_char,
            __file: *const libc::c_char,
            __line: libc::c_uint,
            __function: *const libc::c_char,
        ) -> !;
    }
}
#[c2rust::header_src = "/usr/include/unistd.h:29"]
pub mod unistd_h {
    use super::types_h::__pid_t;
    extern "C" {
        #[c2rust::src_loc = "650:1"]
        pub fn getpid() -> __pid_t;
    }
}
#[c2rust::header_src = "/var/home/oro/bubblewrap/utils.h:29"]
pub mod utils_h {
    extern "C" {
        #[c2rust::src_loc = "57:1"]
        pub fn die_with_error(format: *const libc::c_char, _: ...) -> !;
    }
}
use self::assert_h::__assert_fail;
pub use self::if_addr_h::{
    ifaddrmsg, C2RustUnnamed_1, IFA_ADDRESS, IFA_ANYCAST, IFA_BROADCAST, IFA_CACHEINFO, IFA_FLAGS,
    IFA_LABEL, IFA_LOCAL, IFA_MULTICAST, IFA_PROTO, IFA_RT_PRIORITY, IFA_TARGET_NETNSID,
    IFA_UNSPEC, __IFA_MAX,
};
pub use self::if_h::{
    if_nametoindex, C2RustUnnamed_0, IFF_ALLMULTI, IFF_AUTOMEDIA, IFF_BROADCAST, IFF_DEBUG,
    IFF_DYNAMIC, IFF_LOOPBACK, IFF_MASTER, IFF_MULTICAST, IFF_NOARP, IFF_NOTRAILERS,
    IFF_POINTOPOINT, IFF_PORTSEL, IFF_PROMISC, IFF_RUNNING, IFF_SLAVE, IFF_UP,
};
pub use self::in_h::{
    htonl, in6_addr, in_addr, in_addr_t, in_port_t, sockaddr_in, sockaddr_in6, C2RustUnnamed,
};
pub use self::int_ll64_h::{__u16, __u32, __u8};
pub use self::linux_socket_h::__kernel_sa_family_t;
pub use self::netlink_h::{nlmsghdr, sockaddr_nl};
pub use self::rtnetlink_h::{
    ifinfomsg, rt_scope_t, rtattr, C2RustUnnamed_2, RTM_BASE, RTM_DELACTION, RTM_DELADDR,
    RTM_DELADDRLABEL, RTM_DELCHAIN, RTM_DELLINK, RTM_DELLINKPROP, RTM_DELMDB, RTM_DELNEIGH,
    RTM_DELNETCONF, RTM_DELNEXTHOP, RTM_DELNEXTHOPBUCKET, RTM_DELNSID, RTM_DELQDISC, RTM_DELROUTE,
    RTM_DELRULE, RTM_DELTCLASS, RTM_DELTFILTER, RTM_DELTUNNEL, RTM_DELVLAN, RTM_GETACTION,
    RTM_GETADDR, RTM_GETADDRLABEL, RTM_GETANYCAST, RTM_GETCHAIN, RTM_GETDCB, RTM_GETLINK,
    RTM_GETLINKPROP, RTM_GETMDB, RTM_GETMULTICAST, RTM_GETNEIGH, RTM_GETNEIGHTBL, RTM_GETNETCONF,
    RTM_GETNEXTHOP, RTM_GETNEXTHOPBUCKET, RTM_GETNSID, RTM_GETQDISC, RTM_GETROUTE, RTM_GETRULE,
    RTM_GETSTATS, RTM_GETTCLASS, RTM_GETTFILTER, RTM_GETTUNNEL, RTM_GETVLAN, RTM_NEWACTION,
    RTM_NEWADDR, RTM_NEWADDRLABEL, RTM_NEWCACHEREPORT, RTM_NEWCHAIN, RTM_NEWLINK, RTM_NEWLINKPROP,
    RTM_NEWMDB, RTM_NEWNDUSEROPT, RTM_NEWNEIGH, RTM_NEWNEIGHTBL, RTM_NEWNETCONF, RTM_NEWNEXTHOP,
    RTM_NEWNEXTHOPBUCKET, RTM_NEWNSID, RTM_NEWPREFIX, RTM_NEWQDISC, RTM_NEWROUTE, RTM_NEWRULE,
    RTM_NEWSTATS, RTM_NEWTCLASS, RTM_NEWTFILTER, RTM_NEWTUNNEL, RTM_NEWVLAN, RTM_SETDCB,
    RTM_SETLINK, RTM_SETNEIGHTBL, RTM_SETSTATS, RT_SCOPE_HOST, RT_SCOPE_LINK, RT_SCOPE_NOWHERE,
    RT_SCOPE_SITE, RT_SCOPE_UNIVERSE, __RTM_MAX,
};
pub use self::sockaddr_h::sa_family_t;
pub use self::socket_h::{sockaddr, socklen_t};
pub use self::socket_type_h::{
    __socket_type, SOCK_CLOEXEC, SOCK_DCCP, SOCK_DGRAM, SOCK_NONBLOCK, SOCK_PACKET, SOCK_RAW,
    SOCK_RDM, SOCK_SEQPACKET, SOCK_STREAM,
};
pub use self::stddef_h::size_t;
pub use self::stdint_uintn_h::{uint16_t, uint32_t, uint8_t};
use self::string_h::memset;
pub use self::sys_socket_h::{
    bind, recv, sendto, sockaddr_at, sockaddr_ax25, sockaddr_dl, sockaddr_eon, sockaddr_inarp,
    sockaddr_ipx, sockaddr_iso, sockaddr_ns, sockaddr_un, sockaddr_x25, socket,
    __CONST_SOCKADDR_ARG,
};
pub use self::sys_types_h::{pid_t, ssize_t};
pub use self::types_h::{__pid_t, __socklen_t, __ssize_t, __uint16_t, __uint32_t, __uint8_t};
use self::unistd_h::getpid;
use self::utils_h::die_with_error;
#[c2rust::src_loc = "32:1"]
unsafe extern "C" fn add_rta(
    mut header: *mut nlmsghdr,
    mut type_0: libc::c_int,
    mut size: size_t,
) -> *mut libc::c_void {
    let mut rta: *mut rtattr = 0 as *mut rtattr;
    let mut rta_size: size_t = ((::core::mem::size_of::<rtattr>() as libc::c_ulong)
        .wrapping_add(4 as libc::c_uint as libc::c_ulong)
        .wrapping_sub(1 as libc::c_int as libc::c_ulong)
        & !(4 as libc::c_uint).wrapping_sub(1 as libc::c_int as libc::c_uint) as libc::c_ulong)
        .wrapping_add(size);
    rta = (header as *mut libc::c_char).offset(
        (((*header).nlmsg_len)
            .wrapping_add(4 as libc::c_uint)
            .wrapping_sub(1 as libc::c_int as libc::c_uint)
            & !(4 as libc::c_uint).wrapping_sub(1 as libc::c_int as libc::c_uint)) as isize,
    ) as *mut rtattr;
    (*rta).rta_type = type_0 as libc::c_ushort;
    (*rta).rta_len = rta_size as libc::c_ushort;
    (*header).nlmsg_len = ((((*header).nlmsg_len)
        .wrapping_add(4 as libc::c_uint)
        .wrapping_sub(1 as libc::c_int as libc::c_uint)
        & !(4 as libc::c_uint).wrapping_sub(1 as libc::c_int as libc::c_uint))
        as libc::c_ulong)
        .wrapping_add(rta_size) as __u32;
    return (rta as *mut libc::c_char).offset(
        ((::core::mem::size_of::<rtattr>() as libc::c_ulong)
            .wrapping_add(4 as libc::c_uint as libc::c_ulong)
            .wrapping_sub(1 as libc::c_int as libc::c_ulong)
            & !(4 as libc::c_uint).wrapping_sub(1 as libc::c_int as libc::c_uint) as libc::c_ulong)
            .wrapping_add(0 as libc::c_int as libc::c_ulong) as isize,
    ) as *mut libc::c_void;
}
#[c2rust::src_loc = "49:1"]
unsafe extern "C" fn rtnl_send_request(
    mut rtnl_fd: libc::c_int,
    mut header: *mut nlmsghdr,
) -> libc::c_int {
    let mut dst_addr: sockaddr_nl = {
        let mut init = sockaddr_nl {
            nl_family: 16 as libc::c_int as __kernel_sa_family_t,
            nl_pad: 0 as libc::c_int as libc::c_ushort,
            nl_pid: 0,
            nl_groups: 0,
        };
        init
    };
    let mut sent: ssize_t = 0;
    sent = sendto(
        rtnl_fd,
        header as *mut libc::c_void,
        (*header).nlmsg_len as size_t,
        0 as libc::c_int,
        __CONST_SOCKADDR_ARG {
            __sockaddr__: &mut dst_addr as *mut sockaddr_nl as *mut sockaddr,
        },
        ::core::mem::size_of::<sockaddr_nl>() as libc::c_ulong as socklen_t,
    );
    if sent < 0 as libc::c_int as libc::c_long {
        return -(1 as libc::c_int);
    }
    return 0 as libc::c_int;
}
#[c2rust::src_loc = "64:1"]
unsafe extern "C" fn rtnl_read_reply(
    mut rtnl_fd: libc::c_int,
    mut seq_nr: libc::c_uint,
) -> libc::c_int {
    let mut buffer: [libc::c_char; 1024] = [0; 1024];
    let mut received: ssize_t = 0;
    let mut rheader: *mut nlmsghdr = 0 as *mut nlmsghdr;
    loop {
        received = recv(
            rtnl_fd,
            buffer.as_mut_ptr() as *mut libc::c_void,
            ::core::mem::size_of::<[libc::c_char; 1024]>() as libc::c_ulong,
            0 as libc::c_int,
        );
        if received < 0 as libc::c_int as libc::c_long {
            return -(1 as libc::c_int);
        }
        rheader = buffer.as_mut_ptr() as *mut nlmsghdr;
        while received
            >= ((::core::mem::size_of::<nlmsghdr>() as libc::c_ulong)
                .wrapping_add(4 as libc::c_uint as libc::c_ulong)
                .wrapping_sub(1 as libc::c_int as libc::c_ulong)
                & !(4 as libc::c_uint).wrapping_sub(1 as libc::c_int as libc::c_uint)
                    as libc::c_ulong) as libc::c_int as libc::c_long
        {
            if (*rheader).nlmsg_seq != seq_nr {
                return -(1 as libc::c_int);
            }
            if (*rheader).nlmsg_pid as pid_t != getpid() {
                return -(1 as libc::c_int);
            }
            if (*rheader).nlmsg_type as libc::c_int == 0x2 as libc::c_int {
                let mut err: *mut uint32_t = (rheader as *mut libc::c_char).offset(
                    ((::core::mem::size_of::<nlmsghdr>() as libc::c_ulong)
                        .wrapping_add(4 as libc::c_uint as libc::c_ulong)
                        .wrapping_sub(1 as libc::c_int as libc::c_ulong)
                        & !(4 as libc::c_uint).wrapping_sub(1 as libc::c_int as libc::c_uint)
                            as libc::c_ulong) as libc::c_int as isize,
                ) as *mut libc::c_void
                    as *mut uint32_t;
                if *err == 0 as libc::c_int as libc::c_uint {
                    return 0 as libc::c_int;
                }
                return -(1 as libc::c_int);
            }
            if (*rheader).nlmsg_type as libc::c_int == 0x3 as libc::c_int {
                return 0 as libc::c_int;
            }
            received -= (((*rheader).nlmsg_len)
                .wrapping_add(4 as libc::c_uint)
                .wrapping_sub(1 as libc::c_int as libc::c_uint)
                & !(4 as libc::c_uint).wrapping_sub(1 as libc::c_int as libc::c_uint))
                as libc::c_long;
            rheader = (rheader as *mut libc::c_char).offset(
                (((*rheader).nlmsg_len)
                    .wrapping_add(4 as libc::c_uint)
                    .wrapping_sub(1 as libc::c_int as libc::c_uint)
                    & !(4 as libc::c_uint).wrapping_sub(1 as libc::c_int as libc::c_uint))
                    as isize,
            ) as *mut nlmsghdr;
        }
    }
}
#[c2rust::src_loc = "101:1"]
unsafe extern "C" fn rtnl_do_request(
    mut rtnl_fd: libc::c_int,
    mut header: *mut nlmsghdr,
) -> libc::c_int {
    if rtnl_send_request(rtnl_fd, header) != 0 as libc::c_int {
        return -(1 as libc::c_int);
    }
    if rtnl_read_reply(rtnl_fd, (*header).nlmsg_seq) != 0 as libc::c_int {
        return -(1 as libc::c_int);
    }
    return 0 as libc::c_int;
}
#[c2rust::src_loc = "114:1"]
unsafe extern "C" fn rtnl_setup_request(
    mut buffer: *mut libc::c_char,
    mut type_0: libc::c_int,
    mut flags: libc::c_int,
    mut size: size_t,
) -> *mut nlmsghdr {
    let mut header: *mut nlmsghdr = 0 as *mut nlmsghdr;
    let mut len: size_t = size.wrapping_add(
        ((::core::mem::size_of::<nlmsghdr>() as libc::c_ulong)
            .wrapping_add(4 as libc::c_uint as libc::c_ulong)
            .wrapping_sub(1 as libc::c_int as libc::c_ulong)
            & !(4 as libc::c_uint).wrapping_sub(1 as libc::c_int as libc::c_uint) as libc::c_ulong)
            as libc::c_int as libc::c_ulong,
    );
    static mut counter: uint32_t = 0 as libc::c_int as uint32_t;
    memset(buffer as *mut libc::c_void, 0 as libc::c_int, len);
    header = buffer as *mut nlmsghdr;
    (*header).nlmsg_len = len as __u32;
    (*header).nlmsg_type = type_0 as __u16;
    (*header).nlmsg_flags = (flags | 0x1 as libc::c_int) as __u16;
    let fresh0 = counter;
    counter = counter.wrapping_add(1);
    (*header).nlmsg_seq = fresh0;
    (*header).nlmsg_pid = getpid() as __u32;
    return header;
}
#[no_mangle]
#[c2rust::src_loc = "136:1"]
pub unsafe extern "C" fn loopback_setup() {
    let mut r: libc::c_int = 0;
    let mut if_loopback: libc::c_int = 0;
    let mut rtnl_fd: libc::c_int = -(1 as libc::c_int);
    let mut buffer: [libc::c_char; 1024] = [0; 1024];
    let mut src_addr: sockaddr_nl = {
        let mut init = sockaddr_nl {
            nl_family: 16 as libc::c_int as __kernel_sa_family_t,
            nl_pad: 0 as libc::c_int as libc::c_ushort,
            nl_pid: 0,
            nl_groups: 0,
        };
        init
    };
    let mut header: *mut nlmsghdr = 0 as *mut nlmsghdr;
    let mut addmsg: *mut ifaddrmsg = 0 as *mut ifaddrmsg;
    let mut infomsg: *mut ifinfomsg = 0 as *mut ifinfomsg;
    let mut ip_addr: *mut in_addr = 0 as *mut in_addr;
    src_addr.nl_pid = getpid() as __u32;
    if_loopback = if_nametoindex(b"lo\0" as *const u8 as *const libc::c_char) as libc::c_int;
    if if_loopback <= 0 as libc::c_int {
        die_with_error(b"loopback: Failed to look up lo\0" as *const u8 as *const libc::c_char);
    }
    rtnl_fd = socket(
        16 as libc::c_int,
        SOCK_RAW as libc::c_int | SOCK_CLOEXEC as libc::c_int,
        0 as libc::c_int,
    );
    if rtnl_fd < 0 as libc::c_int {
        die_with_error(
            b"loopback: Failed to create NETLINK_ROUTE socket\0" as *const u8
                as *const libc::c_char,
        );
    }
    r = bind(
        rtnl_fd,
        __CONST_SOCKADDR_ARG {
            __sockaddr__: &mut src_addr as *mut sockaddr_nl as *mut sockaddr,
        },
        ::core::mem::size_of::<sockaddr_nl>() as libc::c_ulong as socklen_t,
    );
    if r < 0 as libc::c_int {
        die_with_error(
            b"loopback: Failed to bind NETLINK_ROUTE socket\0" as *const u8 as *const libc::c_char,
        );
    }
    header = rtnl_setup_request(
        buffer.as_mut_ptr(),
        RTM_NEWADDR as libc::c_int,
        0x400 as libc::c_int | 0x200 as libc::c_int | 0x4 as libc::c_int,
        ::core::mem::size_of::<ifaddrmsg>() as libc::c_ulong,
    );
    addmsg = (header as *mut libc::c_char).offset(
        ((::core::mem::size_of::<nlmsghdr>() as libc::c_ulong)
            .wrapping_add(4 as libc::c_uint as libc::c_ulong)
            .wrapping_sub(1 as libc::c_int as libc::c_ulong)
            & !(4 as libc::c_uint).wrapping_sub(1 as libc::c_int as libc::c_uint) as libc::c_ulong)
            as libc::c_int as isize,
    ) as *mut libc::c_void as *mut ifaddrmsg;
    (*addmsg).ifa_family = 2 as libc::c_int as __u8;
    (*addmsg).ifa_prefixlen = 8 as libc::c_int as __u8;
    (*addmsg).ifa_flags = 0x80 as libc::c_int as __u8;
    (*addmsg).ifa_scope = RT_SCOPE_HOST as libc::c_int as __u8;
    (*addmsg).ifa_index = if_loopback as __u32;
    ip_addr = add_rta(
        header,
        IFA_LOCAL as libc::c_int,
        ::core::mem::size_of::<in_addr>() as libc::c_ulong,
    ) as *mut in_addr;
    (*ip_addr).s_addr = htonl(0x7f000001 as libc::c_int as in_addr_t);
    ip_addr = add_rta(
        header,
        IFA_ADDRESS as libc::c_int,
        ::core::mem::size_of::<in_addr>() as libc::c_ulong,
    ) as *mut in_addr;
    (*ip_addr).s_addr = htonl(0x7f000001 as libc::c_int as in_addr_t);
    if ((*header).nlmsg_len as libc::c_ulong)
        < ::core::mem::size_of::<[libc::c_char; 1024]>() as libc::c_ulong
    {
    } else {
        __assert_fail(
            b"header->nlmsg_len < sizeof (buffer)\0" as *const u8 as *const libc::c_char,
            b"../network.c\0" as *const u8 as *const libc::c_char,
            179 as libc::c_int as libc::c_uint,
            (*::core::mem::transmute::<&[u8; 26], &[libc::c_char; 26]>(
                b"void loopback_setup(void)\0",
            ))
            .as_ptr(),
        );
    }
    'c_6427: {
        if ((*header).nlmsg_len as libc::c_ulong)
            < ::core::mem::size_of::<[libc::c_char; 1024]>() as libc::c_ulong
        {
        } else {
            __assert_fail(
                b"header->nlmsg_len < sizeof (buffer)\0" as *const u8 as *const libc::c_char,
                b"../network.c\0" as *const u8 as *const libc::c_char,
                179 as libc::c_int as libc::c_uint,
                (*::core::mem::transmute::<&[u8; 26], &[libc::c_char; 26]>(
                    b"void loopback_setup(void)\0",
                ))
                .as_ptr(),
            );
        }
    };
    if rtnl_do_request(rtnl_fd, header) != 0 as libc::c_int {
        die_with_error(b"loopback: Failed RTM_NEWADDR\0" as *const u8 as *const libc::c_char);
    }
    header = rtnl_setup_request(
        buffer.as_mut_ptr(),
        RTM_NEWLINK as libc::c_int,
        0x4 as libc::c_int,
        ::core::mem::size_of::<ifinfomsg>() as libc::c_ulong,
    );
    infomsg = (header as *mut libc::c_char).offset(
        ((::core::mem::size_of::<nlmsghdr>() as libc::c_ulong)
            .wrapping_add(4 as libc::c_uint as libc::c_ulong)
            .wrapping_sub(1 as libc::c_int as libc::c_ulong)
            & !(4 as libc::c_uint).wrapping_sub(1 as libc::c_int as libc::c_uint) as libc::c_ulong)
            as libc::c_int as isize,
    ) as *mut libc::c_void as *mut ifinfomsg;
    (*infomsg).ifi_family = 0 as libc::c_int as libc::c_uchar;
    (*infomsg).ifi_type = 0 as libc::c_int as libc::c_ushort;
    (*infomsg).ifi_index = if_loopback;
    (*infomsg).ifi_flags = IFF_UP as libc::c_int as libc::c_uint;
    (*infomsg).ifi_change = IFF_UP as libc::c_int as libc::c_uint;
    if ((*header).nlmsg_len as libc::c_ulong)
        < ::core::mem::size_of::<[libc::c_char; 1024]>() as libc::c_ulong
    {
    } else {
        __assert_fail(
            b"header->nlmsg_len < sizeof (buffer)\0" as *const u8 as *const libc::c_char,
            b"../network.c\0" as *const u8 as *const libc::c_char,
            195 as libc::c_int as libc::c_uint,
            (*::core::mem::transmute::<&[u8; 26], &[libc::c_char; 26]>(
                b"void loopback_setup(void)\0",
            ))
            .as_ptr(),
        );
    }
    'c_6175: {
        if ((*header).nlmsg_len as libc::c_ulong)
            < ::core::mem::size_of::<[libc::c_char; 1024]>() as libc::c_ulong
        {
        } else {
            __assert_fail(
                b"header->nlmsg_len < sizeof (buffer)\0" as *const u8 as *const libc::c_char,
                b"../network.c\0" as *const u8 as *const libc::c_char,
                195 as libc::c_int as libc::c_uint,
                (*::core::mem::transmute::<&[u8; 26], &[libc::c_char; 26]>(
                    b"void loopback_setup(void)\0",
                ))
                .as_ptr(),
            );
        }
    };
    if rtnl_do_request(rtnl_fd, header) != 0 as libc::c_int {
        die_with_error(b"loopback: Failed RTM_NEWLINK\0" as *const u8 as *const libc::c_char);
    }
}
