use ::libc;
#[c2rust::header_src = "internal:0"]
pub mod internal {
    #[c2rust::src_loc = "0:0"]
    pub type __builtin_va_list = [__va_list_tag; 1];
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "0:0"]
    pub struct __va_list_tag {
        pub gp_offset: libc::c_uint,
        pub fp_offset: libc::c_uint,
        pub overflow_arg_area: *mut libc::c_void,
        pub reg_save_area: *mut libc::c_void,
    }
}
#[c2rust::header_src = "/usr/lib/clang/15.0.7/include/stdarg.h:20"]
pub mod stdarg_h {
    #[c2rust::src_loc = "14:1"]
    pub type va_list = __builtin_va_list;
    use super::internal::__builtin_va_list;
}
#[c2rust::header_src = "/usr/include/bits/types.h:21"]
pub mod types_h {
    #[c2rust::src_loc = "73:1"]
    pub type __uintmax_t = libc::c_ulong;
    #[c2rust::src_loc = "152:1"]
    pub type __off_t = libc::c_long;
    #[c2rust::src_loc = "153:1"]
    pub type __off64_t = libc::c_long;
}
#[c2rust::header_src = "/usr/include/stdint.h:21"]
pub mod stdint_h {
    #[c2rust::src_loc = "102:1"]
    pub type uintmax_t = __uintmax_t;
    use super::types_h::__uintmax_t;
}
#[c2rust::header_src = "/usr/lib/clang/15.0.7/include/stddef.h:22"]
pub mod stddef_h {
    #[c2rust::src_loc = "46:1"]
    pub type size_t = libc::c_ulong;
}
#[c2rust::header_src = "/usr/include/bits/types/struct_FILE.h:22"]
pub mod struct_FILE_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "49:8"]
    pub struct _IO_FILE {
        pub _flags: libc::c_int,
        pub _IO_read_ptr: *mut libc::c_char,
        pub _IO_read_end: *mut libc::c_char,
        pub _IO_read_base: *mut libc::c_char,
        pub _IO_write_base: *mut libc::c_char,
        pub _IO_write_ptr: *mut libc::c_char,
        pub _IO_write_end: *mut libc::c_char,
        pub _IO_buf_base: *mut libc::c_char,
        pub _IO_buf_end: *mut libc::c_char,
        pub _IO_save_base: *mut libc::c_char,
        pub _IO_backup_base: *mut libc::c_char,
        pub _IO_save_end: *mut libc::c_char,
        pub _markers: *mut _IO_marker,
        pub _chain: *mut _IO_FILE,
        pub _fileno: libc::c_int,
        pub _flags2: libc::c_int,
        pub _old_offset: __off_t,
        pub _cur_column: libc::c_ushort,
        pub _vtable_offset: libc::c_schar,
        pub _shortbuf: [libc::c_char; 1],
        pub _lock: *mut libc::c_void,
        pub _offset: __off64_t,
        pub _codecvt: *mut _IO_codecvt,
        pub _wide_data: *mut _IO_wide_data,
        pub _freeres_list: *mut _IO_FILE,
        pub _freeres_buf: *mut libc::c_void,
        pub __pad5: size_t,
        pub _mode: libc::c_int,
        pub _unused2: [libc::c_char; 20],
    }
    #[c2rust::src_loc = "43:1"]
    pub type _IO_lock_t = ();
    use super::stddef_h::size_t;
    use super::types_h::{__off64_t, __off_t};
    extern "C" {
        #[c2rust::src_loc = "38:8"]
        pub type _IO_wide_data;
        #[c2rust::src_loc = "37:8"]
        pub type _IO_codecvt;
        #[c2rust::src_loc = "36:8"]
        pub type _IO_marker;
    }
}
#[c2rust::header_src = "/usr/include/bits/types/FILE.h:22"]
pub mod FILE_h {
    #[c2rust::src_loc = "7:1"]
    pub type FILE = _IO_FILE;
    use super::struct_FILE_h::_IO_FILE;
}
#[c2rust::header_src = "/var/home/oro/bubblewrap/utils.h:24"]
pub mod utils_h {
    #[c2rust::src_loc = "46:1"]
    pub type bool_0 = libc::c_int;
    extern "C" {
        #[c2rust::src_loc = "59:1"]
        pub fn die(format: *const libc::c_char, _: ...) -> !;
        #[c2rust::src_loc = "77:1"]
        pub fn strconcat(s1: *const libc::c_char, s2: *const libc::c_char) -> *mut libc::c_char;
        #[c2rust::src_loc = "79:1"]
        pub fn strconcat3(
            s1: *const libc::c_char,
            s2: *const libc::c_char,
            s3: *const libc::c_char,
        ) -> *mut libc::c_char;
        #[c2rust::src_loc = "84:1"]
        pub fn has_prefix(str: *const libc::c_char, prefix: *const libc::c_char) -> bool_0;
        #[c2rust::src_loc = "86:1"]
        pub fn has_path_prefix(str: *const libc::c_char, prefix: *const libc::c_char) -> bool_0;
    }
}
#[c2rust::header_src = "/usr/include/stdio.h:22"]
pub mod stdio_h {
    use super::internal::__va_list_tag;
    use super::stddef_h::size_t;
    use super::FILE_h::FILE;
    extern "C" {
        #[c2rust::src_loc = "144:14"]
        pub static mut stdout: *mut FILE;
        #[c2rust::src_loc = "332:1"]
        pub fn setvbuf(
            __stream: *mut FILE,
            __buf: *mut libc::c_char,
            __modes: libc::c_int,
            __n: size_t,
        ) -> libc::c_int;
        #[c2rust::src_loc = "356:12"]
        pub fn printf(_: *const libc::c_char, _: ...) -> libc::c_int;
        #[c2rust::src_loc = "371:12"]
        pub fn vprintf(_: *const libc::c_char, _: ::core::ffi::VaList) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/stdlib.h:24"]
pub mod stdlib_h {
    extern "C" {
        #[c2rust::src_loc = "568:13"]
        pub fn free(_: *mut libc::c_void);
    }
}
#[c2rust::header_src = "/usr/include/string.h:24"]
pub mod string_h {
    extern "C" {
        #[c2rust::src_loc = "156:12"]
        pub fn strcmp(_: *const libc::c_char, _: *const libc::c_char) -> libc::c_int;
    }
}
pub use self::internal::{__builtin_va_list, __va_list_tag};
pub use self::stdarg_h::va_list;
pub use self::stddef_h::size_t;
pub use self::stdint_h::uintmax_t;
use self::stdio_h::{printf, setvbuf, stdout, vprintf};
use self::stdlib_h::free;
use self::string_h::strcmp;
pub use self::struct_FILE_h::{_IO_codecvt, _IO_lock_t, _IO_marker, _IO_wide_data, _IO_FILE};
pub use self::types_h::{__off64_t, __off_t, __uintmax_t};
pub use self::utils_h::{bool_0, die, has_path_prefix, has_prefix, strconcat, strconcat3};
pub use self::FILE_h::FILE;
#[derive(Copy, Clone)]
#[repr(C)]
#[c2rust::src_loc = "166:16"]
pub struct C2RustUnnamed {
    pub str_0: *const libc::c_char,
    pub prefix: *const libc::c_char,
    pub expected: bool_0,
}
#[c2rust::src_loc = "27:21"]
static mut test_number: libc::c_uint = 0 as libc::c_int as libc::c_uint;
#[c2rust::src_loc = "29:1"]
unsafe extern "C" fn ok(mut format: *const libc::c_char, mut args: ...) {
    let mut ap: ::core::ffi::VaListImpl;
    test_number = test_number.wrapping_add(1);
    printf(
        b"ok %u - \0" as *const u8 as *const libc::c_char,
        test_number,
    );
    ap = args.clone();
    vprintf(format, ap.as_va_list());
    printf(b"\n\0" as *const u8 as *const libc::c_char);
}
#[c2rust::src_loc = "106:1"]
unsafe extern "C" fn strcmp0(
    mut left: *const libc::c_char,
    mut right: *const libc::c_char,
) -> libc::c_int {
    if left == right {
        return 0 as libc::c_int;
    }
    if left.is_null() {
        return -(1 as libc::c_int);
    }
    if right.is_null() {
        return 1 as libc::c_int;
    }
    return strcmp(left, right);
}
#[c2rust::src_loc = "122:1"]
unsafe extern "C" fn test_n_elements() {
    let mut three: [libc::c_int; 3] = [1 as libc::c_int, 2 as libc::c_int, 3 as libc::c_int];
    let mut left: uintmax_t = (::core::mem::size_of::<[libc::c_int; 3]>() as libc::c_ulong)
        .wrapping_div(::core::mem::size_of::<libc::c_int>() as libc::c_ulong);
    let mut right: uintmax_t = 3 as libc::c_int as uintmax_t;
    if left == right {
        ok(
            b"%s (%ju) %s %s (%ju)\0" as *const u8 as *const libc::c_char,
            b"N_ELEMENTS (three)\0" as *const u8 as *const libc::c_char,
            left,
            b"==\0" as *const u8 as *const libc::c_char,
            b"3\0" as *const u8 as *const libc::c_char,
            right,
        );
    } else {
        die(
            b"expected %s (%ju) %s %s (%ju)\0" as *const u8 as *const libc::c_char,
            b"N_ELEMENTS (three)\0" as *const u8 as *const libc::c_char,
            left,
            b"==\0" as *const u8 as *const libc::c_char,
            b"3\0" as *const u8 as *const libc::c_char,
            right,
        );
    };
}
#[c2rust::src_loc = "129:1"]
unsafe extern "C" fn test_strconcat() {
    let mut a: *const libc::c_char = b"aaa\0" as *const u8 as *const libc::c_char;
    let mut b: *const libc::c_char = b"bbb\0" as *const u8 as *const libc::c_char;
    let mut ab: *mut libc::c_char = strconcat(a, b);
    let mut left: *const libc::c_char = ab;
    let mut right: *const libc::c_char = b"aaabbb\0" as *const u8 as *const libc::c_char;
    if strcmp0(left, right) == 0 as libc::c_int {
        ok(
            b"%s (\"%s\") %s %s (\"%s\")\0" as *const u8 as *const libc::c_char,
            b"ab\0" as *const u8 as *const libc::c_char,
            left,
            b"==\0" as *const u8 as *const libc::c_char,
            b"\"aaabbb\"\0" as *const u8 as *const libc::c_char,
            right,
        );
    } else {
        die(
            b"expected %s (\"%s\") %s %s (\"%s\")\0" as *const u8 as *const libc::c_char,
            b"ab\0" as *const u8 as *const libc::c_char,
            left,
            b"==\0" as *const u8 as *const libc::c_char,
            b"\"aaabbb\"\0" as *const u8 as *const libc::c_char,
            right,
        );
    }
    free(ab as *mut libc::c_void);
}
#[c2rust::src_loc = "139:1"]
unsafe extern "C" fn test_strconcat3() {
    let mut a: *const libc::c_char = b"aaa\0" as *const u8 as *const libc::c_char;
    let mut b: *const libc::c_char = b"bbb\0" as *const u8 as *const libc::c_char;
    let mut c: *const libc::c_char = b"ccc\0" as *const u8 as *const libc::c_char;
    let mut abc: *mut libc::c_char = strconcat3(a, b, c);
    let mut left: *const libc::c_char = abc;
    let mut right: *const libc::c_char = b"aaabbbccc\0" as *const u8 as *const libc::c_char;
    if strcmp0(left, right) == 0 as libc::c_int {
        ok(
            b"%s (\"%s\") %s %s (\"%s\")\0" as *const u8 as *const libc::c_char,
            b"abc\0" as *const u8 as *const libc::c_char,
            left,
            b"==\0" as *const u8 as *const libc::c_char,
            b"\"aaabbbccc\"\0" as *const u8 as *const libc::c_char,
            right,
        );
    } else {
        die(
            b"expected %s (\"%s\") %s %s (\"%s\")\0" as *const u8 as *const libc::c_char,
            b"abc\0" as *const u8 as *const libc::c_char,
            left,
            b"==\0" as *const u8 as *const libc::c_char,
            b"\"aaabbbccc\"\0" as *const u8 as *const libc::c_char,
            right,
        );
    }
    free(abc as *mut libc::c_void);
}
#[c2rust::src_loc = "150:1"]
unsafe extern "C" fn test_has_prefix() {
    if has_prefix(
        b"foo\0" as *const u8 as *const libc::c_char,
        b"foo\0" as *const u8 as *const libc::c_char,
    ) != 0
    {
        ok(
            b"%s\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"foo\", \"foo\")\0" as *const u8 as *const libc::c_char,
        );
    } else {
        die(
            b"expected %s to be true\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"foo\", \"foo\")\0" as *const u8 as *const libc::c_char,
        );
    }
    if has_prefix(
        b"foobar\0" as *const u8 as *const libc::c_char,
        b"foo\0" as *const u8 as *const libc::c_char,
    ) != 0
    {
        ok(
            b"%s\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"foobar\", \"foo\")\0" as *const u8 as *const libc::c_char,
        );
    } else {
        die(
            b"expected %s to be true\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"foobar\", \"foo\")\0" as *const u8 as *const libc::c_char,
        );
    }
    if has_prefix(
        b"foobar\0" as *const u8 as *const libc::c_char,
        b"fool\0" as *const u8 as *const libc::c_char,
    ) == 0
    {
        ok(
            b"!(%s)\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"foobar\", \"fool\")\0" as *const u8 as *const libc::c_char,
        );
    } else {
        die(
            b"expected %s to be false\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"foobar\", \"fool\")\0" as *const u8 as *const libc::c_char,
        );
    }
    if has_prefix(
        b"foo\0" as *const u8 as *const libc::c_char,
        b"fool\0" as *const u8 as *const libc::c_char,
    ) == 0
    {
        ok(
            b"!(%s)\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"foo\", \"fool\")\0" as *const u8 as *const libc::c_char,
        );
    } else {
        die(
            b"expected %s to be false\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"foo\", \"fool\")\0" as *const u8 as *const libc::c_char,
        );
    }
    if has_prefix(
        b"foo\0" as *const u8 as *const libc::c_char,
        b"\0" as *const u8 as *const libc::c_char,
    ) != 0
    {
        ok(
            b"%s\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"foo\", \"\")\0" as *const u8 as *const libc::c_char,
        );
    } else {
        die(
            b"expected %s to be true\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"foo\", \"\")\0" as *const u8 as *const libc::c_char,
        );
    }
    if has_prefix(
        b"\0" as *const u8 as *const libc::c_char,
        b"\0" as *const u8 as *const libc::c_char,
    ) != 0
    {
        ok(
            b"%s\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"\", \"\")\0" as *const u8 as *const libc::c_char,
        );
    } else {
        die(
            b"expected %s to be true\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"\", \"\")\0" as *const u8 as *const libc::c_char,
        );
    }
    if has_prefix(
        b"\0" as *const u8 as *const libc::c_char,
        b"no\0" as *const u8 as *const libc::c_char,
    ) == 0
    {
        ok(
            b"!(%s)\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"\", \"no\")\0" as *const u8 as *const libc::c_char,
        );
    } else {
        die(
            b"expected %s to be false\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"\", \"no\")\0" as *const u8 as *const libc::c_char,
        );
    }
    if has_prefix(
        b"yes\0" as *const u8 as *const libc::c_char,
        b"no\0" as *const u8 as *const libc::c_char,
    ) == 0
    {
        ok(
            b"!(%s)\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"yes\", \"no\")\0" as *const u8 as *const libc::c_char,
        );
    } else {
        die(
            b"expected %s to be false\0" as *const u8 as *const libc::c_char,
            b"has_prefix (\"yes\", \"no\")\0" as *const u8 as *const libc::c_char,
        );
    };
}
#[c2rust::src_loc = "163:1"]
unsafe extern "C" fn test_has_path_prefix() {
    static mut tests: [C2RustUnnamed; 8] = [
        {
            let mut init = C2RustUnnamed {
                str_0: b"/run/host/usr\0" as *const u8 as *const libc::c_char,
                prefix: b"/run/host\0" as *const u8 as *const libc::c_char,
                expected: 1 as libc::c_int,
            };
            init
        },
        {
            let mut init = C2RustUnnamed {
                str_0: b"/run/host/usr\0" as *const u8 as *const libc::c_char,
                prefix: b"/run/host/\0" as *const u8 as *const libc::c_char,
                expected: 1 as libc::c_int,
            };
            init
        },
        {
            let mut init = C2RustUnnamed {
                str_0: b"/run/host\0" as *const u8 as *const libc::c_char,
                prefix: b"/run/host\0" as *const u8 as *const libc::c_char,
                expected: 1 as libc::c_int,
            };
            init
        },
        {
            let mut init = C2RustUnnamed {
                str_0: b"////run///host////usr\0" as *const u8 as *const libc::c_char,
                prefix: b"//run//host\0" as *const u8 as *const libc::c_char,
                expected: 1 as libc::c_int,
            };
            init
        },
        {
            let mut init = C2RustUnnamed {
                str_0: b"////run///host////usr\0" as *const u8 as *const libc::c_char,
                prefix: b"//run//host////\0" as *const u8 as *const libc::c_char,
                expected: 1 as libc::c_int,
            };
            init
        },
        {
            let mut init = C2RustUnnamed {
                str_0: b"/run/hostage\0" as *const u8 as *const libc::c_char,
                prefix: b"/run/host\0" as *const u8 as *const libc::c_char,
                expected: 0 as libc::c_int,
            };
            init
        },
        {
            let mut init = C2RustUnnamed {
                str_0: b"foo/bar\0" as *const u8 as *const libc::c_char,
                prefix: b"/foo\0" as *const u8 as *const libc::c_char,
                expected: 1 as libc::c_int,
            };
            init
        },
        {
            let mut init = C2RustUnnamed {
                str_0: b"/foo/bar\0" as *const u8 as *const libc::c_char,
                prefix: b"foo\0" as *const u8 as *const libc::c_char,
                expected: 1 as libc::c_int,
            };
            init
        },
    ];
    let mut i: size_t = 0;
    i = 0 as libc::c_int as size_t;
    while i
        < (::core::mem::size_of::<[C2RustUnnamed; 8]>() as libc::c_ulong)
            .wrapping_div(::core::mem::size_of::<C2RustUnnamed>() as libc::c_ulong)
    {
        let mut str: *const libc::c_char = tests[i as usize].str_0;
        let mut prefix: *const libc::c_char = tests[i as usize].prefix;
        let mut expected: bool_0 = tests[i as usize].expected;
        if expected != 0 {
            printf(
                b"# %s should have path prefix %s\n\0" as *const u8 as *const libc::c_char,
                str,
                prefix,
            );
        } else {
            printf(
                b"# %s should not have path prefix %s\n\0" as *const u8 as *const libc::c_char,
                str,
                prefix,
            );
        }
        if expected != 0 {
            if has_path_prefix(str, prefix) != 0 {
                ok(
                    b"%s\0" as *const u8 as *const libc::c_char,
                    b"has_path_prefix (str, prefix)\0" as *const u8 as *const libc::c_char,
                );
            } else {
                die(
                    b"expected %s to be true\0" as *const u8 as *const libc::c_char,
                    b"has_path_prefix (str, prefix)\0" as *const u8 as *const libc::c_char,
                );
            }
        } else if has_path_prefix(str, prefix) == 0 {
            ok(
                b"!(%s)\0" as *const u8 as *const libc::c_char,
                b"has_path_prefix (str, prefix)\0" as *const u8 as *const libc::c_char,
            );
        } else {
            die(
                b"expected %s to be false\0" as *const u8 as *const libc::c_char,
                b"has_path_prefix (str, prefix)\0" as *const u8 as *const libc::c_char,
            );
        }
        i = i.wrapping_add(1);
        i;
    }
}
#[c2rust::src_loc = "203:1"]
unsafe fn main_0(mut argc: libc::c_int, mut argv: *mut *mut libc::c_char) -> libc::c_int {
    setvbuf(
        stdout,
        0 as *mut libc::c_char,
        2 as libc::c_int,
        0 as libc::c_int as size_t,
    );
    test_n_elements();
    test_strconcat();
    test_strconcat3();
    test_has_prefix();
    test_has_path_prefix();
    printf(
        b"1..%u\n\0" as *const u8 as *const libc::c_char,
        test_number,
    );
    return 0 as libc::c_int;
}
pub fn main() {
    let mut args: Vec<*mut libc::c_char> = Vec::new();
    for arg in ::std::env::args() {
        args.push(
            (::std::ffi::CString::new(arg))
                .expect("Failed to convert argument into CString.")
                .into_raw(),
        );
    }
    args.push(::core::ptr::null_mut());
    unsafe {
        ::std::process::exit(main_0(
            (args.len() - 1) as libc::c_int,
            args.as_mut_ptr() as *mut *mut libc::c_char,
        ) as i32)
    }
}
