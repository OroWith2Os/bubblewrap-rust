use ::libc;
#[c2rust::header_src = "/usr/lib/clang/15.0.7/include/stddef.h:16"]
pub mod stddef_h {
    #[c2rust::src_loc = "46:1"]
    pub type size_t = libc::c_ulong;
}
#[c2rust::header_src = "/usr/include/bits/types.h:16"]
pub mod types_h {
    #[c2rust::src_loc = "150:1"]
    pub type __mode_t = libc::c_uint;
    #[c2rust::src_loc = "152:1"]
    pub type __off_t = libc::c_long;
    #[c2rust::src_loc = "153:1"]
    pub type __off64_t = libc::c_long;
}
#[c2rust::header_src = "/usr/include/bits/types/struct_FILE.h:16"]
pub mod struct_FILE_h {
    #[derive(Copy, Clone)]
    #[repr(C)]
    #[c2rust::src_loc = "49:8"]
    pub struct _IO_FILE {
        pub _flags: libc::c_int,
        pub _IO_read_ptr: *mut libc::c_char,
        pub _IO_read_end: *mut libc::c_char,
        pub _IO_read_base: *mut libc::c_char,
        pub _IO_write_base: *mut libc::c_char,
        pub _IO_write_ptr: *mut libc::c_char,
        pub _IO_write_end: *mut libc::c_char,
        pub _IO_buf_base: *mut libc::c_char,
        pub _IO_buf_end: *mut libc::c_char,
        pub _IO_save_base: *mut libc::c_char,
        pub _IO_backup_base: *mut libc::c_char,
        pub _IO_save_end: *mut libc::c_char,
        pub _markers: *mut _IO_marker,
        pub _chain: *mut _IO_FILE,
        pub _fileno: libc::c_int,
        pub _flags2: libc::c_int,
        pub _old_offset: __off_t,
        pub _cur_column: libc::c_ushort,
        pub _vtable_offset: libc::c_schar,
        pub _shortbuf: [libc::c_char; 1],
        pub _lock: *mut libc::c_void,
        pub _offset: __off64_t,
        pub _codecvt: *mut _IO_codecvt,
        pub _wide_data: *mut _IO_wide_data,
        pub _freeres_list: *mut _IO_FILE,
        pub _freeres_buf: *mut libc::c_void,
        pub __pad5: size_t,
        pub _mode: libc::c_int,
        pub _unused2: [libc::c_char; 20],
    }
    #[c2rust::src_loc = "43:1"]
    pub type _IO_lock_t = ();
    use super::stddef_h::size_t;
    use super::types_h::{__off64_t, __off_t};
    extern "C" {
        #[c2rust::src_loc = "38:8"]
        pub type _IO_wide_data;
        #[c2rust::src_loc = "37:8"]
        pub type _IO_codecvt;
        #[c2rust::src_loc = "36:8"]
        pub type _IO_marker;
    }
}
#[c2rust::header_src = "/usr/include/bits/types/FILE.h:16"]
pub mod FILE_h {
    #[c2rust::src_loc = "7:1"]
    pub type FILE = _IO_FILE;
    use super::struct_FILE_h::_IO_FILE;
}
#[c2rust::header_src = "/usr/include/errno.h:15"]
pub mod errno_h {
    extern "C" {
        #[c2rust::src_loc = "37:1"]
        pub fn __errno_location() -> *mut libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/stdio.h:16"]
pub mod stdio_h {
    use super::FILE_h::FILE;
    extern "C" {
        #[c2rust::src_loc = "145:14"]
        pub static mut stderr: *mut FILE;
        #[c2rust::src_loc = "350:12"]
        pub fn fprintf(_: *mut FILE, _: *const libc::c_char, _: ...) -> libc::c_int;
        #[c2rust::src_loc = "356:12"]
        pub fn printf(_: *const libc::c_char, _: ...) -> libc::c_int;
        #[c2rust::src_loc = "804:1"]
        pub fn perror(__s: *const libc::c_char);
    }
}
#[c2rust::header_src = "/usr/include/string.h:17"]
pub mod string_h {
    extern "C" {
        #[c2rust::src_loc = "156:12"]
        pub fn strcmp(_: *const libc::c_char, _: *const libc::c_char) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/unistd.h:18"]
pub mod unistd_h {
    extern "C" {
        #[c2rust::src_loc = "977:1"]
        pub fn chroot(__path: *const libc::c_char) -> libc::c_int;
        #[c2rust::src_loc = "1091:1"]
        pub fn syscall(__sysno: libc::c_long, _: ...) -> libc::c_long;
    }
}
#[c2rust::header_src = "/usr/include/sys/ioctl.h:19"]
pub mod ioctl_h {
    extern "C" {
        #[c2rust::src_loc = "42:1"]
        pub fn ioctl(__fd: libc::c_int, __request: libc::c_ulong, _: ...) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/sys/prctl.h:20"]
pub mod prctl_h {
    extern "C" {
        #[c2rust::src_loc = "42:1"]
        pub fn prctl(__option: libc::c_int, _: ...) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/sys/socket.h:21"]
pub mod socket_h {
    extern "C" {
        #[c2rust::src_loc = "296:1"]
        pub fn listen(__fd: libc::c_int, __n: libc::c_int) -> libc::c_int;
    }
}
#[c2rust::header_src = "/usr/include/sys/stat.h:23"]
pub mod stat_h {
    use super::types_h::__mode_t;
    extern "C" {
        #[c2rust::src_loc = "352:1"]
        pub fn chmod(__file: *const libc::c_char, __mode: __mode_t) -> libc::c_int;
    }
}
use self::errno_h::__errno_location;
use self::ioctl_h::ioctl;
use self::prctl_h::prctl;
use self::socket_h::listen;
use self::stat_h::chmod;
pub use self::stddef_h::size_t;
use self::stdio_h::{fprintf, perror, printf, stderr};
use self::string_h::strcmp;
pub use self::struct_FILE_h::{_IO_codecvt, _IO_lock_t, _IO_marker, _IO_wide_data, _IO_FILE};
pub use self::types_h::{__mode_t, __off64_t, __off_t};
use self::unistd_h::{chroot, syscall};
pub use self::FILE_h::FILE;
#[c2rust::src_loc = "78:1"]
unsafe fn main_0(mut argc: libc::c_int, mut argv: *mut *mut libc::c_char) -> libc::c_int {
    let mut errsv: libc::c_int = 0 as libc::c_int;
    let mut i: libc::c_int = 0;
    i = 1 as libc::c_int;
    while i < argc {
        let mut arg: *const libc::c_char = *argv.offset(i as isize);
        if strcmp(
            arg,
            b"print-errno-values\0" as *const u8 as *const libc::c_char,
        ) == 0 as libc::c_int
        {
            printf(
                b"EBADF=%d\n\0" as *const u8 as *const libc::c_char,
                9 as libc::c_int,
            );
            printf(
                b"EFAULT=%d\n\0" as *const u8 as *const libc::c_char,
                14 as libc::c_int,
            );
            printf(
                b"ENOENT=%d\n\0" as *const u8 as *const libc::c_char,
                2 as libc::c_int,
            );
            printf(
                b"ENOSYS=%d\n\0" as *const u8 as *const libc::c_char,
                38 as libc::c_int,
            );
            printf(
                b"EPERM=%d\n\0" as *const u8 as *const libc::c_char,
                1 as libc::c_int,
            );
        } else if strcmp(arg, b"chmod\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            if chmod(
                1 as libc::c_int as *mut libc::c_char,
                0o700 as libc::c_int as __mode_t,
            ) != 0 as libc::c_int
            {
                errsv = *__errno_location();
                perror(arg);
            }
        } else if strcmp(arg, b"chroot\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            if chroot(1 as libc::c_int as *mut libc::c_char) != 0 as libc::c_int {
                errsv = *__errno_location();
                perror(arg);
            }
        } else if strcmp(arg, b"clone3\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            if syscall(
                435 as libc::c_int as libc::c_long,
                1 as libc::c_int as *mut libc::c_char,
                88 as libc::c_int as size_t,
            ) != 0 as libc::c_int as libc::c_long
            {
                errsv = *__errno_location();
                perror(arg);
            }
        } else if strcmp(
            arg,
            b"ioctl TIOCNOTTY\0" as *const u8 as *const libc::c_char,
        ) == 0 as libc::c_int
        {
            if ioctl(-(1 as libc::c_int), 0x5422 as libc::c_int as libc::c_ulong)
                != 0 as libc::c_int
            {
                errsv = *__errno_location();
                perror(arg);
            }
        } else if strcmp(arg, b"ioctl TIOCSTI\0" as *const u8 as *const libc::c_char)
            == 0 as libc::c_int
        {
            if ioctl(
                -(1 as libc::c_int),
                0x5412 as libc::c_int as libc::c_ulong,
                1 as libc::c_int as *mut libc::c_char,
            ) != 0 as libc::c_int
            {
                errsv = *__errno_location();
                perror(arg);
            }
        } else if strcmp(
            arg,
            b"ioctl TIOCSTI CVE-2019-10063\0" as *const u8 as *const libc::c_char,
        ) == 0 as libc::c_int
        {
            let mut not_TIOCSTI: libc::c_ulong = (0x123 as libc::c_ulong) << 32 as libc::c_int
                | 0x5412 as libc::c_int as libc::c_ulong;
            if syscall(
                16 as libc::c_int as libc::c_long,
                -(1 as libc::c_int),
                not_TIOCSTI,
                1 as libc::c_int as *mut libc::c_char,
            ) != 0 as libc::c_int as libc::c_long
            {
                errsv = *__errno_location();
                perror(arg);
            }
        } else if strcmp(arg, b"listen\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            if listen(-(1 as libc::c_int), 42 as libc::c_int) != 0 as libc::c_int {
                errsv = *__errno_location();
                perror(arg);
            }
        } else if strcmp(arg, b"prctl\0" as *const u8 as *const libc::c_char) == 0 as libc::c_int {
            if prctl(
                37 as libc::c_int,
                1 as libc::c_int as *mut libc::c_char,
                0 as libc::c_int,
                0 as libc::c_int,
                0 as libc::c_int,
            ) != 0 as libc::c_int
            {
                errsv = *__errno_location();
                perror(arg);
            }
        } else {
            fprintf(
                stderr,
                b"Unsupported syscall \"%s\"\n\0" as *const u8 as *const libc::c_char,
                arg,
            );
            errsv = 2 as libc::c_int;
        }
        i += 1;
        i;
    }
    return errsv;
}
pub fn main() {
    let mut args: Vec<*mut libc::c_char> = Vec::new();
    for arg in ::std::env::args() {
        args.push(
            (::std::ffi::CString::new(arg))
                .expect("Failed to convert argument into CString.")
                .into_raw(),
        );
    }
    args.push(::core::ptr::null_mut());
    unsafe {
        ::std::process::exit(main_0(
            (args.len() - 1) as libc::c_int,
            args.as_mut_ptr() as *mut *mut libc::c_char,
        ) as i32)
    }
}
